//
//  AppDelegate.swift
//  TuReta
//
//  Created by DC on 9/2/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import AppCenter
import AppCenterAnalytics
import AppCenterCrashes
import IQKeyboardManagerSwift
import Swinject
import GoogleMaps
import Firebase

import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging

import AppCenter
import AppCenterPush

import FBSDKCoreKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UINavigationControllerDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    static let container = Container()
    var navigationBarAppearace = UINavigationBar.appearance()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        navigationBarAppearace.tintColor = .white
        navigationBarAppearace.barTintColor = Constants.greenOnePrimaryColor
        navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        GMSServices.provideAPIKey(Constants.googleMapSDKKey)
        //setup my Container
        TuRetaContainer().setupContainer(using: AppDelegate.container)
        
        // Override point for customization after application launch.
        MSAppCenter.start(Constants.appCenterApiKey, withServices:[
            MSAnalytics.self,
            MSCrashes.self,
            MSPush.self
            ])
        IQKeyboardManager.shared.enable = true
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {(granted, error) in
                    if let error = error {
                        print("D'oh: \(error.localizedDescription)")
                    } else {
                        // success!
                        DispatchQueue.main.async {
                            application.registerForRemoteNotifications()
                        }
                    }
            })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        FirebaseApp.configure()
        // For iOS 10 data message (sent via FCM)
        Messaging.messaging().delegate = self
        
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      print("Firebase registration token: \(fcmToken)")

      let dataDict:[String: String] = ["token": fcmToken]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let appId: String = Settings.appID
        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" {
            return ApplicationDelegate.shared.application(app, open: url, options: options)
        }
        return false
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if UIApplication.shared.applicationIconBadgeNumber != 0 {
            if let rootViewController = self.window?.rootViewController {
                if let tabBarController = rootViewController as? UITabBarController {
                    let tabBarItem = tabBarController.tabBar.items![1]
                    tabBarItem.badgeValue = "New"
                }
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        AppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func presentHomeVC(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateInitialViewController()
        controller?.view.backgroundColor = UIColor.white
        UIView.transition(with: self.window!, duration: 0.5, options: UIView.AnimationOptions.transitionFlipFromRight, animations: {
            
            //self.window?.rootViewController?.dismiss(animated: false) {
                self.window?.rootViewController = controller
            //}
            
        }, completion: nil)
    }
    
    func presentHomeOwner(){
        let storyboard = UIStoryboard(name: "MainOwner", bundle: nil)
        let controller = storyboard.instantiateInitialViewController()
        controller?.view.backgroundColor = UIColor.white
        UIView.transition(with: self.window!, duration: 0.5, options: UIView.AnimationOptions.transitionFlipFromRight, animations: {
            
            //self.window?.rootViewController?.dismiss(animated: false) {
                self.window?.rootViewController = controller
            //}
            
        }, completion: nil)
    }
    
    func presentInit(){
        let storyboard = UIStoryboard(name: "LoginView", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "loginView")
        
        if let therootController = UIApplication.shared.keyWindow?.rootViewController {
           
            if let tabCon = therootController as? UITabBarController {
                guard var viewControllers = tabCon.viewControllers else { return }
                viewControllers.remove(at: 0)
                tabCon.dismiss(animated: false, completion: nil)
                
            } else {
                therootController.dismiss(animated: false, completion: nil)
            }
            
        }
        UIApplication.shared.keyWindow?.rootViewController = nil
        
        let nav = UINavigationController(rootViewController: controller)
        nav.view.backgroundColor = UIColor.white
        nav.navigationBar.tintColor = .white
        
        UIView.transition(with: self.window!, duration: 0.5, options: UIView.AnimationOptions.transitionFlipFromBottom, animations: {
            
            self.window?.rootViewController = nav
        }, completion: nil)
        
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      InstanceID.instanceID().instanceID { (result, error) in
        if let error = error {
          print("Error: \(error)")
        } else if let result = result {
          print("Instance ID token: \(result.token)")
            let userToken = UserToken(firebaseToken: result.token)
            UserData.sharedFirebase = userToken
            
            let defaults = UserDefaults.standard
            defaults.set(result.token, forKey: "deviceToken")
            defaults.synchronize()
        }
      }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        let state = application.applicationState
        switch state {

        case .inactive:
            print("Inactive")
            UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1

        case .background:
            print("Background")
            // update badge count here
            application.applicationIconBadgeNumber = application.applicationIconBadgeNumber + 1

        case .active:
            print("Active")
            UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1

        @unknown default:
            break
        }
      completionHandler(.newData)
    }
    
    

}

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }

        // Print full message.
        print(userInfo)

        // Change this to your preferred presentation option
        completionHandler(.alert)
        if let rootViewController = self.window?.rootViewController {
            if let tabBarController = rootViewController as? UITabBarController {
                let tabBarItem = tabBarController.tabBar.items![1]
                tabBarItem.badgeValue = "New"
                UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
            }
        }
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle push from background or closed")
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }

        completionHandler()
    }
}

extension AppDelegate : MessagingDelegate {
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
}
