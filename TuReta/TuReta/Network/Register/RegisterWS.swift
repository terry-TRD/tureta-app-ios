//
//  RegisterWS.swift
//  TuReta
//
//  Created by DC on 9/14/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import Alamofire


enum NetworkErrorRegister: Error {
    
    case urlInvalid
    case parse
    case invalidCredential
    case unkwonError
    
}

class RegisterWS: NSObject {
    
    class func registerWS(email: String, password: String, name: String, username: String , completionHandler: @escaping ( _ user: UserResponse?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void ){
        
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            //"Authorization" : UserData.shared?.authorization ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            "email": email,
            "password": password,
            "device_id": deviceToken,
            "password_confirmation": password,
            "name": name,
            "username": username,
            "push_token": UserData.sharedFirebase?.firebaseToken ?? ""
            //"identifier": Bundle.main.bundleIdentifier as AnyObject,
            //"platform": "ios",
            //"version": version
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.register.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { response in
            //.authenticate(user: UserProvider.sharedInstance.email, password: UserProvider.sharedInstance.password)
            debugPrint(response)
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    let dataResponse = (response.result.value as! NSDictionary).object(forKey: "data") as! NSDictionary
                    //let userResponse = (dataResponse as! NSDictionary).object(forKey: "user") as! NSDictionary
                    
                    do{
                        let data = try JSONSerialization.data(withJSONObject: dataResponse, options: .prettyPrinted)
                        let user = try JSONDecoder().decode(UserResponse.self, from: data)
                        
                        completionHandler(user)
                        
                    }
                    catch{}
                    
                }else if (response.response?.statusCode == Constants.successWithMessage){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }else if(response.response?.statusCode == Constants.successWithError){
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    let errors = (responseDictionary as NSDictionary).object(forKey: "errors") as! NSDictionary
                    //let message = Array(errors.allValues)
                    var message: String? = ""
                    
                    if(errors.count > 0){
                        for obj in errors{
                            message =  NSArray(array:errors.object(forKey: obj.key) as! Array).firstObject as? String
                            break
                        }
                    }
                    errorHandler(.invalidCredential, message!)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        }
    }
    
}
