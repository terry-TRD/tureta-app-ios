//
//  ProfileNetworkClient.swift
//  TuReta
//
//  Created by Daniel Coria on 10/5/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

protocol ProfileNetworkClient {
    
    func getProfile( completionHandler: @escaping ( _ user: User?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void )
    
    func closeSession( completionHandler:  @escaping ( _ user: Bool) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void )
}

extension NetworkClient: ProfileNetworkClient{
    func getProfile( completionHandler:  @escaping ( _ user: User?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void ){
        
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.profile.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = ProfileWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.getProfileWS( completionHandler: {[weak self] (user) in
            guard let self = self else { return }
            
                completionHandler(user) // aqui
            
            }, { (networkError, message) in
                print(networkError!, message)
                errorHandler(.parse, message)
        })
    }
    
    func closeSession( completionHandler:  @escaping ( _ user: Bool) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void ){
        
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ,
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.profile.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = ProfileWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.closeSessionWS(completionHandler: {[weak self] (isSuccessful) in
            guard let self = self else { return }
            
            completionHandler(isSuccessful)
        }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    
}
