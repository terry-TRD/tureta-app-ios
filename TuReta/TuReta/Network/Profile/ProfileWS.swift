//
//  ProfileWS.swift
//  TuReta
//
//  Created by DC on 9/28/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import Alamofire

class ProfileWS: NSObject {
    
    let apiURL: URL
    let parameters: KeyValueStringAnyType
    let header: KeyValueStringType
    let networkClient: NetworkClient
    
    init(requestURL: URL, parameters: [String: Any], header: [String: String], networkClient: NetworkClient){
        self.apiURL = requestURL
        self.parameters = parameters
        self.header = header
        self.networkClient = networkClient
    }
    
    func closeSessionWS(completionHandler: @escaping(_ isSuccessful: Bool) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestPOST(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(true)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        })
    }
    
    func getProfileWS(completionHandler: @escaping ( _ user: User?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void ){
        
        networkClient.requestGET(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
        switch response.result{
        case .success:
            
            if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){

                do {
                    var info: User
                    guard let jsonObject = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                        return
                    }
                    
                    guard let json = jsonObject["data"] as? JSONObjectType else{
                        return
                    }
                    
                    info = User(JSONObject: json)!
                    
                    completionHandler(info)

                }catch {
                    print("JSONSerialization error:", error)
                }
                
            }else if (response.response?.statusCode == Constants.successWithMessage){
                
                guard let responseDictionary = response.result.value as? JSON else {
                    errorHandler(.parse, "")
                    return
                }
                errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
            }
            break
            
        case .failure(let error):
            print (error)
            errorHandler(.unkwonError, "")
            
            break
        }
        })
    }
    
    class func editProfileWS(name: String, completionHandler: @escaping ( _ user: User?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void ){
        
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            "name": name,
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.profile.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { response in
            debugPrint(response)
            
            //            DispatchQueue.main.async(execute: {
            //                LoadingOverlay.shared.hideOverlayView()
            //            })
            
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    let dataResponse = (response.result.value as! NSDictionary).object(forKey: "data") as! NSDictionary
                    //let userResponse = (dataResponse as! NSDictionary).object(forKey: "user") as! NSDictionary
                    
                    do{
                        let data = try JSONSerialization.data(withJSONObject: dataResponse, options: .prettyPrinted)
                        let user = try JSONDecoder().decode(User.self, from: data)
                        
                        completionHandler(user)
                        
                    }
                    catch{}
                    
                }else if (response.response?.statusCode == Constants.successWithMessage){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
            
        }
    }
    
    class func editAvatarProfileWS(color: String, completionHandler: @escaping ( _ user: User?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void ){
        
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            "color": color,
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.profile.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { response in
            debugPrint(response)
            
            //            DispatchQueue.main.async(execute: {
            //                LoadingOverlay.shared.hideOverlayView()
            //            })
            
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    let dataResponse = (response.result.value as! NSDictionary).object(forKey: "data") as! NSDictionary
                    //let userResponse = (dataResponse as! NSDictionary).object(forKey: "user") as! NSDictionary
                    
                    do{
                        let data = try JSONSerialization.data(withJSONObject: dataResponse, options: .prettyPrinted)
                        let user = try JSONDecoder().decode(User.self, from: data)
                        
                        completionHandler(user)
                        
                    }
                    catch{}
                    
                }else if (response.response?.statusCode == Constants.successWithMessage){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
            
        }
    }

}
