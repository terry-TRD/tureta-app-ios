//
//  FetchProfileOperation.swift
//  TuReta
//
//  Created by Daniel Coria on 10/5/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

final class FetchProfileOperation {
    
    private let networkClient: ProfileNetworkClient
    
    required init(networkClient: ProfileNetworkClient){
        self.networkClient = networkClient
    }
    
    func startRequest(completionHandler: @escaping ( _ user: User?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void ){
        
        networkClient.getProfile( completionHandler: {[weak self] (user) in
            //guard let self = self else { return }
            
                completionHandler(user)
            }, { (networkError, message) in
                print(networkError!, message)
                errorHandler(.parse, message)
                
        })
    }
    
    func startRequestPostCloseSession( completionHandler: @escaping( _ response: Bool) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        
        networkClient.closeSession( completionHandler: {[weak self] (response) in
            
                completionHandler(response)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
}
