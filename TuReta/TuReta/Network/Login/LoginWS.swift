//
//  LoginWS.swift
//  TuReta
//
//  Created by DC on 9/10/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import Alamofire

typealias JSON = [AnyHashable: Any]

enum NetworkErrorLogin: Error {
    
    case urlInvalid
    case parse
    case invalidCredential
    case unkwonError
    
}

class LoginWS: NSObject {
    
    class func loginWS(email: String, password: String, userName: String?, completionHandler: @escaping ( _ user: UserResponse?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void ){
        
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "deviceToken") != nil ? defaults.object(forKey: "deviceToken") as! String : "ios"
        let header = [
            //"Authorization" : UserData.shared?.authorization ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        
        var parameters: [String: Any]
        var userToken: String = ""
        
        if(password.isEmpty){
            if defaults.object(forKey: "appleToken") != nil {
                userToken = defaults.object(forKey: "appleToken") as! String
                
                parameters = [
                    "email": email,
                    "provider": "sign-in-with-apple",
                    "access_token": userToken,
                    "device_id": deviceToken,
                    "grant_type": "social",
                    "user_name": userName ?? "",
                    "push_token": UserData.sharedFirebase?.firebaseToken ?? ""
                    //"identifier": Bundle.main.bundleIdentifier as AnyObject,
                    //"platform": "ios",
                    //"version": version
                ]
            } else {
                userToken = defaults.object(forKey: "facebookToken") as! String
                
                parameters = [
                    "email": email,
                    "provider": "facebook",
                    "access_token": userToken,
                    "device_id": deviceToken,
                    "grant_type": "social",
                    "push_token": UserData.sharedFirebase?.firebaseToken ?? ""
                    //"identifier": Bundle.main.bundleIdentifier as AnyObject,
                    //"platform": "ios",
                    //"version": version
                ]
            }
           
        }else{
            parameters = [
                "email": email,
                "password": password,
                "device_id": deviceToken,
                "grant_type": "password",
                "push_token": UserData.sharedFirebase?.firebaseToken ?? ""
                //"identifier": Bundle.main.bundleIdentifier as AnyObject,
                //"platform": "ios",
                //"version": version
            ]
        }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.login.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { response in
            //.authenticate(user: UserProvider.sharedInstance.email, password: UserProvider.sharedInstance.password)
            debugPrint(response)
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    do{
                        
                        guard let jsonObject = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                            return
                        }
                        
                        guard let json = jsonObject["data"] as? JSONObjectType else{
                            return
                        }
                        let user = UserResponse(JSONObject: json)
                    
                        completionHandler(user)
                        
                    }
                    catch{}
                    
                }else if (response.response?.statusCode == Constants.successWithMessage){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                    }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        }
    }

}
