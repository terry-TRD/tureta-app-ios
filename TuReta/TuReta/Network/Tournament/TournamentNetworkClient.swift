//
//  TournamentNetworkClient.swift
//  TuReta
//
//  Created by DC on 03/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

protocol TournamentNetworkClient {
    
    func requestPost(url: URL, params: [String: Any], completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestPostWithImage(url: URL, params: [String: Any], image: Data?, completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestGet(url: URL, params: [String: Any], completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestUpdate(url: URL, params: [String: Any], completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestDelete(url: URL, params: [String: Any], completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestNetworkCustomPost(url: URL, params: [String: Any], array: [Int], completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestNetworkCustomPut(url: URL, params: [String: Any], array: [Int], completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
}

extension NetworkClient: TournamentNetworkClient {
    func requestPost(url: URL, params: [String: Any], completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let ws = OwnerWS(requestURL: url, parameters: params, header: header, networkClient: self)
        ws.requestPost(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    // custom post to  send teams as part of params
    func requestNetworkCustomPost(url: URL, params: [String: Any], array: [Int], completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(UserData.shared!.access_token)", forHTTPHeaderField: "Authorization")
        
       
        var parameters = params
        parameters["teams"] =  array
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let ws = OwnerWS(requestURL: url, parameters: params, header: header, networkClient: self)
        ws.requestWSCustomPost(request: request, completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestNetworkCustomPut(url: URL, params: [String: Any], array: [Int], completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(UserData.shared!.access_token)", forHTTPHeaderField: "Authorization")
        
       
        var parameters = params
        parameters["teams"] =  array
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let ws = OwnerWS(requestURL: url, parameters: params, header: header, networkClient: self)
        ws.requestWSCustomPut(request: request, completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestPostWithImage(url: URL, params: [String: Any], image: Data?, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let ws = OwnerWS(requestURL: url, parameters: params, header: header, networkClient: self)
        ws.requestPostWithImage(imageData: image, completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestGet(url: URL, params: [String: Any], completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let ws = OwnerWS(requestURL: url, parameters: params, header: header, networkClient: self)
        ws.basicRequestGet(completionHandler: {[weak self] (establishment) in
            guard let self = self else { return }
            
            completionHandler(establishment)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestUpdate(url: URL, params: [String: Any], completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let ws = OwnerWS(requestURL: url, parameters: params, header: header, networkClient: self)
        ws.requestUpdate(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
        
    }
    
    func requestDelete(url: URL, params: [String: Any], completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let ws = OwnerWS(requestURL: url, parameters: params, header: header, networkClient: self)
        ws.requestDelete(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    
}
