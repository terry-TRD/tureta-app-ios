//
//  NetworkClient.swift
//  TuReta
//
//  Created by Daniel Coria on 10/5/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation
import Alamofire

final class NetworkClient {
    
    //shared instance
    static let shared = NetworkClient()

    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    var dataResponse : DataResponse<Any>!
    
    public func requestGET(url: URL, parameters: [String: Any], header: [String: String], completionHandle: @escaping ( _ response: DataResponse<Any> ) -> Void){
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { response in
            debugPrint(response)
            completionHandle(response)
        }
    }
    
    public func requestPOST(url: URL, parameters: KeyValueStringAnyType, header: KeyValueStringType, completionHandle: @escaping(_ response: DataResponse<Any>) -> Void){
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { response in
            completionHandle(response)
            
        }
    }
    
    public func requestCustomPOST(request: URLRequest, completionHandle: @escaping(_ response: DataResponse<Any>) -> Void){
        
        Alamofire.request(request).responseJSON { response in
            completionHandle(response)
            
        }
    }
    
    public func requestCustomPUT(request: URLRequest, completionHandle: @escaping(_ response: DataResponse<Any>) -> Void){
        
        Alamofire.request(request).responseJSON { response in
            completionHandle(response)
            
        }
    }
    
    public func requestPostWithImage(url: URL, imagedata: Data?, parameters: KeyValueStringAnyType, header: KeyValueStringType, completionHandle: @escaping (_ response: DataResponse<Any>) -> Void){
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }

            if let data = imagedata{
                multipartFormData.append(data, withName: "media", fileName: "imageTeam.jpg", mimeType: "image/jpeg")
            }

        }, to: url,headers: header) { result in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })

                upload.responseJSON { response in
                    completionHandle(response)
                }

            case .failure(let encodingError):
                print("no Error :\(encodingError)")
            }
        }
        
    }
    
    public func requestPUT(url: URL, parameters: KeyValueStringAnyType, header: KeyValueStringType, completionHandle: @escaping(_ response: DataResponse<Any>) -> Void){
     
        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding.httpBody, headers: header).responseJSON { response in
            debugPrint(response)
            completionHandle(response)
        }
    }
    
    public func requestDelete(url: URL, parameters: KeyValueStringAnyType, header: KeyValueStringType, completionHandle: @escaping(_ response: DataResponse<Any>) -> Void){
     
        Alamofire.request(url, method: .delete, parameters: parameters, encoding: URLEncoding.httpBody, headers: header).responseJSON { response in
            debugPrint(response)
            completionHandle(response)
        }
    }
    
}
