//
//  EstablishmentWS.swift
//  TuReta
//
//  Created by Daniel Coria on 10/26/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class EstablishmentWS: NSObject{
    let apiURL: URL
    let parameters: [String: Any]
    let header: [String: String]
    let networkClient: NetworkClient
    
    init(requestURL: URL, parameters: [String: Any], header: [String: String], networkClient: NetworkClient){
        self.apiURL = requestURL
        self.parameters = parameters
        self.header = header
        self.networkClient = networkClient
    }
    
    func basicRequestGet( completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestGET(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        })
    }
    
    func requestDelete( completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestDelete(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        })
        
    }
    
    
    func requestRentFieldWS( completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
     
        networkClient.requestPOST(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        })
        
    }
    
    func getEstablishmentAvailabilityWS( completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestGET(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        })
    }
    
    
    
    func getEstablishmentWS(completionHandler: @escaping ( _ establishment: EstablishmentArray?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void ){
        
        networkClient.requestGET(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    let dataResponse = (response.result.value as! NSDictionary).object(forKey: "data") as! NSArray
                    //let userResponse = (dataResponse as! NSDictionary).object(forKey: "user") as! NSDictionary
                    
                    do{
                        var itemsArray : Array<Establishment> = []
                        for item in dataResponse{
                            let data = try JSONSerialization.data(withJSONObject: item, options: .prettyPrinted)
                            let establishment = try JSONDecoder().decode(Establishment.self, from: data)
                            itemsArray.append(establishment)
                        }
                        let items = EstablishmentArray(items: itemsArray)
                        //                        let data = try JSONSerialization.data(withJSONObject: dataResponse, options: .prettyPrinted)
                        //                        let establishment = try JSONDecoder().decode(Establishment.self, from: data)
                        
                        completionHandler(items)
                        
                    }
                        
                    catch{}
                    
                }else if (response.response?.statusCode == Constants.successWithMessage){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        })
    }
}
