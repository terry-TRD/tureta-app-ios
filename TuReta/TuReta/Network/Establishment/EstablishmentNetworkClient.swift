//
//  EstablishmentNetworkClient.swift
//  TuReta
//
//  Created by Daniel Coria on 10/26/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

protocol EstablishmentNetworkClient{
    func getEstablishment(center_lat: Double, center_lng: Double, border_lat: Double, border_lng: Double, search: String, completionHandler: @escaping ( _ establishment: EstablishmentArray?) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void )
    
    func getEstablishmentAvailability(establishmentID: Int, date: String, completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestRentField(field: Field, from: String, to: String, type: Int, manualPrice: Double?, completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestEstablishmentDetail(idPlace: Int, completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestEstablishmentFavorite(establishmentID: Int, type: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
}

extension NetworkClient: EstablishmentNetworkClient{
    
    func requestEstablishmentFavorite(establishmentID: Int, type: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "establishment_id": establishmentID
            
        ]
        
        if type == "add" {
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.users.rawValue)/\(UserData.shared!.user.id)/\(Constants.UrlWS.favorites.rawValue)") else {
                errorHandler(.urlInvalid, "")
                return
            }
            
            let ws = EstablishmentWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            // using requestRentFieldWS as POST method
            ws.requestRentFieldWS(completionHandler: {[weak self] (establishment) in
                guard let self = self else { return }
                
                completionHandler(establishment)
            }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
        }else if type == "remove" {
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.users.rawValue)/\(UserData.shared!.user.id)/\(Constants.UrlWS.favorites.rawValue)/\(establishmentID)") else {
                errorHandler(.urlInvalid, "")
                return
            }
            
            let ws = EstablishmentWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            ws.requestDelete(completionHandler: {[weak self] (establishment) in
                guard let self = self else { return }
                
                completionHandler(establishment)
            }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
        }else if type == "list" {
            let parameters: [String: Any] = [
                
                "identifier": Bundle.main.bundleIdentifier as AnyObject,
                "platform": "ios",
                "version": version,
                
            ]
            
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.users.rawValue)/\(UserData.shared!.user.id)/\(Constants.UrlWS.favorites.rawValue)") else {
                errorHandler(.urlInvalid, "")
                return
            }
            
            let ws = EstablishmentWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            ws.getEstablishmentAvailabilityWS(completionHandler: {[weak self] (jsonResponse) in
                guard let self = self else { return }
                completionHandler(jsonResponse)
                }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
        }
    
    }
    
    func requestEstablishmentDetail(idPlace: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared?.access_token ?? "")",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(idPlace)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = EstablishmentWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.basicRequestGet(completionHandler: {[weak self] (establishment) in
            guard let self = self else { return }
            
            completionHandler(establishment)
        }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestRentField(field: Field, from: String, to: String, type: Int, manualPrice: Double?, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "field_id": field.id,
            "from": from,
            "to": to,
            "game_type": field.game_types,
            "type": type,
            "price": manualPrice ?? 0.0
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.reservation.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = EstablishmentWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.requestRentFieldWS(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
                completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
            })
    }
    
    
    
    func getEstablishmentAvailability(establishmentID: Int, date: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "date": date
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentID)/\(Constants.UrlWS.availability.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = EstablishmentWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.getEstablishmentAvailabilityWS(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
        }, { (networkError, message) in
            errorHandler(.parse, message)
        })
    }
    
    func getEstablishment(center_lat: Double, center_lng: Double, border_lat: Double, border_lng: Double, search: String, completionHandler: @escaping (EstablishmentArray?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            //"Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "center_lat": center_lat,
            "center_lng": center_lng,
            "border_lat": border_lat,
            "border_lng": border_lng,
            "search": search
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = EstablishmentWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.getEstablishmentWS(completionHandler: {[weak self] (establishment) in
            guard let self = self else { return }
            
            completionHandler(establishment)
        }, { (networkError, message) in
                errorHandler(.parse, message)
        })
        
        
    }
    
    
}
