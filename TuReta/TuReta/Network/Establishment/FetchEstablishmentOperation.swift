//
//  FetchEstablishmentOperation.swift
//  TuReta
//
//  Created by Daniel Coria on 10/26/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

final class FetchEstablishmentOperation {
    
    private let networkClient: EstablishmentNetworkClient
    
    required init(networkClient: EstablishmentNetworkClient){
        self.networkClient = networkClient
    }
    
    func startRequest(center_lat: Double, center_lng: Double, border_lat: Double, border_lng: Double, search: String, completionHandler: @escaping ( _ user: EstablishmentArray?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void ){
        
        networkClient.getEstablishment(center_lat: center_lat, center_lng: center_lng, border_lat: border_lat, border_lng: border_lng, search: search, completionHandler: {[weak self] (establishment) in
    
            //guard let self = self else { return }
            
                //print(user?.email)
                completionHandler(establishment)
            }, { (networkError, message) in
                print(networkError!, message)
                errorHandler(.parse, message)
                
        })
        
    }
    
    func startRequestAvailability(establishmentID: Int, date: String, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        
        networkClient.getEstablishmentAvailability(establishmentID: establishmentID, date: date, completionHandler: {[weak self] (jsonResponse) in
            
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func starRequestRentField(field: Field, from: String, to: String, type: Int, manualPrice: Double?, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        
        networkClient.requestRentField(field: field, from: from, to: to, type: type, manualPrice: manualPrice, completionHandler:
        {[weak self] (jsonResponse) in
            
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestEstablishmentDetail(idPlace: Int, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        networkClient.requestEstablishmentDetail(idPlace: idPlace, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestEstablishmentFavorite(establishmentID: Int, type: String, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        networkClient.requestEstablishmentFavorite(establishmentID: establishmentID, type: type, completionHandler:
        {[weak self] (jsonResponse) in
            
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
}
