//
//  OwnerWS.swift
//  TuReta
//
//  Created by Daniel Coria on 11/28/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

class OwnerWS: NSObject{
    let apiURL: URL
    let parameters: [String: Any]
    let header: [String: String]
    let networkClient: NetworkClient
    
    init(requestURL: URL, parameters: [String: Any], header: [String: String], networkClient: NetworkClient){
        self.apiURL = requestURL
        self.parameters = parameters
        self.header = header
        self.networkClient = networkClient
    }
    
    func requestUpdateStatusField( completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestPUT(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }else if response.response?.statusCode == Constants.codeError {
                    errorHandler(.unkwonError, "Error")
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        })
        
    }
    
    func requestUpdate( completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestPUT(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        })
        
    }
    
    func requestPost( completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestPOST(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                print("\(response.result)")
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, error.localizedDescription)
                
                break
            }
        })
    }
    
    func requestWSCustomPut(request: URLRequest, completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestCustomPUT(request: request, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                print("\(response.result)")
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, error.localizedDescription)
                
                break
            }
        })
    }
    
    func requestWSCustomPost(request: URLRequest, completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestCustomPOST(request: request, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                print("\(response.result)")
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, error.localizedDescription)
                
                break
            }
        })
    }
    
    func requestPostWithImage(imageData: Data?, completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestPostWithImage(url: self.apiURL, imagedata: imageData, parameters: parameters, header: header) { [weak self ](response) in
            switch response.result {
            case .success:
                print("\(response.result)")
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                errorHandler(.unkwonError, error.localizedDescription)
                
            }
        }
    }
    
    func requestDelete( completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestDelete(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        })
        
    }
    
    func requestReservations( completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
    
       networkClient.requestGET(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
           switch response.result{
           case .success:
               
               if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                   
                   completionHandler(response.data!)
                   
               }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                   
                   guard let responseDictionary = response.result.value as? JSON else {
                       errorHandler(.parse, "")
                       return
                   }
                   errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
               }
               break
               
           case .failure(let error):
               print (error)
               errorHandler(.unkwonError, "")
               
               break
           }
       })
    }
    
    func basicRequestGet( completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
        
        networkClient.requestGET(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
            switch response.result{
            case .success:
                
                if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                    
                    completionHandler(response.data!)
                    
                }else if (response.response?.statusCode == Constants.successWithMessage){
                    
                    guard let responseDictionary = response.result.value as? JSON else {
                        errorHandler(.parse, "")
                        return
                    }
                    errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
                }
                break
                
            case .failure(let error):
                print (error)
                errorHandler(.unkwonError, "")
                
                break
            }
        })
    }
    
    func requestAllServices( completionHandler: @escaping(_ response: Data) -> Void, _ errorHandler: @escaping( NetworkErrorLogin?, _ message: String) -> Void){
    
       networkClient.requestGET(url: self.apiURL, parameters: parameters, header: header, completionHandle: {[weak self](response) in
           switch response.result{
           case .success:
               
               if(response.response?.statusCode == Constants.success || response.response?.statusCode == Constants.successCreated){
                   
                   completionHandler(response.data!)
                   
               }else if (response.response?.statusCode == Constants.successWithMessage || response.response?.statusCode == Constants.successWithMessage2){
                   
                   guard let responseDictionary = response.result.value as? JSON else {
                       errorHandler(.parse, "")
                       return
                   }
                   errorHandler(.invalidCredential, (responseDictionary as NSDictionary).object(forKey: "message") as! String)
               }
               break
               
           case .failure(let error):
               print (error)
               errorHandler(.unkwonError, "")
               
               break
           }
       })
    }
    

}
