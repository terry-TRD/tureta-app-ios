//
//  OwnerNetworkClient.swift
//  TuReta
//
//  Created by Daniel Coria on 11/28/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

protocol OwnerNetworkClient{
    
    func requestUpdateStatusField(establishmentId: Int, fieldId: Int, isActive: Bool, completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestUpdateServiceStatus(establishmentId: Int, serviceToUpdate: Int, activateStatus: Bool, completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestReservationsOwner(establishmentId: Int, type: String,  completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestAllServices( completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestUpdateStatusResevation(establishmentID: Int, reservationID: Int, type: String, completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestUpdateRate(reservationID: Int, rate: Int, completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestRecoveryPassword(email: String, completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestCancelReservation(reservationID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestCreateNewField(newFieldParams: NewField, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestUpdateField(fieldParams: NewField, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestRemoveField(fieldID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
}


extension NetworkClient: OwnerNetworkClient{
    
    func requestRemoveField(fieldID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(UserData.shared?.user.establishments?[0].id ?? 0)/\(Constants.UrlWS.fields.rawValue)/\(fieldID)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.requestDelete(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    func requestCreateNewField(newFieldParams: NewField, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "name": newFieldParams.name,
            "game_types[]": newFieldParams.gameType,
            "grass_type": newFieldParams.grassType,
            "is_active": newFieldParams.isActive,
            "price_type[]": newFieldParams.priceType,
            "price_hour[]": newFieldParams.priceHour,
            "price_half_hour[]": newFieldParams.priceHalf
            
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(UserData.shared?.user.establishments?[0].id ?? 0)/\(Constants.UrlWS.fields.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.requestPost(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestUpdateField(fieldParams: NewField, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "name": fieldParams.name,
            "game_types[]": fieldParams.gameType,
            "grass_type": fieldParams.grassType,
            "is_active": fieldParams.isActive,
            "price_type[]": fieldParams.priceType,
            "price_hour[]": fieldParams.priceHour,
            "price_half_hour[]": fieldParams.priceHalf
            
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(UserData.shared?.user.establishments?[0].id ?? 0)/\(Constants.UrlWS.fields.rawValue)/\(fieldParams.fieldID)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.requestUpdateStatusField(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    
    func requestCancelReservation(reservationID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
       guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.reservation.rawValue)/\(reservationID)/\(Constants.UrlWS.cancel.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = EstablishmentWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.requestDelete(completionHandler: {[weak self] (establishment) in
            guard let self = self else { return }
            
            completionHandler(establishment)
        }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestRecoveryPassword(email: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let header = [
            //"Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "email": email
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.resetPasswordUser.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.requestPost(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestUpdateRate(reservationID: Int, rate: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        //let defaults = UserDefaults.standard
        //let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "rate": rate
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.reservation.rawValue)/\(reservationID)/\(Constants.UrlWS.reviews.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.requestPost(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestUpdateStatusResevation(establishmentID: Int, reservationID: Int, type: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
        ]
        
        if type == "approve" {
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentID)/\(Constants.UrlWS.reservation.rawValue)/\(reservationID)/approve") else {
                errorHandler(.urlInvalid, "")
                return
            }
            
            let ws = EstablishmentWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            ws.requestRentFieldWS(completionHandler: {[weak self] (jsonResponse) in
                guard let self = self else { return }
                completionHandler(jsonResponse)
                }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
        }else if type == "reject" {
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentID)/\(Constants.UrlWS.reservation.rawValue)/\(reservationID)/reject") else {
                errorHandler(.urlInvalid, "")
                return
            }
            
            let ws = EstablishmentWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            ws.requestRentFieldWS(completionHandler: {[weak self] (jsonResponse) in
                guard let self = self else { return }
                completionHandler(jsonResponse)
                }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
        }
    }
    
    func requestAllServices(completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            //"Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.services.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.requestAllServices(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestReservationsOwner(establishmentId: Int, type: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ,
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        if type == "Owner" {
            
            let parameters: [String: Any] = [
                
                "identifier": Bundle.main.bundleIdentifier as AnyObject,
                "platform": "ios",
                "version": version,
                "filter": "future"
            ]
            
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentId)/\(Constants.UrlWS.reservation.rawValue)") else {
                errorHandler(.urlInvalid, "")
                return
            }
            
            let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            ws.requestReservations(completionHandler: {[weak self] (jsonResponse) in
                guard let self = self else { return }
                completionHandler(jsonResponse)
                }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
        }else if type == "OwnerList" {
            
            let parameters: [String: Any] = [
                
                "identifier": Bundle.main.bundleIdentifier as AnyObject,
                "platform": "ios",
                "version": version,
                "filter": "past"
            ]
            
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentId)/\(Constants.UrlWS.reservation.rawValue)") else {
                errorHandler(.urlInvalid, "")
                return
            }
            
            let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            ws.requestReservations(completionHandler: {[weak self] (jsonResponse) in
                guard let self = self else { return }
                completionHandler(jsonResponse)
                }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
        }else if type == "User" {
            
            let parameters: [String: Any] = [
                
                "identifier": Bundle.main.bundleIdentifier as AnyObject,
                "platform": "ios",
                "version": version,
                "filter": "future"
            ]
            
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.reservation.rawValue)") else {
                errorHandler(.urlInvalid, "")
                return
            }
            
            let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            ws.requestReservations(completionHandler: {[weak self] (jsonResponse) in
                guard let self = self else { return }
                completionHandler(jsonResponse)
                }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
        }else if type == "list" {
            
            let parameters: [String: Any] = [
                
                "identifier": Bundle.main.bundleIdentifier as AnyObject,
                "platform": "ios",
                "version": version,
                "past": "past"
            ]
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.reservation.rawValue)") else {
                errorHandler(.urlInvalid, "")
                return
            }
            
            let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            ws.basicRequestGet(completionHandler: {[weak self] (establishment) in
                guard let self = self else { return }
                
                completionHandler(establishment)
                }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
        }
    }
    
    
    func requestUpdateStatusField(establishmentId: Int, fieldId: Int, isActive:Bool, completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void){
        
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "is_active": isActive,
            "_method": "PUT"
        ]
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentId)/\(Constants.UrlWS.fields.rawValue)/\(fieldId)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
        ws.requestUpdateStatusField(completionHandler: {[weak self] (jsonResponse) in
            guard let self = self else { return }
            completionHandler(jsonResponse)
            }, { (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestUpdateServiceStatus(establishmentId: Int, serviceToUpdate: Int, activateStatus: Bool, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let defaults = UserDefaults.standard
        let deviceToken = defaults.object(forKey: "token") != nil ? defaults.object(forKey: "token") as! String : "ios"
        let header = [
            "Authorization" : "Bearer \(UserData.shared!.access_token)" ?? "",
            "Accept": "application/json"
        ]
        
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
        
        let parameters: [String: Any] = [
            
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "service_id": serviceToUpdate,
            //"_method": "PUT"
        ]
        
        if activateStatus{
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentId)/\(Constants.UrlWS.services.rawValue)") else {
                errorHandler(.urlInvalid, "")
                return
            }
            let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            ws.requestPost(completionHandler: {[weak self] (jsonResponse) in
                guard let self = self else { return }
                completionHandler(jsonResponse)
                }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
            
        }else{
            guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentId)/\(Constants.UrlWS.services.rawValue)/\(serviceToUpdate)") else {
                errorHandler(.urlInvalid, "")
                return
            }
            let ws = OwnerWS(requestURL: url, parameters: parameters, header: header, networkClient: self)
            
            
            ws.requestDelete(completionHandler: {[weak self] (jsonResponse) in
                guard let self = self else { return }
                completionHandler(jsonResponse)
                }, { (networkError, message) in
                    errorHandler(.parse, message)
            })
        }
        
    }
    
}
