//
//  FetchOwnerOperation.swift
//  TuReta
//
//  Created by Daniel Coria on 11/28/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

final class FetchOwnerOperation {
    
    private let networkClient: OwnerNetworkClient
    
    required init(networkClient: OwnerNetworkClient){
        self.networkClient = networkClient
    }
    
    func startRequestUpdateStatusField(establishmentId: Int, fieldId: Int, isActive: Bool, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        
        networkClient.requestUpdateStatusField(establishmentId: establishmentId, fieldId: fieldId, isActive: isActive, completionHandler:
        {[weak self] (jsonResponse) in
            
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestUpdateServiceStatus(establishmentId: Int, service: Int, activatedStatus: Bool, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        
        networkClient.requestUpdateServiceStatus(establishmentId: establishmentId, serviceToUpdate: service, activateStatus: activatedStatus, completionHandler:
        {[weak self] (jsonResponse) in
            
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestReservationsOwner(establishmentId: Int, type: String, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        
        networkClient.requestReservationsOwner(establishmentId: establishmentId, type: type, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestAllServices( completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        
        networkClient.requestAllServices( completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestUpdateStatusReservation(establishmentID: Int, reservationID: Int, type: String, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        networkClient.requestUpdateStatusResevation(establishmentID: establishmentID, reservationID: reservationID, type: type, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestUpdateRate(reservationID: Int, rate: Int, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        networkClient.requestUpdateRate(reservationID: reservationID, rate: rate, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestRecoveryPassword(email: String, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        networkClient.requestRecoveryPassword(email: email, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestCancelReservation(reservationID: Int, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        
        networkClient.requestCancelReservation(reservationID: reservationID, completionHandler:
        {[weak self] (jsonResponse) in
            
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestRemoveField(fieldID: Int, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        networkClient.requestRemoveField(fieldID: fieldID, completionHandler:
        {[weak self] (jsonResponse) in
            
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func startRequestCreateNewField(newFieldParams: NewField, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        
        networkClient.requestCreateNewField(newFieldParams: newFieldParams, completionHandler:
        {[weak self] (jsonResponse) in
            
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
        
    }
    
    func startRequestUpdateField(newFieldParams: NewField, completionHandler: @escaping( _ jsonResponse: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void ) {
        
        networkClient.requestUpdateField(fieldParams: newFieldParams, completionHandler:
        {[weak self] (jsonResponse) in
            
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
        
    }
    
}
