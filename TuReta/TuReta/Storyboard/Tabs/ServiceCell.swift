//
//  ServiceCell.swift
//  TuReta
//
//  Created by Daniel Coria on 11/23/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class ServiceCell: UIView {

    @IBOutlet weak var iconService: UIImageView!
    @IBOutlet weak var nameService: UILabel!
    
    

}
