//
//  TuRetaTabBarViewController.swift
//  TuReta
//
//  Created by DC on 9/28/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class TuRetaTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let storyBoardHome = UIStoryboard(name: "Home", bundle: nil)
        let storyBoardReservs = UIStoryboard(name: "Reservaciones", bundle: nil)
        let storyBoardProfile = UIStoryboard(name: "Profile", bundle: nil)
        let storyBoardNews = UIStoryboard(name: "News", bundle: nil)
        
        let homeView = storyBoardHome.instantiateInitialViewController()
        let reservsVC = storyBoardReservs.instantiateInitialViewController()
        let profileVC = storyBoardProfile.instantiateInitialViewController()
        let newsVC = storyBoardNews.instantiateInitialViewController()
        
        let someArray = NSMutableArray(array: [homeView as Any,reservsVC!,newsVC!, profileVC!])
        self.viewControllers = someArray as? [UIViewController]
        
        
        let tab1 = self.tabBar.items?[0]
        tab1?.title = "Canchas"
        tab1?.image = UIImage(named: "home")?.withRenderingMode(.alwaysOriginal)
        tab1?.selectedImage = UIImage(named: "homeSelected")?.withRenderingMode(.alwaysOriginal)
        tab1?.badgeColor = Constants.greenOnePrimaryColor
        tab1?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.greenOnePrimaryColor], for: .selected)
        tab1?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.blackTwoPrimaryColor], for: .normal)
        //tab1?.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        
        let tab2 = self.tabBar.items?[1]
        tab2?.title = "Reservaciones"
        tab2?.image = UIImage(named: "reservas")?.withRenderingMode(.alwaysOriginal)
        tab2?.selectedImage = UIImage(named: "reservasSelected")?.withRenderingMode(.alwaysOriginal)
        tab2?.badgeColor = Constants.greenOnePrimaryColor
        tab2?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.greenOnePrimaryColor], for: .selected)
        tab2?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.blackTwoPrimaryColor], for: .normal)
        //tab2?.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        
        let tab3 = self.tabBar.items?[3]
        tab3?.title = "Perfil"
        tab3?.image = UIImage(named: "shirt")?.withRenderingMode(.alwaysOriginal)
        tab3?.selectedImage = UIImage(named: "shirtSelected")?.withRenderingMode(.alwaysOriginal)
        tab3?.badgeColor = Constants.greenOnePrimaryColor
        tab3?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.greenOnePrimaryColor], for: .selected)
        tab3?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.blackTwoPrimaryColor], for: .normal)
        //tabOrdes?.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        let tab4 = self.tabBar.items?[2]
        tab4?.title = "Noticias"
        tab4?.image = UIImage(named: "noticias")?.withRenderingMode(.alwaysOriginal)
        tab4?.selectedImage = UIImage(named: "noticiasSelected")?.withRenderingMode(.alwaysOriginal)
        tab4?.badgeColor = Constants.greenOnePrimaryColor
        tab4?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.greenOnePrimaryColor], for: .selected)
        tab4?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.blackTwoPrimaryColor], for: .normal)
        //tab1?.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)

    }
    

}
