//
//  TuRetaOwnerTabBarViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 10/15/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class TuRetaOwnerTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyBoardHome = UIStoryboard(name: "Home", bundle: nil)
        let storyBoardFavs = UIStoryboard(name: "Reservaciones", bundle: nil)
        let storyBoardTournament = UIStoryboard(name: "Torneos", bundle: nil)
        let storyBoardStats = UIStoryboard(name: "Profile", bundle: nil)
        
        
        guard let homeView = storyBoardHome.instantiateInitialViewController() else { return }
        let favsVC = storyBoardFavs.instantiateInitialViewController()
        let tournamentVC = storyBoardTournament.instantiateInitialViewController()
        let statsVC = storyBoardStats.instantiateInitialViewController()
        
        
        let someArray = NSArray(array: [homeView,favsVC!, tournamentVC!,  statsVC!])
        self.viewControllers = someArray as? [UIViewController]
        
        
        let tab1 = self.tabBar.items?[0]
        tab1?.title = "Canchas"
        tab1?.image = UIImage(named: "home")?.withRenderingMode(.alwaysOriginal)
        tab1?.selectedImage = UIImage(named: "homeSelected")?.withRenderingMode(.alwaysOriginal)
        tab1?.badgeColor = Constants.blackOnePrimaryColor
        tab1?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.greenOnePrimaryColor], for: .selected)
        //tab1?.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        
        let tab2 = self.tabBar.items?[1]
        tab2?.title = "Reservaciones"
        tab2?.image = UIImage(named: "reservas")?.withRenderingMode(.alwaysOriginal)
        tab2?.selectedImage = UIImage(named: "reservasSelected")?.withRenderingMode(.alwaysOriginal)
        tab2?.badgeColor = Constants.blackOnePrimaryColor
        tab2?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.greenOnePrimaryColor], for: .selected)
        //tab2?.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        if UIApplication.shared.applicationIconBadgeNumber != 0 {
            tab2?.badgeValue = "New"
        }
        
        let tab3 = self.tabBar.items?[2]
        tab3?.title = "Torneos"
        tab3?.image = UIImage(named: "icon-tournament")?.withRenderingMode(.alwaysOriginal)
        tab3?.selectedImage = UIImage(named: "icon-tournament-selected")?.withRenderingMode(.alwaysOriginal)
        tab3?.badgeColor = Constants.blackOnePrimaryColor
        tab3?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.greenOnePrimaryColor], for: .selected)
        //tab2?.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        
        let tab4 = self.tabBar.items?[3]
        tab4?.title = "Perfil"
        tab4?.image = UIImage(named: "shirt")?.withRenderingMode(.alwaysOriginal)
        tab4?.selectedImage = UIImage(named: "shirtSelected")?.withRenderingMode(.alwaysOriginal)
        tab4?.badgeColor = Constants.blackOnePrimaryColor
        tab4?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : Constants.greenOnePrimaryColor], for: .selected)
        //tabOrdes?.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
    }
    
    
}
