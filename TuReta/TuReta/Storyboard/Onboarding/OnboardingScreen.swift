//
//  OnboardingScreen.swift
//  TuReta
//
//  Created by DC on 08/08/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

protocol OnboardingActionDelegate: class {
    func goTocreateUserPage()
}

final class OnboardingScreen: UIViewController, UIPageViewControllerDataSource, UIScrollViewDelegate{
    
    fileprivate var pageViewController: UIPageViewController?
    @IBOutlet weak var scrollContent: UIView!
    @IBOutlet weak var headerText: UILabel!
    
    weak var onboardingDelegate: OnboardingActionDelegate?
    var indexScroll = Int()
    
    fileprivate let contentImages = ["tuto-img1",
                                     "tuto-img2",
                                     "tuto-img3"]
    fileprivate let headersText = ["¡Busca!",
                                   "¡Reserva!",
                                   "¡Diviértete!"]


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        createPageViewController()
        setupPageControl()
        
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(OnboardingScreen.moveSlide), userInfo: nil, repeats: true)
        indexScroll = 0
        self.pageViewController?.delegate = self
        headerText.text = headersText[0]
    }

    @IBAction func createUserAction(_ sender: Any) {
        self.dismiss(animated: true, completion:  {
            self.onboardingDelegate?.goTocreateUserPage()
        })
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func skipAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func moveSlide(){
        if currentControllerIndex() < 2{
            let firstController = getItemController(currentControllerIndex() + 1)!
            let startingViewControllers = [firstController]
            
            if currentControllerIndex() == 0 {
                indexScroll = 1
                headerText.text = headersText[1]
            }else if currentControllerIndex() == 1 {
                indexScroll = 2
                headerText.text = headersText[2]
            }
//            else if currentControllerIndex() == 2 {
//                indexScroll = 0
//                headerText.text = headersText[0]
//            }
   
            pageViewController?.setViewControllers(startingViewControllers, direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
            
        }else if currentControllerIndex() == 2{
            
            let firstController = getItemController(0)!
            let startingViewControllers = [firstController]
        
            indexScroll = 0
            headerText.text = headersText[0]
            pageViewController?.setViewControllers(startingViewControllers, direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
        }
        
    }
    
    

    fileprivate func createPageViewController() {
        
        let pageController = self.storyboard!.instantiateViewController(withIdentifier: "PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if contentImages.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers = [firstController]
            pageController.setViewControllers(startingViewControllers, direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        //pageViewController?.view.frame = contentView.frame;
        pageViewController?.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pageViewController?.view.frame = CGRect(x: 0, y: 0, width: scrollContent.layer.frame.width, height: scrollContent.layer.frame.height)
        addChild(pageViewController!)
        scrollContent.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
    }
    
    fileprivate func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = .gray
        appearance.currentPageIndicatorTintColor = .white
        appearance.backgroundColor = UIColor.clear
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! SlideImageViewController
        
        if itemController.itemIndex > 0 {
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! SlideImageViewController
        
        if itemController.itemIndex+1 < contentImages.count {
            return getItemController(itemController.itemIndex+1)
        }
        
        return nil
    }
    
    fileprivate func getItemController(_ itemIndex: Int) -> SlideImageViewController? {
        
        
        if itemIndex < contentImages.count {
            let pageItemController = self.storyboard!.instantiateViewController(withIdentifier: "ItemController") as! SlideImageViewController
            pageItemController.itemIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            return pageItemController
        }
        
        return nil
    }
    
    // MARK: - Page Indicator
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return contentImages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        if indexScroll == 0{
            return 0
        }else{
            
            return indexScroll
        }
    }
    
    // MARK: - Additions
    
    func currentControllerIndex() -> Int {
        
        let pageItemController = self.currentController()
        
        if let controller = pageItemController as? SlideImageViewController {
            //headerText.text = headersText[controller.itemIndex]
            return controller.itemIndex
        }
        
        return -1
    }
    
    func currentController() -> UIViewController? {
        
        if (self.pageViewController?.viewControllers?.count)! > 0 {
            return self.pageViewController?.viewControllers![0]
        }
        
        return nil
    }
}

extension OnboardingScreen: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            if let currentViewController = pageViewController.viewControllers![0] as? SlideImageViewController {
                DispatchQueue.main.async {
                    self.headerText.text = self.headersText[currentViewController.itemIndex]
                }
            }
        }
    }
}
