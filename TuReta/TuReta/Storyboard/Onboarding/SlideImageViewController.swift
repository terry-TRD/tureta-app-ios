//
//  SlideImageViewController.swift
//  Fabnu
//
//  Created by DC on 20/12/16.
//  Copyright © 2016 PixanApps. All rights reserved.
//

import UIKit

class SlideImageViewController: UIViewController {

    var itemIndex: Int = 0
    var imageName: String = "" {
        didSet {
            if let imageView = contentImageView {
                imageView.image = UIImage(named: imageName)
            }
            
        }
    }
    
    @IBOutlet var contentImageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contentImageView?.contentMode = UIView.ContentMode.scaleAspectFill
        contentImageView!.image = UIImage(named: imageName)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
