//
//  TuRetaContainer.swift
//  TuReta
//
//  Created by DC on 10/5/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation
import Swinject

class TuRetaContainer {
    
    func setupContainer(using container: Container) {
        
        container.register(ProfileServices.self) { _ in
            let networkClient = NetworkClient()
            return DefaultProfileServices(networkClient: networkClient)
        }.inObjectScope(.container)
        
        container.register(EstablishmentServices.self){ _ in
            let networkClient = NetworkClient()
            return DefaultEstablishmentServices(networkClient: networkClient)
        }.inObjectScope(.container)
    
        container.register(OwnerServices.self){ _ in
            let networkClient = NetworkClient()
            return DefaultOwnerServices(networkClient: networkClient)
        }.inObjectScope(.container)
        
        container.register(TournamentServices.self){ _ in
            let networkClient = NetworkClient()
            return DefaultTournamentServices(networkClient: networkClient)
        }
        
    }
    
    
    func profileService() -> ProfileServices {
        return AppDelegate.container.resolve(ProfileServices.self)!
    }
    
    func profileModel() -> ProfileViewModel {
        return AppDelegate.container.resolve(ProfileViewModel.self)!
    }
    
    func userModel() -> DefaultUserViewModel {
        return AppDelegate.container.resolve(DefaultUserViewModel.self)!
    }
    
    func establishmentService() -> EstablishmentServices {
        return AppDelegate.container.resolve(EstablishmentServices.self)!
    }
    
    func availableFieldsService() -> EstablishmentServices {
        return AppDelegate.container.resolve(EstablishmentServices.self)!
    }
    
    func infoEstablishmentService() -> EstablishmentServices {
        return AppDelegate.container.resolve(EstablishmentServices.self)!
    }
    
    func ownerEstablishmentService() -> OwnerServices {
        return AppDelegate.container.resolve(OwnerServices.self)!
    }
    
    func profileCloseSessionService() -> ProfileServices {
        return AppDelegate.container.resolve(ProfileServices.self)!
    }
    
    func rentFieldService() -> EstablishmentServices {
        return AppDelegate.container.resolve(EstablishmentServices.self)!
    }
    
    func tournamentService() -> TournamentServices {
        return AppDelegate.container.resolve(TournamentServices.self)!
    }
    
}
