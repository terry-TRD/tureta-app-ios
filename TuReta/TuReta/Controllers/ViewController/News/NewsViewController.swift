//
//  NewsViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 1/18/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import WebKit

class NewsViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        self.openURL()
    }
    
    func openURL() {
        let stringPath = "http://www.tureta.com.mx/news?lat=\(UserData.sharedUserLocation?.location?.latitude ?? 0.0)&lng=\(UserData.sharedUserLocation?.location?.longitude ?? 0.0)"
        if let url = URL(string: stringPath) {
            print(url)
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }


}

extension NewsViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let host = navigationAction.request.url?.host {
            if host == "www.tureta.com.mx" {
                decisionHandler(.allow)
                return
            }
        }

        decisionHandler(.cancel)
    }
}
