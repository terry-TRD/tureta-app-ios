//
//  PrivacyPolicyViewController.swift
//  TuReta
//
//  Created by DC on 29/01/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyViewController: TuRetaBaseViewController {
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Términos y condiciones"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        webView.navigationDelegate = self
        self.openURL()
    }
    
    func openURL() {
        let stringPath = "http://www.tureta.com.mx/AvisoPrivacidad.pdf"
        if let url = URL(string: stringPath) {
            
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        TranslucentNavBar()
    }
    
    
}

extension PrivacyPolicyViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let host = navigationAction.request.url?.host {
            if host == "www.tureta.com.mx" {
                decisionHandler(.allow)
                return
            }
        }
        
        decisionHandler(.cancel)
    }
}

