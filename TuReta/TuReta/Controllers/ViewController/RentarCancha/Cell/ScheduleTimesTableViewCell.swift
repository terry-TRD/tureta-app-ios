//
//  ScheduleTimesTableViewCell.swift
//  TuReta
//
//  Created by DC on 04/07/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class ScheduleTimesTableViewCell: UITableViewCell {

    @IBOutlet weak var headerText: UILabel!
    @IBOutlet weak var collection: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        
        
        return self.collection.collectionViewLayout.collectionViewContentSize
    }
    
}

extension ScheduleTimesTableViewCell {

    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

        collection.delegate = dataSourceDelegate
        collection.dataSource = dataSourceDelegate
        
        collection.allowsMultipleSelection = true
        collection.tag = row
        collection.setContentOffset(collection.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collection.reloadData()
        collection.layoutIfNeeded()
    }
}
