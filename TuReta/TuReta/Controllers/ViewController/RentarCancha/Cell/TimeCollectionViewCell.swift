//
//  TimeCollectionViewCell.swift
//  TuReta
//
//  Created by DC on 04/07/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class TimeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override var isSelected: Bool {
        didSet {
            // TODO: replace .red & .blue with desired colors
            self.contentView.backgroundColor = isSelected ? Constants.greenOnePrimaryColor : Constants.greenThreePrimaryColor
            
        }
    }
}
