//
//  DefaultRentarCanchasViewController.swift
//  TuReta
//
//  Created by DC on 16/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class DefaultRentarCanchasViewController: TuRetaBaseViewController {
    
    var pager: ViewPager?
    let options = ViewPagerOptions()
    let tabs = [
        ViewPagerTab(title: "Información", image: nil),
        ViewPagerTab(title: "Torneos y Estadísticas", image: nil)
    ]
    
    var currentMarkerSelected: EstablishmentViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = currentMarkerSelected?.name
        
        options.tabType = .basic
        options.distribution = .segmented
        options.isTabIndicatorAvailable = true
        options.tabViewBackgroundDefaultColor = .white
        options.tabViewBackgroundHighlightColor = .white
        options.tabIndicatorViewBackgroundColor = Constants.greenOnePrimaryColor
        options.tabViewTextHighlightColor = Constants.greenOnePrimaryColor
        options.tabViewTextDefaultColor = Constants.blackOnePrimaryColor
        
        pager = ViewPager(viewController: self, containerView: self.view)
        pager?.setOptions(options: options)
        pager?.setDataSource(dataSource: self)
        pager?.setDelegate(delegate: self)
        pager?.build()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NonTranslucentNavBar()
    }
    
}


extension DefaultRentarCanchasViewController: ViewPagerDataSource {
    func viewControllerAtPosition(position: Int) -> UIViewController {
        switch position {
        case 0:
            let vc = RentarCanchaViewController()
            vc.currentMarkerSelected = currentMarkerSelected
            return vc
        case 1:
            let vc = CalendarViewController()
            vc.establishmentID = currentMarkerSelected?.idE
            return vc
        default:
            return UIViewController()
        }
        
    }
    
    
    func numberOfPages() -> Int {
        return tabs.count
    }
    
    
    func tabsForPages() -> [ViewPagerTab] {
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
}

extension DefaultRentarCanchasViewController: ViewPagerDelegate {
    
    func willMoveToControllerAtIndex(index:Int) {
        print("Moving to page \(index)")
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        print("Moved to page \(index)")
    }
}



