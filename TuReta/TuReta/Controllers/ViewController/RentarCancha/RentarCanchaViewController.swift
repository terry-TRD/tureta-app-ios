//
//  RentarCanchaViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 10/30/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import DatePickerDialog
import CoreLocation

protocol CustomDelegate {
    
    func tapCell(cellIndex: IndexPath)
    func tapFav(isActive: Bool)
}

class RentarCanchaViewController: TuRetaBaseViewController, UITableViewDelegate, UITableViewDataSource, CustomDelegate {

    //@IBOutlet weak var table: UITableView!
    @IBOutlet weak var pickerDateButton: UIButton!
    @IBOutlet weak var heightContinueReservationView: NSLayoutConstraint!
    
    let pickerSet = ["1","1.5","2","2.5","3","3.5","4","4.5","5"]
    let table = UITableView()
    let pickDateButton = UIButton()
    var isGuest: Bool = true
    var numRowsAsFields: Int = 0
    var indexFieldSelected: Int = 0
    var indexFieldSelectedNew: Int = 0
    var indexCell: Int = 0
    var canSelectSchedule: Bool?
    var hoursSelected: String = ""
    
    let container = TuRetaContainer()
    var viewModelDefault: DefaultRentarCanchaViewModel?
    
    var viewModelFields: [Field]? {
        didSet{
            updateUI()
        }
    }
    
    var viewModelEstablishment: EstablishmentDetail? {
        didSet{
            self.table.reloadData()
        }
    }
    
    //MARK: - local variables
    var currentMarkerSelected: EstablishmentViewModel?
    var scheduleSelectedArray: [FieldAvailability] = []
    var fieldSelected: Field?
    var arrSelectedIndex = [IndexPath]()
    var datePicked: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if defaults.object(forKey: "isGuest") as? Bool == true{
            isGuest = true
            self.goToLogin()
        } else {
            
            table.delegate = self
            table.dataSource = self
            setupTableView()
            table.tableFooterView = UIView()
            //self.heightContinueReservationView.constant = 0
            
            //NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveChangeInSchedule, object: nil)
            
            //NotificationCenter.default.addObserver(self, selector: #selector(validateScheduleCell), name: .checkValidation, object: nil)
            
            //NotificationCenter.default.addObserver(self, selector: #selector(fillScheduleSelected), name: .checkHoursSelected, object: nil)
            
            self.requestInfoDetail()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = false
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "open", style: .plain, target: self, action: #selector(tapSendSchedule))
        //NotificationCenter.default.post(name: .didReceiveChangeInSchedule, object: nil)
    }
    
    func setupTableView() {
        view.addSubview(table)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        table.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        table.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        table.backgroundColor = .clear
        table.separatorStyle = .none
        
        view.addSubview(pickDateButton)
        pickDateButton.setTitle("Elegir fecha", for: .normal)
        pickDateButton.backgroundColor = Constants.greenOnePrimaryColor
        pickDateButton.translatesAutoresizingMaskIntoConstraints = false
        pickDateButton.layer.cornerRadius = 5
        pickDateButton.topAnchor.constraint(equalTo: table.bottomAnchor, constant: 10).isActive = true
        pickDateButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 28).isActive = true
        pickDateButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -28).isActive = true
        pickDateButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -17).isActive = true
        pickDateButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        table.bottomAnchor.constraint(equalTo: pickDateButton.topAnchor, constant: -10).isActive = true
        
        pickDateButton.addTarget(self, action: #selector(self.pickDate), for: .touchUpInside)
        
        table.register(UINib(nibName: "ReservacionOwnerTableViewCell", bundle: nil), forCellReuseIdentifier: "ReservacionOwnerTableViewCell")
        table.register(UINib(nibName: CellResusableIdentifier.emptyCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.emptyCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.headerImage.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.headerImage.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.descriptionCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.descriptionCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.chooseDateButtonCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.chooseDateButtonCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.availableFieldCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.availableFieldCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.availableFielsdCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.availableFielsdCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.basicDetailCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.basicDetailCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.mapCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.mapCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.servicesCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.servicesCell.rawValue)
        

    }
    
    func goToLogin() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        appDelegate.presentInit()
    }
    
    @objc func pickDate() {
        datePickerTapped()
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        // Do something now
        if scheduleSelectedArray.count > 1 {
            self.hideRighButtonItem(isHidden: false)
        }else{
            self.hideRighButtonItem(isHidden: true)
        }
        
        if scheduleSelectedArray.count == 0{
            fieldSelected = nil
        }
    }
    
    @IBAction func tapContinueWithReservation(_ sender: Any) {
        
        if fieldSelected!.game_types.contains(",") {
            
            let alert = HelperExtension.TwoButtonsCustomTitlesControllerWithCancelHandler(title: "¿Que tipo de cancha prefieres?", buttonTitle1: "Cancha de 11", buttonTitle2: "Cancha de 7", message: "", handler: {[weak self] flag  in
                
                if flag {
                    print("cancha de 11")
                    self?.fieldSelected?.game_types = "11"
                    for item in (self?.viewModelEstablishment?.fields[self!.indexFieldSelected].prices)! {
                        if item.game_type == 11 {
                            self?.fieldSelected?.prices?.append(item)
                            break
                        }
                    }
                        
                } else {
                    print("cancha de 7")
                    self?.fieldSelected?.game_types = "7"
                    for item in (self?.viewModelEstablishment?.fields[self!.indexFieldSelected].prices)! {
                        if item.game_type == 7 {
                            self?.fieldSelected?.prices?.append(item)
                            break
                        }
                    }
                }
                self?.showConfirmReservation()
            })
            self.present(alert, animated: true, completion: nil)
            
        } else {
            self.fieldSelected?.prices = viewModelEstablishment?.fields[indexFieldSelectedNew].prices
            self.showConfirmReservation()
        }
        
    }
    
    func showConfirmReservation() {
        performSegue(withIdentifier: "segueScheduleSelected", sender: self)
    }
    
    func hideRighButtonItem(isHidden: Bool){
        if isHidden{
            UIView.animate(withDuration: 0.2) {
                
                //self.heightContinueReservationView.constant = 0
            }
        }else{
            UIView.animate(withDuration: 0.2) {
                
                //self.heightContinueReservationView.constant = 80
            }
        }
    }

    private func updateUI(){
        
        if(self.viewModelFields!.count > 0){
            
            numRowsAsFields = viewModelFields!.count
            
            self.table.reloadData()
        }
    }
    
    func resetAll(){
        self.arrSelectedIndex.removeAll()
        self.scheduleSelectedArray.removeAll()
        self.hoursSelected = ""
        self.fieldSelected = nil
        self.indexFieldSelected = 0
        
        self.numRowsAsFields = 0
        indexCell = -1
        
        self.table.reloadData()
        NotificationCenter.default.post(name: .didReceiveChangeInSchedule, object: nil)
    }
    
    func pushToPickSchedule() {
        // TODO push to choose availabitiy time screen
        guard let pickSchedule = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "PickScheduleViewController") as? PickScheduleViewController else { return }
        
        //pickSchedule.fieldSelected = fieldSelected
        pickSchedule.datePicked = datePicked
        pickSchedule.establishmentViewModel = currentMarkerSelected
        pickSchedule.establishmentDetail = viewModelEstablishment
        
        self.navigationController?.pushViewController(pickSchedule, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if (indexPath.row == 0){
//            return 207
//        }else if (indexPath.row == 1){
//            return 130
//        }else if (indexPath.row == 4){
//            return 80
//        }else{
//            return 0
//        }
//    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 0 || indexPath.row == 3 {
//            return 207
//        } else {
//            return 0
//        }
//    }
//
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 6 {
            guard let fullMap = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "FullMapViewController") as? FullMapViewController else { return }

            let latitude = self.viewModelDefault?.establishmentDetailViewModel?.lat as! CLLocationDegrees
            let longitude = self.viewModelDefault?.establishmentDetailViewModel?.lng as! CLLocationDegrees
            let location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            
            fullMap.setupMap(location: location)
            
            self.navigationController?.pushViewController(fullMap, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 8
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderImageTableViewCell", for: indexPath) as! HeaderImageTableViewCell
            
            tableView.rowHeight = 207
            cell.delegate = self
            if viewModelEstablishment != nil {
                if viewModelEstablishment!.is_favorite{
                    cell.favoriteButton.setImage(UIImage(named: ImageName.type.iconLiked.rawValue), for: .normal)
                    cell.favoriteButton.isSelected = true
                }else{
                    cell.favoriteButton.setImage(UIImage(named: ImageName.type.iconUnLiked.rawValue), for: .normal)
                    cell.favoriteButton.isSelected = false
                }
            }
            cell.headerImage.sd_setImage(with: URL(string: currentMarkerSelected?.profile_picture_url ?? ""), placeholderImage: UIImage(named: "default-image1"))
            
            return cell
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.descriptionCell.rawValue, for: indexPath) as! DescriptionTableViewCell
            
            tableView.rowHeight = 130
            cell.establishmentRate.isUserInteractionEnabled = false
            cell.basicInfoViewModel = currentMarkerSelected
            //cell.viewModel = viewModelEstablishment
            
            cell.updateConstraintsIfNeeded()
            
            return cell
        }else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.basicDetailCell.rawValue, for: indexPath) as! BasicCellDetailFieldTableViewCell
            
            guard let establishment = self.viewModelDefault?.establishmentDetailViewModel else { return cell }
            if establishment.schedules?.count ?? 0 > 0 {
                
                cell.textOptionButton.setTitle("\(establishment.schedules?.first?.from ?? "") HRS - \(establishment.schedules?.last?.to ?? "")", for: .normal)
                tableView.rowHeight = 40
                cell.iconImage.image = UIImage(named: "icn-calendar2")
            } else {
                tableView.rowHeight = 0
            }
            cell.updateConstraintsIfNeeded()
            
            return cell
        }else if(indexPath.row == 3){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.basicDetailCell.rawValue, for: indexPath) as? BasicCellDetailFieldTableViewCell else {
                return UITableViewCell()
            }
            
            guard let establishment = self.viewModelDefault?.establishmentDetailViewModel else { return cell }
            
            if !(establishment.phone?.isEmpty ?? false) {
                tableView.rowHeight = 40
                cell.iconImage.image = UIImage(named: "icn-phone")
                cell.textOptionButton.setTitle(establishment.phone ?? "", for: .normal)
                cell.textOptionButton.addTarget(self, action: #selector(self.makeCallPlace(_:)), for: .touchUpInside)
            } else {
                tableView.rowHeight = 0
            }
            cell.updateConstraintsIfNeeded()
            
            return cell
        }else if(indexPath.row == 4){
            let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.basicDetailCell.rawValue, for: indexPath) as! BasicCellDetailFieldTableViewCell
            
            // TODO check if web is available -- if no, put rowHeight in 0
            guard let establishment = self.viewModelDefault?.establishmentDetailViewModel else { return cell }
            tableView.rowHeight = 0
            cell.iconImage.image = UIImage(named: "icn-global")
            cell.textOptionButton.setTitle("Sitio web", for: .normal)
            cell.updateConstraintsIfNeeded()
            
            return cell
        }else if(indexPath.row == 5){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.basicDetailCell.rawValue, for: indexPath) as? BasicCellDetailFieldTableViewCell else {
                return UITableViewCell()
            }
            
            if !(currentMarkerSelected?.price?.isEmpty ?? false) {
                tableView.rowHeight = 40
                cell.iconImage.image = UIImage(named: "icn-coin")
                cell.textOptionButton.setTitle(currentMarkerSelected?.price, for: .normal)
            } else {
                tableView.rowHeight = 0
            }
            cell.updateConstraintsIfNeeded()
            
            return cell
        }else if(indexPath.row == 6){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.mapCell.rawValue, for: indexPath) as? MapTableViewCell else {
                return UITableViewCell()
            }
            
            tableView.rowHeight = 140
            
            if self.viewModelDefault?.establishmentDetailViewModel != nil {
                let latitude = self.viewModelDefault?.establishmentDetailViewModel?.lat as! CLLocationDegrees
                let longitude = self.viewModelDefault?.establishmentDetailViewModel?.lng as! CLLocationDegrees
                let location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                
                cell.setupMap(location: location)
                cell.addressLabel.text = self.viewModelDefault?.establishmentDetailViewModel?.address
            }
            cell.updateConstraintsIfNeeded()
            
            return cell
        }else if(indexPath.row == 7){
            let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.servicesCell.rawValue, for: indexPath) as! ServicesPlaceTableViewCell
            
            cell.collectionView.register(UINib(nibName: CellResusableIdentifier.serviceCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellResusableIdentifier.serviceCell.rawValue)
            cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            
            ///  check services
            guard let serviceItem = viewModelEstablishment?.services else {
                tableView.rowHeight = 50
                cell.updateConstraintsIfNeeded()
                return cell
            }
            
            if serviceItem.count > 0 {
                tableView.rowHeight = 300
                
            } else {
                tableView.rowHeight = 50
                cell.updateConstraintsIfNeeded()
            }
            
            return cell
        } else {
            let emptyCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.emptyCell.rawValue, for: indexPath) as! EmptyTableViewCell
            return emptyCell
        }
//        else{
//            if viewModelDefault?.fieldViewModel?.count != 0 && viewModelDefault?.fieldViewModel != nil {
//
//                indexCell = indexPath.row - 3
//                guard let sectionViewModel = viewModelDefault?.getItemByIndex(index: indexCell) else {
//                    fatalError("unable to resolve row")
//                }
//                let itemViewModel = sectionViewModel
//                if itemViewModel.availability?.count ?? 0 > 0{
//                    let cell = tableView.dequeueReusableCell(withIdentifier: "AvailableCourtsTableViewCell", for: indexPath) as! AvailableCourtsTableViewCell
//
//                    cell.delegate = self
//                    cell.selectedAtIndex = indexPath
//                    cell.viewModel = itemViewModel
//                    cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
//
//                    return cell
//                } else {
//                    let emptyCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.emptyCell.rawValue, for: indexPath) as! EmptyTableViewCell
//                    emptyCell.messageLabel.text = "No hay horarios disponibles para la cancha \(itemViewModel.name)"
//                    return emptyCell
//                }
//            } else {
//                let emptyCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.emptyCell.rawValue, for: indexPath) as! EmptyTableViewCell
//                emptyCell.messageLabel.text = "Busca tu horario favorito"
//                return emptyCell
//            }
//        }
    }
    @objc func makeCallPlace(_ sender : UIButton!){
        guard let establishment = self.viewModelDefault?.establishmentDetailViewModel else { return }
        makeACall(phone: establishment.phone ?? "")
    }
    
    @objc func fillScheduleSelected() {
        
        guard let intHoursSelected: Double = Double(hoursSelected) else { return }
        let totalSelected = Int(intHoursSelected / 0.5)
        
        for item in arrSelectedIndex[0].row..<arrSelectedIndex[0].row + totalSelected{
           
            let indexPath = IndexPath(item: item, section: 0)
            
            if indexPath.row < self.viewModelFields?[indexFieldSelected].availability?.count ?? 0 {
                arrSelectedIndex.append(indexPath)
                let scheduleSelected = self.viewModelFields?[indexFieldSelected].availability?[indexPath.row]
                let schedule = self.viewModelFields?[indexFieldSelected].availability?.filter{ $0.to == scheduleSelected?.to }
                let _ = schedule?.compactMap{
                    scheduleSelectedArray.append($0)
                }
            }
        }
        
        table.reloadData()
        NotificationCenter.default.post(name: .didReceiveChangeInSchedule, object: nil)
    }
    
    func tapFav(isActive: Bool) {
        viewModelDefault = DefaultRentarCanchaViewModel(establishmentService: container.availableFieldsService())
        
        if isActive{
            viewModelDefault?.requestFav(establishmentID: viewModelEstablishment?.id ?? 0, type: "add", completion: {
                [weak self] in
                guard let self = self else { return }
                print("added as favorite")
                //self?.table.reloadData()
                self.requestInfoDetail()
            }, { (errorMessage) in
                print(errorMessage)
            })
        }else{
            viewModelDefault?.requestFav(establishmentID: viewModelEstablishment?.id ?? 0, type: "remove", completion: {
                [weak self] in
                guard let self = self else { return }
                print("removed as favorite")
                //self?.table.reloadData()
                self.requestInfoDetail()
            }, { (errorMessage) in
                print(errorMessage)
            })
        }
    }
    
    
    func tapCell(cellIndex: IndexPath) {
        
        indexFieldSelectedNew = (cellIndex.row - 3)
        if(fieldSelected != nil && indexFieldSelected != (cellIndex.row - 3)){
            // TODO:
            let alertError = HelperExtension.buildConfirmationAlertController(title: "", message: "¿Reiniciar selección de horarios?") {[weak self] in
                guard let self = self else { return }
                
                self.resetAll()
            }
            self.present(alertError, animated: true, completion: nil)
        }else{
            if arrSelectedIndex.count == 0 {
                indexFieldSelected = cellIndex.row - 3
                fieldSelected = viewModelFields?[indexFieldSelected]
            }else{
                
                let alertError = HelperExtension.buildConfirmationAlertController(title: "", message: "¿Reiniciar selección de horarios?") {[weak self] in
                    guard let self = self else { return }
                    
                    self.resetAll()
                }
                self.present(alertError, animated: true, completion: nil)
            }
            
        }

    }
    
    @objc func buttonClicked(sender:UIButton) {
        datePickerTapped()
    }
    
    func datePickerTapped() {
        DatePickerDialog().show("Elige una fecha", doneButtonTitle: "Aceptar", cancelButtonTitle: "Cancelar", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                self.datePicked = formatter.string(from: dt)
                
                self.pushToPickSchedule()
                
                //let dateStringDict:[String: String] = ["newDateString": self.datePicked]
                //NotificationCenter.default.post(name: .didChangeDate, object: nil, userInfo: dateStringDict )
                
                //print(formatter.string(from: dt))
                
                //self.requestAvailableFields()
            }
        }
    }
    
    func requestAvailableFields(){
        resetAll()
        viewModelDefault = DefaultRentarCanchaViewModel(establishmentService: container.availableFieldsService())
        
        viewModelDefault?.gettingAvailableFieldsData(establishmentID: self.currentMarkerSelected?.idE ?? 0, date: self.datePicked, completion: { [weak self] in
            guard let self = self else { return }
            self.viewModelFields = self.viewModelDefault?.fieldViewModel
        })
    }
    
    func requestInfoDetail(){
        
        viewModelDefault = DefaultRentarCanchaViewModel(establishmentService: container.infoEstablishmentService())
        
        viewModelDefault?.requestEstablismentDetail(idPlace: currentMarkerSelected!.idE, completion: { [weak self] in
            guard let self = self else { return }
            self.viewModelEstablishment = self.viewModelDefault?.establishmentDetailViewModel
            
            }, {( message) in
                let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueScheduleSelected"{
            let vc = segue.destination as! ConfirmRentViewController
            vc.currentMarkerSelected = self.currentMarkerSelected
            vc.scheduleSelectedArray = scheduleSelectedArray
            vc.fieldSelected = fieldSelected
            vc.dateSelected = datePicked
            vc.typeReservation = 1
        }
    }
}


extension RentarCanchaViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        
        // if theres more tan 6, divide in 2 columns
        guard let serviceItem = viewModelEstablishment?.services else { return CGSize(width: 0, height: 0) }
        if serviceItem.count > 6 {
            return CGSize(width: collectionViewSize/2, height: 30)
        } else if serviceItem.count > 12 {
            return CGSize(width: collectionViewSize/3, height: 30)
        } else {
            return CGSize(width: collectionViewSize, height: 30)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let serviceItem = viewModelEstablishment?.services else { return 0 }
        return serviceItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellResusableIdentifier.serviceCell.rawValue, for: indexPath) as!  ServiceCollectionViewCell
        
        guard let serviceItem = viewModelEstablishment?.services?[indexPath.row] else { return cell }
        cell.configCell(service: serviceItem)
        return cell
    }

}


extension Notification.Name {
    static let didReceiveChangeInSchedule = Notification.Name("didReceiveToCheckSchedules")
    static let didCompleteTask = Notification.Name("didCompleteTask")
    static let completedLengthyDownload = Notification.Name("completedLengthyDownload")
    static let checkValidation = Notification.Name("checkValidation")
    static let checkHoursSelected = Notification.Name("checkHoursSelected")
    static let updateSummary = Notification.Name("updateSummary")
    static let updateCollection = Notification.Name("updateCollection")
}

extension RentarCanchaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func showPickerInActionSheet() {
        let message = "\n\n\n\n\n\n\n\n"
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let alert = UIAlertController(title: "Cuantas horas quieres jugar?", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        
        let titleFont : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont),
            NSAttributedString.Key.foregroundColor: Constants.blackOnePrimaryColor
        ]
        let titleAttrString = NSMutableAttributedString(string: "Cuantas horas quieres jugar?", attributes: titleFont)
        alert.setValue(titleAttrString, forKey: "attributedTitle")

        //Create a frame (placeholder/wrapper) for the picker and then create the picker
        let pickerFrame: CGRect = CGRect(x: 0, y: 0, width: alert.view.frame.width - 20, height: 200) // CGRectMake(left, top, width, height) - left and top are like margins
        let picker: UIPickerView = UIPickerView(frame: pickerFrame)
        picker.backgroundColor = UIColor.clear

        //set the pickers datasource and delegate
        picker.delegate = self
        picker.dataSource = self

        //Add the picker to the alert controller
        alert.view.addSubview(picker)

        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        let okAction = UIAlertAction(title: "Continuar", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in self.valueSelected(value: self.pickerSet[picker.selectedRow(inComponent: 0)])
        })
        alert.addAction(okAction)

        self.present(alert, animated: true, completion: nil)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerSet[row]
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        pickerSet.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int { return 1 }
    

    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let titleData = pickerSet[row]
       let titleFont : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont),
            NSAttributedString.Key.foregroundColor: Constants.blackOnePrimaryColor
        ]
        let messageAttrString = NSMutableAttributedString(string: titleData, attributes: titleFont)
        return messageAttrString
    }


    func valueSelected(value: String) {
        hoursSelected = value
        NotificationCenter.default.post(name: .checkHoursSelected, object: nil)
    }
}
