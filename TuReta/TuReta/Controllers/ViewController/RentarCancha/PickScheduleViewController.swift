//
//  PickScheduleViewController.swift
//  TuReta
//
//  Created by DC on 03/07/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import Cosmos

class PickScheduleViewController: TuRetaBaseViewController {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var establishmentNameLabel: UILabel!
    @IBOutlet weak var rate: CosmosView!
    @IBOutlet weak var bottomButton: UIButton!
    
    var datePicked: String = ""
    var fieldSelected: Field? {
        willSet(newValue) {
            self.fieldSelected = newValue
            gameTypeSelected = nil
            if newValue != nil {
                let _ = self.viewModelDefault?.fieldViewModel?.compactMap {
                    if fieldSelected?.id == $0.id {
                        resetAll()
                        availabilityScheduleByField = $0.availability as! [FieldAvailability]
                        bottomButton.setTitle("Reservar", for: .normal)
                    }
                }
            }
            self.table.reloadData()
        }
    }
    var gameTypeSelected: Int? {
        willSet(newValue) {
            self.gameTypeSelected = newValue
            gameTypeString = "\(gameTypeSelected ?? 0)"
            self.table.reloadData()
        }
        didSet {}
    }
    let container = TuRetaContainer()
    var viewModelDefault: DefaultRentarCanchaViewModel?
    var availabilityFields: [Field] = []
    var availabilityScheduleByField: [FieldAvailability] = []
    var openTime: String?
    var closeTime: String?
    var lunchTime: String?
    var arrayTimes: [String] = []
    var establishmentViewModel : EstablishmentViewModel?
    var establishmentDetail: EstablishmentDetail?
    
    var indexStartSelected: IndexPath?
    var indexEndSelected: IndexPath?
    var timeSelectedStart: FieldAvailability?
    var timeSelectedEnd: FieldAvailability?
    var startTimeChanged: Bool?
    var isEndTimeSelected: Bool?
    var gameTypeString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO:
        // change back button text
        table.delegate = self
        table.dataSource = self
        establishmentNameLabel.text = establishmentDetail?.name
        openTime = establishmentDetail?.schedules?.first?.from
        lunchTime = establishmentDetail?.schedules?[1].from
        closeTime = establishmentDetail?.schedules?.last?.to
        rate.settings.fillMode = .full
        rate.rating = Double(establishmentViewModel?.rate ?? Int(0.0))
        bottomButton.setTitle("Elegir otra fecha", for: .normal)
        gettingAllTimes()
        
        viewModelDefault = DefaultRentarCanchaViewModel(establishmentService: container.availableFieldsService())
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateSummaryCell), name: .updateSummary, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateCollections), name: .updateCollection, object: nil)
        
        table.register(UINib(nibName: CellResusableIdentifier.basicCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.basicCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.scheduleTimes.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.scheduleTimes.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.summaryCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.summaryCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.emptyCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.emptyCell.rawValue)
        
        requestAvailableFields()
    }
    
    func gettingAllTimes() {
        guard let _openTime = openTime else { return }
        guard let _lunchTime = lunchTime else { return }
        guard let _closeTime = closeTime else { return }
        
        var newTime: Date? = _openTime.getDate()
        
        while newTime != nil {
            if newTime?.addingTimeInterval(TimeInterval(60*30)) == _closeTime.getDate() {
                newTime = nil
            } else {
                newTime = newTime?.addingTimeInterval(TimeInterval(60*30))
                
                arrayTimes.append(newTime?.getString() ?? "")
            }
        }
        
    }
    
    func requestAvailableFields(){
        resetAll()
        
        viewModelDefault?.gettingAvailableFieldsData(establishmentID: self.establishmentViewModel?.idE ?? 0, date: self.datePicked, completion: { [weak self] in
            guard let self = self else { return }
            self.availabilityFields = self.viewModelDefault?.fieldViewModel as! [Field]
            self.table.reloadData()
        })
    }
    
    func resetAll(){
        
        self.fieldSelected = nil
        self.indexStartSelected = nil
        self.indexEndSelected = nil
        self.timeSelectedStart = nil
        self.timeSelectedEnd = nil
        
        self.table.reloadData()
        NotificationCenter.default.post(name: .didReceiveChangeInSchedule, object: nil)
    }
    
    func confirmReservation() {
        
        // TODO consider this
        //        if fieldSelected!.game_types.contains(",") {
        //        }
        
        if timeSelectedStart != nil {} else {
            let alertError = HelperExtension.buildBasicAlertController(title: "", message: "Elige hora de inicio", handler: nil)
            self.present(alertError, animated: true, completion: nil)
            return
        }
        
        if timeSelectedEnd != nil {} else {
            let alertError = HelperExtension.buildBasicAlertController(title: "", message: "Elige hora final", handler: nil)
            self.present(alertError, animated: true, completion: nil)
            return
        }
        
        if gameTypeSelected != nil {} else {
            let alertError = HelperExtension.buildBasicAlertController(title: "", message: "Elige el numero de jugadores", handler: nil)
            self.present(alertError, animated: true, completion: nil)
            return
        }
        // typeReservation = 1 user ; 2 manual
        requestRentField(typeReservation: 1, timeStart: self.timeSelectedStart?.from ?? "", timeEnd: self.timeSelectedEnd?.from ?? "")
    }
    
    func requestRentField(typeReservation: Int, timeStart: String, timeEnd: String){
        viewModelDefault = DefaultRentarCanchaViewModel(establishmentService: container.rentFieldService())
        
        fieldSelected?.game_types = gameTypeString ?? ""
        
        guard let animationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AnimationBaseViewController") as? AnimationBaseViewController else { return }
        
        animationVC.typeReservation = typeReservation
        self.present(animationVC, animated: true, completion: nil)
        
        viewModelDefault?.requestingRentField(field: fieldSelected!, from: timeStart, to: timeEnd, type: typeReservation, manualPrice: nil, completion: { [weak self] (response)  in
            guard let self = self else { return }
            if(response){
                
                NotificationCenter.default.post(name: .reservationReady, object: nil, userInfo: nil)
                
            }
            }, {( message) in
                
                NotificationCenter.default.post(name: .reservationFailed, object: nil, userInfo: nil)
                
        })
        
    }
    
    @IBAction func tapBottomButton(_ sender: Any) {
        if availabilityScheduleByField.count > 0 {
            self.confirmReservation()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}

extension PickScheduleViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 { }
        else if indexPath.row == 1 {
            if self.availabilityFields.count > 0 {
                if let schedules = self.availabilityFields[0].availability, schedules.count > 0 {
                    guard let pickFieldVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "PickFieldViewController") as? PickFieldViewController else { return }
                    pickFieldVC.fieldsArray = establishmentDetail?.fields as! [Field]
                    pickFieldVC.fieldSelected = fieldSelected
                    
                    self.navigationController?.pushViewController(pickFieldVC, animated: true)
                }
            }
            
        } else if indexPath.row == 5 {
            if fieldSelected != nil {
                guard let gameTypeVC = UIStoryboard(name: "NewField", bundle: nil).instantiateViewController(withIdentifier: "GameTypeViewController") as? GameTypeViewController else { return }
                let gameType = HelperExtension.GameTypeSelected(type: Int(fieldSelected?.game_types ?? "") ?? 0)
                gameTypeVC.gameTypeAvailability = [HelperExtension.GameType(type: gameType)]
                
                self.navigationController?.pushViewController(gameTypeVC, animated: true)
            } else {
                // TODO show alert " elige cancha"
            }
        }
    }
}

extension PickScheduleViewController: UITableViewDataSource {
    
    func returnEmptyCell(tableView: UITableView, indexPath: IndexPath, text: String) -> EmptyTableViewCell {
        let emptyCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.emptyCell.rawValue, for: indexPath) as! EmptyTableViewCell
        tableView.rowHeight = 390
        emptyCell.messageLabel.text = text
        emptyCell.messageLabel.textColor = Constants.blackTwoPrimaryColor
        return emptyCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let basicCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.basicCell.rawValue, for: indexPath) as? BasicCellTableViewCell else {
            return UITableViewCell()
        }
        
        switch indexPath.row {
        case 0:
            guard let basicCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.basicCell.rawValue, for: indexPath) as? BasicCellTableViewCell else {
                return UITableViewCell()
            }
            
            tableView.rowHeight = 51
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = datePicked.getNiceTimeFormat3()
            basicCell.iconImage.image = UIImage(named: ImageName.type.iconCalendar.rawValue)
            basicCell.isUserInteractionEnabled = false
            basicCell.arrowImage = nil
            return basicCell
        case 1:
            guard let basicCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.basicCell.rawValue, for: indexPath) as? BasicCellTableViewCell else {
                return UITableViewCell()
            }
            
            if self.availabilityFields.count > 0 {
                if let schedules = self.availabilityFields[0].availability {
                    if schedules.count > 0 {
                        tableView.rowHeight = 51
                    } else {
                        return returnEmptyCell(tableView: tableView, indexPath: indexPath, text: "No hay horario para esta fecha")
                    }
                } else {
                    return returnEmptyCell(tableView: tableView, indexPath: indexPath, text: "No hay horario para esta cancha")
                }
            } else {
                return returnEmptyCell(tableView: tableView, indexPath: indexPath, text: "No hay horario para esta fecha")
            }
            
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = fieldSelected?.name == nil ? "Añade la cancha" : fieldSelected?.name
            basicCell.iconImage.image = UIImage(named: ImageName.type.iconField.rawValue)
            basicCell.arrowImage.image = UIImage(named: "icn-forward")
            basicCell.isUserInteractionEnabled = true
            return basicCell
            
        case 2:
            guard let basicCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.basicCell.rawValue, for: indexPath) as? BasicCellTableViewCell else {
                return UITableViewCell()
            }
            
            if self.availabilityFields.count > 0 {
                if let schedules = self.availabilityFields[0].availability {
                    if schedules.count > 0 {
                        tableView.rowHeight = 51
                    } else {
                        tableView.rowHeight = 0
                    }
                } else {
                    tableView.rowHeight = 0
                }
            } else {
                tableView.rowHeight = 0
            }
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = "Elige un horario"
            basicCell.iconImage.image = UIImage(named: ImageName.type.iconClock.rawValue)
            basicCell.isUserInteractionEnabled = false
            basicCell.bottomView.backgroundColor = .clear
            basicCell.arrowImage = nil
            return basicCell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.scheduleTimes.rawValue, for: indexPath) as? ScheduleTimesTableViewCell else {
                return UITableViewCell()
            }
            
            guard let field = fieldSelected else {
                tableView.rowHeight = 0
                return cell
            }
            if self.availabilityFields.count > 0 {
                if let schedules = self.availabilityFields[0].availability {
                    if schedules.count > 0 {
                        tableView.rowHeight = 80
                    } else {
                        tableView.rowHeight = 0
                    }
                } else {
                    tableView.rowHeight = 0
                }
            } else {
                tableView.rowHeight = 0
            }
            cell.backgroundColor = .clear
            
            cell.collection.tag = 3
            cell.collection.register(UINib(nibName: CellResusableIdentifier.timeCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellResusableIdentifier.timeCell.rawValue)
            
            cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            if indexStartSelected != nil {
                cell.collection.scrollToItem(at: indexStartSelected ?? IndexPath(row: 0, section: 0), at: .left, animated: true)
            }
            cell.headerText.text = "Hora inicio"
            return cell
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.scheduleTimes.rawValue, for: indexPath) as? ScheduleTimesTableViewCell else {
                return UITableViewCell()
            }
            guard let field = fieldSelected else {
                tableView.rowHeight = 0
                return cell
            }
            if self.availabilityFields.count > 0 {
                if let schedules = self.availabilityFields[0].availability {
                    if schedules.count > 0 {
                        tableView.rowHeight = 80
                    } else {
                        tableView.rowHeight = 0
                    }
                } else {
                    tableView.rowHeight = 0
                }
            } else {
                tableView.rowHeight = 0
            }
            cell.backgroundColor = .clear
            
            cell.collection.tag = 4
            cell.collection.register(UINib(nibName: CellResusableIdentifier.timeCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellResusableIdentifier.timeCell.rawValue)
            
            cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            
            if indexStartSelected != nil {
                if indexEndSelected == nil {
                    cell.collection.scrollToItem(at: indexStartSelected ?? IndexPath(row: 0, section: 0), at: .left, animated: true)
                } else {
                    cell.collection.scrollToItem(at: indexEndSelected ?? IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
                }
            }
            cell.headerText.text = "Hora Final"
            return cell
        case 5:
            if self.availabilityFields.count > 0 {
                if let schedules = self.availabilityFields[0].availability {
                    if schedules.count > 0 {
                        tableView.rowHeight = 51
                    } else {
                        tableView.rowHeight = 0
                    }
                } else {
                    tableView.rowHeight = 0
                }
            } else {
                tableView.rowHeight = 0
            }
            basicCell.backgroundColor = .clear
            let gameType = HelperExtension.GameTypeSelected(type: gameTypeSelected ?? 0)
            basicCell.titleLabel.text = gameTypeSelected == nil ? "Numero de jugadores" : HelperExtension.GameType(type: gameType)
            basicCell.iconImage.image = UIImage(named: ImageName.type.iconGroup.rawValue)
            return basicCell
        case 6:
            // if is manual reservation set row rowHeight = 51
            if self.availabilityFields.count > 0 {
                if let schedules = self.availabilityFields[0].availability {
                    if schedules.count > 0 {
                        tableView.rowHeight = 0
                    } else {
                        tableView.rowHeight = 0
                    }
                } else {
                    tableView.rowHeight = 0
                }
            } else {
                tableView.rowHeight = 0
            }
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = "Costo Opcional"
            return basicCell
        case 7:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.summaryCell.rawValue, for: indexPath) as? SummaryTableViewCell else {
                return UITableViewCell()
            }
            
            if self.availabilityFields.count > 0 {
                if let schedules = self.availabilityFields[0].availability {
                    if schedules.count > 0 {
                        tableView.rowHeight = 165
                    } else {
                        tableView.rowHeight = 0
                    }
                } else {
                    tableView.rowHeight = 0
                }
            } else {
                tableView.rowHeight = 0
            }
            cell.backgroundColor = .clear
            cell.isUserInteractionEnabled = false
            guard let _ = fieldSelected else {
                tableView.rowHeight = 0
                return cell
            }
            let pricePerHour = fieldSelected?.prices?[0].price_per_hour ?? ""
            cell.perHourLabel.text = "$\(pricePerHour) MX"
            guard let selectedStartIndex = indexStartSelected else {
                cell.numberOfHoursLabel.text = ""
                cell.totalPriceLabel.text = ""
                return cell }
            guard let selectedEndIndex = indexEndSelected else {
                cell.numberOfHoursLabel.text = ""
                cell.totalPriceLabel.text = ""
                return cell }
            
            let numberOfHours = CGFloat(selectedEndIndex.row - selectedStartIndex.row) * 0.5
            cell.numberOfHoursLabel.text = "\(numberOfHours)"
            
            let totalValue = Double("\(numberOfHours)")! * Double(pricePerHour)!
            cell.totalPriceLabel.text = "\(totalValue) MX"
            
            return cell
        default:
            if self.availabilityFields.count > 0 {
                if let schedules = self.availabilityFields[0].availability {
                    if schedules.count > 0 {
                        tableView.rowHeight = 51
                    } else {
                        tableView.rowHeight = 0
                    }
                } else {
                    tableView.rowHeight = 0
                }
            } else {
                tableView.rowHeight = 0
            }
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = "Resumen"
            return basicCell
        }
    }
    
    @objc func updateSummaryCell() {
        table.reloadData()
    }
    
    @objc func updateCollections() {
        print("reload collection")
        table.reloadData()
    }
    
    
    @objc func updateCells() {
        if startTimeChanged ?? false {
            table.reloadData()
        }
    }
}


extension PickScheduleViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 95, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayTimes.count //availabilityScheduleByField.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellResusableIdentifier.timeCell.rawValue, for: indexPath) as!  TimeCollectionViewCell
            
            cell.timeLabel.text = "\(arrayTimes[indexPath.row]) HRS"
            cell.contentView.backgroundColor = .gray
            cell.isUserInteractionEnabled = false
            for value in availabilityScheduleByField {
                if arrayTimes[indexPath.row] == value.from.getTimeFormat() {
                    cell.contentView.backgroundColor = cell.isSelected ? Constants.greenOnePrimaryColor : Constants.greenThreePrimaryColor
                    if indexStartSelected == indexPath {
                        cell.isSelected = true
                        cell.contentView.backgroundColor = Constants.greenOnePrimaryColor
                    }
                    cell.isUserInteractionEnabled = true
                    return cell
                }
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellResusableIdentifier.timeCell.rawValue, for: indexPath) as!  TimeCollectionViewCell
            
            cell.timeLabel.text = "\(arrayTimes[indexPath.row]) HRS"
            cell.contentView.backgroundColor = .gray
            cell.isUserInteractionEnabled = false
            for value in availabilityScheduleByField {
                if arrayTimes[indexPath.row] == value.from.getTimeFormat() {
                    cell.contentView.backgroundColor = cell.isSelected ? Constants.greenOnePrimaryColor : Constants.greenThreePrimaryColor
                    if indexEndSelected == indexPath {
                        if self.startTimeChanged ?? false {
                            cell.isSelected = false
                            self.indexEndSelected = nil
                            self.timeSelectedEnd = nil
                            cell.contentView.backgroundColor = Constants.greenThreePrimaryColor
                        }else {
                            cell.isSelected = true
                            cell.contentView.backgroundColor = Constants.greenOnePrimaryColor
                        }
                        cell.isUserInteractionEnabled = true
                        return cell
                    }
                    cell.isUserInteractionEnabled = true
                    return cell
                }
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView.tag == 3 {
            guard let selectedIndex = indexStartSelected else {
                let cell: TimeCollectionViewCell = collectionView.cellForItem(at: indexPath) as! TimeCollectionViewCell
                indexStartSelected = indexPath
                timeSelectedStart = availabilityScheduleByField[indexPath.row]
                self.startTimeChanged = true
                NotificationCenter.default.post(name: .updateCollection, object: nil)
                return
            }
            
            if selectedIndex != indexPath {
                guard let cell: TimeCollectionViewCell = collectionView.cellForItem(at: selectedIndex) as? TimeCollectionViewCell else {
                    timeSelectedStart = availabilityScheduleByField[indexPath.row]
                    self.startTimeChanged = true
                    indexStartSelected = indexPath
                    NotificationCenter.default.post(name: .updateCollection, object: nil)
                    return
                }
                collectionView.deselectItem(at: selectedIndex, animated: true)
                cell.isSelected = false
                
                indexStartSelected = indexPath
                timeSelectedStart = availabilityScheduleByField[indexPath.row]
                self.startTimeChanged = true
                NotificationCenter.default.post(name: .updateCollection, object: nil)
            } else {
                indexStartSelected = indexPath
                timeSelectedStart = availabilityScheduleByField[indexPath.row]
                self.startTimeChanged = true
                NotificationCenter.default.post(name: .updateCollection, object: nil)
            }
            
            
        } else {
            guard let indexStart = indexStartSelected else {
                collectionView.deselectItem(at: indexPath, animated: true)
                return
            }
            guard let selectedIndex = indexEndSelected else {
                
                if indexPath.row <= indexStart.row {
                    collectionView.deselectItem(at: indexPath, animated: true)
                    return
                } else {
                    if indexPath.row > indexStart.row + 1 {
                        
                        guard let someCell: TimeCollectionViewCell = collectionView.cellForItem(at: IndexPath(row: indexStart.row + 1, section: 0)) as? TimeCollectionViewCell else {
                            timeSelectedStart = availabilityScheduleByField[indexStart.row]
                            self.startTimeChanged = true
                            NotificationCenter.default.post(name: .updateCollection, object: nil)
                            return
                        }
                        
                        if someCell.isUserInteractionEnabled {
                            
                            indexEndSelected = indexPath
                            timeSelectedEnd = availabilityScheduleByField[indexPath.row]
                            self.startTimeChanged = false
                            NotificationCenter.default.post(name: .updateSummary, object: nil)
                            return
                        } else {
                            collectionView.deselectItem(at: indexPath, animated: true)
                            self.startTimeChanged = false
                            NotificationCenter.default.post(name: .updateSummary, object: nil)
                            return
                        }
                        
                    } else {
                        collectionView.deselectItem(at: indexPath, animated: true)
                        self.startTimeChanged = false
                        NotificationCenter.default.post(name: .updateSummary, object: nil)
                        return
                    }
                }
                
            }
            
            
            if indexPath.row <= indexStart.row {
                collectionView.deselectItem(at: indexPath, animated: true)
            } else {
                if selectedIndex != indexPath {
                    if indexPath.row > indexStart.row + 1 {
                        if validateConsecutiveTime(indexStart.row) {
                            collectionView.deselectItem(at: selectedIndex, animated: true)
                            indexEndSelected = indexPath
                            timeSelectedEnd = availabilityScheduleByField[indexPath.row]
                            self.startTimeChanged = false
                            NotificationCenter.default.post(name: .updateSummary, object: nil)
                            return
                        } else {
                            collectionView.deselectItem(at: indexPath, animated: true)
                            self.startTimeChanged = false
                            NotificationCenter.default.post(name: .updateSummary, object: nil)
                            return
                        }
                    } else {
                        collectionView.deselectItem(at: indexPath, animated: true)
                        self.startTimeChanged = false
                        NotificationCenter.default.post(name: .updateSummary, object: nil)
                        return
                    }
                }
            }
        }
        
    }
    
    func validateConsecutiveTime(_ indexStart: Int) -> Bool{
        // check if time is consecutive
        
        var allItemsArray: [String] = []
        var availableItemsArray: [String] = []
        var isConsecutive: Bool = true
        
        // to get times from startIndex
        let _ = stride(from: indexStart, to: arrayTimes.count, by: 1).map {
            allItemsArray.append(arrayTimes[$0])
        }
        
        // to get available times from startSelected
        for item in allItemsArray {
            let _ = availabilityScheduleByField.compactMap {
                if $0.from.getTimeFormat() == item {
                    availableItemsArray.append($0.from.getTimeFormat())
                    return
                }
            }
        }
        
        // to validate if has equal times if not means times not consecutive
        for (index, item) in allItemsArray.enumerated() {
            if item != availableItemsArray[index] {
                isConsecutive = false
                break
            }
        }
        
        return isConsecutive ? true : false
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 3{
            let cell: TimeCollectionViewCell = collectionView.cellForItem(at: indexPath) as! TimeCollectionViewCell
            timeSelectedStart = nil
        } else {
            let cell: TimeCollectionViewCell = collectionView.cellForItem(at: indexPath) as! TimeCollectionViewCell
            timeSelectedEnd = nil
        }
    }
}
