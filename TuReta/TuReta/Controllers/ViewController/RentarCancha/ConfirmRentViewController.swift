//
//  ConfirmRentViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 11/16/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class ConfirmRentViewController: TuRetaBaseViewController {
    @IBOutlet weak var nameEstablishment: UILabel!
    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var dateBegin: UILabel!
    @IBOutlet weak var dateEnd: UILabel!
    @IBOutlet weak var timeMatch: UILabel!
    @IBOutlet weak var dateConfirmation: UILabel!
    @IBOutlet weak var imageField: UIImageView!
    
    @IBOutlet weak var totalPerHourLabel: UILabel!
    @IBOutlet weak var totalHoursLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    var popUpVC: AnimationBaseViewController?
    
    // MARK: Local variables
    var currentMarkerSelected: EstablishmentViewModel?
    var scheduleSelectedArray: [FieldAvailability] = []
    var fieldSelected: Field?
    var dateSelected: String = ""
    var typeReservation: Int = 1
    
    let container = TuRetaContainer()
    var viewModelDefault: DefaultRentarCanchaViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateUI()
    }
    
    func updateUI(){
        guard let urlImage = URL(string: currentMarkerSelected?.profile_picture_url ?? "") else { return }
        imageField.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default-image1"))
        nameEstablishment.text = currentMarkerSelected?.name
        nameField.text = "Cancha: \(fieldSelected?.name ?? "")"
        dateBegin.text = "Hora de inicio: \(scheduleSelectedArray.first?.from.getTimeFormat() ?? "") HRS"
        dateEnd.text = "Hora de fin: \(scheduleSelectedArray.last?.to.getTimeFormat() ?? "") HRS"
        timeMatch.text = "Duración: \(scheduleSelectedArray.first?.from.getTimeFormat().getTimeBewteenTwoDates(dateTwo: scheduleSelectedArray.last?.to.getTimeFormat() ?? "") ?? "") horas"
        dateConfirmation.text = "Cancha disponible para el día: \(dateSelected.getTimeOnlyDay()) de \(dateSelected.getTimeOnlyMonth()) de \(dateSelected.getTimeOnlyYear())"
        
        totalPerHourLabel.text = "Subtotal: $\(fieldSelected?.prices?[0].price_per_hour ?? "") por hora"
        let pricePerHour = fieldSelected?.prices?[0].price_per_hour ?? ""
        totalHoursLabel.text = "Total de horas: \(scheduleSelectedArray.first?.from.getTimeFormat().getTimeBewteenTwoDates(dateTwo: scheduleSelectedArray.last?.to.getTimeFormat() ?? "") ?? "") horas"
        
        let timeSeconds = "\(scheduleSelectedArray.first?.from.getTimeFormat().getTimeBewteenTwoDates(dateTwo: scheduleSelectedArray.last?.to.getTimeFormat() ?? "") ?? "")"
        
        let totalValue = Double(pricePerHour)! * Double(timeSeconds)!
        totalLabel.text = "Total: $\(totalValue)"
    }
    
    @IBAction func bookFieldButton(_ sender: Any) {
        requestRentField()
    }
    
    func requestRentField(){
        viewModelDefault = DefaultRentarCanchaViewModel(establishmentService: container.rentFieldService())
        self.presentAnimation()
        viewModelDefault?.requestingRentField(field: fieldSelected!, from: scheduleSelectedArray.first!.from, to: scheduleSelectedArray.last!.to, type: typeReservation, manualPrice: nil, completion: { [weak self] (response)  in
            if(response){
                
                NotificationCenter.default.post(name: .reservationReady, object: nil, userInfo: nil )                
            }
            }, {( message) in
                self.stopAnimation()
                let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
    
    }
    
    func presentAnimation() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "AnimationBaseViewController") as! AnimationBaseViewController
        newViewController.animationName = "checklist"
        newViewController.typeReservation = typeReservation
        self.present(newViewController, animated: true, completion: nil)
    }
}
