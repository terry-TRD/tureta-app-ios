//
//  DefaultReservationsViewController.swift
//  TuReta
//
//  Created by DC on 01/05/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class DefaultReservationsViewController: TuRetaBaseViewController {
    
    var isGuest: Bool = true
    var pager: ViewPager?
    let options = ViewPagerOptions()
    let tabs = [
        ViewPagerTab(title: "Próximas", image: nil),
        ViewPagerTab(title: "Anteriores", image: nil)
    ]
    
    var viewModel: DefaultUserViewModel?
    let container = TuRetaContainer()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Reservaciones"
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
        } else {
            isGuest = true
            self.goToLogin()
        }
        
        
        isGuest = false
        options.tabType = .basic
        options.distribution = .segmented
        options.isTabIndicatorAvailable = true
        options.tabViewBackgroundDefaultColor = .white
        options.tabViewBackgroundHighlightColor = .white
        options.tabIndicatorViewBackgroundColor = Constants.greenOnePrimaryColor
        options.tabViewTextHighlightColor = Constants.greenOnePrimaryColor
        options.tabViewTextDefaultColor = Constants.blackTwoPrimaryColor
        
        pager = ViewPager(viewController: self, containerView: self.view)
        pager?.setOptions(options: options)
        pager?.setDataSource(dataSource: self)
        pager?.setDelegate(delegate: self)
        pager?.build()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
        super.viewWillAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func goToLogin() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        self.dismiss(animated:false, completion: nil)
        appDelegate.presentInit()
    }

}

extension DefaultReservationsViewController: ViewPagerDataSource {
    func viewControllerAtPosition(position: Int) -> UIViewController {
        switch position {
        case 0:
            let vc = ReservacionesViewController()
            return vc
        case 1:
            let vc = HistoryReservationViewController()
            return vc
        default:
            return UIViewController()
        }
        
    }
    
    
    func numberOfPages() -> Int {
        return tabs.count
    }
    
    
    func tabsForPages() -> [ViewPagerTab] {
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
}

extension DefaultReservationsViewController: ViewPagerDelegate {
    
    func willMoveToControllerAtIndex(index:Int) {
        print("Moving to page \(index)")
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        print("Moved to page \(index)")
    }
}

