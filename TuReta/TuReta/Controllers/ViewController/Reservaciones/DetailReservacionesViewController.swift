//
//  DetailReservacionesViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 12/23/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import Cosmos

class DetailReservacionesViewController: TuRetaBaseViewController {
    @IBOutlet weak var heightRateView: NSLayoutConstraint!
    @IBOutlet weak var heightYellowView: NSLayoutConstraint!
    @IBOutlet weak var ratePlayerView: UIView!
    @IBOutlet weak var confirmButtons: UIStackView!
    
    var rateViewConstant: CGFloat = 80.0 // rate
    
    @IBOutlet weak var reservationNameLabel: UILabel!
    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var rateNumber: UILabel!
    
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var lengthReservationLabel: UILabel!
    
    @IBOutlet weak var playerLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var ratingCosmos: CosmosView!
    @IBOutlet weak var sendRate: UIButton!
    
    @IBOutlet weak var priceUnitLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var reservationTimeLabel: UILabel!
    @IBOutlet weak var reservationDayLabel: UILabel!
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var rateButton: UIButton!
    
    //var isOwner: Bool?
    var viewModelReservation: Reservation?
    var viewModel: DetailReservationViewModel?
    
    var viewModelDefaultCanchasOwner: DefaultReservationsOwnerViewModel?
    let container = TuRetaContainer()
    
    init(viewModel: DetailReservationViewModel) {
        self.viewModelReservation = viewModel.modelReservation
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewModel = DetailReservationViewModel(modelReservation: viewModelReservation!)
        
        //reservationNameLabel.text = viewModel?.reservationName
        //lengthReservationLabel.text = viewModel?.lengthTime
        //reservationDayLabel.text = viewModel?.dayReservation
        playerLabel.text = isOwner! ? (viewModelReservation?.type == ReservationType.manual ? UserData.shared?.user.name : viewModelReservation?.user?.name) : viewModelReservation?.establishment?.name
        //scheduleLabel.text = viewModel?.schedule
        //priceUnitLabel.text = viewModel?.priceHour
        //totalPriceLabel.text = viewModel?.totalPrice
        ratingCosmos.isUserInteractionEnabled = false
        if !isOwner!{
            //rateNumber.text = "(\(viewModelReservation?.establishment?.rate ?? 0))"
            ratingCosmos.rating = Double(viewModelReservation?.establishment?.rate ?? Int(0.0))
        } else {
            //rateNumber.text = "(\(viewModelReservation?.user?.rate ?? 0))"
            ratingCosmos.rating = Double(viewModelReservation?.user?.rate ?? Int(0.0))
        }
        
        ratingCosmos.settings.fillMode = .full
        //ratePlayer.settings.fillMode = .full
        
        prepareViewsByStatus(status: viewModelReservation!.status)
        
        table.register(UINib(nibName: CellResusableIdentifier.headerImage.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.headerImage.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.basicCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.basicCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.summaryCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.summaryCell.rawValue)
    }
    
    func prepareViewsByStatus(status: Int) {
        if viewModelReservation?.status == 1 {
            //heightGreenView.constant = greenViewConstant
            //heightYellowView.constant = 0
            heightRateView.constant = rateViewConstant
            //ratePlayerView.alpha = 0
            if !isOwner! {
                self.confirmButtons.subviews[0].isHidden = true
            }
            confirmButtons.alpha = 1
            //ratePlayer.alpha = 0
            rateButton.alpha = 0
        }else if viewModelReservation?.status == 2 || viewModelReservation?.status == 4 {
            //heightGreenView.constant = greenViewConstant
            //heightYellowView.constant = 0
            heightRateView.constant = 0
            //ratePlayerView.alpha = 0
            confirmButtons.alpha = 0
            //ratePlayer.alpha = 0
            rateButton.alpha = 0
        }else if viewModelReservation?.status == 4 {
            //heightGreenView.constant = 0
            //heightYellowView.constant = yellowViewConstant
            heightRateView.constant = rateViewConstant
            confirmButtons.alpha = 0
            //ratePlayer.alpha = 1
            //ratePlayerView.alpha = 1
            rateButton.alpha = 1
            
        }else if viewModelReservation?.status == 5 {
            //heightGreenView.constant = 0
            //heightYellowView.constant = yellowViewConstant
            confirmButtons.alpha = 0
            
            if viewModel!.isRatedByUser {
                //ratePlayer.alpha = 0
                //ratePlayerView.alpha = 0
                rateButton.alpha = 0
                heightRateView.constant = 0
            } else {
                //ratePlayer.alpha = 1
                //ratePlayerView.alpha = 1
                rateButton.alpha = 1
                heightRateView.constant = rateViewConstant
//                ratePlayer.didFinishTouchingCosmos = { rating in
//                    self.newRate = Int(rating)
//                    self.requestUpdateRate(rate: Int(rating))
//                }
            }
        }else if viewModelReservation?.status == 6 || viewModelReservation?.status == 7 || viewModelReservation?.status == 3{
            
            heightRateView.constant = 0
            //heightYellowView.constant = 0
            confirmButtons.alpha = 0
            //ratePlayer.alpha = 0
            //ratePlayerView.alpha = 0
            rateButton.alpha = 0
        }
        
        // just for reference
//        switch status {
//        case 1:
//            return StatusField.newReservation.rawValue
//        case 2:
//            return StatusField.approved.rawValue
//        case 3:
//            return StatusField.rejected.rawValue
//        case 4:
//            return StatusField.happening.rawValue
//        case 5:
//            return StatusField.finished.rawValue
//        case 6:
//            return StatusField.cancel.rawValue
        //case 7: noResponse
//        default:
//
//        }
    }
    
//    @IBAction func sendNewRate(_ sender: Any) {
//        requestUpdateRate(rate: self.newRate!)
//    }
    
    
    @IBAction func confirmReservationButton(_ sender: Any) {
        updateStatusReservation(establishmentID: UserData.shared?.user.establishments?[0].id ?? 0, reservationID: viewModelReservation?.id ?? 0, type: "approve")
    }
    
    @IBAction func rejectReservationButton(_ sender: Any) {
        
        if isOwner! {
            updateStatusReservation(establishmentID: UserData.shared?.user.establishments?[0].id ?? 0, reservationID: viewModelReservation?.id ?? 0, type: "reject")
        } else {
            cancelReservation(reservationID: viewModelReservation?.id ?? 0)
        }
    }
    
    func updateStatusReservation(establishmentID: Int, reservationID: Int, type: String){
        viewModelDefaultCanchasOwner = DefaultReservationsOwnerViewModel(ownerService: container.ownerEstablishmentService())
        self.playAnimation(animationName: "loading-football")
        viewModelDefaultCanchasOwner?.requestingUpdateStatusReservation(establishmentID: establishmentID, reservationID: reservationID, type: type, completion: { [weak self] (response) in
            guard let self = self else { return }
            
            if response{
                //print("succeded")
                self.stopAnimation()
                if type == "approve"{
                    let alertError = HelperExtension.buildBasicAlertController(title: "Cancha reservada", message: "Haz aceptado la reservación de la cancha", handler: {
                        
                        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                            return
                        }
                        if self.isOwner! {
                            appDelegate.presentHomeOwner()
                        } else {
                            appDelegate.presentHomeVC()
                        }
                        
                    })
                    self.present(alertError, animated: true, completion: nil)
                }else {
                    let alertError = HelperExtension.buildBasicAlertController(title: "Cancha rechazada", message: "La reservación de la cancha ha sido rechazada y se notificará al usuario", handler: {
                        
                        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                            return
                        }
                        if self.isOwner! {
                            appDelegate.presentHomeOwner()
                        } else {
                            appDelegate.presentHomeVC()
                        }
                        
                    })
                    self.present(alertError, animated: true, completion: nil)
                }
            }else{
                self.stopAnimation()
                print("failed")
            }
            }, {( message) in
                self.stopAnimation()
                let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
    }
    
    func cancelReservation(reservationID: Int) {
        viewModelDefaultCanchasOwner = DefaultReservationsOwnerViewModel(ownerService: container.ownerEstablishmentService())
        self.playAnimation(animationName: "loading-football")
        viewModelDefaultCanchasOwner?.requestCancelReservation(reservationID: reservationID, completion: { [weak self] (response) in
            guard let self = self else { return }
            if response{
                self.stopAnimation()
                let alertError =
                    HelperExtension.buildBasicAlertController(title: "Cancha cancelada", message: "La reservación de la cancha ha sido cancelada", handler: {
                        
                        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                            return
                        }
                        if self.isOwner! {
                            appDelegate.presentHomeOwner()
                        } else {
                            appDelegate.presentHomeVC()
                        }
                        
                    })
                self.present(alertError, animated: true, completion: nil)
            } else {
                self.stopAnimation()
                print("failed")
            }
            }, {( message) in
                self.stopAnimation()
                let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
        
    }
    
    @IBAction func tapGoToRateButton(_ sender: Any) {
        
        guard let rateVC = UIStoryboard(name: "Reservaciones", bundle: nil).instantiateViewController(withIdentifier: "RateViewController") as? RateViewController else { return }
        rateVC.viewModelReservation = viewModel?.modelReservation
        
        self.present(rateVC, animated: true, completion: nil)
        
    }
    
    func dissmisView() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension DetailReservacionesViewController: UITableViewDataSource {
    
    func returnEmptyCell(tableView: UITableView, indexPath: IndexPath, text: String) -> EmptyTableViewCell {
        let emptyCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.emptyCell.rawValue, for: indexPath) as! EmptyTableViewCell
        tableView.rowHeight = 390
        emptyCell.messageLabel.text = text
        emptyCell.messageLabel.textColor = Constants.blackTwoPrimaryColor
        return emptyCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let basicCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.basicCell.rawValue, for: indexPath) as? BasicCellTableViewCell else {
            return UITableViewCell()
        }
        
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.headerImage.rawValue, for: indexPath) as? HeaderImageTableViewCell else {
                return UITableViewCell()
            }
            
            tableView.rowHeight = 180
            cell.isUserInteractionEnabled = false
            cell.favoriteButton.setImage(UIImage(named: ""), for: .normal)
            guard let model = viewModel else { return basicCell }
            cell.headerImage.sd_setImage(with: URL(string: model.imagePlace), placeholderImage: UIImage(named: "default-image1"))
            
            return cell
        case 1:
            
            tableView.rowHeight = isOwner! ? 0 : 51
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = viewModelReservation?.establishment?.address
            basicCell.iconImage.image = UIImage(named: ImageName.type.iconPin.rawValue)
            basicCell.isUserInteractionEnabled = false
            basicCell.arrowImage = nil
            return basicCell
            
        case 2:
            tableView.rowHeight = 51
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = viewModel?.dayReservation
            basicCell.iconImage.image = UIImage(named: ImageName.type.iconCalendar.rawValue)
            basicCell.isUserInteractionEnabled = false
            basicCell.arrowImage = nil
            return basicCell
        case 3:
            tableView.rowHeight = 51
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = viewModel?.schedule
            basicCell.iconImage.image = UIImage(named: ImageName.type.iconClock.rawValue)
            basicCell.isUserInteractionEnabled = false
            basicCell.arrowImage = nil
            return basicCell
        case 4:
            tableView.rowHeight = 51
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = "\(HelperExtension.GrassType(type: (viewModelReservation?.field.grass_type ?? 0) - 1))"
            basicCell.iconImage.image = UIImage(named: ImageName.type.iconField.rawValue)
            basicCell.isUserInteractionEnabled = false
            basicCell.arrowImage = nil
            return basicCell
        case 5:
            tableView.rowHeight = 51
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = "jugadores"
            basicCell.iconImage.image = UIImage(named: ImageName.type.iconGroup.rawValue)
            basicCell.isUserInteractionEnabled = false
            basicCell.arrowImage = nil
            return basicCell
        case 6:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.summaryCell.rawValue, for: indexPath) as? SummaryTableViewCell else {
                return UITableViewCell()
            }
            
            tableView.rowHeight = 165
            cell.backgroundColor = .clear
            cell.isUserInteractionEnabled = false
            basicCell.titleLabel.text = "Resumen"
            
            cell.perHourLabel.text = "\(viewModel?.priceHour ?? "") MX"
            cell.numberOfHoursLabel.text = "\(viewModel?.lengthTime ??  "")"
            cell.totalPriceLabel.text = "\(viewModel?.totalPrice ?? "") MX"
            
            return cell
        default:
            
            basicCell.backgroundColor = .clear
            basicCell.titleLabel.text = "Resumen"
            return basicCell
        }
    }
}
