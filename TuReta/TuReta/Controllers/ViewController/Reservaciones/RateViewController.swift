//
//  RateViewController.swift
//  TuReta
//
//  Created by DC on 12/07/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import Cosmos

class RateViewController: TuRetaBaseViewController {

    @IBOutlet weak var ratePlayer: CosmosView!
    
    var newRate: Int?
    var viewModelReservation: Reservation?
    var viewModelDefaultCanchasOwner: DefaultReservationsOwnerViewModel?
    let container = TuRetaContainer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        ratePlayer.didFinishTouchingCosmos = { rating in
            self.newRate = Int(rating)
        }
        
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
        } else {
            isOwner = false
        }
    }
    

    func requestUpdateRate(rate: Int) {
        viewModelDefaultCanchasOwner = DefaultReservationsOwnerViewModel(ownerService: container.ownerEstablishmentService())
        
        viewModelDefaultCanchasOwner?.requestUpdateRate(reservationID: viewModelReservation?.id ?? 0, rate: rate, completion: { [weak self] (response) in
            guard let self = self else { return }
            if response{
                print("new rate")
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                if self.isOwner! {
                    appDelegate.presentHomeOwner()
                } else {
                    appDelegate.presentHomeVC()
                }
            }else{
                print("failed")
            }
            }, {( message) in
                let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
    }
    
    @IBAction func tapSendRateButton(_ sender: Any) {
        self.requestUpdateRate(rate: newRate ?? 0)
    }

}
