//
//  ReservacionesViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 11/30/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import DatePickerDialog

class ReservacionesViewController: TuRetaBaseViewController {
        
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.lightGray

        return refreshControl
    }()
    
    //MARK: - Local variables
    var datePicked: String = ""
    var isGuest: Bool = true
    var reservation: Reservation?
    var viewModelDefaultCanchasOwner: DefaultReservationsOwnerViewModel?  {
           didSet{
            //filteredData = viewModelDefaultCanchasOwner?.reservationsViewModel
               self.reservaTableview.reloadData()
           }
       }
    var filteredData: [Reservation]?
    let container = TuRetaContainer()
    let reservaTableview = UITableView()
    let manualReservationButton = UIButton()

    
    override func loadView() {
      super.loadView()
        //searchBar.delegate = self
        //self.reservaTableview.refreshControl = self.refreshControl
        //reservaTableview.tableFooterView = UIView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        self.reservaTableview.delegate = self
        self.reservaTableview.dataSource = self
        
        viewModelDefaultCanchasOwner = DefaultReservationsOwnerViewModel(ownerService: container.ownerEstablishmentService())
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupTableView() {
        view.addSubview(reservaTableview)
        
        reservaTableview.translatesAutoresizingMaskIntoConstraints = false
        reservaTableview.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        reservaTableview.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        
        reservaTableview.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        reservaTableview.backgroundColor = .clear
        reservaTableview.separatorStyle = .none
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
        }
        
        if isOwner!{
            view.addSubview(manualReservationButton)
            manualReservationButton.setTitle("Reserva Manual", for: .normal)
            manualReservationButton.backgroundColor = Constants.blueOnePrimaryColor
            manualReservationButton.translatesAutoresizingMaskIntoConstraints = false
            manualReservationButton.layer.cornerRadius = 5
            manualReservationButton.topAnchor.constraint(equalTo: reservaTableview.bottomAnchor, constant: 10).isActive = true
            manualReservationButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 28).isActive = true
            manualReservationButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -28).isActive = true
            manualReservationButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -17).isActive = true
            manualReservationButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            manualReservationButton.addTarget(self, action: #selector(self.pushToCreateManualReservation), for: .touchUpInside)
            
            reservaTableview.bottomAnchor.constraint(equalTo: manualReservationButton.topAnchor, constant: -10).isActive = true
        } else {
            reservaTableview.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        
        reservaTableview.register(UINib(nibName: "ReservacionOwnerTableViewCell", bundle: nil), forCellReuseIdentifier: "ReservacionOwnerTableViewCell")
        reservaTableview.register(UINib(nibName: CellResusableIdentifier.emptyCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.emptyCell.rawValue)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if defaults.object(forKey: "isGuest") as? Bool == true{
            isGuest = true
            self.goToLogin()
        } else {
            
            if defaults.object(forKey: "isOwner") != nil{
                isOwner = defaults.object(forKey: "isOwner") as? Bool
            }
            
            if(!isOwner!){
                requestData(establishmentId: UserData.shared?.user.id ?? 0, isOwner: isOwner ?? false)
            }else{
                requestData(establishmentId: UserData.shared?.user.establishments?[0].id ?? 0, isOwner: isOwner ?? true)
            }
        }
        
        
        guard let tabBarItem = self.navigationController?.tabBarController?.tabBar.items?[1] else { return }
        tabBarItem.badgeValue = nil
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        
    }
    
    @objc func pushToCreateManualReservation() {
        
        datePickerTapped()
    }
    
    func datePickerTapped() {
        DatePickerDialog().show("Elige una fecha", doneButtonTitle: "Aceptar", cancelButtonTitle: "Cancelar", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                self.datePicked = formatter.string(from: dt)
                
                self.pushToPickSchedule()
            
            }
        }
    }
    
    func pushToPickSchedule() {
        
        guard let pickSchedule = UIStoryboard(name: "Reservaciones", bundle: nil).instantiateViewController(withIdentifier: "ManualReservationViewController") as? ManualReservationViewController else { return }
        
        //pickSchedule.fieldSelected = fieldSelected
        pickSchedule.datePicked = datePicked
        pickSchedule.establishmentViewModel = nil
        pickSchedule.establishmentDetail = nil
        
        self.navigationController?.pushViewController(pickSchedule, animated: true)
    }
    
    func goToLogin() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        self.dismiss(animated:false, completion: nil)
        appDelegate.presentInit()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
       if !reservaTableview.isDragging {
        refreshData()
       }
    }
    
    func refreshData() {
        if(!isOwner!){
            requestData(establishmentId: UserData.shared?.user.id ?? 0, isOwner: isOwner ?? false)
        }else{
            requestData(establishmentId: UserData.shared?.user.establishments?[0].id ?? 0, isOwner: isOwner ?? true)
        }
    }
    
    func requestData(establishmentId: Int, isOwner: Bool){
        self.playAnimation(animationName: "loading-football")
        
        
        if isOwner {
            viewModelDefaultCanchasOwner?.requestingReservationsOwner(establishmentId: establishmentId, type: "Owner", completion: { [weak self] in
                guard let self = self else { return }
                self.filteredData = self.viewModelDefaultCanchasOwner?.reservationsViewModel
                //self.viewModelDefaultCanchasOwner = self.viewModelDefaultCanchasOwner
                self.reservaTableview.reloadData()
                self.stopAnimation()
                }, {( message) in
                    self.stopAnimation()
                    let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                    self.present(alertError, animated: true, completion: nil)
            })
        }else{
            viewModelDefaultCanchasOwner?.requestingReservationsOwner(establishmentId: establishmentId, type: "User", completion: { [weak self] in
                guard let self = self else { return }
                self.filteredData = self.viewModelDefaultCanchasOwner?.reservationsViewModel
                //self.viewModelDefaultCanchasOwner = self.viewModelDefaultCanchasOwner
                self.reservaTableview.reloadData()
                self.stopAnimation()
                }, {( message) in
                    self.stopAnimation()
                    let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                    self.present(alertError, animated: true, completion: nil)
            })
        }
        self.refreshControl.endRefreshing()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "reservaSelected"{
            
            let vc = segue.destination as! DetailReservacionesViewController
            vc.viewModelReservation = reservation
            vc.isOwner = isOwner!
        }
    }
    
    func getItemIndex(index: Int, items: [Reservation]?) -> Reservation {
        return items![index]
    }

}

extension ReservacionesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let withContent = viewModelDefaultCanchasOwner?.reservationsViewModel?.count ?? 0
        return withContent > 1 ? withContent : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if viewModelDefaultCanchasOwner?.reservationsViewModel?.count != 0 && viewModelDefaultCanchasOwner?.reservationsViewModel?.count != nil {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReservacionOwnerTableViewCell", for: indexPath) as! ReservacionOwnerTableViewCell
            
            //cell.isUserInteractionEnabled = isOwner == true ? true : false
            cell.isOwner = isOwner!
            tableView.rowHeight = 190
            cell.viewModel = getItemIndex(index: indexPath.row, items: filteredData) //viewModelDefaultCanchasOwner?.getItemByIndex(index: indexPath.row)
            return cell
        } else {
            let emptyCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.emptyCell.rawValue, for: indexPath) as! EmptyTableViewCell
            tableView.rowHeight = 410
            emptyCell.messageLabel.text = "Todavía no tienes Reservaciones"
            return emptyCell
        }
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 190
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailReservationVC = UIStoryboard(name: "Reservaciones", bundle: nil).instantiateViewController(withIdentifier: "DetailReservacionesViewController") as? DetailReservacionesViewController else { return }
        reservation = filteredData?[indexPath.row] //getItemIndex(index: indexPath.row, items: filteredData)
        //performSegue(withIdentifier: "reservaSelected", sender: nil);
        detailReservationVC.viewModelReservation = reservation
        detailReservationVC.isOwner = isOwner!
        
        self.present(detailReservationVC, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        closeKeyboard()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
      if refreshControl.isRefreshing == true {
       refreshData()
      }
    }
    
}

//extension ReservacionesViewController: UISearchBarDelegate{
//    // This method updates filteredData based on the text in the Search Box
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        // When there is no text, filteredData is the same as the original data
//        // When user has entered text into the search box
//        // Use the filter method to iterate over all items in the data array
//        // For each item, return true if the item should be included and false if the
//        // item should NOT be included
//        filteredData = searchText.isEmpty ? viewModelDefaultCanchasOwner?.reservationsViewModel : viewModelDefaultCanchasOwner?.reservationsViewModel?.filter({(reservation: Reservation) -> Bool in
//            // If dataItem matches the searchText, return true to include it
//
//            return reservation.code.range(of: searchText, options: .caseInsensitive) != nil
//            //return reservationCode. .range(of: searchText, options: .caseInsensitive) != nil
//            //return dataString.range(of: searchText, options: .caseInsensitive) != nil
//        })
//
//        reservaTableview.reloadData()
//    }
//}
