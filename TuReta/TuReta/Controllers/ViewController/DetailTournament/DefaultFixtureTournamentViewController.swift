//
//  DefaultFixtureTournamentViewController.swift
//  TuReta
//
//  Created by DC on 18/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import Parchment

// Calendario Player Default Screen
final class DefaultFixtureTournamentViewController: UIViewController {

    var tournamentSelected: TournamentModel?
    var establishmentID: Int?
    
    var rounds: [RoundModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createRounds()
        guard let customMediumFont = UIFont(name: "Poppins-Medium", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        guard let customRegularFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let pagingViewController = PagingViewController()
        pagingViewController.dataSource = self
        pagingViewController.menuItemSize = .sizeToFit(minWidth: 50, height: 50)
        pagingViewController.textColor = Constants.blackTwoPrimaryColor
        pagingViewController.selectedTextColor = Constants.blackOnePrimaryColor
        pagingViewController.font = customRegularFont
        pagingViewController.selectedFont = customMediumFont
        pagingViewController.indicatorColor = Constants.greenOnePrimaryColor
        
        // Add the paging view controller as a child view controller and
        // contrain it to all edges.
        addChild(pagingViewController)
        view.addSubview(pagingViewController.view)
        view.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        
    }
    
    func createRounds(){
        guard let tournament = tournamentSelected?.number_of_rounds else { return }
        for item in 1...tournament {
            rounds.append(RoundModel(id: item, roundNumber: item))
        }
    }

}

extension DefaultFixtureTournamentViewController: PagingViewControllerDataSource {
    
    func pagingViewController(_: PagingViewController, pagingItemAt index: Int) -> PagingItem {
        return PagingIndexItem(index: index, title: "J\(rounds[index].roundNumber ?? 0)")
    }
    
    func pagingViewController(_: PagingViewController, viewControllerAt index: Int) -> UIViewController {
        return FixturesTournamentViewController(title: "J\(rounds[index].roundNumber ?? 0)", roundInt: rounds[index].roundNumber ?? 0, tournamentModel: tournamentSelected!, establishmentID: establishmentID ?? 0)
    }
    
    func numberOfViewControllers(in pagingViewController: PagingViewController) -> Int {
        return rounds.count
    }
    
}
