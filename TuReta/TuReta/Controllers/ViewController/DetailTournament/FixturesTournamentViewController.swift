//
//  FixturesTournamentViewController.swift
//  TuReta
//
//  Created by DC on 18/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

// Player Calendario Screen
class FixturesTournamentViewController: UIViewController {
    
    var tournamentSelected: TournamentModel?
    var roundMatch: Int = 0
    var establishmentID: Int?
    
    let table = UITableView()
    var viewModel: TournamentViewModel?
    let container = TuRetaContainer()
    
    init(title: String, roundInt: Int, tournamentModel: TournamentModel, establishmentID: Int) {
      super.init(nibName: nil, bundle: nil)
      self.title = title
      
      let label = UILabel(frame: .zero)
      label.font = UIFont.systemFont(ofSize: 50, weight: UIFont.Weight.thin)
      label.textColor = UIColor(red: 95/255, green: 102/255, blue: 108/255, alpha: 1)
      label.textAlignment = .center
      //label.text = content
      roundMatch = roundInt
      tournamentSelected = tournamentModel
      self.establishmentID = establishmentID
      label.sizeToFit()
      
      view.addSubview(label)
      view.backgroundColor = Constants.blackOnePrimaryColor
    }
    
    required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.table.delegate = self
        self.table.dataSource = self
        viewModel = TournamentViewModel(tournamentService: container.tournamentService())
        // TODO fixtures by round request
        setupTableView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        requestFixtures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let index = self.table.indexPathForSelectedRow{
            self.table.deselectRow(at: index, animated: true)
        }
    }
    
    func requestFixtures(){
        // call viewmodel request fixtures
        
        guard let tournamentModel = self.tournamentSelected else { return }
        viewModel?.getFixturesByRound(model: tournamentModel, roundSelected: roundMatch, establishmentID: establishmentID ?? 0, completionHandler: { [weak self] in
            guard let self = self else { return }
            print("result")
            self.table.reloadData()
        }, { (error, message) in
            print("error")
        })
                                      
    }
    
    func setupTableView() {
        view.addSubview(table)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        table.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        table.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        table.backgroundColor = .clear
        table.separatorStyle = .none
        
        table.register(UINib(nibName: CellResusableIdentifier.MatchTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.MatchTableViewCell.rawValue)
        table.register(UINib(nibName: CellResusableIdentifier.dateFixtureCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.dateFixtureCell.rawValue)
    }
    
}

extension FixturesTournamentViewController: UITableViewDelegate {}

extension FixturesTournamentViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let model = self.viewModel?.fixtures else { return 0 }
        return model.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.dateFixtureCell.rawValue, for: indexPath) as? DateFixtureTableViewCell else {
                return UITableViewCell()
            }
            cell.bottomView.backgroundColor = Constants.blackOnePrimaryColor
            tableView.rowHeight = 51
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.MatchTableViewCell.rawValue, for: indexPath) as? MatchTableViewCell else {
                return UITableViewCell()
            }
            
            guard let model = self.viewModel?.fixtures else { return cell }
            cell.configCell(fixtureModel: model[indexPath.row - 1])
            tableView.rowHeight = 53
            return cell
        }
    }
}
