//
//  DefaultDetailTournamentViewController.swift
//  TuReta
//
//  Created by DC on 15/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class DefaultDetailTournamentViewController: TuRetaBaseViewController {
    
    var isGuest: Bool = true
    var pager: ViewPager?
    let options = ViewPagerOptions()
    let tabs = [
        ViewPagerTab(title: "Calendario", image: nil),
        ViewPagerTab(title: "Estadisticas", image: nil)
    ]
    
    var establishmentID: Int?
    var tournamentSelected: TournamentModel?
    var viewModel: DefaultUserViewModel?
    let container = TuRetaContainer()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = tournamentSelected?.name
        // Do any additional setup after loading the view.
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
        } else {
            isGuest = true
            self.goToLogin()
        }
        
        options.tabType = .basic
        options.distribution = .segmented
        options.isTabIndicatorAvailable = true
        options.tabViewBackgroundDefaultColor = Constants.greenOnePrimaryColor
        options.tabViewBackgroundHighlightColor = Constants.greenOnePrimaryColor
        options.tabIndicatorViewBackgroundColor = Constants.blackTwoPrimaryColor
        options.tabViewTextHighlightColor = .white
        options.tabViewTextDefaultColor = .white
        
        pager = ViewPager(viewController: self, containerView: self.view)
        pager?.setOptions(options: options)
        pager?.setDataSource(dataSource: self)
        pager?.setDelegate(delegate: self)
        pager?.build()
    }

    func goToLogin() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        self.dismiss(animated:false, completion: nil)
        appDelegate.presentInit()
    }

}

extension DefaultDetailTournamentViewController: ViewPagerDataSource {
    func viewControllerAtPosition(position: Int) -> UIViewController {
        switch position {
        case 0:
            let vc = DefaultFixtureTournamentViewController()
            vc.tournamentSelected = tournamentSelected
            vc.establishmentID = establishmentID
            return vc
        case 1:
            let vc = TableTeamsViewController()
            vc.tournamentSelected = tournamentSelected
            vc.establishemtnID = establishmentID
            return vc
        default:
            return UIViewController()
        }
        
    }
    
    
    func numberOfPages() -> Int {
        return tabs.count
    }
    
    
    func tabsForPages() -> [ViewPagerTab] {
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
}

extension DefaultDetailTournamentViewController: ViewPagerDelegate {
    
    func willMoveToControllerAtIndex(index:Int) {
        print("Moving to page \(index)")
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        print("Moved to page \(index)")
    }
}


