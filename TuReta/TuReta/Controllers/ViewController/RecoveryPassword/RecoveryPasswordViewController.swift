//
//  RecoveryPasswordViewController.swift
//  TuReta
//
//  Created by DC on 30/01/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class RecoveryPasswordViewController: UIViewController {
    
    @IBOutlet weak var recoveryEmailText: UITextField!
    
    let container = TuRetaContainer()
    var viewModelDefaultRecoveryPassword: DefaultRecoveyPasswordViewModel?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        guard let emailImage = UIImage(named: "icn_email") else { return }
        recoveryEmailText.setLeftIcon(emailImage)
        recoveryEmailText.changePlaceholderColor(text: "Correo electrónico", andColor: .gray)
        
    }
    
    func recoveyPassword(email: String) {
        viewModelDefaultRecoveryPassword = DefaultRecoveyPasswordViewModel(ownerService: container.ownerEstablishmentService())
        
        viewModelDefaultRecoveryPassword?.requestRecoveryPassword(email: email, completion: { [weak self] (response) in
            guard let strongSelf = self else { return }
            if response{
                let alertError = HelperExtension.buildBasicAlertController(title: "Correo enviado", message: "Hemos enviado un correo con los pasos para restablecer la contraseña", handler: nil)
                strongSelf.present(alertError, animated: true, completion: nil)
            }else{
                
            }
            }, {( message) in
                let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
    }
    
    @IBAction func RecoveryPasswordButtonPressed(_ sender: Any) {
        if validateEmail() {
            recoveyPassword(email: recoveryEmailText.text!)
        }
    }

}

extension RecoveryPasswordViewController: UITextFieldDelegate {
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() //Dismiss keyboard
        if validateEmail() {
            recoveyPassword(email: recoveryEmailText.text!)
        }
        return true
    }
    
}

extension RecoveryPasswordViewController {
    func validateEmail() -> Bool{
        // email
        guard let email = recoveryEmailText.text else {
            return false
        }
        if !HelperExtension.isValidEmail(emailID: email){
            // message error
            let alertError = HelperExtension.buildBasicAlertController(title: "Correo invalido", message: "Ingresa un correo valido", handler: nil)
            self.present(alertError, animated: true, completion: nil)
            return false
        } else {
            return true
        }
        
    }
}
