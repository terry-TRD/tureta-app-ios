//
//  EditProfileViewController.swift
//  TuReta
//
//  Created by DC on 9/28/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookCore

import Firebase

import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging

class EditProfileViewController: TuRetaBaseViewController {
    
    @IBOutlet weak var imageProfileView: CustomCornerUIView!
    @IBOutlet weak var shortName: UILabel!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var emailtextField: UITextField!
    @IBOutlet weak var imageAvatar: UIImageView!
    
    var userProfile: ProfileViewModel?
    var nameLetters: String?
    let container = TuRetaContainer()
    var viewModel: DefaultUserViewModel?
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Mis Datos"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageProfileView.layer.cornerRadius = imageProfileView.frame.width / 2
        
        fillProfile()
        setupTextFields()
    }
    
    func setupTextFields(){
        guard let emailImage = UIImage(named: ImageName.type.iconEmail.rawValue) else { return }
        guard let passwordImage = UIImage(named: ImageName.type.iconPassword.rawValue) else { return }
        
        fullNameTextField.setLeftIcon(emailImage)
        emailtextField.setLeftIcon(passwordImage)
        
        fullNameTextField.changePlaceholderColor(text: "Nombre", andColor: .gray)
        emailtextField.changePlaceholderColor(text: "Correo electrónico", andColor: .gray)
    }
    
    func fillProfile(){
        var nameLetters: String!
        guard let fullNameProfile: String  = userProfile?.name else{
            return
        }
        if fullNameProfile.count > 0{
            let arrayName = fullNameProfile.wordList
            
            nameLetters = String(arrayName[0].prefix(1))
            
            if(arrayName.count > 1){
                let name = String(arrayName[1].prefix(1))
                nameLetters.append("\(name.uppercased())")
            }
            
            self.fullNameTextField.text = userProfile?.name.uppercased()
            self.shortName.text = nameLetters.uppercased()
        }
        self.emailtextField.text = self.userProfile?.email
        setColorTshirt()
        
    }
    
    func setColorTshirt() {
        var colorHexa: String
        if let color = userProfile?.color {
            if color.isEmpty {
                colorHexa = "\(AvatarColorsHex.greenOneColor)"
            } else {
                colorHexa = String(color.dropFirst())
            }
        } else {
            colorHexa = "\(AvatarColorsHex.greenOneColor)"
        }
        let scanner = Scanner(string: colorHexa)
        var value: UInt64 = 0
        if scanner.scanHexInt64(&value) {
            imageAvatar.tintColor = UIColor(rgb: Int(value))
        }
    }
    
    func validateEmail() ->Bool{
        // email
        guard let email = emailtextField.text else{
            return false
        }
        if !HelperExtension.isValidEmail(emailID: email){
            // message error
            let alertError = HelperExtension.buildBasicAlertController(title: "Correo invalido", message: "Ingresa un correo valido", handler: nil)
            self.present(alertError, animated: true, completion: nil)
            return false
        }
        return true
        
    }
    
    func validateTwoWords() -> Bool{
        let fullName = self.fullNameTextField.text
        
        return (fullName?.wordList.count)! > 1 ? true: messageErrorAlertFullName()
    }
    
    func messageErrorAlertFullName() -> Bool{
        let alertError = HelperExtension.buildBasicAlertController(title: "Nombre Invalido", message: "Ingresa nombre completo", handler: nil)
        self.present(alertError, animated: true, completion: nil)
        return false
    }
    
    func messageAlertFullNameUpdated(){
        let alertError = HelperExtension.buildBasicAlertController(title: "Nombre Actualizado!", message: "", handler: nil)
        self.present(alertError, animated: true, completion: nil)
    }
    
    @IBAction func updateInfoButton(_ sender: Any) {
        
        if(validateEmail() && validateTwoWords()){
            updateNameProfile(name: fullNameTextField.text!)
        }
    }
    
     //use dependency injection and mvvm
    func updateNameProfile(name: String){
        // validate if is valid string
        ProfileWS.editProfileWS(name: name, completionHandler: {[weak self] (user) in
            guard let self = self else { return }
            //self.userProfile = user
            let arrayName = user?.name.wordList //self.userProfile?.name.wordList

            self.nameLetters = String(arrayName?[0].prefix(1) ?? "")

            if(arrayName?.count ?? 0 > 1){
                let name = String(arrayName?[1].prefix(1) ?? "")
                self.nameLetters!.append("\(name)")
            }

            self.shortName.text = self.nameLetters?.uppercased()
            self.messageAlertFullNameUpdated()
        }, { (networkError, message) in
            print(networkError!, message)
        })
    }
    @IBAction func changaAvatarAction(_ sender: Any) {
        guard let changeAvatarVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ChangeAvatarViewController") as? ChangeAvatarViewController else { return }
        changeAvatarVC.shortName = self.shortName.text
        var colorHexa: String
        
        if let color = userProfile?.color {
            if color.isEmpty {
                colorHexa = "\(AvatarColorsHex.greenOneColor)"
            } else {
                colorHexa = String(color.dropFirst())
            }
        } else {
            colorHexa = "\(AvatarColorsHex.greenOneColor)"
        }
        
        let scanner = Scanner(string: colorHexa)
        var value: UInt64 = 0
        if scanner.scanHexInt64(&value) {
            changeAvatarVC.colorSelected = Int(value)
        }
        self.navigationController?.pushViewController(changeAvatarVC, animated: true)
    }
    
}

extension EditProfileViewController: UITextFieldDelegate{
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() //Dismiss keyboard
        // validate and save
        setCloseKeyboard()
        updateNameProfile(name: textField.text!)
        return true
    }
}
