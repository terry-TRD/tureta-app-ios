//
//  AvatarCollectionViewCell.swift
//  TuReta
//
//  Created by DC on 24/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class AvatarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var shortName: UILabel!
    @IBOutlet weak var globalView: CustomCornerAndBorderUIView!
    
    
}
