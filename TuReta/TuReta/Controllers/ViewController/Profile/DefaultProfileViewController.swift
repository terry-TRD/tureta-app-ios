//
//  DefaultProfileViewController.swift
//  TuReta
//
//  Created by DC on 21/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookCore

import Firebase

import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging

class DefaultProfileViewController: TuRetaBaseViewController {

    @IBOutlet weak var shortNameLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var imageTshirt: UIImageView!
    
    
    // Local variables
    var isGuest: Bool = true
    let container = TuRetaContainer()
    
    
    // ViewModel
    var viewModelDefaultCancha: DefaultRentarCanchaViewModel?
    var viewModel: DefaultUserViewModel?
    var profileViewModel: ProfileViewModel?{
        didSet{
            fillProfile()
        }
    }
    
    var viewModelEstablishmentDetail: EstablishmentDetail? {
        didSet{
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .reloadProfileInfo
               , object: nil)
        
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if defaults.object(forKey: "isGuest") as? Bool == true{
            isGuest = true
            self.goToLogin()
        } else {
            isGuest = false
            viewModel = DefaultUserViewModel(profileService: container.profileService())
            viewModel?.setup { [weak self] in
                guard let self = self else { return }
                self.profileViewModel = ProfileViewModel(userVM: (self.viewModel!))
                self.setColorTshirt()
            }
            
            
            
            if(!isOwner!){
                self.imageField.isHidden = false
                
                
            }else{
                self.imageField.isHidden = true
                requestInfoDetail()
            }
            TranslucentNavBar()
//             self.navigationController?.isNavigationBarHidden = false
//            // Transparent navigation bar
//            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//            self.navigationController?.navigationBar.shadowImage = UIImage()
//            self.navigationController?.navigationBar.isTranslucent = true
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        if !isOwner!{ TranslucentNavBar() }
        
    }
    
    func setColorTshirt() {
        var colorHexa: String = ""
        if var color = profileViewModel?.color {
            if color.isEmpty {
                imageTshirt.tintColor = UIColor(rgb: AvatarColorsHex.greenOneColor)
                return
            } else {
                colorHexa = String(color.dropFirst())
            }
        } else {
            imageTshirt.tintColor = UIColor(rgb: AvatarColorsHex.greenOneColor)
            return
        }
        let scanner = Scanner(string: colorHexa)
        var value: UInt64 = 0
        if scanner.scanHexInt64(&value) {
            imageTshirt.tintColor = UIColor(rgb: Int(value))
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func goToLogin() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        self.dismiss(animated:false, completion: nil)
        appDelegate.presentInit()
    }
    
    func fillProfile(){
        var nameLetters: String!
        
        guard let fullNameProfile: String  = profileViewModel?.name else { return }
        guard let emailProfile: String  = profileViewModel?.email else { return }
    
        if fullNameProfile.count > 0{
            let arrayName = fullNameProfile.wordList
            nameLetters = String(arrayName[0].prefix(1))
            
            if(arrayName.count > 1){
                let name = String(arrayName[1].prefix(1))
                nameLetters.append("\(name.uppercased())")
            }
            
            self.shortNameLabel.text = nameLetters.uppercased()
            self.fullNameLabel.text = fullNameProfile
            self.emailLabel.text = emailProfile
        }
    }
    
    func requestInfoDetail() {
        
        viewModelDefaultCancha = DefaultRentarCanchaViewModel(establishmentService: container.infoEstablishmentService())
        
        viewModelDefaultCancha?.requestEstablismentDetail(idPlace: UserData.shared?.user.establishments?[0].id ?? 0, completion: { [weak self] in
            self!.viewModelEstablishmentDetail = self!.viewModelDefaultCancha?.establishmentDetailViewModel
            
            }, {( message) in
                let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
        
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        self.requestInfoDetail()
    }
    
    func closeSession(){
        viewModel = DefaultUserViewModel(profileService: container.profileCloseSessionService())
        viewModel?.closeSession {[weak self]  response in
            if(response){
                // logout
                Messaging.messaging().unsubscribe(fromTopic: "user\(UserData.shared?.user.id ?? 0)") { error in
                    print("Unsubscribed to \(UserData.shared?.user.id ?? 0) topic")
                }
                
                LoginManager().logOut()
                AccessToken.current = nil
                let defaults = UserDefaults.standard
                defaults.removeObject(forKey: "email")
                defaults.removeObject(forKey: "password")
                defaults.removeObject(forKey: "token")
                defaults.removeObject(forKey: "facebookToken")
                defaults.removeObject(forKey: "appleToken")
                
                defaults.synchronize()
                
                UserData.shared = nil
                
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                
                appDelegate.presentInit()

            }else{
                print("something went wrong")
            }
            
        
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "servicesList"{
            
            let vc = segue.destination as! ServicesViewController
            vc.currentServicesActivated = self.viewModelEstablishmentDetail?.services
        } else if segue.identifier == "editProfile" {
            
            guard let vc = segue.destination as? EditProfileViewController else {
                return
            }
            
            vc.userProfile = profileViewModel
            //            vc.userProfile = self.user
            //            vc.nameLetters = nameLetters
            
        }
    }

}

extension DefaultProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let privacyVC = UIStoryboard(name: "LoginView", bundle: nil).instantiateViewController(withIdentifier: "privacy") as? PrivacyPolicyViewController else { return }
        
        //guard let privacyVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "favourites") as? FavoritesViewController else { return }
        
        if(!isOwner!){
            switch indexPath.row {
            case 0:
                performSegue(withIdentifier: "editProfile", sender: nil)
            case 1:
                performSegue(withIdentifier: "favourites", sender: nil)
            case 2:
                self.navigationController?.pushViewController(privacyVC, animated: true)
            case 3:
                // end session
                // pop up alert
                closeSession()
                break
            case 4:
                // version
                break
            default:
                self.navigationController?.pushViewController(privacyVC, animated: true)
            }
            
        } else {
            // Owner table
            guard let servicesVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ServicesViewController") as? ServicesViewController else { return }
            
            guard let statisitcsVC = UIStoryboard(name: "Statistics", bundle: nil).instantiateViewController(withIdentifier: "StatisticsViewController") as? StatisticsViewController else { return }
            
            
            
            switch indexPath.row {
            case 0:
                performSegue(withIdentifier: "servicesList", sender: nil)
            case 1:
                self.navigationController?.pushViewController(statisitcsVC, animated: true)
            case 2:
                self.navigationController?.pushViewController(privacyVC, animated: true)
            case 3:
                // end session
                // pop up alert
                closeSession()
                break
            case 4:
                // version
                break
            default:
                self.navigationController?.pushViewController(servicesVC, animated: true)
            }
        }
    }
}


extension DefaultProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(!isOwner!){
            return 5
        } else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.optionCell.rawValue, for: indexPath) as? BasicOptionTableViewCell else {
            return UITableViewCell()
        }
        if(!isOwner!){
            
            switch indexPath.row {
            case 0:
                cell.nameLabel.text = "Mis Datos"
            case 1:
                cell.nameLabel.text = "Mis Favoritos"
            case 2:
                cell.nameLabel.text = "Términos y condiciones"
            case 3:
                cell.nameLabel.text = "Cerrar sesión"
                cell.imageIcon.image = UIImage(named: "icon_arrow2")
            case 4:
                cell.nameLabel.text = "Versión: " + UIApplication.getVersionApp + " (" + UIApplication.getBuildApp + ")"
                cell.imageIcon.image = UIImage(named: "")
            default:
                cell.nameLabel.text = ""
            }
            
            return cell
        } else {
            
            switch indexPath.row {
            case 0:
                cell.nameLabel.text = "Sobre mi establecimiento"
            case 1:
                cell.nameLabel.text = "Metricas"
            case 2:
                cell.nameLabel.text = "Términos y condiciones"
            case 3:
                cell.nameLabel.text = "Cerrar sesión"
                cell.imageIcon.image = UIImage(named: "icon_arrow2")
            case 4:
                cell.nameLabel.text = "Versión: " + UIApplication.getVersionApp + " (" + UIApplication.getBuildApp + ")"
                cell.imageIcon.image = UIImage(named: "")
            default:
                cell.nameLabel.text = ""
            }
            
            return cell
        }
    }
    
}
