//
//  FavoritesViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 1/16/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class FavoritesViewController: TuRetaBaseTableViewController {
    
    let container = TuRetaContainer()
    var viewModel: FavoriteViewModel?
    var establishmentModel: [EstablishmentDetail]? {
        didSet{
         //filteredData = viewModelDefaultCanchasOwner?.reservationsViewModel
            self.tableView.reloadData()
        }
    }
    
    override func loadView() {
      super.loadView()
        setupTableView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Mis Favoritos"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.requestData()
    }
    
    func setupTableView() {

        tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: "FavoriteTableViewCell", bundle: nil), forCellReuseIdentifier: "FavoriteTableViewCell")
        tableView.register(UINib(nibName: CellResusableIdentifier.emptyCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.emptyCell.rawValue)
    }
    
    func requestData() {
        viewModel = FavoriteViewModel(establishmentService: container.availableFieldsService())
        
        viewModel?.requestFav(establishmentID: UserData.shared?.user.id ?? 0, type: "list", completion: {
            self.establishmentModel = self.viewModel?.establishmentDetailViewModel
            
            }, { (errorMessage) in
                print(errorMessage)
        })
    }
    
    func returnDetailModel(viewModel: EstablishmentDetail) -> EstablishmentViewModel{
        
        let establishment = Establishment(id: viewModel.id, name: viewModel.name, lat: viewModel.lat, lng: viewModel.lng, description: viewModel.description, address: viewModel.address, profile_picture_url: viewModel.profile_picture_url, phone: viewModel.phone, rate: 0, price: viewModel.fields[0].prices?.last?.price_per_half)
        return EstablishmentViewModel(establishment: establishment)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let withContent = establishmentModel?.count ?? 0
        return withContent > 1 ? withContent : 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if establishmentModel?.count != 0 && establishmentModel?.count != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteTableViewCell", for: indexPath) as! FavoriteTableViewCell
            tableView.rowHeight = 300
            cell.establishmentRate.isUserInteractionEnabled = false
            cell.viewModel = self.establishmentModel?[indexPath.row]
            return cell
        }else{
            let emptyCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.emptyCell.rawValue, for: indexPath) as! EmptyTableViewCell
            tableView.rowHeight = 410
            emptyCell.messageLabel.text = "Todavía no tienes Favoritos"
            return emptyCell
        }
    }
    
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 300
//    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = self.establishmentModel else { return }
        if model.count > 0 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "DefaultRentarCanchasViewController") as! DefaultRentarCanchasViewController
            newViewController.currentMarkerSelected = returnDetailModel(viewModel: model[indexPath.row])
            
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
    }
}

