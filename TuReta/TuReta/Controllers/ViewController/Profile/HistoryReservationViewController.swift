//
//  HistoryReservationViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 1/17/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class HistoryReservationViewController: TuRetaBaseViewController {
    
    var viewModelDefaultCanchasOwner: DefaultReservationsOwnerViewModel?  {
        didSet{
         //filteredData = viewModelDefaultCanchasOwner?.reservationsViewModel
            self.tableView.reloadData()
        }
    }
    var reservation: Reservation?
    
    let container = TuRetaContainer()
    let tableView = UITableView()
    var isGuest: Bool = true
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = Constants.blackOnePrimaryColor
        setupTableView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if defaults.object(forKey: "isGuest") as? Bool == true{
            isGuest = true
            self.goToLogin()
        } else {
            
            if defaults.object(forKey: "isOwner") != nil{
                isOwner = defaults.object(forKey: "isOwner") as? Bool
            }
            
            if(!isOwner!){
                requestData(isOwner: isOwner ?? false)
            }else{
                requestData(isOwner: isOwner ?? true)
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        tableView.register(UINib(nibName: "ReservacionOwnerTableViewCell", bundle: nil), forCellReuseIdentifier: "ReservacionOwnerTableViewCell")
        tableView.register(UINib(nibName: CellResusableIdentifier.emptyCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.emptyCell.rawValue)
    }
    
    func requestData(isOwner: Bool) {
        //self.playAnimation(animationName: "loading-football")
        
        if isOwner {
            viewModelDefaultCanchasOwner = DefaultReservationsOwnerViewModel(ownerService: container.ownerEstablishmentService())
            viewModelDefaultCanchasOwner?.requestingReservationsOwner(establishmentId: UserData.shared?.user.establishments?[0].id ?? 0, type: "OwnerList", completion: { [weak self] in
                self?.viewModelDefaultCanchasOwner = self!.viewModelDefaultCanchasOwner
                self?.stopAnimation()
                }, {( message) in
                    self.stopAnimation()
                    let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                    self.present(alertError, animated: true, completion: nil)
            })
            
        } else {
            viewModelDefaultCanchasOwner = DefaultReservationsOwnerViewModel(ownerService: container.ownerEstablishmentService())
            viewModelDefaultCanchasOwner?.requestingReservationsOwner(establishmentId: UserData.shared?.user.id ?? 0, type: "list", completion: { [weak self] in
                self?.viewModelDefaultCanchasOwner = self!.viewModelDefaultCanchasOwner
                self?.stopAnimation()
                }, {( message) in
                    self.stopAnimation()
                    let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                    self.present(alertError, animated: true, completion: nil)
            })
        }
    }
    
    func goToLogin() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        self.dismiss(animated:false, completion: nil)
        appDelegate.presentInit()
    }
    
}

extension HistoryReservationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let withContent = viewModelDefaultCanchasOwner?.reservationsViewModel?.count ?? 0
        return withContent > 1 ? withContent : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModelDefaultCanchasOwner?.reservationsViewModel?.count != 0 && viewModelDefaultCanchasOwner?.reservationsViewModel?.count != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReservacionOwnerTableViewCell", for: indexPath) as! ReservacionOwnerTableViewCell
            cell.isOwner = isOwner!
            tableView.rowHeight = 190
            cell.viewModel = viewModelDefaultCanchasOwner?.getItemByIndex(index: indexPath.row)
            
            return cell
        } else {
            let emptyCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.emptyCell.rawValue, for: indexPath) as! EmptyTableViewCell
            tableView.rowHeight = 410
            emptyCell.messageLabel.text = "Todavía no tienes Reservaciones"
            return emptyCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailReservationVC = UIStoryboard(name: "Reservaciones", bundle: nil).instantiateViewController(withIdentifier: "DetailReservacionesViewController") as? DetailReservacionesViewController else { return }
        reservation = viewModelDefaultCanchasOwner?.getItemByIndex(index: indexPath.row) //getItemIndex(index: indexPath.row, items: filteredData)
        //performSegue(withIdentifier: "reservaSelected", sender: nil);
        detailReservationVC.viewModelReservation = reservation
        detailReservationVC.isOwner = isOwner!
        
        self.present(detailReservationVC, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 190
//    }
//
}

