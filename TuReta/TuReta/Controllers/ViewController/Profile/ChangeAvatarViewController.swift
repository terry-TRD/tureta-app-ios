//
//  ChangeAvatarViewController.swift
//  TuReta
//
//  Created by DC on 24/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class ChangeAvatarViewController: TuRetaBaseViewController {
    
    @IBOutlet weak var collectionAvatarsView: UICollectionView!
    
    var colorsArray: [Int] = [AvatarColorsHex.greenOneColor, AvatarColorsHex.greenTwoColor, AvatarColorsHex.greenThreeColor, AvatarColorsHex.yellowOneColor, AvatarColorsHex.yellowTwoColor, AvatarColorsHex.yellowThreeColor, AvatarColorsHex.redOneColor, AvatarColorsHex.redTwoColor, AvatarColorsHex.redThreeColor, AvatarColorsHex.blueOneColor, AvatarColorsHex.blueTwoColor, AvatarColorsHex.blueThreeColor]
    
    var shortName: String?
    var indexSelected: IndexPath?
    var colorSelected: Int?
    var color: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //let hexa: String = "0x"
        //let newColor = hexa + self.color!
        //colorSelected = UIColor(rgb: Int(newColor) ?? 0x18B264)
        //colorSelected = Int(color!.capitalized)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Cambiar Icono"
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @IBAction func saveColor(_ sender: Any) {
        self.playAnimation(animationName: "loading-football")
        var colorValue = (String(colorSelected ?? 0, radix:16 , uppercase: false))
       
        ProfileWS.editAvatarProfileWS(color: "#\(colorValue)", completionHandler: {[weak self] (user) in
            guard let self = self else { return }
            self.stopAnimation()
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            appDelegate.presentHomeVC()
            }, { (networkError, message) in
                self.stopAnimation()
                print(networkError!, message)
        })
    }
}

extension ChangeAvatarViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return self.viewModelOwner?.servicesViewModel?.count ?? 0
        return 12
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvatarCell", for: indexPath) as! AvatarCollectionViewCell
        
        cell.shortName.text = shortName
        cell.imageIcon.tintColor = UIColor(rgb: colorsArray[indexPath.row])
        cell.globalView.borderColor = indexSelected == indexPath ? Constants.greenOnePrimaryColor : Constants.blackThreePrimaryColor
        if colorSelected == colorsArray[indexPath.row] {
            cell.globalView.borderColor = Constants.greenOnePrimaryColor
            cell.globalView.borderWidth = 2
            indexSelected = indexPath
        } else {
            cell.globalView.borderColor = Constants.blackThreePrimaryColor
            cell.globalView.borderWidth = 1
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected  \(indexPath.row)")
        guard let selectedIndex = indexSelected else {
            let cell: AvatarCollectionViewCell = collectionView.cellForItem(at: indexPath) as! AvatarCollectionViewCell
            cell.globalView.borderColor = Constants.greenOnePrimaryColor
            cell.globalView.borderWidth = 2
            indexSelected = indexPath
            colorSelected = colorsArray[indexPath.row]
            return
        }
        
        if selectedIndex != indexPath {
        let cell: AvatarCollectionViewCell = collectionView.cellForItem(at: indexPath) as! AvatarCollectionViewCell
        cell.globalView.borderColor = Constants.greenOnePrimaryColor
        cell.globalView.borderWidth = 2
        colorSelected = colorsArray[indexPath.row]
        
        let oldCell: AvatarCollectionViewCell = collectionView.cellForItem(at: selectedIndex) as! AvatarCollectionViewCell
        oldCell.globalView.borderColor = Constants.blackThreePrimaryColor
        oldCell.globalView.borderWidth = 1
            
        indexSelected = indexPath
        }
    }
    
}

extension ChangeAvatarViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.size.width - 12 * 3) / 3 //some width
        
        return CGSize(width: width - 30, height: 90)
    }
    
    
}
