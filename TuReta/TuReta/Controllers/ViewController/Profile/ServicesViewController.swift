//
//  ServicesViewController.swift
//  TuReta
//
//  Created by DC on 21/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class ServicesViewController: TuRetaBaseViewController {
    
    @IBOutlet weak var tableServicesView: UITableView!

    let container = TuRetaContainer()
    var currentServicesActivated: [Service]?
    var viewModelOwner: DefaultUserOwnerViewModel? {
        didSet{
            self.tableServicesView.reloadData()
        }
    }
    
    var viewModelOw: DefaultUserOwnerViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableServicesView.delegate = self
        self.tableServicesView.dataSource = self
        self.tableServicesView.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
        super.viewWillAppear(animated)
        self.playAnimation(animationName: "loading-football")
        requestServicesData()
    
    }
    

    
    func requestServicesData(){
        viewModelOwner = DefaultUserOwnerViewModel(ownerService: container.ownerEstablishmentService())
        
        viewModelOwner?.requestAllServicesOwner( completion: { [weak self] in
            self!.viewModelOwner = self!.viewModelOwner
            self?.stopAnimation()
            }, {( message) in
                let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
    }
    
    func updateServiceStatus(establishmentId: Int, service: Int, isON: Bool){
        viewModelOw = DefaultUserOwnerViewModel(ownerService: container.ownerEstablishmentService())
        
        viewModelOw?.requestUpdateService(establishmentID: establishmentId, service: service, activatedStatus: isON, completion: { [weak self] (response) in
            
            if response{
                NotificationCenter.default.post(name: .reloadProfileInfo, object: nil)
            }else{
                print("failed")
            }
            }, {( message) in
                let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
    }
}

extension ServicesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModelOwner?.servicesViewModel?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableServicesView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ServicesTableCell
        
        cell.currentServicesActivated = currentServicesActivated
        cell.viewModel = self.viewModelOwner?.getItemByIndex(index: indexPath.row)
        cell.statusService.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
        return cell
    }
    
    
    @objc func switchChanged(_ sender : UISwitch!){
        
//        print("field \(viewModelEstablishmentDetail?.fields[sender.tag].name)")
//        print("table row switch Changed \(sender.tag)")
//        print("The switch is \(sender.isOn ? "ON" : "OFF")")
        
        updateServiceStatus(establishmentId: UserData.shared?.user.establishments?[0].id ?? 0, service: sender.tag, isON: sender.isOn)
        
    }
    
}

