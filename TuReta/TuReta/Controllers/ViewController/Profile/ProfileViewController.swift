//
//  ProfileViewController.swift
//  TuReta
//
//  Created by DC on 9/28/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

final class ProfileViewController: TuRetaBaseViewController {
    
    @IBOutlet weak var imageProfileView: CustomCornerUIView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var editProfileButton: UIButton!
    
    @IBOutlet weak var playerViewDetails: UIView!
    @IBOutlet weak var ownerViewDetails: UIView!
    @IBOutlet weak var collectionServicesView: UICollectionView!
    
    var isGuest: Bool = true
    var pager: ViewPager?
    let options = ViewPagerOptions()
    let tabs = [
        ViewPagerTab(title: "Canchas favoritas", image: nil),
        ViewPagerTab(title: "Historial de canchas", image: nil)
    ]
    
    let container = TuRetaContainer()
    var currentServicesActivated: [Service]?
    var profileViewModel: ProfileViewModel?{
        didSet{
            //labelName.text = profileViewModel?.name
            fillProfile()
        }
    }
    var viewModel: DefaultUserViewModel?
    var viewModelOwner: DefaultUserOwnerViewModel? {
        didSet{
            self.collectionServicesView.reloadData()
        }
    }
    
    var viewModelOw: DefaultUserOwnerViewModel?
    
    init(profileViewModel: ProfileViewModel) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageProfileView.layer.cornerRadius = imageProfileView.frame.width / 2
        
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
        }
         
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if defaults.object(forKey: "isGuest") as? Bool == true{
            isGuest = true
            self.goToLogin()
        } else {
            isGuest = false
            viewModel = DefaultUserViewModel(profileService: container.profileService())
            viewModel?.setup { [weak self] in
                self?.profileViewModel = ProfileViewModel(userVM: (self?.viewModel!)!)
            }
            
            if(!isOwner!){
                ownerViewDetails.isHidden = true
                playerViewDetails.isHidden = false
                
                options.tabType = .basic
                options.distribution = .segmented
                options.isTabIndicatorAvailable = true
                options.tabViewBackgroundDefaultColor = Constants.blackOnePrimaryColor
                options.tabIndicatorViewBackgroundColor = Constants.greenOnePrimaryColor
                
                pager = ViewPager(viewController: self, containerView: playerViewDetails)
                pager?.setOptions(options: options)
                pager?.setDataSource(dataSource: self)
                pager?.setDelegate(delegate: self)
                pager?.build()
                
                
            }else{
                ownerViewDetails.isHidden = false
                playerViewDetails.isHidden = true
                //self.requestServicesData()
                self.collectionServicesView.delegate = self
                self.collectionServicesView.dataSource = self
                self.collectionServicesView.layoutIfNeeded()
            }
        }
        
        //ownerViewDetails.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        editProfileButton.isEnabled = false
    }
    
    func goToLogin() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        self.dismiss(animated:false, completion: nil)
        appDelegate.presentInit()
    }
    
    func fillProfile(){
        var nameLetters: String!
        
        
        guard let fullNameProfile: String  = profileViewModel?.name else{
            return
        }
        if fullNameProfile.count > 0{
            let arrayName = fullNameProfile.wordList
            
            nameLetters = String(arrayName[0].prefix(1))
            
            if(arrayName.count > 1){
                let name = String(arrayName[1].prefix(1))
                nameLetters.append("\(name.uppercased())")
            }
            
            self.labelName.text = nameLetters.uppercased()
        }
        editProfileButton.isEnabled = true
    }
    
    
//    func requestServicesData(){
//        viewModelOwner = DefaultUserOwnerViewModel(ownerService: container.ownerEstablishmentService())
//
//        viewModelOwner?.requestAllServicesOwner( completion: { [weak self] in
//            self!.viewModelOwner = self!.viewModelOwner
//
//            }, {( message) in
//                let alertError = HelperExtension.buildBasicAlertController(title: "Alerta", message: message, handler: nil)
//                self.present(alertError, animated: true, completion: nil)
//        })
//    }
    
//    func updateServiceStatus(establishmentId: Int, service: Int, isON: Bool){
//        viewModelOw = DefaultUserOwnerViewModel(ownerService: container.ownerEstablishmentService())
//
//        viewModelOw?.requestUpdateService(establishmentID: establishmentId, service: service, activatedStatus: isON, completion: { [weak self] (response) in
//
//            if response{
//                NotificationCenter.default.post(name: .reloadProfileInfo, object: nil)
//            }else{
//                print("failed")
//            }
//            }, {( message) in
//                let alertError = HelperExtension.buildBasicAlertController(title: "Alerta", message: message, handler: nil)
//                self.present(alertError, animated: true, completion: nil)
//        })
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "editProfile" {
            
            guard let vc = segue.destination as? EditProfileViewController else {
                return
            }
            
            vc.userProfile = profileViewModel
            //            vc.userProfile = self.user
            //            vc.nameLetters = nameLetters
            
        }
        
    }
}

extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModelOwner?.servicesViewModel?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ServicesOwnerCell
        
        cell.currentServicesActivated = currentServicesActivated
        cell.viewModel = self.viewModelOwner?.getItemByIndex(index: indexPath.row)
        cell.statusService.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
        return cell
    }
    
    @objc func switchChanged(_ sender : UISwitch!){
        
//        print("field \(viewModelEstablishmentDetail?.fields[sender.tag].name)")
//        print("table row switch Changed \(sender.tag)")
//        print("The switch is \(sender.isOn ? "ON" : "OFF")")
        
        //updateServiceStatus(establishmentId: UserData.shared?.user.establishments?[0].id ?? 0, service: sender.tag, isON: sender.isOn)
        
    }
    
}

extension ProfileViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.size.width - 12 * 3) / 3 //some width
        
        return CGSize(width: width - 10, height: 130)
    }
    
}


extension ProfileViewController: ViewPagerDataSource {
    func viewControllerAtPosition(position: Int) -> UIViewController {
        switch position {
        case 0:
            let vc = FavoritesViewController()
            return vc
        case 1:
            let vc = HistoryReservationViewController()
            return vc
        default:
            return UIViewController()
        }
        
    }
    
    
    func numberOfPages() -> Int {
        return tabs.count
    }
    
    
    func tabsForPages() -> [ViewPagerTab] {
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
}

extension ProfileViewController: ViewPagerDelegate {
    
    func willMoveToControllerAtIndex(index:Int) {
        print("Moving to page \(index)")
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        print("Moved to page \(index)")
    }
}
