//
//  EditTorneosViewController.swift
//  TuReta
//
//  Created by DC on 26/05/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class EditTorneosViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    
    var tournamentModel: TournamentModel?
    
    let container = TuRetaContainer()
    var viewModel: NewTournamentViewModel?
    var editViewModel: EditTournamentViewModel?
    var gameTypeSelected: Int = 0 {
        willSet(newValue) {
            self.gameTypeSelected = newValue
            //grassTypeButton.setTitle(HelperExtension.GrassType(type: grassTypeSelected), for: .normal)
            self.tournamentModel?.game_type = HelperExtension.GameTypeSelected(type: gameTypeSelected)
        }
        didSet {}
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Editar torneo"
        self.table.keyboardDismissMode = .interactive
        viewModel?.name = tournamentModel?.name
        
        viewModel = NewTournamentViewModel(tournamentService: container.tournamentService())
        editViewModel = EditTournamentViewModel(tournamentService: container.tournamentService())
    }
    

    @IBAction func deleteTournament() {
        editViewModel?.removeTournament(tournamentModel: tournamentModel!, completionHandler: { [weak self] (response) in
            guard let self = self else { return }
            if response {
                let alert = HelperExtension.buildBasicAlertController(title: "", message: "Torneo eliminado con exito") {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = HelperExtension.buildBasicAlertController(title: "Error al eliminar Torneo", message: "") {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
            }
            }, { (error, message) in
                let alert = HelperExtension.buildBasicAlertController(title: "Error", message: message) {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
        })
    }
    
    @IBAction func editTournament() {
        self.view.endEditing(true)
        tournamentModel?.name = self.viewModel?.name
        viewModel?.updateTournament(tournamentModel: tournamentModel!, completionHandler: { [weak self] (response) in
        guard let self = self else { return }
            if response {
                let alert = HelperExtension.buildBasicAlertController(title: "", message: "Torneo editado con exito") {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = HelperExtension.buildBasicAlertController(title: "Error al editar Torneo", message: "") {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
            }
            }, { (error, message) in
                    let alert = HelperExtension.buildBasicAlertController(title: "Error", message: message) {[weak self] in
                        guard let self = self else { return }
                        self.dismissView()
                    }
                    self.present(alert, animated: true, completion: nil)
            })
    }

    
    func dismissView(){
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
}


extension EditTorneosViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let teamsVC = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "TeamsViewController") as? TeamsViewController else { return }
        teamsVC.teams = tournamentModel?.teams as! [TeamModel]
        teamsVC.idTournament = tournamentModel?.id as! Int
        
        guard let gameTypeVC = UIStoryboard(name: "NewField", bundle: nil).instantiateViewController(withIdentifier: "GameTypeViewController") as? GameTypeViewController else { return }
        gameTypeVC.gameTypeSelected = HelperExtension.GameTypeSelected(type: tournamentModel?.game_type ?? 7)
        
        guard let rulesVC = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "TournamentRulesViewController") as? TournamentRulesViewController else { return }
        rulesVC.matchWon = tournamentModel?.match_won_points
        rulesVC.matchLost = tournamentModel?.lost_match_points
        rulesVC.matchTied = tournamentModel?.tied_match_points
        rulesVC.rounds = tournamentModel?.number_of_rounds
        
        switch indexPath.row {
        case 1:
            self.navigationController?.pushViewController(teamsVC, animated: true)
        case 2:
            self.navigationController?.pushViewController(gameTypeVC, animated: true)
        case 3:
            self.navigationController?.pushViewController(rulesVC, animated: true)
        default:
            self.navigationController?.pushViewController(teamsVC, animated: true)
        }
        
        
    }
}

extension EditTorneosViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.inputNameCell.rawValue, for: indexPath) as? InputNameCellTableViewCell else {
                           return UITableViewCell()
                       }
            cell.inputName.delegate = self
            cell.inputName.text = tournamentModel?.name
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.optionCell.rawValue, for: indexPath) as? BasicOptionTableViewCell else {
                return UITableViewCell()
            }
            
            switch indexPath.row {
            case 1:
                cell.nameLabel.text = "Equipos"
            case 2:
                cell.nameLabel.text = "Tipo de juego"
            case 3:
                cell.nameLabel.text = "Reglas"
            default:
                cell.nameLabel.text = ""
            }
            
            return cell
        }
    }
}

extension EditTorneosViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return } // check textfield contains value or not
        if !text.isEmpty {
            self.viewModel?.name = textField.text
            //self.viewModel?.gameType = 7 // temporal
        }
        
    }
}
