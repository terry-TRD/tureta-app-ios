//
//  TeamsViewModel.swift
//  TuReta
//
//  Created by DC on 07/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

class TeamsViewModel {
    
    var teams: [TeamModel] = []
    var tournamentID: Int?
    var isEmpty: Bool? {
        didSet{
            
        }
    }
    
    private let tournamentService: TournamentServices
    
    init(tournamentService: TournamentServices) {
        self.tournamentService = tournamentService
    }
    
    func getAllTeams(idTournament: Int, completionHandler: @escaping () -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        tournamentService.requestTeams(idTournament: idTournament,  completionHandler: { (data) in
            self.parseBasicOperationWithArray(data: data, completionHandler: {[weak self] (response) in
                guard let self = self else { return }
                self.teams = response
                if response.count > 0 {
                    self.isEmpty = false
                }
                completionHandler()
            })
        }) { (networkError, message) in
            errorHandler(.parse, message)
            
        }
    }
    
    func getTeamsByTournament( completionHandler: @escaping () -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        guard let tournamentID = tournamentID else { return }
        tournamentService.requestTeamsByTournament(tournamentID: tournamentID, completionHandler: { (data) in
            self.parseBasicOperationWithArray(data: data, completionHandler: {[weak self] (response) in
                guard let self = self else { return }
                self.teams = response
                if response.count > 0 {
                    self.isEmpty = false
                }
                completionHandler()
            })
        }) { (networkError, message) in
            errorHandler(.parse, message)
            
        }
    }
    
    
    func parseBasicOperationWithArray(data: Data, completionHandler: @escaping(_ response: [TeamModel]) -> Void){
        do {
            var itemsArray : [TeamModel] = []
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            let _ = json.compactMap{
                if let tournament = TeamModel(JSONObject: $0){
                    itemsArray.append(tournament)
                }
            }
            completionHandler(itemsArray)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
}
