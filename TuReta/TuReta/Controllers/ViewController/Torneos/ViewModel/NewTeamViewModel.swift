//
//  NewTeamViewModel.swift
//  TuReta
//
//  Created by DC on 07/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

class NewTeamViewModel {
    
    var nameTeam: String?
    var idTournament: Int = 0
    
    private let tournamentService: TournamentServices
    
    init(tournamentService: TournamentServices) {
        self.tournamentService = tournamentService
    }
    
    func createNewTeam(image: Data?, completionHandler: @escaping (Bool) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        tournamentService.requestCreateTeam(idTournament: idTournament, image: image, nameTeam: self.nameTeam ?? "", completionHandler: { (data) in
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
                
                if !response.isEmpty {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
                print(response)
            })
            
        }) { (networkError, message) in
            errorHandler(.parse, message)
            completionHandler(false)
        }
        
    }
    
    
    func parseBasicOperation(data: Data, completionHandler: @escaping(_ response: JSONObjectType) -> Void){
        do {
            
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONObjectType else{
                return
            }
            
            completionHandler(json)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
    
    
}
