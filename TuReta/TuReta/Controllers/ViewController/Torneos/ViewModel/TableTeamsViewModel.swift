//
//  TableTeamsViewModel.swift
//  TuReta
//
//  Created by DC on 22/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

class TableTeamsViewModel {
    
    var establishemtnID: Int?
    var tableTeam: [TableTeam] = []
    
    private let tournamentService: TournamentServices
    
    init(tournamentService: TournamentServices) {
        self.tournamentService = tournamentService
    }
    
    func getAllData(tournamentModel: TournamentModel, completionHandler: @escaping () -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        tournamentService.requestTableTeams(establishmentID: establishemtnID ?? 0, itemModelTournament: tournamentModel, completionHandler: { (data) in
            self.parseBasicOperationWithArray(data: data, completionHandler: {[weak self] (response) in
                guard let self = self else { return }
                self.tableTeam = response
                completionHandler()
            })
        }) { (networkError, message) in
            errorHandler(.parse, message)
            
        }
    }
    
    func parseBasicOperationWithArray(data: Data, completionHandler: @escaping(_ response: [TableTeam]) -> Void){
        do {
            var itemsArray : [TableTeam] = []
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            let _ = json.compactMap{
                if let tournament = TableTeam(JSONObject: $0){
                    itemsArray.append(tournament)
                }
            }
            completionHandler(itemsArray)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
}
