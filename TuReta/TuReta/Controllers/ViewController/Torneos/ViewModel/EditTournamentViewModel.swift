//
//  EditTournamentViewModel.swift
//  TuReta
//
//  Created by DC on 26/05/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

class EditTournamentViewModel {
    
    var tournamentModel: TournamentModel?
    
    private let tournamentService: TournamentServices
    
    init(tournamentService: TournamentServices) {
        self.tournamentService = tournamentService
    }
    
    func removeTournament(tournamentModel: TournamentModel, completionHandler: @escaping (Bool) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        tournamentService.requestRemoveTournament(itemModel: tournamentModel, completionHandler: { (data) in
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
            
                if response != nil {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
                print(response)
            })
        }) { (networkError, message) in
            errorHandler(.parse, message)
            completionHandler(false)
        }
        
    }

    func parseBasicOperation(data: Data, completionHandler: @escaping(_ response: JSONObjectType) -> Void){
        do {
            
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONObjectType else{
                return
            }
            
            completionHandler(json)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
}
