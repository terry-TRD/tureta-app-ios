//
//  TournamentModel.swift
//  TuReta
//
//  Created by DC on 03/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation
import Parchment

struct TournamentModel {
    var id: Int?
    var name: String?
    var game_type: Int?
    var match_won_points: Int?
    var tied_match_points: Int?
    var lost_match_points: Int?
    var number_of_rounds: Int?
    var teams: [TeamModel]?
    var deleted: Bool?
}

struct FixtureModel {
    var round_number: Int?
    var team_1_id: Int?
    var team_2_id: Int?
    var field_id: Int?
    var date: String?
    var team_1_annotations: Int?
    var team_2_annotations: Int?
    var id: Int?
}

struct FixtureDefaultModel {
    var date: String?
    var field: Field?
    var id: Int?
    var round_number: Int?
    var teams: [TeamModel]?
    var team_1_annotations: Int
    var team_2_annotations: Int
}

struct RoundModel: PagingItem, Hashable, Comparable {
    var id: Int?
    var roundNumber: Int?
    
    static func < (lhs: RoundModel, rhs: RoundModel) -> Bool {
        
        return lhs.roundNumber ?? 0 < rhs.roundNumber ?? 0
    }
}

extension TournamentModel: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType){
        guard let id = JSONObject["id"] as? Int,
            let name = JSONObject["name"] as? String,
            let gameType = JSONObject["game_type"] as? Int,
            let match_won_points = JSONObject["match_won_points"] as? Int,
            let tied_match_points = JSONObject["tied_match_points"] as? Int,
            let lost_match_points = JSONObject["lost_match_points"] as? Int,
            let number_of_rounds = JSONObject["number_of_rounds"] as? Int,
            let deleted = JSONObject["deleted"] as? Bool else {
                return nil
        }
        
        let teamsJSONArray = JSONObject["teams"] as? JSONArrayType
        var teamsList: [TeamModel] = []
        let _ = teamsJSONArray?.compactMap {
            if let team = TeamModel(JSONObject: $0){
                teamsList.append(team)
            }
        }
        
        self.init(id: id,
                  name: name,
                  game_type: gameType,
                  match_won_points: match_won_points,
                  tied_match_points: tied_match_points,
                  lost_match_points: lost_match_points,
                  number_of_rounds: number_of_rounds,
                  teams: teamsList,
                  deleted: deleted)
    }
}

extension FixtureDefaultModel: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType){
        guard let date = JSONObject["date"] as? String,
            let id = JSONObject["id"] as? Int,
            let round_number = JSONObject["round_number"] as? Int else {
                return nil
        }
        
        var field: Field?
        if ((JSONObject["field"] as? JSONObjectType) != nil) {
            field = Field(JSONObject: JSONObject["field"] as! JSONObjectType)
        }else {
            field = nil
        }
        let teamAnnotation1 = JSONObject["team_1_annotations"] as? Int ?? -1
        let teamAnnotation2 = JSONObject["team_2_annotations"] as? Int ?? -1
        
        let teamsJSONArray = JSONObject["teams"] as? JSONArrayType
        var teamsList: [TeamModel] = []
        let _ = teamsJSONArray?.compactMap {
            if let team = TeamModel(JSONObject: $0){
                teamsList.append(team)
            }
        }
        
        self.init(date: date,
                field: field,
                id: id,
                round_number: round_number,
                teams: teamsList,
                team_1_annotations: teamAnnotation1,
                team_2_annotations: teamAnnotation2)
    }
    
}
