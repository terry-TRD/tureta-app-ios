//
//  TournamentViewModel.swift
//  TuReta
//
//  Created by DC on 03/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

class TournamentViewModel {
    
    var tournaments: [TournamentModel] = []
    var fixtures: [FixtureDefaultModel] = []
    var isEmpty: Bool? {
        didSet{
            
        }
    }
    
    private let tournamentService: TournamentServices
    
    init(tournamentService: TournamentServices) {
        self.tournamentService = tournamentService
    }
    
    func getTournaments(page: Int, establishmentID: Int, completionHandler: @escaping () -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        tournamentService.requestTournaments(page: page, establishmentID: establishmentID, completionHandler: { (data) in
            self.parseBasicOperationWithArray(data: data, completionHandler: {[weak self] (response) in
                guard let self = self else { return }
                self.tournaments = response
                if response.count > 0 {
                    self.isEmpty = false
                }
                completionHandler()
            })
        }) { (networkError, message) in
            errorHandler(.parse, message)
            
        }
    }
    
    func getFixturesByRound(model: TournamentModel, roundSelected: Int, establishmentID: Int, completionHandler: @escaping () -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        tournamentService.requestFixturesByRound(itemModel: model, roundSelected: roundSelected, establishmentID:establishmentID, completionHandler: { (data) in
            self.parseFixtureBasicOperationWithArray(data: data, completionHandler: {[weak self] (response) in
                guard let self = self else { return }
                self.fixtures = response
                if response.count > 0 {
                    self.isEmpty = false
                }
                completionHandler()
            })
        }) { (networkError, message) in
            errorHandler(.parse, message)
            
        }
        
    }
    
    func parseBasicOperationWithArray(data: Data, completionHandler: @escaping(_ response: [TournamentModel]) -> Void){
        do {
            var itemsArray : [TournamentModel] = []
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            let _ = json.compactMap{
                if let tournament = TournamentModel(JSONObject: $0){
                    itemsArray.append(tournament)
                }
            }
            completionHandler(itemsArray)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
    
    func parseFixtureBasicOperationWithArray(data: Data, completionHandler: @escaping(_ response: [FixtureDefaultModel]) -> Void){
        do {
            var itemsArray : [FixtureDefaultModel] = []
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            let _ = json.compactMap{
                if let tournament = FixtureDefaultModel(JSONObject: $0){
                    itemsArray.append(tournament)
                }
            }
            completionHandler(itemsArray)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
    
    
    
}
