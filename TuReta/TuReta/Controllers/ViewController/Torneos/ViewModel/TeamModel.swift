//
//  TeamModel.swift
//  TuReta
//
//  Created by DC on 07/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

struct TeamModel {
    var id: Int?
    var name: String?
    var media_url: String?
    var deleted: Bool?
}

struct TableTeam {
    var team: TeamModel?
    var dif: Int?
    var je: Int?
    var jg: Int?
    var jj: Int?
    var jp: Int?
    var pts: Int?
}

extension TeamModel: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType){
        guard let id = JSONObject["id"] as? Int,
            let name = JSONObject["name"] as? String,
            let mediaurl = JSONObject["media_url"] as? String,
            let deleted = JSONObject["deleted"] as? Bool else{
                return nil
        }
        
        
        
        self.init(id: id,
                  name: name,
                  media_url: mediaurl,
                  deleted: deleted)
    }
}

extension TableTeam {
    init?(JSONObject: JSONObjectType){
        guard let dif = JSONObject["dif"] as? Int,
            let je = JSONObject["je"] as? Int,
            let jg = JSONObject["jg"] as? Int,
            let jj = JSONObject["jj"] as? Int,
            let jp = JSONObject["jp"] as? Int,
            let pts = JSONObject["pts"] as? Int,
            let team = TeamModel(JSONObject: JSONObject["team"] as! JSONObjectType) else {
                return nil
        }
        
        self.init(team: team,
                  dif: dif,
                  je: je,
                  jg: jg,
                  jj: jj,
                  jp: jp,
                  pts: pts)
    }
}
