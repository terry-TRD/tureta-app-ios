//
//  EditFixtureViewModel.swift
//  TuReta
//
//  Created by DC on 28/05/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation


class EditFixtureViewModel {
    
    var fixtureModel: FixtureModel = FixtureModel()
    var tournamentModel: TournamentModel?
    var roundMatch: Int = 0
    var fieldID: Int = 0
    var datePicked: String = ""
    var homeTeamIDSelected: Int?
    var awayTeamIDSelected: Int?
    var team_1_annotations: Int?
    var team_2_annotations: Int?
    
    private let tournamentService: TournamentServices
    
    init(tournamentService: TournamentServices) {
        self.tournamentService = tournamentService
    }

    
    func saveFixtureData( completionHandler: @escaping (Bool) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        fixtureModel = FixtureModel(round_number: roundMatch, team_1_id: homeTeamIDSelected, team_2_id: awayTeamIDSelected, field_id: fieldID, date: datePicked, team_1_annotations: team_1_annotations ?? 0, team_2_annotations: team_2_annotations ?? 0, id: 0)
        
        guard let model = tournamentModel else { return }
        tournamentService.requestAddFixtureDetails(itemModel: fixtureModel, itemModelTournament: model, completionHandler: { (data) in
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }

                if response != nil {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
                print(response)
            })
            
        }) { (networkError, message) in
            errorHandler(.parse, message)
            completionHandler(false)
        }
        
    }
    
    func updateFixtureData(fixture: FixtureDefaultModel, completionHandler: @escaping (Bool) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        fixtureModel = FixtureModel(round_number: roundMatch, team_1_id: homeTeamIDSelected, team_2_id: awayTeamIDSelected, field_id: fieldID, date: datePicked, team_1_annotations: team_1_annotations, team_2_annotations: team_2_annotations, id: fixture.id)
        
        guard let model = tournamentModel else { return }
        tournamentService.requestUpdateFixtureDetails(itemModel: fixtureModel, itemModelTournament: model, completionHandler: { (data) in
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }

                if response != nil {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
                print(response)
            })
            
        }) { (networkError, message) in
            errorHandler(.parse, message)
            completionHandler(false)
        }
        
    }
    
    func parseBasicOperation(data: Data, completionHandler: @escaping(_ response: JSONObjectType) -> Void){
        do {
            
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONObjectType else{
                return
            }
            
            completionHandler(json)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
}
