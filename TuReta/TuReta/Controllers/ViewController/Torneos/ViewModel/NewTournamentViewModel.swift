//
//  NewTournamentViewModel.swift
//  TuReta
//
//  Created by DC on 06/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

class NewTournamentViewModel {
    
    var tournamentModel: TournamentModel = TournamentModel()
    var name: String?
    var teams: [TeamModel]?
    var gameType: Int?
    var matchWon: Int?
    var matchTied: Int?
    var matchLost: Int?
    var rounds: Int?
    
    private let tournamentService: TournamentServices
    
    init(tournamentService: TournamentServices) {
        self.tournamentService = tournamentService
    }
    
    
    func createNewTournament( completionHandler: @escaping (Bool) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        tournamentModel = TournamentModel(id: 0, name: self.name, game_type: self.gameType, match_won_points: matchWon, tied_match_points: matchTied, lost_match_points: matchLost, number_of_rounds: rounds, teams: teams, deleted: false)
        
        tournamentService.requestCreateNewTournament(itemModel: tournamentModel, completionHandler: { (data) in
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
            
                if response != nil {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
                print(response)
            })
        }) { (networkError, message) in
            errorHandler(.parse, message)
            completionHandler(false)
        }
    }
    
    func updateTournament(tournamentModel: TournamentModel, completionHandler: @escaping (Bool) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        // not needed
//        let tournament = TournamentModel(id: tournamentModel.id, name: self.name, game_type: tournamentModel.game_type, match_won_points: tournamentModel.match_won_points, tied_match_points: tournamentModel.tied_match_points, lost_match_points: tournamentModel.lost_match_points, teams: tournamentModel.teams, deleted: false)
        
        tournamentService.requestUpdateTournament(itemModel: tournamentModel, completionHandler: { (data) in
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                completionHandler(true)
            })
            completionHandler(false)
        }) { (networkError, message) in
            errorHandler(.parse, message)
            completionHandler(false)
        }
    }
    
    func parseBasicOperation(data: Data, completionHandler: @escaping(_ response: JSONObjectType) -> Void){
        do {
            
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONObjectType else{
                return
            }
            
            completionHandler(json)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
}
