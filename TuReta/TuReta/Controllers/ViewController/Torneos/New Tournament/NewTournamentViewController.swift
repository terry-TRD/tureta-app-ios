//
//  NewTournamentViewController.swift
//  TuReta
//
//  Created by DC on 26/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class NewTournamentViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let container = TuRetaContainer()
    var viewModel: NewTournamentViewModel?
    
    var gameTypeSelected: Int? {
        willSet(newValue) {
            self.gameTypeSelected = newValue
            //grassTypeButton.setTitle(HelperExtension.GrassType(type: grassTypeSelected), for: .normal)
            self.viewModel?.gameType = HelperExtension.GameTypeSelected(type: gameTypeSelected ?? 0)
        }
        didSet {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Crear torneo"
        self.table.keyboardDismissMode = .interactive
        viewModel = NewTournamentViewModel(tournamentService: container.tournamentService())
    }
    
    @IBAction func createNewTournament() {
        self.view.endEditing(true)
        
        
        viewModel?.createNewTournament( completionHandler: { [weak self] (response) in
            guard let self = self else { return }
            if response {
                let alert = HelperExtension.buildBasicAlertController(title: "", message: "Torneo creado con exito") {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = HelperExtension.buildBasicAlertController(title: "Error al crear Torneo", message: "") {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
            }
            }, { (error, message) in
                let alert = HelperExtension.buildBasicAlertController(title: "Error", message: message) {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
        })
        
    }

    func dismissView(){
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }

}

extension NewTournamentViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let teamsVC = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "TeamsViewController") as? TeamsViewController else { return }
        if viewModel?.teams?.count ?? 0 > 0 {
            teamsVC.teams = viewModel?.teams as! [TeamModel]
        }
        
        guard let gameTypeVC = UIStoryboard(name: "NewField", bundle: nil).instantiateViewController(withIdentifier: "GameTypeViewController") as? GameTypeViewController else { return }
        gameTypeVC.gameTypeSelected = gameTypeSelected
        
        guard let rulesVC = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "TournamentRulesViewController") as? TournamentRulesViewController else { return }
        
        switch indexPath.row {
        case 1: break
            //self.navigationController?.pushViewController(teamsVC, animated: true)
        case 2:
            self.navigationController?.pushViewController(gameTypeVC, animated: true)
        case 3:
            self.navigationController?.pushViewController(rulesVC, animated: true)
        default:
            break
        }
        
        
    }
}

extension NewTournamentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.inputNameCell.rawValue, for: indexPath) as? InputNameCellTableViewCell else {
                           return UITableViewCell()
                       }
            cell.inputName.delegate = self
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.optionCell.rawValue, for: indexPath) as? BasicOptionTableViewCell else {
                return UITableViewCell()
            }
            
            switch indexPath.row {
            case 1:
                cell.nameLabel.text = "Equipos"
            case 2:
                cell.nameLabel.text = "Tipo de juego"
            case 3:
                cell.nameLabel.text = "Reglas"
            default:
                cell.nameLabel.text = ""
            }
            
            return cell
        }
    }
}

extension NewTournamentViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return } // check textfield contains value or not
        if !text.isEmpty {
            self.viewModel?.name = textField.text
            self.viewModel?.gameType = 7 // temporal
        }
        
    }
}
