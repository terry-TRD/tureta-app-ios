//
//  TournamentDetailViewController.swift
//  TuReta
//
//  Created by DC on 29/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class TournamentDetailViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    var tournament: TournamentModel?
    var establishmentID: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = tournament?.name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let index = self.table.indexPathForSelectedRow{
            self.table.deselectRow(at: index, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editTorunament"{
            
            let vc = segue.destination as! EditTorneosViewController
            vc.tournamentModel = tournament
        }
    }
    
    @IBAction func editButtonPressed(_ sender: Any) {
     performSegue(withIdentifier: "editTorunament", sender: nil)
    }

}

extension TournamentDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let calendarVC = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "DefaultCalendarViewController") as? DefaultCalendarViewController else { return }
        calendarVC.tournamentModel = tournament
        
        guard let tableTeams = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "TableTeamsViewController") as? TableTeamsViewController else { return }
        tableTeams.tournamentSelected = tournament
        tableTeams.establishemtnID = establishmentID ?? 0
        
        switch indexPath.row {
        case 0:
            self.navigationController?.pushViewController(calendarVC, animated: true)
        case 1:
            self.navigationController?.pushViewController(tableTeams, animated: true)
        default:
            self.navigationController?.pushViewController(calendarVC, animated: true)
        }
        
        
    }
}


extension TournamentDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.optionCell.rawValue, for: indexPath) as? BasicOptionTableViewCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .blue
        switch indexPath.row {
        case 0:
            cell.nameLabel.text = "Calendario"
        case 1:
            cell.nameLabel.text = "Estadisticas"
        default:
            break
        }
        
        
        
        return cell
    }
    
    
}
