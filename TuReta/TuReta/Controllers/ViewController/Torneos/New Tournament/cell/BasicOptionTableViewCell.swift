//
//  BasicOptionTableViewCell.swift
//  TuReta
//
//  Created by DC on 26/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class BasicOptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    
    var tournamentModel: TournamentModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(tournamentModel: TournamentModel) {
        self.tournamentModel = tournamentModel
        nameLabel.text = tournamentModel.name
        
    }

}
