//
//  InputNameCellTableViewCell.swift
//  TuReta
//
//  Created by DC on 26/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class InputNameCellTableViewCell: UITableViewCell {

    @IBOutlet weak var inputName: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        inputName.attributedPlaceholder = NSAttributedString(string: "Nombre del torneo", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
