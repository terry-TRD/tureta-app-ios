//
//  PickTeamFixtureViewController.swift
//  TuReta
//
//  Created by DC on 11/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class PickTeamFixtureViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    
    let container = TuRetaContainer()
    var viewModel: TeamsViewModel?
    var teams: [TeamModel] = []
    var tournamentID: Int?
    var indexTeam: Int? // 0 home - 1 away

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Equipos"
        viewModel = TeamsViewModel(tournamentService: container.tournamentService())
        getData()
    }
    
    func getData() {
        viewModel?.tournamentID = tournamentID
        viewModel?.getTeamsByTournament(completionHandler: { [weak self] in
            guard let self = self else { return }
            print("result")
            self.table.reloadData()
        }, { (error, message) in
            print("error")
        })
    }
   

}

extension PickTeamFixtureViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let i = navigationController?.viewControllers.firstIndex(of: self) {
        guard let presenter = navigationController?.viewControllers[i - 1] as? EditFixtureViewController else {
            if let presenter = navigationController?.viewControllers[i - 2] as? EditFixtureViewController {
                
                    guard let model = self.viewModel?.teams else { return }
                    if indexTeam == 0 {
                        presenter.homeTeamSelected = model[indexPath.row]
                    } else {
                        presenter.awayTeamSelected = model[indexPath.row]
                    }
                    self.navigationController?.popViewController(animated: true)
                }
                
                return
            }
            
            
            guard let model = self.viewModel?.teams else { return }
            if indexTeam == 0 {
                presenter.homeTeamSelected = model[indexPath.row]
            } else {
                presenter.awayTeamSelected = model[indexPath.row]
            }
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}


extension PickTeamFixtureViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel?.teams.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.basicTeamCell.rawValue, for: indexPath) as? BasicTeamTableViewCell else {
            return UITableViewCell()
        }
        
        guard let model = self.viewModel?.teams else { return cell }
       
        cell.configCell(teamModel: model[indexPath.row])
        
        return cell
    }
    
    
}
