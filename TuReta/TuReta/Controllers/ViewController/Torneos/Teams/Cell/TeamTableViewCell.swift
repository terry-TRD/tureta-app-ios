//
//  TeamTableViewCell.swift
//  TuReta
//
//  Created by DC on 29/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageTeam: UIImageView!
    @IBOutlet weak var nameTeam: UILabel!
    @IBOutlet weak var statusTeam: UISwitch!
    var currentTeamsActivated: [Service]?
    var teamModel: TeamModel?
    var index: Int?
    
    override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
           imageTeam.layer.cornerRadius = imageTeam.frame.height / 2
        
    }
    
    func configCell(teamModel: TeamModel) {
        self.teamModel = teamModel
        
        self.nameTeam.text = teamModel.name
        self.statusTeam.isOn = teamModel.deleted ?? false
        guard let urlImage = URL(string: teamModel.media_url ?? "") else { return }
        self.imageTeam.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default-image1"))
        self.statusTeam.tag = index ?? 0
        
    }
    
//    var viewModel: Service?{
//        didSet{
//            updateUI()
//        }
//    }
//
//    func updateUI(){
//        guard let model = self.viewModel else { return }
//        nameService.text = model.name
//        imageService.load(url: URL(string:model.icon_url)!)
//        statusService.tag = model.id
//        guard let currentServices = currentServicesActivated else { return }
//
//        for i in 0..<currentServices.count {
//            if(model.id == currentServicesActivated?[i].id ){
//                statusService.isOn = true
//                break
//            }else{
//                statusService.isOn = false
//            }
//        }
//
//    }

}
