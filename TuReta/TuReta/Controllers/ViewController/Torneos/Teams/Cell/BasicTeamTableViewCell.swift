//
//  BasicTeamTableViewCell.swift
//  TuReta
//
//  Created by DC on 11/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class BasicTeamTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageTeam: UIImageView!
    @IBOutlet weak var nameTeam: UILabel!

    var teamModel: TeamModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageTeam.layer.cornerRadius = imageTeam.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(teamModel: TeamModel) {
        self.teamModel = teamModel
        
        self.nameTeam.text = teamModel.name
        guard let urlImage = URL(string: teamModel.media_url ?? "") else { return }
        
        self.imageTeam.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default-image1"))
    }

}
