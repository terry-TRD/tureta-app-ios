//
//  NewTeamViewController.swift
//  TuReta
//
//  Created by DC on 26/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class NewTeamViewController: TuRetaBaseViewController {

    @IBOutlet weak var imageTeam: CustomBorderUIImageView!
    @IBOutlet weak var buttonImagePick: UIButton!
    @IBOutlet weak var nameTeamLabel: UITextField!
    
    let container = TuRetaContainer()
    var viewModel: NewTeamViewModel?
    var idTournament: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Crear Equipo"
        self.setupUI()
        viewModel = NewTeamViewModel(tournamentService: container.tournamentService())
    }
    
    func setupUI() {
        self.imageTeam.layer.cornerRadius = self.imageTeam.frame.size.width / 2
        self.imageTeam.borderWidth = 0.5
        self.imageTeam.borderColor = Constants.blackThreePrimaryColor
        self.buttonImagePick.layer.cornerRadius = self.buttonImagePick.frame.size.width / 2
        
        nameTeamLabel.attributedPlaceholder = NSAttributedString(string: "Nombre", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        
        self.nameTeamLabel.delegate = self
    }
    
    
    
    @IBAction func addImageAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        /*If you want work actionsheet on ipad
        then you have to use popoverPresentationController to present the actionsheet,
        otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }

        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addTeamAction(_ sender: UIButton) {
        
        // save team if text and image are != nil
        if imageTeam.image != nil && (self.viewModel?.nameTeam != nil) {
        self.viewModel?.idTournament = self.idTournament ?? 0
        viewModel?.createNewTeam(image: imageTeam.image?.jpegData(compressionQuality: 0.5), completionHandler: { [weak self] (response) in
            guard let self = self else { return }
            if response {
                let alert = HelperExtension.buildBasicAlertController(title: "", message: "Equipo creado con exito") {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = HelperExtension.buildBasicAlertController(title: "Error al crear Equipo", message: "") {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
            }
            }, { (error, message) in
                let alert = HelperExtension.buildBasicAlertController(title: "Error", message: message) {[weak self] in
                    guard let self = self else { return }
                    self.dismissView()
                }
                self.present(alert, animated: true, completion: nil)
        })
        } else {
            let alertError = HelperExtension.buildBasicAlertController(title: "", message: "Elige nombre y foto del equipo", handler: nil)
            self.present(alertError, animated: true, completion: nil)
        }
    }
    
    func dismissView(){
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

}


extension NewTeamViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            imageTeam.contentMode = .scaleToFill
            imageTeam.image = pickedImage
            buttonImagePick.setTitle("", for: .normal)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension NewTeamViewController: UINavigationControllerDelegate {
    
}

extension NewTeamViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return } // check textfield contains value or not
        if !text.isEmpty {
            self.viewModel?.nameTeam = textField.text
        }
        
    }
}
