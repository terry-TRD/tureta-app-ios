//
//  TeamsViewController.swift
//  TuReta
//
//  Created by DC on 29/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class TeamsViewController: TuRetaBaseViewController {

    @IBOutlet weak var viewWithTeams: UIView!
    @IBOutlet weak var viewWithoutTeams: UIView!
    @IBOutlet weak var table: UITableView!
    
    let container = TuRetaContainer()
    var viewModel: TeamsViewModel?
    var teams: [TeamModel] = []
    var idTournament: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Equipos"
        
        viewModel = TeamsViewModel(tournamentService: container.tournamentService())
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // check with WS and update isEmpty
        self.getData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            if let navController = self.navigationController {
                if navController.viewControllers.count > 2 {
                    guard let presenter = navController.viewControllers[2] as? EditTorneosViewController else { return }
                    //presenter.viewModel?.teams = self.teams
                    presenter.tournamentModel?.teams = self.teams
                } else {
                    guard let presenter = navController.viewControllers[1] as? NewTournamentViewController else { return }
                    presenter.viewModel?.teams = self.teams
                }
                
                
            }
            self.teams.removeAll()
        }
    }
    
    func getData() {
        self.playAnimation(animationName: "loading-football")
        viewModel?.getAllTeams(idTournament: idTournament, completionHandler: { [weak self] in
            guard let self = self else { return }
            print("result")
            self.stopAnimation()
            self.updateUI()
            self.table.reloadData()
            }, { (error, message) in
                print("error")
                self.stopAnimation()
        })
    }
    
    func updateUI() {
        if let _ = viewModel?.isEmpty {
            viewWithTeams.isHidden = false
            viewWithoutTeams.isHidden = true
        } else {
            viewWithTeams.isHidden = true
            viewWithoutTeams.isHidden = false
        }
    }
    
    @IBAction func addteamAction(_ sender: UIButton) {
        guard let newTeamVC = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "NewTeamViewController") as? NewTeamViewController else { return }
        guard let model = self.viewModel else { return }
        newTeamVC.idTournament = idTournament
        self.navigationController?.pushViewController(newTeamVC, animated: true)
    }
    
    @objc func switchChanged(_ sender : UISwitch!){
        
        // check TeamID to update status
        //updateServiceStatus(establishmentId: UserData.shared?.user.establishments?[0].id ?? 0, service: sender.tag, isON: sender.isOn)
        guard let model = self.viewModel else { return }
        if sender.isOn {
            self.teams.append(model.teams[sender.tag])
        } else {
            let team = model.teams[sender.tag]
            teams = teams.filter { $0.id != team.id }
        }
    }

}

extension TeamsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
}


extension TeamsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel?.teams.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.teamCell.rawValue, for: indexPath) as? TeamTableViewCell else {
            return UITableViewCell()
        }
        
        guard let model = self.viewModel?.teams else { return cell }
        //cell.currentServicesActivated = currentServicesActivated
        //cell.viewModel = self.viewModelOwner?.getItemByIndex(index: indexPath.row)
        cell.index = indexPath.row
        cell.configCell(teamModel: model[indexPath.row])
        for item in teams{
            if item.id == model[indexPath.row].id{
                cell.statusTeam.isOn = true
            }
        }
        cell.statusTeam.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
        
        return cell
    }
    
    
}
