//
//  TorneosViewController.swift
//  TuReta
//
//  Created by DC on 29/02/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import WebKit

class TorneosViewController: TuRetaBaseViewController {
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var viewWithTournaments: UIView!
    @IBOutlet weak var viewWithoutTournaments: UIView!
   
    var page: Int = 0
    var tournamentSelected: TournamentModel?
    
    var viewModel: TournamentViewModel?
    let container = TuRetaContainer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Torneos"
        viewModel = TournamentViewModel(tournamentService: container.tournamentService())
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // check with WS and update isEmpty
        if let index = self.table.indexPathForSelectedRow{
            self.table.deselectRow(at: index, animated: true)
        }
        self.getData()
    }
    
    func getData() {
        guard let establishment = UserData.shared?.user.establishments else { return }
        self.playAnimation(animationName: "loading-football")
        
        viewModel?.getTournaments(page: page, establishmentID: establishment[0].id, completionHandler: { [weak self] in
            guard let self = self else { return }
            
            self.stopAnimation()
            self.updateUI()
            self.table.reloadData()
            }, { (error, message) in
                print("error")
                self.stopAnimation()
        })
    }
    
    
    
    func updateUI() {
        if let _ = viewModel?.isEmpty {
            viewWithTournaments.isHidden = false
            viewWithoutTournaments.isHidden = true
        } else {
            viewWithTournaments.isHidden = true
            viewWithoutTournaments.isHidden = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tournamentDetail"{
            
            let vc = segue.destination as! TournamentDetailViewController
            vc.tournament = tournamentSelected
            vc.establishmentID = UserData.shared?.user.establishments?[0].id
        }
    }
        
}

extension TorneosViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = self.viewModel?.tournaments else { return }
        
        tournamentSelected = model[indexPath.row]
        performSegue(withIdentifier: "tournamentDetail", sender: nil)
    }
}


extension TorneosViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel?.tournaments.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.optionCell.rawValue, for: indexPath) as? BasicOptionTableViewCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .blue
        guard let model = self.viewModel?.tournaments else { return cell }
        
        cell.configCell(tournamentModel: model[indexPath.row])
        
        return cell
    }
    
}
