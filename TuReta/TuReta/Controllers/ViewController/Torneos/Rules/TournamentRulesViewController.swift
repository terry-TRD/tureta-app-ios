//
//  TournamentRulesViewController.swift
//  TuReta
//
//  Created by DC on 29/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class TournamentRulesViewController: UIViewController {

    var pickerSet: [String] = []
    
    @IBOutlet weak var table: UITableView!
    
    lazy var pickerSelected: String = ""
    var pickerIndex: Int = 0
    var matchWon: Int?
    var matchTied: Int?
    var matchLost: Int?
    var rounds: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Reglas"
        hunderdNumbers()
    }
    
    @IBAction func aceptRules(){
        if let navController = self.navigationController {
            guard let presenter = navController.viewControllers[1] as? NewTournamentViewController else {
                if let presenter = navController.viewControllers[2] as? EditTorneosViewController {
                    presenter.tournamentModel?.match_won_points = matchWon
                    presenter.tournamentModel?.tied_match_points = matchTied
                    presenter.tournamentModel?.lost_match_points = matchLost
                    presenter.tournamentModel?.number_of_rounds = rounds
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
                return
            }
            presenter.viewModel?.matchWon = matchWon
            presenter.viewModel?.matchTied = matchTied
            presenter.viewModel?.matchLost = matchLost
            presenter.viewModel?.rounds = rounds
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func hunderdNumbers() {
        for item in 0...100 {
            pickerSet.append("\(item)")
        }
    }
}


extension TournamentRulesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            self.pickerIndex = 0
            self.showPickerInActionSheet(title: "Puntos por ganado")
            break
        case 1:
            self.pickerIndex = 1
            self.showPickerInActionSheet(title: "Puntos por empate")
            break
        case 2:
            self.pickerIndex = 2
            self.showPickerInActionSheet(title: "Puntos por perdido")
            break
        case 3:
            self.pickerIndex = 3
            self.showPickerInActionSheet(title: "Jornadas del torneo")
            break
        default: break
        }
    }
}


extension TournamentRulesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // rules
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.ruleCell.rawValue, for: indexPath) as? RuleTableViewCell else {
            return UITableViewCell()
        }
        
        switch indexPath.row {
        case 0:
            cell.ruleNameLabel.text = "Puntos por partido ganado"
            cell.itemSelectedLabel.text = "\(matchWon ?? 0)"
        case 1:
            cell.ruleNameLabel.text = "Puntos por partido empatado"
            cell.itemSelectedLabel.text = "\(matchTied ?? 0)"
        case 2:
            cell.ruleNameLabel.text = "Puntos por partido perdido"
            cell.itemSelectedLabel.text = "\(matchLost ?? 0)"
        case 3:
            cell.ruleNameLabel.text = "Jornadas del torneo"
            cell.itemSelectedLabel.text = "\(rounds ?? 0)"
        default:
            break
        }
        
        
        return cell
    }
    
    
}

extension TournamentRulesViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func showPickerInActionSheet(title: String) {
        let message = "\n\n\n\n\n\n\n\n"
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.actionSheet)
        
        let titleFont : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont),
            NSAttributedString.Key.foregroundColor: Constants.blackOnePrimaryColor
        ]
        let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
        alert.setValue(titleAttrString, forKey: "attributedTitle")

        //Create a frame (placeholder/wrapper) for the picker and then create the picker
        let pickerFrame: CGRect = CGRect(x: 0, y: 0, width: alert.view.frame.width - 20, height: 200)
        let picker: UIPickerView = UIPickerView(frame: pickerFrame)
        picker.backgroundColor = UIColor.clear

        //set the pickers datasource and delegate
        picker.delegate = self
        picker.dataSource = self

        //Add the picker to the alert controller
        alert.view.addSubview(picker)

        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        let okAction = UIAlertAction(title: "Continuar", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in self.valueSelected(value: self.pickerSet[picker.selectedRow(inComponent: 0)])
        })
        alert.addAction(okAction)

        self.present(alert, animated: true, completion: nil)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerSet[row]
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        pickerSet.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int { return 1 }
    

//    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//        guard let customFont = UIFont(name: "ElenarLove-Regular", size: UIFont.labelFontSize) else {
//            fatalError("")
//        }
//
//        let titleData = pickerSet[row]
//       let titleFont : [NSAttributedString.Key : Any] = [
//            NSAttributedString.Key.font : UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont),
//            NSAttributedString.Key.foregroundColor: Constants.blackOnePrimaryColor
//        ]
//        let messageAttrString = NSMutableAttributedString(string: titleData, attributes: titleFont)
//        return messageAttrString
//    }


    func valueSelected(value: String) {
        switch pickerIndex {
        case 0:
            self.matchWon = Int(value)
            break
        case 1:
            self.matchTied = Int(value)
            break
        case 2:
            self.matchLost = Int(value)
            break
        case 3:
            self.rounds = Int(value)
            break
        default:
            break
        }
        self.table.reloadData()
        //pickerSelected = value
        //NotificationCenter.default.post(name: .checkHoursSelected, object: nil)
    }
}
