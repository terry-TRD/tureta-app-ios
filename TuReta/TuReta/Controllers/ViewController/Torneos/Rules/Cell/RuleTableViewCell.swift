//
//  RuleTableViewCell.swift
//  TuReta
//
//  Created by DC on 29/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class RuleTableViewCell: UITableViewCell {
   
    @IBOutlet weak var ruleNameLabel: UILabel!
    @IBOutlet weak var itemSelectedLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
