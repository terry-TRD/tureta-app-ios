//
//  FixturesResultsTableViewController.swift
//  TuReta
//
//  Created by DC on 26/05/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

// Owner View 
class FixturesResultsTableViewController: UITableViewController {

    var tournamentSelected: TournamentModel?
    var viewModel: TournamentViewModel?
    let container = TuRetaContainer()
    
    var matchIndex: Int = 0
    var roundMatch: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Partidos"
        self.view.backgroundColor = Constants.blackOnePrimaryColor
        viewModel = TournamentViewModel(tournamentService: container.tournamentService())
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestFixtures()
    }
    
    
    func requestFixtures(){
        // call viewmodel request fixtures
        
        guard let tournamentModel = self.tournamentSelected else { return }
        guard let establishment = UserData.shared?.user.establishments else { return }
        viewModel?.getFixturesByRound(model: tournamentModel, roundSelected: roundMatch, establishmentID: establishment[0].id, completionHandler: { [weak self] in
            guard let self = self else { return }
            print("result")
            self.tableView.reloadData()
        }, { (error, message) in
            print("error")
        })
                                      
    }
    @IBAction func addMatch(_ sender: Any) {
        guard let editFixtureVC = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "EditFixtureViewController") as? EditFixtureViewController else { return }
        
        editFixtureVC.roundMatch = roundMatch
        editFixtureVC.matchIndex =  0
        editFixtureVC.tournamentModel = tournamentSelected
        self.navigationController?.pushViewController(editFixtureVC, animated: true)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        guard let model = self.viewModel?.fixtures else { return 0 }
        return model.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.dateFixtureCell.rawValue, for: indexPath) as? DateFixtureTableViewCell else {
                return UITableViewCell()
            }
            cell.bottomView.backgroundColor = Constants.blackOnePrimaryColor
            tableView.rowHeight = 51
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.MatchTableViewCell.rawValue, for: indexPath) as? MatchTableViewCell else {
                return UITableViewCell()
            }
            
            guard let model = self.viewModel?.fixtures else { return cell }
            cell.configCell(fixtureModel: model[indexPath.row - 1])
            tableView.rowHeight = 53
            return cell
        }
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        guard let editFixtureVC = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "EditFixtureViewController") as? EditFixtureViewController else { return }
        
        guard let model = self.viewModel?.fixtures else { return }
        editFixtureVC.roundMatch = roundMatch
        editFixtureVC.matchIndex =  indexPath.row
        editFixtureVC.tournamentModel = tournamentSelected
        editFixtureVC.fixture = model[indexPath.row - 1]
        self.navigationController?.pushViewController(editFixtureVC, animated: true)
        
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
