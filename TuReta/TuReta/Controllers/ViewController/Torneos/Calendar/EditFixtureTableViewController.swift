//
//  EditFixtureTableViewController.swift
//  TuReta
//
//  Created by DC on 27/05/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import DatePickerDialog

protocol ScoreDelegate {
    
    func updateHomeScore(scoreValue: Int)
    func updateAwayScore(scoreValue: Int)
}

class EditFixtureViewController: UIViewController, ScoreDelegate {
    
    var isNew: Bool = false
    var datePicked: String = ""
    var timePicked: String = ""
    var homeScoreInt: Int = 0
    var awayScoreInt: Int = 0
    var fixture: FixtureDefaultModel?
    
    var matchIndex: Int = 0
    var roundMatch: Int = 0
    var homeTeamSelected: TeamModel? {
        willSet(newValue) {
            self.homeTeamSelected = newValue
            self.table.reloadData()
        }
        didSet {}
    }
    var awayTeamSelected: TeamModel? {
        willSet(newValue) {
            self.awayTeamSelected = newValue
            self.table.reloadData()
        }
        didSet {}
    }
    var fieldSelected: Field? {
        willSet(newValue) {
            self.fieldSelected = newValue
            self.table.reloadData()
        }
    }
    
    var viewModel: EditFixtureViewModel?
    var fixtureModel: FixtureModel?
    var tournamentModel: TournamentModel?
    let container = TuRetaContainer()
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Partido \(matchIndex) Jornada \(roundMatch)"
        viewModel = EditFixtureViewModel(tournamentService: container.tournamentService())
        viewModel?.roundMatch = roundMatch
        
        guard let fixtureItem = fixture else {
            saveButton.setTitle("Guardar", for: .normal)
            isNew = true
            return }
        
        if (fixtureItem.date != nil) {
            saveButton.setTitle("Actualizar", for: .normal)
            isNew = false
            setupFieldWithData()
        } else {
            saveButton.setTitle("Guardar", for: .normal)
            isNew = true
            
        }
    }
    
    func setupFieldWithData(){
        guard let fixtureItem = fixture else { return }
        
        let date = fixtureItem.date?.components(separatedBy: " ")
        datePicked = date?[0] as! String
        timePicked = date?[1] as! String
        homeTeamSelected = fixtureItem.teams?[0]
        awayTeamSelected = fixtureItem.teams?[1]
        homeScoreInt = fixtureItem.team_1_annotations
        awayScoreInt = fixtureItem.team_2_annotations
        if fixtureItem.field != nil {
            viewModel?.fieldID = fixtureItem.field?.id ?? 0
            fieldSelected = fixtureItem.field
        }
        
        // habilitar el boton y WS de editar
        self.table.reloadData()
    }
    
    @IBAction func saveFixtureDataButton(_ sender: Any) {
        
        viewModel?.tournamentModel = tournamentModel
        viewModel?.homeTeamIDSelected = homeTeamSelected?.id
        viewModel?.awayTeamIDSelected = awayTeamSelected?.id
        viewModel?.team_1_annotations = homeScoreInt
        viewModel?.team_2_annotations = awayScoreInt
        
        while(timePicked.last?.isNumber != true) {
            let newItem = timePicked.dropLast()
            timePicked = String(newItem)
        }
        guard let fixtureItem = fixture else {

            self.viewModel?.datePicked = "\(datePicked) \(timePicked)"
            viewModel?.saveFixtureData( completionHandler: { [weak self] (response) in
                    guard let self = self else { return }
                    if response {
                        let alert = HelperExtension.buildBasicAlertController(title: "", message: "Partido creado con exito") {[weak self] in
                            guard let self = self else { return }
                            self.dismissView()
                        }
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let alert = HelperExtension.buildBasicAlertController(title: "Error al crear partido", message: "") {[weak self] in
                            guard let self = self else { return }
                            self.dismissView()
                        }
                        self.present(alert, animated: true, completion: nil)
                    }
                    }, { (error, message) in
                        let alert = HelperExtension.buildBasicAlertController(title: "Error", message: message) {[weak self] in
                            guard let self = self else { return }
                            self.dismissView()
                        }
                        self.present(alert, animated: true, completion: nil)
                })
            return
        }
        
        if (fixtureItem.date != nil) {
            
            
            self.viewModel?.datePicked = "\(datePicked) \(timePicked)"
            viewModel?.updateFixtureData(fixture: fixtureItem, completionHandler: { [weak self] (response) in
                guard let self = self else { return }
                if response {
                    let alert = HelperExtension.buildBasicAlertController(title: "", message: "Partido actualizado con exito") {[weak self] in
                        guard let self = self else { return }
                        self.dismissView()
                    }
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let alert = HelperExtension.buildBasicAlertController(title: "Error al crear partido", message: "") {[weak self] in
                        guard let self = self else { return }
                        self.dismissView()
                    }
                    self.present(alert, animated: true, completion: nil)
                }
                }, { (error, message) in
                    let alert = HelperExtension.buildBasicAlertController(title: "Error", message: message) {[weak self] in
                        guard let self = self else { return }
                        self.dismissView()
                    }
                    self.present(alert, animated: true, completion: nil)
            })
            
        }
    }
    
    
    func dismissView(){
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func datePickerTapped() {
        DatePickerDialog().show("Elige una fecha", doneButtonTitle: "Aceptar", cancelButtonTitle: "Cancelar", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "Y-MM-dd"
                self.datePicked = formatter.string(from: dt)
                
                //let dateStringDict:[String: String] = ["newDateString": self.datePicked]
                //NotificationCenter.default.post(name: .didChangeDate, object: nil, userInfo: dateStringDict )
                //self.textField.text = formatter.string(from: dt)
                //print(formatter.string(from: dt))
                
                self.updateTable()
            }
        }
    }
    
    func timePickerTapped() {
        
        DatePickerDialog().show("Elige una hora", doneButtonTitle: "Aceptar", cancelButtonTitle: "Cancelar", datePickerMode: .time) {
            (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm:ss"
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.amSymbol = ""
                formatter.pmSymbol = ""
                self.timePicked = formatter.string(from: dt)
                
                //self.viewModel?.fixtureModel?.date = self.timePicked // juntar las 2 fechas
                
                //let dateStringDict:[String: String] = ["newDateString": self.datePicked]
                //NotificationCenter.default.post(name: .didChangeDate, object: nil, userInfo: dateStringDict )
                //self.textField.text = formatter.string(from: dt)
                //print(formatter.string(from: dt))
                
                self.updateTable()
            }
        }
    }
    
    func updateHomeScore(scoreValue: Int) {
        homeScoreInt = scoreValue
        updateTable()
    }
    
    func updateAwayScore(scoreValue: Int) {
        awayScoreInt = scoreValue
        updateTable()
    }
    
    @objc func homeChangeTeam(_ sender : UIButton!){
        guard let tableTeams = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "PickTeamFixtureViewController") as? PickTeamFixtureViewController else { return }
        tableTeams.tournamentID = tournamentModel?.id
        tableTeams.indexTeam = 0
        self.navigationController?.pushViewController(tableTeams, animated: true)
    }
    
    @objc func awayChangeTeam(_ sender : UIButton!){
        guard let tableTeams = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "PickTeamFixtureViewController") as? PickTeamFixtureViewController else { return }
        tableTeams.tournamentID = tournamentModel?.id
        tableTeams.indexTeam = 1
        self.navigationController?.pushViewController(tableTeams, animated: true)
    }
    
    func updateTable(){
        
        self.table.reloadData()
    }
    
}

extension EditFixtureViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            datePickerTapped()
        } else if indexPath.row == 1 {
            timePickerTapped()
        } else if indexPath.row == 2 {
            guard let pickFieldVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "PickFieldViewController") as? PickFieldViewController else { return }
            
            self.navigationController?.pushViewController(pickFieldVC, animated: true)
        }
    }
    
}


extension EditFixtureViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row <= 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.dateFixtureCell.rawValue, for: indexPath) as? DateFixtureTableViewCell else {
                return UITableViewCell()
            }
            cell.bottomView.backgroundColor = Constants.blackOnePrimaryColor
            switch indexPath.row {
            case 0:
                cell.titleLabel.text = datePicked == "" ? "Añade la fecha del partido" : datePicked.getNiceTimeFormat3()
            case 1:
                cell.titleLabel.text = timePicked == "" ? "Añade la hora del partido" : "\(timePicked.getNiceTimeFormat4()) hrs"
            case 2:
                cell.titleLabel.text = fieldSelected?.name == "" || fieldSelected?.name == nil ? "Añade la cancha" : "Cancha: \(fieldSelected?.name ?? "")"
            default:
                cell.titleLabel.text = ""
            }
            
            tableView.rowHeight = 51
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.editTeamsScore.rawValue, for: indexPath) as? EditTeamsAndScoreTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            cell.homeScoreInt = homeScoreInt
            cell.awayScoreInt = awayScoreInt
            cell.homeScoreLabel.text = homeScoreInt == -1 ? "-" : "\(homeScoreInt)"
            if homeTeamSelected != nil {
                guard let urlHomeImage = URL(string: homeTeamSelected?.media_url ?? "") else { return cell }
                cell.homeTeamImage.sd_setImage(with: urlHomeImage, placeholderImage: UIImage(named: "default-image1"))
                cell.homePickTeamButton.setTitle(homeTeamSelected?.name, for: .normal)
            }
            if awayTeamSelected != nil {
                guard let urlAwayImage = URL(string: awayTeamSelected?.media_url ?? "") else { return cell }
                cell.awayTeamImage.sd_setImage(with: urlAwayImage, placeholderImage: UIImage(named: "default-image1"))
                cell.awayPickTeamButton.setTitle(awayTeamSelected?.name, for: .normal)
            }
            
            cell.awayScoreLabel.text = awayScoreInt == -1 ? "-" : "\(awayScoreInt)"
            
            if isNew {
                cell.homePlusButton.isHidden = true
                cell.homeMinusButton.isHidden = true
                
                cell.awayPlusButton.isHidden = true
                cell.awayMinusButton.isHidden = true
            } else {
                cell.homePlusButton.isHidden = false
                cell.homeMinusButton.isHidden = false
                
                cell.awayPlusButton.isHidden = false
                cell.awayMinusButton.isHidden = false
            }
            
            cell.homePickTeamButton.addTarget(self, action: #selector(self.homeChangeTeam), for: .touchUpInside)
            cell.awayPickTeamButton.addTarget(self, action: #selector(self.awayChangeTeam), for: .touchUpInside)
            
            tableView.rowHeight = 180
            return cell
        }
    }
}
