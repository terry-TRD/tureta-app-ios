//
//  TableTeamsViewController.swift
//  TuReta
//
//  Created by DC on 04/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class TableTeamsViewController: TuRetaBaseViewController {

    var tournamentSelected: TournamentModel?
    var establishemtnID: Int?
    let tableView = UITableView()
    
    let container = TuRetaContainer()
    var viewModel: TableTeamsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = tournamentSelected?.name
        viewModel = TableTeamsViewModel(tournamentService: container.tournamentService())
        self.tableView.delegate = self
        self.tableView.dataSource = self
        setupTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func getData(){
        guard let tournament = tournamentSelected else { return }
        self.playAnimation(animationName: "loading-football")
        viewModel?.establishemtnID = establishemtnID
        viewModel?.getAllData(tournamentModel: tournament, completionHandler: { [weak self] in
            guard let self = self else { return }
            print("result")
            self.stopAnimation()
            self.tableView.reloadData()
        }, { (error, message) in
            self.stopAnimation()
            print("error")
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getData()
    }
    
    func setupTableView() {
        let headerView = HeaderTableTeamView().loadNib()
        view.addSubview(headerView)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        headerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        headerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        let bottomView = InfoBottomTableTeams().loadNib()
        view.addSubview(bottomView)
        
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
        bottomView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 0).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomView.topAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none

        tableView.register(UINib(nibName: "ReservacionOwnerTableViewCell", bundle: nil), forCellReuseIdentifier: "ReservacionOwnerTableViewCell")
        tableView.register(UINib(nibName: CellResusableIdentifier.teamRow.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.teamRow.rawValue)
       
    }
}


extension TableTeamsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
}

extension TableTeamsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let teams = self.viewModel?.tableTeam else { return 0 }
        return teams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.teamRow.rawValue, for: indexPath) as? TeamRowTableViewCell else {
            return UITableViewCell()
        }
        
        guard let teams = self.viewModel?.tableTeam else { return cell }
        
        cell.teamPositionLabel.text = "\(indexPath.row + 1)."
        cell.teamNameLabel.text = teams[indexPath.row].team?.name
        
        cell.teamJJLabel.text = "\(teams[indexPath.row].jj ?? 0)"
        cell.teamJGLabel.text = "\(teams[indexPath.row].jg ?? 0)"
        cell.teamJELabel.text = "\(teams[indexPath.row].je ?? 0)"
        cell.teamJPLabel.text = "\(teams[indexPath.row].jp ?? 0)"
        cell.teamDIFLabel.text = "\(teams[indexPath.row].dif ?? 0)"
        cell.teamPointsLabel.text = "\(teams[indexPath.row].pts ?? 0)"
        cell.configCell(team: teams[indexPath.row])
        
        return cell
    }
}

