//
//  DefaultCalendarViewController.swift
//  TuReta
//
//  Created by DC on 26/05/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class DefaultCalendarViewController: TuRetaBaseViewController {
    
    var tournamentModel: TournamentModel?
    var isGuest: Bool = true

    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Calendario"
        self.table.keyboardDismissMode = .interactive
        
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
        } else {
            isGuest = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let index = self.table.indexPathForSelectedRow{
            self.table.deselectRow(at: index, animated: true)
        }
    }
    
}

extension DefaultCalendarViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let fixturesVC = UIStoryboard(name: "Torneos", bundle: nil).instantiateViewController(withIdentifier: "FixturesResultsTableViewController") as? FixturesResultsTableViewController else { return }
    
        fixturesVC.roundMatch = indexPath.row + 1
        fixturesVC.tournamentSelected = self.tournamentModel
        
        self.navigationController?.pushViewController(fixturesVC, animated: true)
        
    }
}

extension DefaultCalendarViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let rounds = self.tournamentModel?.number_of_rounds else { return 0 }
        return rounds
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.optionCell.rawValue, for: indexPath) as? BasicOptionTableViewCell else {
            return UITableViewCell()
        }
        
        cell.nameLabel.text = "Jornada \(indexPath.row + 1)"
        
        return cell
    }
}
