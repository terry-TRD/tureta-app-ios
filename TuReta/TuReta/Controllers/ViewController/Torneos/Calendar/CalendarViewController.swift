//
//  CalendarViewController.swift
//  TuReta
//
//  Created by DC on 16/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class CalendarViewController: UIViewController {

    var page: Int = 0
    var viewModel: TournamentViewModel?
    let container = TuRetaContainer()
    var establishmentID: Int?
    
    let tableView = UITableView()
    var tournamentSelected: TournamentModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        viewModel = TournamentViewModel(tournamentService: container.tournamentService())
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
        getData()
    }
    
    func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        //tableView.register(UINib(nibName: "ReservacionOwnerTableViewCell", bundle: nil), forCellReuseIdentifier: "ReservacionOwnerTableViewCell")
        tableView.register(UINib(nibName: CellResusableIdentifier.optionCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.optionCell.rawValue)
    }
    
    func getData() {
        guard let item = establishmentID else { return }
        viewModel?.getTournaments(page: page, establishmentID: item, completionHandler: { [weak self] in
            guard let self = self else { return }
            print("result")
            self.tableView.reloadData()
        }, { (error, message) in
            print("error")
        })
    }

}


extension CalendarViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let defaultDetailTournament = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DefaultDetailTournamentViewController") as? DefaultDetailTournamentViewController else { return }

        guard let model = self.viewModel?.tournaments else { return }
        defaultDetailTournament.tournamentSelected = model[indexPath.row]
        defaultDetailTournament.establishmentID = establishmentID
        
        self.navigationController?.pushViewController(defaultDetailTournament, animated: true)
    }
}

extension CalendarViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.tournaments.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.optionCell.rawValue, for: indexPath) as? BasicOptionTableViewCell else {
            return UITableViewCell()
        }
        
        cell.selectionStyle = .blue
        guard let model = self.viewModel?.tournaments else { return cell }
        cell.configCell(tournamentModel: model[indexPath.row])
        cell.imageIcon.image = UIImage(named: "icn-forward")
        cell.imageIcon.tintColor = .white
        
        return cell
    }
}
