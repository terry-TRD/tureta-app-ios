//
//  TeamRowTableViewCell.swift
//  TuReta
//
//  Created by DC on 04/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class TeamRowTableViewCell: UITableViewCell {

    @IBOutlet weak var teamPositionLabel: UILabel!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var teamImageView: UIView!
    
    @IBOutlet weak var teamJJLabel: UILabel!
    @IBOutlet weak var teamJGLabel: UILabel!
    @IBOutlet weak var teamJELabel: UILabel!
    @IBOutlet weak var teamJPLabel: UILabel!
    @IBOutlet weak var teamDIFLabel: UILabel!
    @IBOutlet weak var teamPointsLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        teamImage.layer.cornerRadius = teamImage.frame.size.height / 2
        teamImageView.layer.cornerRadius = teamImageView.frame.size.height / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(team: TableTeam) {
        guard let urlImage = URL(string: team.team?.media_url ?? "") else { return }
        teamImage.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default-image1"))
    }

}
