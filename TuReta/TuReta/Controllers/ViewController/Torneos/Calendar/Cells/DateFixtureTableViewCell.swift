//
//  DateFixtureTableViewCell.swift
//  TuReta
//
//  Created by DC on 26/05/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class DateFixtureTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var bottomView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
