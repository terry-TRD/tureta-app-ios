//
//  EditTeamsAndScoreTableViewCell.swift
//  TuReta
//
//  Created by DC on 27/05/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class EditTeamsAndScoreTableViewCell: UITableViewCell {

    var delegate: ScoreDelegate?
    
    @IBOutlet weak var homeScoreLabel: UITextField!
    @IBOutlet weak var homeTeamImage: UIImageView!
    @IBOutlet weak var homeTeamView: UIView!
    @IBOutlet weak var awayScoreLabel: UITextField!
    @IBOutlet weak var awayTeamImage: UIImageView!
    @IBOutlet weak var awayTeamView: UIView!
    @IBOutlet weak var homePickTeamButton: UIButton!
    @IBOutlet weak var awayPickTeamButton: UIButton!
    
    @IBOutlet weak var homePlusButton: UIButton!
    @IBOutlet weak var homeMinusButton: UIButton!
    
    @IBOutlet weak var awayPlusButton: UIButton!
    @IBOutlet weak var awayMinusButton: UIButton!
    
    var homeScoreInt: Int?
    var awayScoreInt: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        homeTeamView.layer.cornerRadius = homeTeamView.frame.height / 2
        homeTeamImage.layer.cornerRadius = homeTeamImage.frame.height / 2
        
        awayTeamView.layer.cornerRadius = awayTeamView.frame.height / 2
        awayTeamImage.layer.cornerRadius = awayTeamImage.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func homePlusButton(_ sender: Any) {
        guard var score = homeScoreInt else { return }
        score += 1
        delegate?.updateHomeScore(scoreValue: score)
        homeScoreLabel.text = "\(score)"
    }
    
    @IBAction func homeMinusButton(_ sender: Any) {
        guard var score = homeScoreInt else { return }
        if score < 0 { return }
        score -= 1
        delegate?.updateHomeScore(scoreValue: score)
        homeScoreLabel.text = "\(score)"
    }
    
    @IBAction func awayPlusButton(_ sender: Any) {
        guard var score = awayScoreInt else { return }
        score += 1
        delegate?.updateAwayScore(scoreValue: score)
        awayScoreLabel.text = "\(score)"
    }
    
    @IBAction func awayMinusButton(_ sender: Any) {
        guard var score = awayScoreInt else { return }
        if score < 0 { return }
        score -= 1
        delegate?.updateAwayScore(scoreValue: score)
        awayScoreLabel.text = "\(score)"
    }
    
}
