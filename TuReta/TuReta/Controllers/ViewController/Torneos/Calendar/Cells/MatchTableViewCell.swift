//
//  MatchTableViewCell.swift
//  TuReta
//
//  Created by DC on 26/05/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class MatchTableViewCell: UITableViewCell {

    @IBOutlet weak var localTeamView: CustomCornerUIView!
    @IBOutlet weak var localTeamImage: UIImageView!
    @IBOutlet weak var localTeamText: UILabel!
    @IBOutlet weak var localTeamScoreText: UILabel!
    @IBOutlet weak var awayTeamView: CustomCornerUIView!
    @IBOutlet weak var awayTeamImage: UIImageView!
    @IBOutlet weak var awayTeamText: UILabel!
    @IBOutlet weak var awayTeamScoreText: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        localTeamView.layer.cornerRadius = localTeamView.frame.height / 2
        localTeamImage.layer.cornerRadius = localTeamImage.frame.height / 2
        awayTeamView.layer.cornerRadius = awayTeamView.frame.height / 2
        awayTeamImage.layer.cornerRadius = awayTeamImage.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(fixtureModel: FixtureDefaultModel) {
        guard let teams = fixtureModel.teams else { return }
        if teams.count > 0 {
            localTeamText.text = teams[0].name
            localTeamScoreText.text = fixtureModel.team_1_annotations != -1 ? "\(fixtureModel.team_1_annotations)" : ""
            guard let urlLocalImage = URL(string: teams[0].media_url ?? "") else { return }
            localTeamImage.sd_setImage(with: urlLocalImage, placeholderImage: UIImage(named: "default-image1"))
            
            awayTeamText.text = teams[1].name
            awayTeamScoreText.text = fixtureModel.team_2_annotations != -1 ? "\(fixtureModel.team_2_annotations)" : ""
            guard let urlAwayImage = URL(string: teams[1].media_url ?? "") else { return }
            awayTeamImage.sd_setImage(with: urlAwayImage, placeholderImage: UIImage(named: "default-image1"))
        }
    }

}
