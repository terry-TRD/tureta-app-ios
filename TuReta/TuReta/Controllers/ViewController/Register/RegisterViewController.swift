//
//  RegisterViewController.swift
//  TuReta
//
//  Created by DC on 9/14/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase

import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging

class RegisterViewController: TuRetaBaseViewController {
    
    @IBOutlet weak var registerButton: CustomCornerButton!
    @IBOutlet weak var nameTxt: CustomTextField!
    @IBOutlet weak var secondPasswordTxt: CustomTextField!
    @IBOutlet weak var emailTxt: CustomTextField!
    @IBOutlet weak var passwordTxt: CustomTextField!
    @IBOutlet weak var loginFacebookView: UIView!
    @IBOutlet weak var termsConditionsSwitch: UISwitch!

    @IBOutlet weak var privacyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.passwordTxt.delegate = self
        setCloseKeyboard()
        setupTextFields()
        loginFacebookView.layoutIfNeeded()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        registerButton.backgroundColor = Constants.greenOnePrimaryColor
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        registerButton.titleLabel?.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        registerButton.titleLabel?.adjustsFontForContentSizeCategory = true
        
        nameTxt.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        nameTxt.adjustsFontForContentSizeCategory = true
        
        registerButton.titleLabel?.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        registerButton.titleLabel?.adjustsFontForContentSizeCategory = true
        
        emailTxt.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        emailTxt.adjustsFontForContentSizeCategory = true
        
        passwordTxt.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        passwordTxt.adjustsFontForContentSizeCategory = true
        
        secondPasswordTxt.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        secondPasswordTxt.adjustsFontForContentSizeCategory = true
        
        privacyButton.titleLabel?.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        privacyButton.titleLabel?.adjustsFontForContentSizeCategory = true
        setupFacebookLoginButton()
        
    }
    
    func setupTextFields(){
        guard let emailImage = UIImage(named: ImageName.type.iconEmail.rawValue) else { return }
        guard let passwordImage = UIImage(named: ImageName.type.iconPassword.rawValue) else { return }
        guard let userImage = UIImage(named: ImageName.type.iconUser.rawValue) else { return }
        
        nameTxt.setLeftIcon(userImage)
        emailTxt.setLeftIcon(emailImage)
        passwordTxt.setLeftIcon(passwordImage)
        secondPasswordTxt.setLeftIcon(passwordImage)
        
        nameTxt.changePlaceholderColor(text: "Nombre Completo", andColor: .gray)
        emailTxt.changePlaceholderColor(text: "Correo electrónico", andColor: .gray)
        passwordTxt.changePlaceholderColor(text: "Contraseña", andColor: .gray)
        secondPasswordTxt.changePlaceholderColor(text: "Confirmar contraseña", andColor: .gray)
    }
    
    func setupFacebookLoginButton() {
        let loginButton = FBLoginButton()
        loginButton.delegate = self
        loginButton.center = loginFacebookView.center
        loginButton.frame = CGRect(x: 0, y: 0, width: loginFacebookView.frame.size.width, height: loginFacebookView.frame.size.height)

        loginFacebookView.addSubview(loginButton)
        
        
        
        if (AccessToken.current != nil) {
            loginWithFacebook()
        }else{
            loginButton.permissions = ["public_profile", "email"]
        }
    }
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        if termsConditionsSwitch.isOn{
            validateEmail()
        } else {
            let alertError = HelperExtension.buildBasicAlertController(title: "Términos y Condiciones", message: "Confirme que está de acuerdo.", handler: nil)
            self.present(alertError, animated: true, completion: nil)
        }
    }
    
    func validateEmail(){
        // email
        guard let email = emailTxt.text else{
            return
        }
        if !HelperExtension.isValidEmail(emailID: email){
            // message error
            let alertError = HelperExtension.buildBasicAlertController(title: "Correo invalido", message: "Ingresa un correo valido", handler: nil)
            self.present(alertError, animated: true, completion: nil)
        }
        
        // password
        guard let password = passwordTxt.text, passwordTxt.text?.count != 0, passwordTxt.text!.count > 6  else {
            // message error
            let alertError = HelperExtension.buildBasicAlertController(title: "", message: "Ingresa una contraseña valida con mas de 6 caracteres", handler: nil)
            self.present(alertError, animated: true, completion: nil)
            return
        }
        
        getRegisterRequest(email:email , password: password, name: nameTxt.text!, username: "")
    }
    
    func getRegisterRequest(email: String, password: String, name: String, username: String){
        RegisterWS.registerWS(email: email, password: password, name: name, username: username, completionHandler: {[weak self] (user) in
            guard let self = self else { return }
            UserData.shared = user
            let isOwner: Bool!
            if((user?.user.establishments?.count)! > 0 && (user?.user.establishments != nil)){
                isOwner = true
            }else{
                isOwner = false
            }
            
            self.initDefaults(email: (self.emailTxt!.text!), password: (self.passwordTxt!.text!), isOwner: isOwner, token: user!.access_token)
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            if(isOwner){
                //
                print("enviar a dueño de cancha")
                appDelegate.presentHomeOwner()
            }else{
                appDelegate.presentHomeVC()
            }
            Messaging.messaging().subscribe(toTopic: "user\(user?.user.id ?? 0)") { error in
                print("Subscribed to \(user?.user.id ?? 0) topic")
            }
            
            }, { (networkError, message) in
                print(networkError!, message)
                let alertError = HelperExtension.buildBasicAlertController(title: "", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
    }
    
    
    func initDefaults(email: String, password: String, isOwner: Bool, token: String){
        let defaults = UserDefaults.standard
        defaults.set(email, forKey: "email")
        defaults.set(password, forKey: "password")
        defaults.set(token, forKey: "token")
        defaults.set(isOwner, forKey: "isOwner")
        defaults.synchronize()
        
    }
    
    func saveFacebookTokenDefaults(token: String){
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "facebookToken")
        defaults.synchronize()
    }
    
//    private func setCloseKeyboard() {
//        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard))
//        tapGesture.cancelsTouchesInView = false
//        self.view.addGestureRecognizer(tapGesture)
//        
//    }
//    @objc func closeKeyboard() {
//        
//        view.endEditing(true)
//        
//    }
    
    func loginWithFacebook(){
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields": "email"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in

            if ((error) != nil)
            {
                // Process error
               // TODO
            }
            else
            {
                guard let userInfo = result as? [String: Any] else { return }
                var emailFacebook: String = ""
                if let email = userInfo["email"] as? String {
                    emailFacebook = email
                }else{
                    emailFacebook = "\(userInfo["id"] as! String)@facebook.com)"
                }
                self.saveFacebookTokenDefaults(token: AccessToken.current!.tokenString)
                self.getLoginRequest(email: emailFacebook, password: "")
            }
        })
    }
    
    func getLoginRequest(email: String, password: String){
        
        LoginWS.loginWS(email: email, password: password, userName: nil, completionHandler: {[weak self] (user) in
            guard let self = self else { return }
            UserData.shared = user
            let isOwner: Bool!
            if((user?.user.establishments?.count)! > 0 && (user?.user.establishments != nil)){
                isOwner = true
            }else{
                isOwner = false
            }
            
            self.initDefaults(email: (self.emailTxt!.text!), password: (self.passwordTxt!.text!), isOwner: isOwner, token: user!.access_token)
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            // check if is user or admi
            if(isOwner){
                print("enviar a dueño de cancha")
                appDelegate.presentHomeOwner()
            }else{
                appDelegate.presentHomeVC()
            }
            Messaging.messaging().subscribe(toTopic: "user\(user?.user.id ?? 0)") { error in
                print("Subscribed to \(user?.user.id ?? 0) topic")
            }
        }, { (networkError, message) in
            print(networkError!, message)
            let alertError = HelperExtension.buildBasicAlertController(title: "", message: message, handler: nil)
            self.present(alertError, animated: true, completion: nil)
        })
    }
    
     @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension RegisterViewController: UITextFieldDelegate{
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() //Dismiss keyboard
        self.validateEmail()
        return true
    }
}


extension RegisterViewController: LoginButtonDelegate{
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("Successfully logout")
    }

    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if error != nil{
            // TODO
            // something when wrong alert
            return
        }else if result!.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if (result?.grantedPermissions.contains("email"))!
            {
                // Do work
                print("Successfully login")
                print("\(AccessToken.current?.tokenString)")
                loginWithFacebook()
            }

            
        }
        
    }
}
