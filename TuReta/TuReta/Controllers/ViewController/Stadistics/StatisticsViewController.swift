//
//  StadisticsViewController.swift
//  TuReta
//
//  Created by DC on 29/02/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import WebKit

class StatisticsViewController: TuRetaBaseViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Metricas"
        webView.navigationDelegate = self
        self.openURL()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func openURL() {
        let stringPath = "https://tureta.com.mx/api/v1/stats?access_token=\(UserData.shared?.access_token ?? "")"
        if let url = URL(string: stringPath) {
            print(url)
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    
    
}

extension StatisticsViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let host = navigationAction.request.url?.host {
            if host == "tureta.com.mx" {
                decisionHandler(.allow)
                return
            }
        }
        
        decisionHandler(.cancel)
    }
}

