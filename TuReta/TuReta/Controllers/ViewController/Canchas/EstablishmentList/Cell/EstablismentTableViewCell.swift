//
//  EstablismentTableViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 1/11/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class EstablismentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var establishmentName: UILabel!
    @IBOutlet weak var establishmentAddress: UILabel!
    @IBOutlet weak var establishmentImage: UIImageView!
    
    var viewModelField: EstablishmentViewModel? {
        didSet{
            loadInfo()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadInfo() {
        establishmentName.text = viewModelField?.name ?? ""
        establishmentAddress.text = viewModelField?.address ?? ""
        //guard let urlImage = URL(string: viewModelField?.profile_picture_url ?? "") else { return }
        //establishmentImage.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default-image1"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
