//
//  CanchasViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 10/15/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

protocol CanchasViewControllerDelegate: AnyObject{
    func GoToOwnerProfile()
}

class CanchasViewController: TuRetaBaseViewController {
    
    //MARK: - IBOutlet
    @IBOutlet weak var viewOwner: UIView!
    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var viewPlayerMap: UIView!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var searchContentView: CustomOnlyCornerUIView!
    
    @IBOutlet weak var yPlayerMapConstraint: NSLayoutConstraint!
    
    //MARK: - List Establishment
    //@IBOutlet weak var establishmentsListView: UIView!
    @IBOutlet weak var playerEstablishmentsTableView: UITableView!
    @IBOutlet weak var heightConstantEstablishmentTable: NSLayoutConstraint!
    
    
    //MARK: - Establishment View items
    @IBOutlet weak var detailMarkEstablishment: UIView!
    @IBOutlet weak var imageEstablishment: UIImageView!
    @IBOutlet weak var descriptionEstablishmentLabel: UILabel!
    @IBOutlet weak var serviciosEstablishmentLabel: UILabel!
    @IBOutlet weak var nameEstablishmentLabel: UILabel!
    
    //MARK: - Detail Establishmet
    
    
    //MARK: - Owner view Canchas
    @IBOutlet weak var ownerEstablishmentName: UILabel!
    @IBOutlet weak var ownerTableview: UITableView!
    
    //MARK: - Local variables
    var isGuest: Bool = true
    var playerMap: GMSMapView?
    var locationManager : CLLocationManager?
    var currentLocationUser: CLLocationCoordinate2D?
    var centerMap: CLLocationCoordinate2D?
    var imageIcon: UIImageView?
    var localMarkers: [String] = []
    var currentMarkerSelected: EstablishmentViewModel?
    var currentFieldSelected: Field?
    
    var isSearching: Bool?
    let container = TuRetaContainer()
    var establishmentListViewModel: Array<EstablishmentViewModel>? {
        didSet{
            UIView.animate(withDuration: 1) {
                self.playerEstablishmentsTableView.isHidden = false
            }
            playerEstablishmentsTableView.reloadData()
        }
    }
    var establishmentViewModel: Array<EstablishmentViewModel>?{
        didSet{
//            if establishmentsListView.alpha == 1 {
//                //playerEstablishmentsTableView.reloadData()
//            } else {
                setupMarks()
            //}
            
        }
    }
    
    var viewModelEstablishmentDetail: EstablishmentDetail? {
        didSet{
            //ownerEstablishmentName.text = viewModelEstablishmentDetail?.name
            self.navigationItem.title = viewModelEstablishmentDetail?.name
            self.ownerTableview.reloadData()
        }
    }
    
    var itemsEstablishments: [EstablishmentViewModel] = []
    
    var viewModelDefault: DefaultEstablishmentViewModel?
    var viewModelDefaultCancha: DefaultRentarCanchaViewModel?
    var viewModelDefaultCanchasOwner: DefaultCanchasOwnerViewModel?
    
    var defaultLongitud: CLLocationDegrees = -103.347768
    var defaultLatitude: CLLocationDegrees = 20.677376
    
    init(establishmentViewModel: EstablishmentViewModel) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
//        if !establishmentsListView.isHidden && detailMarkEstablishment.isHidden && self.yPlayerMapConstraint.constant != 80  {
//            search.becomeFirstResponder()
//        }
        
        if isOwner! {
            self.requestInfoDetail()
        } else {
            if let index = self.playerEstablishmentsTableView.indexPathForSelectedRow{
                self.playerEstablishmentsTableView.deselectRow(at: index, animated: true)
            }
            TranslucentNavBar()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .reloadProfileInfo
        , object: nil)
        
        // Do any additional setup after loading the view.
        self.searchContentView = self.view as? CustomOnlyCornerUIView
        self.search.delegate = self
        
        if defaults.object(forKey: "isGuest") as? Bool == true{
            isGuest = true
            isOwner = false
            
        } else {
            isGuest = false
            if defaults.object(forKey: "isOwner") != nil{
                isOwner = defaults.object(forKey: "isOwner") as? Bool
                if !isOwner! {
                    TranslucentNavBar()
                }
            }else{
                isOwner = false
                
            }
        }
        
        if(!isOwner! || isGuest){
            
            //establishmentsListView.alpha = 0
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            locationManager?.requestWhenInUseAuthorization()
            if (CLLocationManager.locationServicesEnabled()){
                locationManager?.startUpdatingLocation()
            }
            self.viewPlayerMap.layoutIfNeeded()
            isOwnerHidden(isOwnerHidden: isOwner!)
            playerMap = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.viewPlayerMap.frame.size.width, height: self.viewPlayerMap.frame.size.height), camera: GMSCameraPosition(latitude: locationManager?.location?.coordinate.latitude ?? defaultLatitude, longitude: locationManager?.location?.coordinate.longitude ?? defaultLongitud, zoom: 15.0))
            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
            playerMap?.mapType = .terrain
            playerMap?.padding = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
            viewPlayerMap.addSubview(playerMap!)
            playerMap?.delegate = self
            //addPanGestureRecognition()
            //imageIcon = creatingIconMarker()
            playerEstablishmentsTableView.tableFooterView = UIView()
            
            playerEstablishmentsTableView.isHidden = true
            searchViewSize()
            
        }else{
            
            locationManager?.stopUpdatingLocation()
            isOwnerHidden(isOwnerHidden: isOwner!)
            self.requestInfoDetail()
            
            ownerTableview.tableFooterView = UIView()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("here")
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
      if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        //playerEstablishmentsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 110, right: 0)
      }
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
      if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        playerEstablishmentsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
      }
    }
    
    func requestInfoDetail() {
        
        viewModelDefaultCancha = DefaultRentarCanchaViewModel(establishmentService: container.infoEstablishmentService())
        
        viewModelDefaultCancha?.requestEstablismentDetail(idPlace: UserData.shared?.user.establishments?[0].id ?? 0, completion: { [weak self] in
            self!.viewModelEstablishmentDetail = self!.viewModelDefaultCancha?.establishmentDetailViewModel
            
            }, {( message) in
                let alertError = HelperExtension.buildBasicAlertController(title: "Falló la conexión", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
        
    }
    
    func updateStatusField(establishmentId: Int, fieldId: Int, isActive: Bool){
        viewModelDefaultCanchasOwner = DefaultCanchasOwnerViewModel(ownerService: container.ownerEstablishmentService())
        
        viewModelDefaultCanchasOwner?.requestingUpdateStatusField(establishmentId: establishmentId, fieldId: fieldId, isActive: isActive, completion: { [weak self] (response) in
            
            if response{
                
            }else{
                
            }
            }, {( message) in
                let alertError = HelperExtension.buildBasicAlertController(title: "Alerta", message: message, handler: nil)
                self.present(alertError, animated: true, completion: nil)
        })
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        self.requestInfoDetail()
    }

    
    func requestLocation(){
       
        let status = CLLocationManager.authorizationStatus()

        switch status {
       
        case .notDetermined:
                locationManager?.requestWhenInUseAuthorization()
                return

        case .denied, .restricted:
            let alert = UIAlertController(title: "Servicio de ubicación deshabilitado", message: "Porfavor habilita el servicio de ubicación Ajustes de tu dispositivo", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)

            present(alert, animated: true, completion: nil)
            return
        case .authorizedWhenInUse:
            let camera = GMSCameraPosition.camera(withLatitude: (locationManager?.location?.coordinate.latitude)!, longitude: (locationManager?.location?.coordinate.longitude)!, zoom: 15.0)
            playerMap?.camera = camera
            playerMap?.isMyLocationEnabled = true
            
            break
        case .authorizedAlways:
            break

        }
    }

    func getLastCenterCoordinate() -> CLLocationCoordinate2D {
        // to get coordinate from CGPoint of your map
        let topCenterCoor = self.playerMap?.convert(CGPoint(x: (self.playerMap?.frame.size.width)!, y: (self.playerMap?.frame.size.height)!), to: self.playerMap)
        
        let point = self.playerMap?.projection.coordinate(for: topCenterCoor!)
        return point!
    }
    
    func setupMap(){
        //requesting info
        viewModelDefault = DefaultEstablishmentViewModel(establishmentService: container.establishmentService())
        viewModelDefault?.gettingEstablishmentData(center_lat: centerMap?.latitude ?? defaultLatitude, center_lng: centerMap?.longitude ?? defaultLongitud, border_lat: getLastCenterCoordinate().latitude, border_lng: getLastCenterCoordinate().longitude, search: "", completion: { [weak self] in
            self!.establishmentViewModel = self!.viewModelDefault?.establishmenViewModel
        })

    }
    
    func searchEstablishmentByWord(searchWord: String) {
        viewModelDefault = DefaultEstablishmentViewModel(establishmentService: container.establishmentService())
        viewModelDefault?.gettingEstablishmentData(center_lat: centerMap?.latitude ?? defaultLatitude, center_lng: centerMap?.longitude ?? defaultLongitud, border_lat: getLastCenterCoordinate().latitude, border_lng: getLastCenterCoordinate().longitude, search: searchWord, completion: { [weak self] in
            guard let self = self else { return }
            self.establishmentListViewModel = self.viewModelDefault?.establishmenViewModel
        })
    }
    
    func setupMarks(){
        if(self.establishmentViewModel!.count > 0){
            DispatchQueue.main.async {[unowned self] in
                var tempLocalMakers = [String]()
                for itemCancha in self.establishmentViewModel!{
                    
                    if self.localMarkers.contains(itemCancha.name) {
                       
                    }else{
                        
                        if self.localMarkers.count < 5{
                            self.setupCustomMarkerMap(item: itemCancha)
                            self.localMarkers.append(itemCancha.name)
                            tempLocalMakers.append(itemCancha.name)
                        }
                    }
                }
                self.localMarkers = tempLocalMakers
            }
        }else{
            self.playerMap?.clear()
            self.localMarkers.removeAll()
        }
    }
    
    
    func isMarkerWithinScreen(marker: GMSMarker) -> Bool {
        let region = (self.playerMap?.projection.visibleRegion())!
        let bounds = GMSCoordinateBounds(region: region)
        return bounds.contains(marker.position)
    }
    
    func setupCustomMarkerMap(item: EstablishmentViewModel){
        
        let marker = GMSMarker()
        
        //creating a marker view
        marker.position = CLLocationCoordinate2D(latitude: item.lat, longitude: item.lng)
        //marker.iconView = imageIcon
        marker.icon = createImageIcon()
        marker.userData = item
        if(isMarkerWithinScreen(marker: marker)){
            marker.map = playerMap
        }
    }
    
    func createImageIcon() -> UIImage{
        let markerImage = UIImage(named: "icn-pin")!.withRenderingMode(.alwaysOriginal)
        let markerView = self.imageWithImage(image: markerImage, scaledToSize: CGSize(width: 35.0, height: 35.0))
        return markerView
    }
//    func creatingIconMarker() -> UIImageView{
//        let markerImage = UIImage(named: "icn_pin_yellow")!.withRenderingMode(.alwaysTemplate)
//        let markerView = UIImageView(image: self.imageWithImage(image: markerImage, scaledToSize: CGSize(width: 130.0, height: 110.0)))
//        markerView.image = markerView.image?.withRenderingMode(.alwaysTemplate)
//        markerView.tintColor = Constants.yellowOnePrimaryColor
//        return markerView
//    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    override func GoToOwnerProfile() {
        
    }
    
    func searchViewSize(){
        
        if let textfield = search.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = UIColor.white
        }
        
        search.sizeToFit()
    }
    
    func isOwnerHidden(isOwnerHidden: Bool){
        viewOwner.isHidden = !isOwnerHidden
        viewPlayer.isHidden = isOwnerHidden
    }
    
    @IBAction func enableEditMode(_ sender: Any) {
        self.navigationItem.leftBarButtonItems?[1] = self.editButtonItem
    }
    
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ( segue.identifier == "detailRentCourt"){
            if let destinationVC = segue.destination as? DefaultRentarCanchasViewController {
                destinationVC.currentMarkerSelected = currentMarkerSelected
            }
        }else if(segue.identifier == "profileVC"){
            if let destinationVC = segue.destination as? ProfileViewController {
                destinationVC.currentServicesActivated = self.viewModelEstablishmentDetail?.services
            }
        }
//        else if(segue.identifier == "detailField"){
//            if let destinationVC = segue.destination as? NewFieldViewController {
//                destinationVC.fieldData = currentFieldSelected
//            }
//        }
            
    }
    
}

extension CanchasViewController: UISearchBarDelegate{
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        UIView.animate(withDuration: 1) {
            self.isSearching = false
//            self.detailMarkEstablishment.isHidden = true
//            self.yPlayerMapConstraint.constant = 80
            self.playerEstablishmentsTableView.isHidden = true
            self.establishmentListViewModel?.removeAll()
            self.playerEstablishmentsTableView.reloadData()
            searchBar.text = ""
            self.view.layoutIfNeeded()
        }
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        UIView.animate(withDuration: 1) {
            if (self.isSearching ?? false) {
                self.playerEstablishmentsTableView.isHidden = false
            }
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //self.establishmentsListView.isHidden = false
        if searchText.count > 1 {
            self.isSearching = true
        } else {
            self.isSearching = false
            UIView.animate(withDuration: 1) {
                self.playerEstablishmentsTableView.isHidden = true
            }
        }
        // validate string
        if searchText.count >= 3 {
            self.searchEstablishmentByWord(searchWord: searchText)
        } else {
            self.establishmentListViewModel?.removeAll()
            self.playerEstablishmentsTableView.reloadData()
        }
    }
}

extension CanchasViewController: GMSMapViewDelegate{
    // tap map marker
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let itemCancha = marker.userData as! EstablishmentViewModel
        currentMarkerSelected = itemCancha
        //push to DefaultRentarCanchasViewController
        performSegue(withIdentifier: "detailRentCourt", sender: nil)
        
        return true
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.centerMap = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        //print("didchange")
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        //print("end drag")
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        //print("did drag")
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("idleAt position")
        UIView.animate(withDuration: 1) {
            self.playerEstablishmentsTableView.isHidden = true
            self.search.resignFirstResponder()
        }
        setupMap()
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture){
            print("dragged")
            UIView.animate(withDuration: 1) {
                self.playerEstablishmentsTableView.isHidden = true
                self.search.resignFirstResponder()
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        //Coordinates where user has tapped
        
        //push to DefaultRentarCanchasViewController
        
        
//        UIView.animate(withDuration: 0.5) {
//                self.detailMarkEstablishment.isHidden = true
//                self.establishmentsListView.isHidden = true
//                self.yPlayerMapConstraint.constant = 80
//                self.view.layoutIfNeeded()
//            }
//        view.endEditing(true)
    }
}

extension CanchasViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
            return true
        }
}

extension CanchasViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = locations.last {
            DispatchQueue.main.async {[unowned self] in
                let center = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
                self.currentLocationUser = center
                var userLocation = UserLocation()
                userLocation.location = self.currentLocationUser
                UserData.sharedUserLocation = userLocation
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        DispatchQueue.main.async {[unowned self] in
            self.requestLocation()
        }
    }
}

extension CanchasViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == ownerTableview {
            let withContent = viewModelEstablishmentDetail?.fields.count ?? 0
            return withContent > 1 ? withContent : 1
        } else {
            let withContent = establishmentListViewModel?.count ?? 0
            if withContent > 0 {
                self.heightConstantEstablishmentTable.constant = CGFloat(withContent * 60)
                
                if CGFloat(withContent * 60) < self.viewPlayerMap.frame.height {
                    self.playerEstablishmentsTableView.isScrollEnabled = false
                } else {
                    self.playerEstablishmentsTableView.isScrollEnabled = true
                }
                
            } else {
                self.heightConstantEstablishmentTable.constant = 80
            }
            return withContent > 1 ? withContent : 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == ownerTableview {
            if viewModelEstablishmentDetail?.fields.count != 0 && viewModelEstablishmentDetail?.fields.count != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CanchasOwnerTableViewCell

                cell.switchActiveField.tag = indexPath.row
                cell.switchActiveField.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
                cell.viewModelField = viewModelEstablishmentDetail?.fields[indexPath.row]
                tableView.rowHeight = 91
                return cell
            } else {
                let emptyCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.emptyCell.rawValue, for: indexPath) as! EmptyTableViewCell
                tableView.rowHeight = 410
                emptyCell.messageLabel.text = "Crea una cancha!"
                return emptyCell
            }
        } else {
            if establishmentListViewModel?.count != 0 && establishmentListViewModel?.count != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! EstablismentTableViewCell
                tableView.rowHeight = 60
                cell.viewModelField = establishmentListViewModel?[indexPath.row]
                return cell
            } else {
                let emptyCell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.emptyCell.rawValue, for: indexPath) as! EmptyTableViewCell
                
                if !(self.isSearching ?? false) {
                    tableView.rowHeight = 80
                    emptyCell.messageLabel.text = "¡Busca la cancha donde quieres jugar!"
                } else {
                    tableView.rowHeight = 80
                    emptyCell.messageLabel.text = "No hay resultados en esta busqueda"
                }
                
                return emptyCell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == playerEstablishmentsTableView {
            currentMarkerSelected = establishmentListViewModel![indexPath.row]
            performSegue(withIdentifier: "detailRentCourt", sender: nil)
        } else if tableView == ownerTableview {
            if viewModelEstablishmentDetail?.fields.count != 0 && viewModelEstablishmentDetail?.fields.count != nil {
                currentFieldSelected = viewModelEstablishmentDetail?.fields[indexPath.row]
                //performSegue(withIdentifier: "detailField", sender: nil)
                
                if let vc = UIStoryboard(name: "NewField", bundle: nil).instantiateViewController(withIdentifier: "NewFieldViewController") as? NewFieldViewController
                {
                    
                    vc.fieldData = currentFieldSelected
                    self.navigationController?.pushViewController(vc, animated: true)
                    //vc.modalPresentationStyle = .fullScreen
                    //present(vc, animated: true, completion: nil)
                }
            } else {
//                guard let vc = UIStoryboard(name: "NewField", bundle: nil).instantiateViewController(withIdentifier: "NewFieldViewController") as? NewFieldViewController
//                    else { return }
//
//                vc.fieldData = currentFieldSelected
//                self.navigationController?.pushViewController(vc, animated: true)
//
                
            }
        }
        
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        if tableView == ownerTableview {
//            return 91
//        } else {
//            if establishmentViewModel?.count != 0 && establishmentViewModel?.count != nil {
//                return 80
//            } else {
//                return 410
//            }
//        }
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if tableView == ownerTableview {
//            return 91
//        } else {
//            if establishmentViewModel?.count != 0 && establishmentViewModel?.count != nil {
//                return 80
//            } else {
//                return 410
//            }
//        }
//    }
    
    @objc func switchChanged(_ sender : UISwitch!){
        
        //print("field \(viewModelEstablishmentDetail?.fields[sender.tag].name)")
        //print("table row switch Changed \(sender.tag)")
        //print("The switch is \(sender.isOn ? "ON" : "OFF")")
        
        updateStatusField(establishmentId: viewModelEstablishmentDetail!.id, fieldId: (viewModelEstablishmentDetail?.fields[sender.tag].id)!, isActive: sender.isOn)
        
    }
    
    
}

extension Notification.Name {
    static let reloadProfileInfo = Notification.Name("reloadProfileInfo")
    static let reservationReady = Notification.Name("reservationReady")
    static let reservationFailed = Notification.Name("reservationFailed")
}
