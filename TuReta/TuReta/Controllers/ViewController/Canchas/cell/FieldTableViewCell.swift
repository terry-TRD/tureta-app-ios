//
//  FieldTableViewCell.swift
//  TuReta
//
//  Created by DC on 06/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class FieldTableViewCell: UITableViewCell {

    @IBOutlet weak var fieldName: UILabel!
    @IBOutlet weak var fieldTypeName: UILabel!
    @IBOutlet weak var fieldAddress: UILabel!
    
    @IBOutlet weak var globalView: CustomCornerAndBorderUIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
