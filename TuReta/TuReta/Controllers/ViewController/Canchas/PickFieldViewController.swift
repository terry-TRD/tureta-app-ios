//
//  PickFieldViewController.swift
//  TuReta
//
//  Created by DC on 06/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class PickFieldViewController: TuRetaBaseViewController {
    
    @IBOutlet weak var table: UITableView!

    var fieldsArray: [Field] = []
    var indexSelected: IndexPath?
    var fieldSelected: Field?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Elegir una cancha"
        table.register(UINib(nibName: CellResusableIdentifier.fieldCell.rawValue, bundle: nil), forCellReuseIdentifier: CellResusableIdentifier.fieldCell.rawValue)
        
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
        }
        if isOwner!{
            getActiveFields()
        } else{
            getActiveFieldsPlayer()
        }
    }
    
    func getActiveFields() {
        guard let fields = UserData.sharedEstablishment?.fields else { return }
    
        let _ = fields.compactMap {
            if $0.is_active {
                fieldsArray.append($0)
            }
        }
    }
    
    func getActiveFieldsPlayer() {
        
        fieldsArray = fieldsArray.filter {
//            if $0.is_active {
//                fieldsArray.append($0)
//            }
            return $0.is_active
        }
    }
    
    @IBAction func pickFieldButtonSelected(_ sender: Any) {
        // look who is de previews viewcontroller and send field selected
        
        
        if let i = navigationController?.viewControllers.firstIndex(of: self) {
            guard let presenter = navigationController?.viewControllers[i - 1] as? EditFixtureViewController else {
                
                if let presenter = navigationController?.viewControllers[i - 1] as? PickScheduleViewController {
                    presenter.fieldSelected?.id = fieldSelected?.id as! Int
                    presenter.fieldSelected = fieldSelected
                    self.navigationController?.popViewController(animated: true)
                }
                
                if let presenter = navigationController?.viewControllers[i - 1] as? ManualReservationViewController {
                    presenter.fieldSelected?.id = fieldSelected?.id as! Int
                    presenter.fieldSelected = fieldSelected
                    self.navigationController?.popViewController(animated: true)
                }
                
                return
            }
            
            presenter.viewModel?.fieldID = fieldSelected?.id as! Int
            presenter.fieldSelected = fieldSelected
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension PickFieldViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 121.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedIndex = indexSelected else {
            let cell: FieldTableViewCell = tableView.cellForRow(at: indexPath) as! FieldTableViewCell
            cell.globalView.borderColor = Constants.greenOnePrimaryColor
            cell.globalView.borderWidth = 2
            indexSelected = indexPath
            fieldSelected = fieldsArray[indexPath.row]
            return
        }
        
        if selectedIndex != indexPath {
        let cell: FieldTableViewCell = tableView.cellForRow(at: indexPath) as! FieldTableViewCell
        cell.globalView.borderColor = Constants.greenOnePrimaryColor
        cell.globalView.borderWidth = 2
        fieldSelected = fieldsArray[indexPath.row]
        
        let oldCell: FieldTableViewCell = tableView.cellForRow(at: selectedIndex) as! FieldTableViewCell
        oldCell.globalView.borderColor = .white
        oldCell.globalView.borderWidth = 1
            
        indexSelected = indexPath
        }
    }
}

extension PickFieldViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellResusableIdentifier.fieldCell.rawValue, for: indexPath) as? FieldTableViewCell else {
            return UITableViewCell()
        }
        
        cell.fieldName.text = "\(fieldsArray[indexPath.row].name)"
        cell.globalView.borderColor = indexSelected == indexPath ? Constants.greenOnePrimaryColor : .white
        
        return cell
    }
}

