//
//  LoginViewController.swift
//  TuReta
//
//  Created by DC on 9/6/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import AuthenticationServices

import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging

class LoginViewController: TuRetaBaseViewController {

    // MARK: Local variables
    
    @IBOutlet weak var loginButton: CustomCornerButton!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var loginFacebookView: UIView!
    
    @IBOutlet weak var ForgotPassword: UIButton!
    @IBOutlet weak var privacyButton: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var appleLoginView: UIView!
    
    @IBOutlet weak var heightAppleLoginView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        UIApplication.shared.applicationIconBadgeNumber = 0
        setCloseKeyboard()
        passwordTxt.delegate = self
        setupTextFields()
        loginFacebookView.layoutIfNeeded()
        validateUserInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        // Transparent navigation bar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        loginButton.backgroundColor = Constants.greenOnePrimaryColor
        
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        ForgotPassword.titleLabel?.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        ForgotPassword.titleLabel?.adjustsFontForContentSizeCategory = true
        
        privacyButton.titleLabel?.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        privacyButton.titleLabel?.adjustsFontForContentSizeCategory = true
        
        loginButton.titleLabel?.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        loginButton.titleLabel?.adjustsFontForContentSizeCategory = true
        
        emailTxt.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        emailTxt.adjustsFontForContentSizeCategory = true
        
        passwordTxt.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        passwordTxt.adjustsFontForContentSizeCategory = true
        
        if #available(iOS 13.0, *) {
            setupAppleLoginButton()
            heightAppleLoginView.constant = 50
        } else {
            heightAppleLoginView.constant = 0
        }
        
        setupFacebookLoginButton()
        
    }
    
    func validateUserInfo(){
        let defaults = UserDefaults.standard
        if defaults.object(forKey: "email") == nil {
            presentTutorialViewController()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupTextFields(){
        guard let emailImage = UIImage(named: ImageName.type.iconEmail.rawValue) else { return }
        guard let passwordImage = UIImage(named: ImageName.type.iconPassword.rawValue) else { return }
        
        emailTxt.setLeftIcon(emailImage)
        passwordTxt.setLeftIcon(passwordImage)
        
        emailTxt.changePlaceholderColor(text: "Correo electrónico", andColor: .gray)
        passwordTxt.changePlaceholderColor(text: "Contraseña", andColor: .gray)
    }
    
    func changePlaceholderColr(txtField: UITextField, andText text: String, andColor color: UIColor) {
        txtField.attributedPlaceholder =
        NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor : color])
    }
    
    func setupFacebookLoginButton() {
        let loginButton = FBLoginButton()
        loginButton.delegate = self
        loginButton.center = loginFacebookView.center
        loginButton.frame = CGRect(x: 0, y: 0, width: loginFacebookView.frame.size.width, height: loginFacebookView.frame.size.height)

        loginFacebookView.addSubview(loginButton)
        
        
        
        if (AccessToken.current != nil) {
            loginWithFacebook()
        }else{
            loginButton.permissions = ["public_profile", "email"]
        }
    }
    
    func setupAppleLoginButton() {
        if #available(iOS 13.0, *) {
            
            let appleLogin = ASAuthorizationAppleIDButton()
            appleLogin.translatesAutoresizingMaskIntoConstraints = false
            appleLogin.center = appleLoginView.center
            appleLogin.addTarget(self, action: #selector(handleLogInWithAppleIDButtonPress), for: .touchUpInside)
            appleLoginView.addSubview(appleLogin)
            NSLayoutConstraint.activate([
                appleLogin.heightAnchor.constraint(equalToConstant: 50),
                appleLogin.widthAnchor.constraint(equalToConstant: appleLoginView.frame.width),
                appleLogin.leadingAnchor.constraint(equalTo: appleLoginView.leadingAnchor, constant: 0),
                appleLogin.trailingAnchor.constraint(equalTo: appleLoginView.trailingAnchor, constant: 0)
            ])
        } else {
            heightAppleLoginView.constant = 0
        }
        
    }
    
    @available(iOS 13.0, *)
    @objc func handleLogInWithAppleIDButtonPress() {
        //performExistingAccountSetupFlows()
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @available(iOS 13.0, *)
    private func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
        
        // Create an authorization controller with the given requests.
        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        validateEmail()
    }
    
    @IBAction func continueWithoutLogin(_ sender: Any) {
        self.initWithoutCredentials()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        appDelegate.presentHomeVC()
    }
    
    
    //MARK: validation
    func validateEmail(){
        // email
        guard let email = emailTxt.text else{
            return
        }
        if !HelperExtension.isValidEmail(emailID: email){
            // message error
            let alertError = HelperExtension.buildBasicAlertController(title: "Correo invalido", message: "Ingresa un correo valido", handler: nil)
            self.present(alertError, animated: true, completion: nil)
        }
        
        // password
        guard let password = passwordTxt.text, passwordTxt.text?.count != 0, passwordTxt.text!.count > 6  else {
            // message error
            let alertError = HelperExtension.buildBasicAlertController(title: "", message: "Ingresa una contraseña valida con mas de 6 caracteres", handler: nil)
            self.present(alertError, animated: true, completion: nil)
            return
        }
        
        getLoginRequest(email:email , password: password, userName: nil)
    }
    
    func getLoginRequest(email: String, password: String, userName: String?){
        
        LoginWS.loginWS(email: email, password: password, userName: userName, completionHandler: {[weak self] (user) in
            guard let self = self else { return }
            UserData.shared = user
            let isOwner: Bool!
            if((user?.user.establishments?.count)! > 0 && (user?.user.establishments != nil)){
                isOwner = true
            }else{
                isOwner = false
            }
            
            self.initDefaults(email: (self.emailTxt!.text!), password: (self.passwordTxt!.text!), isOwner: isOwner, token: user!.access_token)
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            // check if is user or admi
            if(isOwner){
                print("enviar a dueño de cancha")
                appDelegate.presentHomeOwner()
            }else{
                appDelegate.presentHomeVC()
            }
            
            Messaging.messaging().subscribe(toTopic: "user\(user?.user.id ?? 0)") { error in
                print("Subscribed to \(user?.user.id ?? 0) topic")
            }
        }, { (networkError, message) in
            print(networkError!, message)
            let alertError = HelperExtension.buildBasicAlertController(title: "", message: message, handler: nil)
            self.present(alertError, animated: true, completion: nil)
        })
    }
    
    
    func initDefaults(email: String, password: String, isOwner: Bool, token: String){
        let defaults = UserDefaults.standard
        defaults.set(email, forKey: "email")
        defaults.set(token, forKey: "token")
        defaults.set(password, forKey: "password")
        defaults.set(isOwner, forKey: "isOwner")
        defaults.set(false, forKey: "isGuest")
        defaults.synchronize()
        
    }
    
    func initWithoutCredentials() {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "isGuest")
        defaults.synchronize()
    }
    
    func saveFacebookTokenDefaults(token: String) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "facebookToken")
        defaults.synchronize()
    }
    
    func saveAppleTokenDefaults(token: String, userID: String, email: String) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "appleToken")
        defaults.set(userID, forKey: "userID")
        defaults.set(email, forKey: "appleEmail")
        defaults.synchronize()
    }
    
//    private func setCloseKeyboard() {
//        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard))
//        tapGesture.cancelsTouchesInView = false
//        self.view.addGestureRecognizer(tapGesture)
//        
//    }
//    @objc func closeKeyboard() {
//        
//        view.endEditing(true)
//        
//    }
    
    func loginWithFacebook(){
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields": "email"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in

            if ((error) != nil)
            {
                // Process error
               // TODO
            }
            else
            {
                guard let userInfo = result as? [String: Any] else { return }
                var emailFacebook: String = ""
                if let email = userInfo["email"] as? String {
                    emailFacebook = email
                }else{
                    emailFacebook = "\(userInfo["id"] as! String)@facebook.com)"
                }
                self.saveFacebookTokenDefaults(token: AccessToken.current!.tokenString)
                self.getLoginRequest(email: emailFacebook, password: "", userName: nil)
            }
        })
    }

}

extension LoginViewController: UITextFieldDelegate{
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() //Dismiss keyboard
        self.validateEmail()
        return true
    }
}

extension LoginViewController: LoginButtonDelegate{
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("Successfully logout")
    }

    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if error != nil{
            // TODO
            // something when wrong alert
            return
        }else if result!.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if (result?.grantedPermissions.contains("email"))!
            {
                // Do work
                print("Successfully login")
                print("\(AccessToken.current?.tokenString)")
                loginWithFacebook()
            }

            
        }
        
    }
}

extension LoginViewController : ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        let alert = UIAlertController(title: "Error", message: "Tuvimos problemas al iniciar con tu cuenta de Apple", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            print("User Name - \(appleIDCredential.fullName?.description ?? "N/A")")
            let userIdentifier = appleIDCredential.user
            
            let defaults = UserDefaults.standard
            if defaults.object(forKey: "userID") != nil {
                let userEmail = defaults.object(forKey: "appleEmail") as! String
                if let identityTokenData = appleIDCredential.identityToken,
                    let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
                    self.saveAppleTokenDefaults(token: identityTokenString, userID: userIdentifier, email: userEmail)
                }
                self.getLoginRequest(email: userEmail, password: "", userName: nil)
                
            } else {
                
                
                let email = appleIDCredential.email
                var fullName: String = ""
                guard let givenName = appleIDCredential.fullName?.givenName,
                    let familyName = appleIDCredential.fullName?.familyName else {
                        return
                }
                fullName = "\(givenName) \(familyName)"
                
                guard let appleEmail = email else {
                    let alert = UIAlertController(title: "Error", message: "Tuvimos problemas al iniciar con tu cuenta de Apple", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                    
                }
                if let identityTokenData = appleIDCredential.identityToken,
                    let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
                   
                    self.saveAppleTokenDefaults(token: identityTokenString, userID: userIdentifier, email: appleEmail)
                }
                
                self.getLoginRequest(email: appleEmail, password: "", userName: fullName)
            }
        }
    }
    
}

extension LoginViewController : ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

extension LoginViewController {
    func presentTutorialViewController() {
        guard let tutorialVC = UIStoryboard(name: "OnboardingScreen", bundle: nil).instantiateViewController(withIdentifier: "OnboardingScreen") as? OnboardingScreen else { return }
        tutorialVC.modalPresentationStyle = .fullScreen
        tutorialVC.onboardingDelegate = self
        self.present(tutorialVC, animated: true, completion: nil)
    }
}

extension LoginViewController: OnboardingActionDelegate {
    func goTocreateUserPage() {
        performSegue(withIdentifier: "register", sender: nil)
    }
    
}
