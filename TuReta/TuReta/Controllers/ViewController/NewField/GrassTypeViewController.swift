//
//  GrassTypeViewController.swift
//  TuReta
//
//  Created by DC on 03/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class GrassTypeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let grassTypes: [String] = ["Cancha natural", "Cancha sintetica"]
    
    var grassTypeSelected: Int? // not needed
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
}

extension GrassTypeViewController: UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}

extension GrassTypeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return grassTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BasicOptionTableViewCell", for: indexPath) as? BasicOptionTableViewCell else {
            return UITableViewCell()
        }
        
        tableView.rowHeight = 55
        cell.nameLabel.text = grassTypes[indexPath.row]
        
        // not needed
//        if grassTypeSelected != nil && grassTypeSelected == indexPath.row{
//            cell.imageIcon.image = UIImage(named: "icn-check")
//        } else {
//            cell.imageIcon.image = nil
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let navController = self.navigationController {
            guard let presenter = navController.viewControllers[0] as? NewFieldViewController else {
                if let presenter = navController.viewControllers[1] as? NewFieldViewController {
                    presenter.grassTypeSelected = indexPath.row
                    self.navigationController?.popViewController(animated: true)
                }
                
                return
            }
            
            presenter.grassTypeSelected = indexPath.row
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
