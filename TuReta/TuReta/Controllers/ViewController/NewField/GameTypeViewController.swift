//
//  GameTypeViewController.swift
//  TuReta
//
//  Created by DC on 04/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class GameTypeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let gameTypes: [String] = [HelperExtension.GameType(type: 0), HelperExtension.GameType(type: 1)]
    
    var gameTypeSelected: Int?
    var gameTypeAvailability: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Tipo de juego"
        // Do any additional setup after loading the view.
        
    }
    
}

extension GameTypeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}

extension GameTypeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if gameTypeAvailability.count > 0 {
            return gameTypeAvailability.count
        } else {
            return gameTypes.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BasicOptionTableViewCell", for: indexPath) as? BasicOptionTableViewCell else {
            return UITableViewCell()
        }
        
        if gameTypeAvailability.count > 0 {
            cell.nameLabel.text = gameTypeAvailability[indexPath.row]
        } else {
            cell.nameLabel.text = gameTypes[indexPath.row]
        }
        if gameTypeSelected != nil && gameTypeSelected == indexPath.row{
            cell.imageIcon.image = UIImage(named: "icn-check")
        } else {
            cell.imageIcon.image = nil
        }
        // what type is gameTypeSelected
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let navController = self.navigationController {
            guard let presenter = navController.viewControllers[0] as? NewFieldViewController else {
                if let presenter = navController.viewControllers[1] as? NewFieldViewController {
                    presenter.gameTypeSelected = indexPath.row
                    self.navigationController?.popViewController(animated: true)
                    
                    return
                }
                
                if let presenter = navController.viewControllers[1] as? ManualReservationViewController {
                    
                    for (index, item) in gameTypes.enumerated() {
                        if gameTypeAvailability[indexPath.row] == item {
                            presenter.gameTypeSelected = index
                        }
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    return
                }
                
                if let presenter = navController.viewControllers[1] as? NewTournamentViewController {
                    presenter.gameTypeSelected = indexPath.row
                    self.navigationController?.popViewController(animated: true)
                    
                    return
                }
                
                
                if let presenter = navController.viewControllers[2] as? EditTorneosViewController {
                    presenter.gameTypeSelected = indexPath.row
                    self.navigationController?.popViewController(animated: true)
                    
                    return 
                }
                
                if let presenter = navController.viewControllers[2] as? PickScheduleViewController {
                    var typeSelected: Int = 0
                    if gameTypeAvailability[indexPath.row] == HelperExtension.GameType(type: 0) {
                        typeSelected = HelperExtension.GameTypeSelected(type: 0)
                    } else if gameTypeAvailability[indexPath.row] == HelperExtension.GameType(type: 1){
                        typeSelected = HelperExtension.GameTypeSelected(type: 1)
                    }
                    
                    presenter.gameTypeSelected = typeSelected
                    self.navigationController?.popViewController(animated: true)
                    
                    return
                }
                
                if let presenter = navController.viewControllers[3] as? PickScheduleViewController {
                    var typeSelected: Int = 0
                    if gameTypeAvailability[indexPath.row] == HelperExtension.GameType(type: 0) {
                        typeSelected = HelperExtension.GameTypeSelected(type: 0)
                    } else if gameTypeAvailability[indexPath.row] == HelperExtension.GameType(type: 1){
                        typeSelected = HelperExtension.GameTypeSelected(type: 1)
                    }
                    
                    presenter.gameTypeSelected = typeSelected
                    self.navigationController?.popViewController(animated: true)
                    
                    return
                }
                
                return
            }
            
            presenter.gameTypeSelected = indexPath.row
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
