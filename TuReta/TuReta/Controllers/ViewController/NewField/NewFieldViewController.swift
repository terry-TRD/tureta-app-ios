//
//  NewFieldViewController.swift
//  TuReta
//
//  Created by DC on 29/02/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

struct NewField {
    var name: String
    var gameType: Int
    var grassType: Int
    var isActive: Bool
    var priceType: Int
    var priceHour: String
    var priceHalf: String
    var fieldID: Int
    
    init(name: String, gameType: Int, grassType: Int, isActive: Bool, priceType: Int, priceHour: String, priceHalf: String, fieldID: Int) {
        self.name = name
        self.gameType = gameType
        self.grassType = grassType
        self.isActive = isActive
        self.priceType = priceType
        self.priceHour = priceHour
        self.priceHalf = priceHalf
        self.fieldID = fieldID
    }
}

class NewFieldViewController: TuRetaBaseViewController {

    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var oneHourText: UITextField!
    @IBOutlet weak var halfHourText: UITextField!
    @IBOutlet weak var grassTypeButton: UIButton!
    @IBOutlet weak var gameTypeButton: UIButton!
    @IBOutlet weak var switchState: UISwitch!
    
    @IBOutlet weak var createButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var createFieldButton: UIButton!
    @IBOutlet weak var updateFieldButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    
    var viewModel: NewFieldViewModel?
    let container = TuRetaContainer()
    var fieldData: Field?
    var grassTypeSelected: Int = 0 {
        willSet(newValue) {
            self.grassTypeSelected = newValue
            grassTypeButton.setTitle(HelperExtension.GrassType(type: grassTypeSelected), for: .normal)
        }
        didSet {}
    }
    var gameTypeSelected: Int = 0 {
        willSet(newValue) {
            self.gameTypeSelected = newValue
            gameTypeButton.setTitle(HelperExtension.GameType(type: gameTypeSelected), for: .normal)
        }
        didSet {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if fieldData != nil {
            removeButton.isHidden = false
            createFieldButton.isHidden = true
            createButtonConstraint.constant = 0
            updateFieldButton.isHidden = false
            self.navigationItem.rightBarButtonItem = nil
            //self.navigationItem.rightBarButtonItem? = UIBarButtonItem(title: "Actualizar", style: .plain, target: self, action: #selector(closeView))
            self.navigationItem.title = fieldData?.name
            
            self.nameText.text = fieldData?.name
            self.switchState.isOn = fieldData?.is_active ?? false
            self.grassTypeButton.setTitle(HelperExtension.GrassType(type: (fieldData?.grass_type ?? 0) - 1), for: .normal)
            let gameType = HelperExtension.GameTypeSelected(type: Int(fieldData?.game_types ?? "") ?? 0)
            self.gameTypeButton.setTitle(HelperExtension.GameType(type: gameType) , for: .normal)
            self.oneHourText.text = "$\(fieldData?.prices?.first?.price_per_hour ?? "")"
            self.halfHourText.text = "$\(fieldData?.prices?.first?.price_per_half ?? "")"
            
        } else {
            removeButton.isHidden = true
            createFieldButton.isHidden = false
            createButtonConstraint.constant = 50
            updateFieldButton.isHidden = true
        }
        
        setupTextField()
        self.view.layoutIfNeeded()
        
        viewModel = NewFieldViewModel(ownerService: container.ownerEstablishmentService())
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func setupTextField(){
        nameText.changePlaceholderColor(text: "Nombre", andColor: .gray)
        oneHourText.changePlaceholderColor(text: "Costo por hora", andColor: .gray)
        halfHourText.changePlaceholderColor(text: "Costo por media hora", andColor: .gray)
        
        nameText.setBottomLine(color: UIColor.white)
        oneHourText.setBottomLine(color: UIColor.white)
        halfHourText.setBottomLine(color: UIColor.white)
    }
    
    @IBAction func updateInfo() {
        guard let field = fieldData else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        guard let nameField = nameText.text,
            let pricePerHour = oneHourText.text,
            let priceHalfHour = halfHourText.text else {
                return
        }
        
        // TODO LOADER
        let paramsField = NewField(name: nameField, gameType: gameTypeSelected == 0 ? GameType.seven : GameType.eleven, grassType: grassTypeSelected == 0 ? GrassType.FIELD_GRASS_TYPE_NATURAL : GrassType.FIELD_GRASS_TYPE_SYNTHETIC, isActive: switchState.isOn, priceType: gameTypeSelected == 0 ? GameType.seven : GameType.eleven, priceHour: String(pricePerHour.removeFormatAmount()), priceHalf: String(priceHalfHour.removeFormatAmount()), fieldID: field.id )
        viewModel?.updateField(params: paramsField, completion: { [weak self] (response) in
            if response{
                print("field updated")
                self?.navigationController?.popViewController(animated: true)
                
            }else{
                print("failed")
            }
            
            }, {( message) in
                
        })
        
    }
    
    @IBAction func createNewField() {
        // TODO LOADER
        guard let nameField = nameText.text,
            let pricePerHour = oneHourText.text,
            let priceHalfHour = halfHourText.text else {
                return
        }
        
        let paramsField = NewField(name: nameField, gameType: gameTypeSelected == 0 ? GameType.seven : GameType.eleven, grassType: grassTypeSelected == 0 ? GrassType.FIELD_GRASS_TYPE_NATURAL : GrassType.FIELD_GRASS_TYPE_SYNTHETIC, isActive: switchState.isOn, priceType: gameTypeSelected == 0 ? GameType.seven : GameType.eleven, priceHour: String(pricePerHour.removeFormatAmount()), priceHalf: String(priceHalfHour.removeFormatAmount()), fieldID: fieldData?.id ?? 0 )
        viewModel?.createNewField(params: paramsField, completion: { [weak self] (response) in
            if response{
                print("new field created")
                self?.dismiss(animated: true, completion: nil)
                
            }else{
                print("failed")
            }
            
            }, {( message) in
                
        })
    }
    
    @IBAction func removeField() {
        //TODO show alert
        viewModel = NewFieldViewModel(ownerService: container.ownerEstablishmentService())
        viewModel?.removeField(fieldID: fieldData?.id ?? 0, completion: { [weak self] (response) in
            if response{
                print("removed field")
                //self?.dismiss(animated: true, completion: nil)
                self?.fieldData = nil
                self?.navigationController?.popViewController(animated: true)
                
            }else{
                print("failed")
            }
            
            }, {( message) in
                
        })
        
    }
}

//    @IBAction func buttonOnClick(_ sender: UIButton)
//    {
//
//        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
//            self.openCamera()
//        }))
//
//        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
//            self.openGallary()
//        }))
//
//        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
//
//        /*If you want work actionsheet on ipad
//        then you have to use popoverPresentationController to present the actionsheet,
//        otherwise app will crash on iPad */
//        switch UIDevice.current.userInterfaceIdiom {
//        case .pad:
//            alert.popoverPresentationController?.sourceView = sender
//            alert.popoverPresentationController?.sourceRect = sender.bounds
//            alert.popoverPresentationController?.permittedArrowDirections = .up
//        default:
//            break
//        }
//
//        self.present(alert, animated: true, completion: nil)
//    }
//
//    func openCamera()
//    {
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = UIImagePickerController.SourceType.camera
//            imagePicker.allowsEditing = false
//            self.present(imagePicker, animated: true, completion: nil)
//        }
//        else
//        {
//            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
//    }
//
//    func openGallary()
//    {
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.allowsEditing = true
//            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
//            self.present(imagePicker, animated: true, completion: nil)
//        }
//        else
//        {
//            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
//    }
//
//}
//
//extension NewFieldViewController: UIImagePickerControllerDelegate {
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        if let pickedImage = info[.originalImage] as? UIImage {
//            imageViewPic.contentMode = .scaleToFill
//            imageViewPic.image = pickedImage
//            imageButtonPick.setTitle("", for: .normal)
//        }
//        picker.dismiss(animated: true, completion: nil)
//    }
//}
//
//extension NewFieldViewController: UINavigationControllerDelegate {
//
//}

extension NewFieldViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == oneHourText {
            oneHourText.text = "$"
        } else if textField == halfHourText {
            halfHourText.text = "$"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == oneHourText {
            return range.location == 0 ? false : true
        }else if textField == halfHourText {
            return range.location == 0 ? false : true
        }else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() //Dismiss keyboard
        
        setCloseKeyboard()
        
        return true
    }
}

//extension NewFieldViewController: UITextViewDelegate {
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        if descriptionTextView.textColor == Constants.blackThreePrimaryColor {
//            descriptionTextView.text = nil
//            descriptionTextView.textColor = Constants.blackOnePrimaryColor
//        }
//    }
//    func textViewDidEndEditing(_ textView: UITextView) {
//        if textView.text.isEmpty {
//            textView.text = "Descripción de la cancha"
//            textView.textColor = UIColor.lightGray
//        }
//    }
//}
