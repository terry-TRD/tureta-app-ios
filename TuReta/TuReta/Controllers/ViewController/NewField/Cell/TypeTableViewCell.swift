//
//  GrassTypeTableViewCell.swift
//  TuReta
//
//  Created by DC on 03/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class TypeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
