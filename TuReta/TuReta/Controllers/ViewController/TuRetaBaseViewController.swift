//
//  TuRetaBaseViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 10/15/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import Lottie

class TuRetaBaseViewController: UIViewController {
    
    var checkMarkAnimation = AnimationView()
    weak var delegate: CanchasViewControllerDelegate?
    let hostNames = [nil, "google.com", "invalidhost"]
    var hostIndex = 0
    
    var animationView = CustomCornerAndBorderUIView()
    var myView = UIView()
    var isOwner: Bool?
    let defaults = UserDefaults.standard
    let networkStatus = NetworkClient()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setCloseKeyboard()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.networkViewReachability()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
            //if !isOwner!{ TranslucentNavBar() }
            if defaults.object(forKey: "isGuest") as? Bool == true{
                TranslucentNavBar()
            }
            
            
        }else{
            isOwner = false
            TranslucentNavBar()
        }
        
        if let _ = self.navigationController {
        // Add label to subview hierarchy
        for subview in self.navigationController!.navigationBar.subviews {
            let stringFromClass = NSStringFromClass(subview.classForCoder)
            if stringFromClass.contains("UINavigationBarLargeTitleView") {

                let largeSubViews = subview.subviews
                for sbv in largeSubViews
                {
                    if let lbl = sbv as? UILabel
                    {
                        lbl.setNeedsLayout()
                    }
                }
            }
        }

        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont(name: "Poppins-SemiBold", size: 26)!
        ]
        
        self.navigationController!.navigationBar.largeTitleTextAttributes = attrs
        } else { return }

        self.myView.isHidden = true
        
        
//        networkStatus.reachabilityManager?.listener = { status in
//            if self.networkStatus.reachabilityManager?.isReachable ?? false {
//                
//                switch status {
//                    
//                case .reachable(.ethernetOrWiFi):
//                    
//                    self.myView.isHidden = true
//                    self.myView.alpha = 0
//                    
//                case .reachable(.wwan):
//                    
//                    self.myView.isHidden = true
//                    self.myView.alpha = 0
//                    
//                case .notReachable:
//                    
//                    self.myView.isHidden = false
//                    self.myView.alpha = 1
//                    
//                case .unknown :
//                    
//                    self.myView.isHidden = true
//                    self.myView.alpha = 0
//                    
//                }
//            }else{
//                
//                self.myView.isHidden = false
//                self.myView.alpha = 1
//            }
//            
//        }
//        
//        networkStatus.reachabilityManager?.startListening()
        
    }
    
    func playAnimation(animationName: String) {
        removeAnimationView()
        createAnimationView()
        checkMarkAnimation = AnimationView(name: animationName)
        checkMarkAnimation.contentMode = .scaleAspectFit
        self.animationView.addSubview(checkMarkAnimation)
        checkMarkAnimation.frame = self.animationView.bounds
        checkMarkAnimation.centerXAnchor.constraint(equalTo: animationView.centerXAnchor).isActive = true
        checkMarkAnimation.centerYAnchor.constraint(equalTo: animationView.centerYAnchor).isActive = true
        checkMarkAnimation.loopMode = .loop
        checkMarkAnimation.play()
    }
    
    func stopAnimation() {
        checkMarkAnimation.stop()
        checkMarkAnimation.removeFromSuperview()
        self.removeAnimationView()
    }
    
    func createAnimationView() {
        self.animationView = CustomCornerAndBorderUIView(frame:self.view.frame)
        self.animationView.cornerRadius = CGFloat(10)
        //self.animationView.backgroundColor = Constants.blackOnePrimaryColor
        self.animationView.alpha = 1
        if UIDevice.current.hasNotch {
            self.animationView.frame = CGRect(x: self.view.center.x, y: self.view.center.y, width: self.view.frame.width - 30, height: 200)
        }else{
            self.animationView.frame = CGRect(x: self.view.center.x, y: self.view.center.y + 11, width: self.view.frame.width - 30, height: 200)
        }
        self.animationView.center = self.view.center
        self.view.addSubview(animationView)
    }
    
    func removeAnimationView() {
        self.animationView.removeFromSuperview()
    }
    
    func networkViewReachability(){
        self.myView = UIView(frame:self.view.frame)
        self.myView.backgroundColor = .red
        if UIDevice.current.hasNotch {
            self.myView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 104)
        }else{
            self.myView.frame = CGRect(x: 0, y: 22, width: self.view.frame.width, height: 85)
        }
        self.myView.isHidden = true
        self.myView.alpha = 0
        self.view.addSubview(myView)
    }
    
    func TranslucentNavBar(){
        self.navigationController?.isNavigationBarHidden = false
        // Transparent navigation bar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItems = nil
    }
    
    func NonTranslucentNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    func setCloseKeyboard() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        
    }
    @objc func closeKeyboard() {
        
        view.endEditing(true)
        
    }

    
    @objc func GoToOwnerProfile(){
        self.delegate?.GoToOwnerProfile()
    }
    
    func makeACall(phone: String) {
        guard   phone.isValid(regex: .phone),
            let url = URL(string: "tel://\(phone.onlyDigits())"),
            UIApplication.shared.canOpenURL(url) else { return }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    deinit {
        self.networkStatus.reachabilityManager?.stopListening()
    }
}

class TuRetaBaseTableViewController: UITableViewController {

    var isOwner: Bool?
    let defaults = UserDefaults.standard
    let networkStatus = NetworkClient()
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        
    
        // Add label to subview hierarchy
        for subview in self.navigationController!.navigationBar.subviews {
            let stringFromClass = NSStringFromClass(subview.classForCoder)
            if stringFromClass.contains("UINavigationBarLargeTitleView") {

                let largeSubViews = subview.subviews
                for sbv in largeSubViews
                {
                    if let lbl = sbv as? UILabel
                    {
                        lbl.setNeedsLayout()
                    }
                }
            }
        }

        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont(name: "Poppins-SemiBold", size: 26)!
        ]
        
        self.navigationController!.navigationBar.largeTitleTextAttributes = attrs

        
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
            if !isOwner!{ TranslucentNavBar() }
            if defaults.object(forKey: "isGuest") as? Bool == true{
                TranslucentNavBar()
            }
            
            
        }else{
            isOwner = false
            TranslucentNavBar()
        }
    }
    
    func TranslucentNavBar(){
        self.navigationController?.isNavigationBarHidden = false
        // Transparent navigation bar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItems = nil
    }
}
