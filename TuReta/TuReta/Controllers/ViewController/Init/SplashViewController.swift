//
//  SplashViewController.swift
//  TuReta
//
//  Created by DC on 9/11/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase

import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging

import AuthenticationServices

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        
        if (AccessToken.current != nil) {
            let userEmail = defaults.object(forKey: "email") as! String
            loginMethod(userEmail: userEmail, userPassword: "")
            
        }else if defaults.object(forKey: "email") != nil {
            let userEmail = defaults.object(forKey: "email") as! String
            let userPassword = defaults.object(forKey: "password") as! String
            
            loginMethod(userEmail: userEmail, userPassword: userPassword)
            
        }else if defaults.object(forKey: "appleToken") != nil {
            
//            let userEmail = defaults.object(forKey: "email") as! String
//            let userID = defaults.object(forKey: "userID") as! String
            
            if #available(iOS 13.0, *) {
                appleValidation(userID: "", email: "")
            } else {
                // Fallback on earlier versions
            }
            
            
        }else{
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                appDelegate.presentInit()
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    @available(iOS 13.0, *)
    func appleValidation(userID: String, email: String) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: userID) { (credentialState, error) in
            switch credentialState {
            case .authorized:
                // The Apple ID credential is valid. Show Home UI Here
                DispatchQueue.main.async {
                    self.loginMethod(userEmail: email, userPassword: "")
                }
                break
            case .revoked:
                // The Apple ID credential is revoked. Show SignIn UI Here.
                DispatchQueue.main.async {
                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                        return
                    }
                    _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                        appDelegate.presentInit()
                    }
                }
                break
            case .notFound:
                // No credential was found. Show SignIn UI Here.
                DispatchQueue.main.async {
                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                        return
                    }
                    _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                        appDelegate.presentInit()
                    }
                }
                break
            default:
                break
            }
        }
    }
    
    func loginMethod(userEmail: String, userPassword: String){
        LoginWS.loginWS(email: userEmail, password: userPassword, userName: nil, completionHandler: {[weak self] (user) in
            UserData.shared = user
            DispatchQueue.main.async {
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
                    
                    // check if is user or admi
                    if((user?.user.establishments?.count)! > 0 && (user?.user.establishments != nil)){
                        //
                        print("enviar a dueño de cancha")
                        appDelegate.presentHomeOwner()
                    }else{
                        appDelegate.presentHomeVC()
                    }
                }
            }
                
            Messaging.messaging().subscribe(toTopic: "user\(user?.user.id ?? 0)") { error in
                print("Subscribed to \(user?.user.id ?? 0) topic")
            }
            }, {[weak self] (error, message) in
                DispatchQueue.main.async {
                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                        return
                    }
                    _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                        appDelegate.presentInit()
                    }
                }
        })
    }
}
