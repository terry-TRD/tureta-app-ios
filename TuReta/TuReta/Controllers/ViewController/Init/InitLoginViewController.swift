//
//  InitLoginViewController.swift
//  TuReta
//
//  Created by DC on 9/6/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class InitLoginViewController: TuRetaBaseViewController {

    @IBOutlet weak var registerButton: CustomCornerButton!
    @IBOutlet weak var loginButton: CustomCornerButton!
    
    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
        registerButton.backgroundColor = Constants.greenOnePrimaryColor
        loginButton.backgroundColor = Constants.blueOnePrimaryColor
        
        registerLabel.textColor = Constants.blackOnePrimaryColor
        loginLabel.textColor = Constants.blueOnePrimaryColor
        
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        loginLabel.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        loginLabel.adjustsFontForContentSizeCategory = true
        
        registerLabel.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        registerLabel.adjustsFontForContentSizeCategory = true
        
    }
    

}
