//
//  AnimationBaseViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 1/14/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import Lottie

class AnimationBaseViewController: UIViewController {

    //var checkMarkAnimation = AnimationView()
    @IBOutlet var animationView: UIView!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    var checkMarkAnimation = AnimationView()
    var animationName: String?
    var isSuccess: Bool?
    var typeReservation: Int = 1
    //var animationView = CustomCornerAndBorderUIView()
    let defaults = UserDefaults.standard
    var isOwner: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .reservationReady
        , object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveFailed), name: .reservationFailed
        , object: nil)
        
        if defaults.object(forKey: "isOwner") != nil{
            isOwner = defaults.object(forKey: "isOwner") as? Bool
        } else {
            isOwner = false
        }
                       
        
        
        headerTitleLabel.text = "Reservando..."
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //playAnimation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    @IBAction func closeAnimation(_ sender: Any) {
        
        if isSuccess ?? false {
            //stopAnimation()
            self.dismiss(animated: true, completion: nil)
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            if isOwner! {
                print("enviar a dueño de cancha")
                appDelegate.presentHomeOwner()
            }else{
                appDelegate.presentHomeVC()
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func onDidReceiveData(_ notification: Notification) {
        doneButton.alpha = 1
        isSuccess = true
        image.image = UIImage(named: "success")
        doneButton.setTitle("Aceptar", for: .normal)
        if typeReservation == ReservationType.app {
            headerTitleLabel.text = "¡Éxito!"
            descriptionLabel.text = "Reservación enviada. Ahora solo espera que confirmen tu reservación."
        } else {
            headerTitleLabel.text = "¡Reservación creada!"
            descriptionLabel.text = "La reservación del cliente ha sido creada correctamente"
        }
    }
    
    @objc func onDidReceiveFailed(_ notification: Notification) {
        doneButton.alpha = 1
        isSuccess = false
        doneButton.setTitle("Reintentar", for: .normal)
        image.image = UIImage(named: "failed")
        if typeReservation == ReservationType.app {
            headerTitleLabel.text = "¡Error!"
            descriptionLabel.text = "Ocurrió un error al procesar tu solicitud, por favor intenta más tarde"
        } else {
            headerTitleLabel.text = "¡Error!"
            descriptionLabel.text = "Ocurrió un error al procesar tu solicitud, por favor intenta más tarde"
        }
    }
    
    
    func playAnimation() {
        //createAnimationView()
    
        checkMarkAnimation = AnimationView(name: animationName!)
        checkMarkAnimation.contentMode = .scaleAspectFit
        self.animationView.addSubview(checkMarkAnimation)
        checkMarkAnimation.frame = self.animationView.bounds
        checkMarkAnimation.centerXAnchor.constraint(equalTo: animationView.centerXAnchor).isActive = true
        checkMarkAnimation.centerYAnchor.constraint(equalTo: animationView.centerYAnchor).isActive = true
        checkMarkAnimation.loopMode = .playOnce
        checkMarkAnimation.play()
    }
    
    func stopAnimation() {
        checkMarkAnimation.stop()
        checkMarkAnimation.removeFromSuperview()
        //self.removeAnimationView()
    }
    
//    func createAnimationView() {
//        self.animationView = CustomCornerAndBorderUIView(frame:self.view.frame)
//        self.animationView.cornerRadius = CGFloat(10)
//        self.animationView.backgroundColor = Constants.blackOnePrimaryColor
//        self.animationView.alpha = 1
//        if UIDevice.current.hasNotch {
//            self.animationView.frame = CGRect(x: self.view.center.x, y: self.view.center.y, width: self.view.frame.width - 30, height: 400)
//        }else{
//            self.animationView.frame = CGRect(x: self.view.center.x, y: self.view.center.y + 11, width: self.view.frame.width - 30, height: 400)
//        }
//        self.animationView.center = self.view.center
//        self.view.addSubview(animationView)
//    }
    
//    func removeAnimationView() {
//        self.animationView.removeFromSuperview()
//    }

}
