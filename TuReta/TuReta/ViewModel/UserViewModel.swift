//
//  UserViewModel.swift
//  TuReta
//
//  Created by DC on 10/5/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

struct UserViewModel {
    
    var id: Int
    var name: String
    var email: String
    var is_verified: Bool?
    var color: String
    
    init(user: User){
        self.id = user.id
        self.name = user.name
        self.email = user.email
        self.is_verified = user.is_verified
        self.color = user.color.capitalized
    }
    
}

final class DefaultUserOwnerViewModel {
    var servicesViewModel : [Service]?
    
    private let ownerService: OwnerServices
    init(ownerService: OwnerServices) {
        self.ownerService = ownerService
    }
    
    func getItemByIndex(index: Int) -> Service{
        return servicesViewModel![index]
    }
    
    func requestAllServicesOwner(completion: @escaping() -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        
        ownerService.requestAllServices( completionHandler: { (data) in
            
            self.parseOperation(data: data, completionHandler: {[weak self] (reservationsList) in
                guard let strongSelf = self else { return }
                strongSelf.servicesViewModel = reservationsList.map({
                    return Service(id: $0.id, name: $0.name, icon_url: $0.icon_url)
                })
            })
            completion()
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func requestUpdateService(establishmentID: Int, service: Int, activatedStatus: Bool, completion: @escaping(_ response: Bool) -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        ownerService.requestUpdateServiceSatatus(establishmentId: establishmentID, service: service, activatedStatus: activatedStatus, completionHandler: { (data) in
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
                print(response)
            })
            completion(true)
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
}

final class DefaultUserViewModel {
    internal init(profileService: ProfileServices, userViewModel: UserViewModel?) {
        self.profileService = profileService
        self.userViewModel = userViewModel
    }
    
    private let profileService: ProfileServices
    
    var userViewModel: UserViewModel? {
        didSet{
            guard let userViewModel = userViewModel else{
                return
            }
        }
    }
    
    //Dependency injection
    init(profileService: ProfileServices){
        self.profileService = profileService
        
    }
    
    func setup( completion: @escaping () -> Void) {
        
        profileService.requestProfile( completionHandler: {[weak self] (user) in
            guard let strongSelf = self else {
                return
            }
            //print(user?.name)
            
            strongSelf.userViewModel = user.map({ return UserViewModel(user: $0)})!
            completion()
            }, { (networkError, message) in
                print(networkError!, message)
        })
    }
    
    func closeSession( completion: @escaping(_ isSuccessful: Bool) -> Void) {
        profileService.requestCloseSession(completionHandler: {[weak self] isSuccessful in
            guard let strongSelf = self else {
                return
            }
            
            completion(isSuccessful)
            }, { (networkError, message) in
        })
    }
    
}

extension DefaultUserOwnerViewModel{
    func parseOperation(data: Data , completionHandler: @escaping( _ response: [Service]) -> Void){
        
        do {
            var itemsArray : [Service] = []
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            
            let _ = json.compactMap{
                if let field = Service(JSONObject: $0){
                    itemsArray.append(field)
                }
            }
            completionHandler(itemsArray)
            
        }catch {
            print("JSONSerialization error:", error)
        }
    }
    
    func parseBasicOperation(data: Data, completionHandler: @escaping(_ response: JSONArrayType) -> Void){
        do {
            
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            
            
            completionHandler(json)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
}

protocol UserViewModelProtocol {
    var userProfile: UserViewModel {get}
}
