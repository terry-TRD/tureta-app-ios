//
//  ReservacionOwnerTableViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 11/30/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import Cosmos

class ReservacionOwnerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var canchaName: UILabel!
    @IBOutlet weak var dateRenta: UILabel!
    @IBOutlet weak var dateRentaImage: UIImageView!
    @IBOutlet weak var statusRenta: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var horarioRenta: UILabel!
    @IBOutlet weak var horarioImage: UIImageView!
    @IBOutlet weak var namePlayer: UILabel!
    @IBOutlet weak var addresUserImage: UIImageView!
    
    @IBOutlet weak var codigoReservation: UILabel!
    @IBOutlet weak var manualReservationView: UIView!
    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var globalView: CustomCornerAndBorderUIView!
    @IBOutlet weak var stack: UIStackView!
    
    @IBOutlet weak var stars: CosmosView!
    @IBOutlet weak var heightStarsLayout: NSLayoutConstraint!
    @IBOutlet weak var aspectImage: NSLayoutConstraint!
    var isOwner: Bool?
    var viewModel: Reservation? {
        didSet{
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateUI(){
        
        if viewModel?.type == ReservationType.manual {
            manualReservationView.isHidden = false
        } else {
            manualReservationView.isHidden = true
        }
        stack.subviews[0].widthAnchor
        canchaName.text = "\(viewModel?.field.name ?? "")"
        dateRenta.text = "\(viewModel?.from.getNiceTimeFormat2() ?? "")"
        var statusLabel = HelperExtension.StatusType(status: viewModel!.status)
        statusRenta.text = "\(viewModel?.from.getOnlyHour() ?? "") HRS - \(viewModel?.to.getOnlyHour() ?? "") HRS" //"Estatus: \(statusLabel)"
        horarioRenta.text = " \(viewModel?.from.getOnlyHour() ?? "") HRS - \(viewModel?.to.getOnlyHour() ?? "") HRS"
        namePlayer.text = isOwner! == true ?  viewModel?.user?.name : viewModel?.establishment?.name
        
        //horarioRenta.text = "\(viewModel?.from.getOnlyHour() ?? "") HRS - \(viewModel?.to.getOnlyHour() ?? "") HRS" //"Estatus: \(statusLabel)"
        if isOwner! {
            codigoReservation.text = "\(viewModel?.code ?? "")" //codigoReservation.text = "Código: \(viewModel?.code ?? "")"
            
            imageField.isHidden = true
            infoView.leftAnchor.constraint(equalTo: globalView.leftAnchor, constant: 15).isActive = true
            addresUserImage.image = UIImage(named: "icn-user")
            globalView.borderColor = .clear
            
            stars.alpha = 0
            heightStarsLayout.constant = 0
            
        } else {
            codigoReservation.text = ""
            addresUserImage.image = UIImage(named: "icn-pin")
        }
        
        statusRenta.text = "\(statusLabel)"
        switch viewModel?.status {
        case 1:
            statusRenta.textColor = Constants.blueOnePrimaryColor
        case 2:
            statusRenta.textColor = Constants.greenOnePrimaryColor
        case 3:
            statusRenta.textColor = Constants.redOnePrimaryColor
        case 4:
            statusRenta.textColor = Constants.yellowOnePrimaryColor
        case 5:
            statusRenta.textColor = Constants.redThreePrimaryColor
        default:
            statusRenta.textColor = Constants.blackOnePrimaryColor
        }
        
        // reference
//        class func StatusType(status: Int) -> String {
//            switch status {
//            case 1:
//                return StatusField.newReservation.rawValue
//            case 2:
//                return StatusField.approved.rawValue
//            case 3:
//                return StatusField.rejected.rawValue
//            case 4:
//                return StatusField.happening.rawValue
//            case 5:
//                return StatusField.finished.rawValue
        //case 6:
        //case 7:
//            default:
//                return ""
//            }
//        }
        
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
