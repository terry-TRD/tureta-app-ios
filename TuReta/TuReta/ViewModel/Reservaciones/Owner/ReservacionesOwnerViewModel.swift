//
//  ReservacionesOwnerViewModel.swift
//  TuReta
//
//  Created by Daniel Coria on 12/1/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

final class DefaultReservationsOwnerViewModel {
    
    private let ownerService: OwnerServices
    var reservationsViewModel : [Reservation]? 
    
    init(ownerService: OwnerServices) {
        self.ownerService = ownerService
    }
    
    func getItemByIndex(index: Int) -> Reservation {
        return reservationsViewModel![index]
    }
    
    func requestUpdateRate(reservationID: Int, rate: Int, completion: @escaping(_ response: Bool) -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
           ownerService.requestUpdateRate(reservationID: reservationID, rate: rate, completionHandler: { (data) in
               
               self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                   guard let strongSelf = self else { return }
                   print(response)
               })
               completion(true)
           }, {(networkError, message) in
               print(networkError!, message)
               errorMessage(message)
           })
       }
    
    func requestingReservationsOwner(establishmentId: Int, type: String, completion: @escaping() -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        
        ownerService.requestReservationsOwner(establishmentId: establishmentId, type: type, completionHandler: { (data) in
            
            self.parseOperation(data: data, completionHandler: {[weak self] (reservationsList) in
                guard let strongSelf = self else { return }
                strongSelf.reservationsViewModel = reservationsList.map({
                    return Reservation(id: $0.id, code: $0.code, from: $0.from, to: $0.to, user: $0.user, establishment: $0.establishment, field: $0.field, comments: $0.comments, subtotal: $0.subtotal, fee: $0.fee, total: $0.total, status: $0.status, reviews: $0.reviews, type: $0.type)
                })
            })
            completion()
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func requestingUpdateStatusReservation(establishmentID: Int, reservationID: Int, type: String, completion: @escaping(_ response: Bool) -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        
        ownerService.requestUpdateStatusResevation(establishmentID: establishmentID, reservationID: reservationID, type: type, completionHandler: { (data) in
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
                print(response)
            })
            completion(true)
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func requestCancelReservation(reservationID: Int, completion: @escaping(_ response: Bool) -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        
        ownerService.requestCancelReservation(reservationID: reservationID, completionHandler: { (data) in
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
                print(response)
            })
            completion(true)
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func parseOperation(data: Data , completionHandler: @escaping( _ response: [Reservation]) -> Void){
        
        do {
            var itemsArray : [Reservation] = []
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            
            let _ = json.compactMap{
                if let field = Reservation(JSONObject: $0){
                    itemsArray.append(field)
                }
            }
            completionHandler(itemsArray)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
    
    func parseBasicOperation(data: Data, completionHandler: @escaping(_ response: JSONArrayType) -> Void){
        do {
            
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            
            
            completionHandler(json)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
    
    
    func parseWithObject(data: Data, completionHandler: @escaping( _ response: Reservation) -> Void){
       
           do {
               var info: Reservation
               guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                   return
               }
               
               guard let json = jsonObject["data"] as? JSONObjectType else{
                   return
               }
               
               info = Reservation(JSONObject: json)!
               
               completionHandler(info)

           }catch {
               print("JSONSerialization error:", error)
           }
           
       }
    
}
