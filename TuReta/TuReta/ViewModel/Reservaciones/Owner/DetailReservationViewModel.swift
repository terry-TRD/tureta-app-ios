//
//  DetailReservationViewModel.swift
//  TuReta
//
//  Created by Daniel Coria on 12/23/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

struct DetailReservationViewModel {
    var modelReservation: Reservation

    init(modelReservation: Reservation) {
        self.modelReservation = modelReservation
    }
}

extension DetailReservationViewModel {
    
    var reservationName: String {
        return "Cancha seleccionada: \(modelReservation.field.name)"
    }
    
    var scheduleAt: String {
        return  "\(modelReservation.from.getOnlyHour())"
    }
    var scheduleFor: String {
        return "\(modelReservation.to.getOnlyHour()) hrs"
    }
    
    var dayReservation: String {
        return "Fecha: \(modelReservation.from.getNiceTimeFormat2())"
    }
    
    var schedule: String {
        return "Horario: \(scheduleAt) a \(scheduleFor)"
    }
    
    var lengthTime: String {
        return "\(modelReservation.from.getTimeFormat().getTimeBewteenTwoDates(dateTwo: modelReservation.to.getTimeFormat()))"
    }
    
    var playerName: String {
        return "Usuario: \(modelReservation.user?.name ?? "") "
    }
    
    
    var priceHour: String {
        return "$\(modelReservation.subtotal)"
    }
    
    var totalPrice: String {
        return "$\(modelReservation.total)"
    }
    
    var imagePlace: String {
        guard let place = modelReservation.establishment else { return ""}
        return place.profile_picture_url
    }
    
    var isRatedByUser: Bool {
        var isRatedByUser: Bool = false
        
        for item in modelReservation.reviews {
            if item.createdBy == UserData.shared?.user.id {
                isRatedByUser = true
                break
            }
        }
        return isRatedByUser
    
    }
    
}
