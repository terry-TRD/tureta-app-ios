//
//  CanchasOwnerViewModel.swift
//  TuReta
//
//  Created by Daniel Coria on 11/28/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

final class DefaultCanchasOwnerViewModel {

    private let ownerService: OwnerServices
    
    //var fieldViewModel: [Field]?
    
    //var establishmentDetailViewModel: EstablishmentDetail?
    
    init(ownerService: OwnerServices) {
        self.ownerService = ownerService
    }
    
    func requestingUpdateStatusField(establishmentId: Int, fieldId: Int, isActive: Bool, completion: @escaping(_ response: Bool) -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        
        ownerService.requestUpdateStatusField(establishmentId: establishmentId, fieldId: fieldId, isActive:isActive, completionHandler: { (data) in
            
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
                print(response)
            })
            completion(true)
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func parseWithObject(data: Data, completionHandler: @escaping( _ response: EstablishmentDetail) -> Void){
    
        do {
            var info: EstablishmentDetail
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONObjectType else{
                return
            }
            
            info = EstablishmentDetail(JSONObject: json)!
            
            completionHandler(info)

        }catch {
            print("JSONSerialization error:", error)
        }
        
    }

    func parseBasicOperation(data: Data, completionHandler: @escaping(_ response: JSONArrayType) -> Void){
        do {
            
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            
            
            completionHandler(json)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
    
}
