//
//  CanchasCollectionViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 10/20/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class CanchasCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var nameCancha: UILabel!
    @IBOutlet weak var imageCancha: UIImageView!
    
    var name: String = "" {
        didSet {
            nameCancha.text = name
        }
    }
    
    var imageNameCancha: UIImage? {
        didSet {
            imageCancha.image = imageNameCancha
            imageCancha.contentMode = .scaleAspectFill
        }
    }
    
    func configureCell(cell: CanchasCollectionViewCell) {
        cell.name = "No. 7"
        
        cell.imageNameCancha = UIImage(named: "canchaDemo")
    }

}
