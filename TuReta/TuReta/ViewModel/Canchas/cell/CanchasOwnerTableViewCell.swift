//
//  CanchasOwnerTableViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 11/16/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class CanchasOwnerTableViewCell: UITableViewCell {

    @IBOutlet weak var fieldName: UILabel!
    @IBOutlet weak var switchActiveField: UISwitch!
    @IBOutlet weak var fieldType: UILabel!
    
    var viewModelField: Field? {
        didSet{
            loadInfo()
        }
    }
    
    func loadInfo(){
        fieldName.text = viewModelField?.name
        switchActiveField.isOn = viewModelField?.is_active ?? false
        //fieldType.text = "Tipo de cancha \(HelperExtension.GrassType(type: (viewModelField?.grass_type ?? 0) - 1))\nPrecio por hora $\(viewModelField?.prices?.first?.price_per_hour ?? "") "
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
