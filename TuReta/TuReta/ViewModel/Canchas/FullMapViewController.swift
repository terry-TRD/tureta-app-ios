//
//  FullMapViewController.swift
//  TuReta
//
//  Created by DC on 29/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import GoogleMaps

class FullMapViewController: UIViewController {

    var playerMap: GMSMapView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let mapView = self.playerMap {
            mapView.delegate = self
        }
    }
    
    func setupMap(location: CLLocationCoordinate2D) {
        
        playerMap = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height), camera: GMSCameraPosition(latitude: location.latitude, longitude: location.longitude, zoom: 15.0))
        
        playerMap?.mapType = .terrain
        playerMap?.padding = UIEdgeInsets(top: 0, left: 0, bottom: 85, right: 0)
        playerMap?.isUserInteractionEnabled = true
        self.view.addSubview(playerMap!)
        setupCustomMarkerMap(itemLocation: location)
    }
    
    func setupCustomMarkerMap(itemLocation: CLLocationCoordinate2D){
        
        let marker = GMSMarker()
        
        //creating a marker view
        marker.position = CLLocationCoordinate2D(latitude: itemLocation.latitude, longitude: itemLocation.longitude)
        //marker.iconView = imageIcon
        marker.icon = createImageIcon()
        //marker.userData = item
        
        marker.map = playerMap
    }

    
    func createImageIcon() -> UIImage{
        let markerImage = UIImage(named: "icn-pin")!.withRenderingMode(.alwaysOriginal)
        let markerView = self.imageWithImage(image: markerImage, scaledToSize: CGSize(width: 35.0, height: 35.0))
        return markerView
    }

    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }


}

extension FullMapViewController: GMSMapViewDelegate {
    
}
