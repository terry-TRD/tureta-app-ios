//
//  BasicCellDetailFieldTableViewCell.swift
//  TuReta
//
//  Created by DC on 26/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class BasicCellDetailFieldTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var textOptionButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
