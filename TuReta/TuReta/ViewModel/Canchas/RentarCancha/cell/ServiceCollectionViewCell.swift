//
//  ServiceCollectionViewCell.swift
//  TuReta
//
//  Created by DC on 28/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import SDWebImage

class ServiceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var serviceIcon: UIImageView!
    @IBOutlet weak var nameServiceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(service: Service) {
        serviceIcon.tintColor = Constants.greenOnePrimaryColor
        nameServiceLabel.text = service.name
        guard let urlImage = URL(string: service.icon_url) else { return }
        serviceIcon.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default-image1"), options: .refreshCached) { image, error, cacheType, imageURL in
            self.serviceIcon.image = image?.withRenderingMode(.alwaysTemplate)
        }
    }

}
