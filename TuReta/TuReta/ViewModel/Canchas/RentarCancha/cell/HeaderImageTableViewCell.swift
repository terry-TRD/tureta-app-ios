//
//  HeaderImageTableViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 10/30/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class HeaderImageTableViewCell: UITableViewCell {
    
    var delegate: CustomDelegate?

    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var favoriteButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        favoriteButton.addTarget(self, action: #selector(eventFavorite), for:.touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func eventFavorite(sender: UIButton) {
        
        if sender.isSelected{
            favoriteButton.setImage(UIImage(named: ImageName.type.iconUnLiked.rawValue), for: .normal)
            favoriteButton.isSelected = false
        }else{
            favoriteButton.setImage(UIImage(named: ImageName.type.iconLiked.rawValue), for: .normal)
            favoriteButton.isSelected = true
        }
        delegate?.tapFav(isActive: sender.isSelected)
    }
    
}
