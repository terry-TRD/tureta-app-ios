//
//  DescriptionTableViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 10/30/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import Cosmos

class DescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var horizontallyScrollableStackView: UIStackView!
    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var establishmentRate: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var basicInfoViewModel: EstablishmentViewModel? {
        didSet{
            DispatchQueue.main.async { [unowned self] in
                self.updateUI()
            }
        }
    }
    
    var viewModel: EstablishmentDetail? {
        didSet{
            DispatchQueue.main.async { [unowned self] in
                //self.updateMoreInfoUI()
            }
        }
    }
    
    func updateUI(){
        self.nameLabel.text = basicInfoViewModel?.name
        self.descriptionLabel.text = basicInfoViewModel?.description
        //self.phoneNumberButton.setTitle(basicInfoViewModel?.phone, for: .normal)
        self.establishmentRate.settings.fillMode = .full
        self.establishmentRate.rating = Double(basicInfoViewModel?.rate ?? Int(0.0))
    }
     
    
    func updateMoreInfoUI(){
        if viewModel != nil {
            if(horizontallyScrollableStackView.arrangedSubviews.count == 0){
                for service in viewModel!.services! {
                    if let serviceView = Bundle.main.loadNibNamed("ServiceCell", owner: nil, options: nil)!.first as? ServiceCell {
                        serviceView.translatesAutoresizingMaskIntoConstraints = false
                        
                        serviceView.widthAnchor.constraint(equalToConstant: 75).isActive = true
                        serviceView.nameService.text = service.name
                        guard let urlImage = URL(string: service.icon_url) else { return }
                        serviceView.iconService.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default-image1"))
                        horizontallyScrollableStackView.addArrangedSubview(serviceView)
                    }
                }
            }
        }
    }
    
    @IBAction func callEstablishmentButton(_ sender: UIButton) {
        sender.titleLabel?.text?.makeACall()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
