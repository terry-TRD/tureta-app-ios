//
//  MapTableViewCell.swift
//  TuReta
//
//  Created by DC on 26/06/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import GoogleMaps

class MapTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapImage: UIImageView!
    
    var playerMap: GMSMapView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let mapView = self.playerMap {
            mapView.delegate = self
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupMap(location: CLLocationCoordinate2D) {
        
        self.mapView.layoutIfNeeded()
        playerMap = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.mapView.frame.size.width, height: self.mapView.frame.size.height), camera: GMSCameraPosition(latitude: location.latitude, longitude: location.longitude, zoom: 15.0))
        
        playerMap?.mapType = .terrain
        playerMap?.padding = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
        playerMap?.isUserInteractionEnabled = false
        mapView.addSubview(playerMap!)
        mapView.sendSubviewToBack(playerMap!)
        
        //getMapImage()
    }
    
    func getMapImage() {

        guard let map = playerMap?.snapshotView(afterScreenUpdates: true) else { return }
        mapView.addSubview(map)
       
    }
    
}

extension MapTableViewCell: GMSMapViewDelegate {
    
}
