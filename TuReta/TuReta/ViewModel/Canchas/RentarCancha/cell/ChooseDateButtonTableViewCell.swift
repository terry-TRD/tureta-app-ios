//
//  ChooseDateButtonTableViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 10/30/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class ChooseDateButtonTableViewCell: UITableViewCell {

    
    @IBOutlet weak var pickrDateButton: CustomCornerButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        pickrDateButton.titleLabel?.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)
        pickrDateButton.titleLabel?.adjustsFontForContentSizeCategory = true
        pickrDateButton.tintColor = .white
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveDate(_:)), name: .didChangeDate, object: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func onDidReceiveDate(_ notification:Notification) {
        if let dateString = notification.userInfo?["newDateString"] as? String{
            pickrDateButton.setTitle(dateString.getNiceTimeFormat(), for: .normal)
        }
    }
}

extension Notification.Name {
    static let didChangeDate = Notification.Name("didChangeDate")
}
