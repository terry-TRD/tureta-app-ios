//
//  RentarCanchaViewModel.swift
//  TuReta
//
//  Created by Daniel Coria on 11/8/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation


final class DefaultRentarCanchaViewModel {

    private let establishmentService: EstablishmentServices
    var fieldViewModel: [Field]?
    var establishmentDetailViewModel: EstablishmentDetail?
    
    
    init(establishmentService: EstablishmentServices) {
        self.establishmentService = establishmentService
    }
    
    func getItemByIndex(index: Int) -> Field{
        return fieldViewModel![index]
    }
    
    
    func gettingAvailableFieldsData(establishmentID: Int, date: String, completion: @escaping() -> Void){
        establishmentService.requestEstablishmentAvailable(establishmentID: establishmentID, date: date, completionHandler: { (data) in
            
            self.parseOperation(data: data, completionHandler: {[weak self] (fieldsList) in
                guard let strongSelf = self else { return }
                strongSelf.fieldViewModel = fieldsList.map({
                    return Field(id: $0.id, name: $0.name, game_types: $0.game_types, grass_type: $0.grass_type, availability: $0.availability, prices: $0.prices, is_active: false)
                })
            })
            completion()
        }, {(networkError, message) in
            print(networkError!, message)
        })
    }
    
    func requestingRentField(field: Field, from: String, to: String, type: Int, manualPrice: Double?, completion: @escaping(_ response: Bool) -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        establishmentService.requestRentField(field: field, from: from, to: to, type: type, manualPrice: manualPrice, completionHandler: { (data) in
            
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
                print(response)
            })
            completion(true)
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func requestEstablismentDetail(idPlace: Int, completion: @escaping() -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        establishmentService.requestEstablishmentDetail(idPlace: idPlace, completionHandler: { (data) in
            
            self.parseWithObject(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
                strongSelf.establishmentDetailViewModel = response
                UserData.sharedEstablishment = response
                //print(response)
            })
            completion()
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }

    func requestFav(establishmentID: Int, type: String, completion: @escaping() -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        establishmentService.requestEstablishmentFavorite(establishmentID: establishmentID, type: type, completionHandler: { (data) in
            
            self.parseWithObject(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
                //strongSelf.establishmentDetailViewModel = response
                
            })
            completion()
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func parseOperation(data: Data , completionHandler: @escaping( _ response: [Field]) -> Void){
        
        do {
            var itemsArray : [Field] = []
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            
            let _ = json.compactMap{
                if let field = Field(JSONObject: $0){
                    itemsArray.append(field)
                }
            }
            completionHandler(itemsArray)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
    
    func parseWithObject(data: Data, completionHandler: @escaping( _ response: EstablishmentDetail) -> Void){
    
        do {
            var info: EstablishmentDetail
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONObjectType else{
                return
            }
            
            info = EstablishmentDetail(JSONObject: json)!
            
            completionHandler(info)

        }catch {
            print("JSONSerialization error:", error)
        }
        
    }

    func parseBasicOperation(data: Data, completionHandler: @escaping(_ response: JSONArrayType) -> Void){
        do {
            
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            
            
            completionHandler(json)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
    

}
    
