//
//  AvailableCourtCollectionViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 10/31/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class AvailableCourtCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var hourLabel: UILabel!
}
