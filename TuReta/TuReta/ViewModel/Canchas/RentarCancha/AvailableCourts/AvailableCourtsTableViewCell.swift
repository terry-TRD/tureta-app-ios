//
//  AvailableCourtsTableViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 10/31/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class AvailableCourtsTableViewCell: UITableViewCell, UICollectionViewDelegateFlowLayout {
    
    var delegate: CustomDelegate?
    var selectedAtIndex: IndexPath?
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet weak var FieldName: UILabel!
    @IBOutlet weak var descriptionField: UILabel!
    @IBOutlet weak var pricesField: UILabel!
    @IBOutlet weak var imageOneFieldType: UIImageView!
    @IBOutlet weak var fieldOneType: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(dothis))
        singleTap.cancelsTouchesInView = false
        //singleTap.numberOfTapsRequired = 1
        collectionView.addGestureRecognizer(singleTap)
    }

    @objc func dothis(){
        delegate?.tapCell(cellIndex: selectedAtIndex!)
    }
    
    var viewModel: Field? {
        didSet{
            updateUI()
            
        }
    }
    
    func updateUI(){
        FieldName.text = viewModel?.name
        
        descriptionField.text = "Cancha para #Jugadores: \(viewModel!.game_types)"
        if(viewModel?.grass_type == GrassType.FIELD_GRASS_TYPE_NATURAL){
            imageOneFieldType.image = UIImage.addNaturalField
            fieldOneType.text = "Natural"
        }else{
            imageOneFieldType.image = UIImage.addNaturalField
            fieldOneType.text = "Sintetico"
        }
        var priceString: String = ""
        let _ = viewModel?.prices?.compactMap{
            priceString += "Precio de cancha tipo:  \($0.game_type) - $\($0.price_per_half)\n"
        }
        pricesField.text = priceString
        
    }
    
    func didSelect(_ cell: UICollectionViewCell) {
        
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {

        // If the cell's size has to be exactly the content
        // Size of the collection View, just return the
        // collectionViewLayout's collectionViewContentSize.

        self.collectionView.frame = CGRect(x: 0, y: 0,
                                       width: targetSize.width, height: 600)
        self.collectionView.layoutIfNeeded()

        // It Tells what size is required for the CollectionView
        return self.collectionView.collectionViewLayout.collectionViewContentSize

    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}
extension AvailableCourtsTableViewCell {

    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        
        collectionView.allowsMultipleSelection = true
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
        collectionView.layoutIfNeeded()
    }
}


