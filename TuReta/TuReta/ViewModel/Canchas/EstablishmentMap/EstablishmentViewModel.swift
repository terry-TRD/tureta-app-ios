//
//  EstablishmentViewModel.swift
//  TuReta
//
//  Created by Daniel Coria on 10/26/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

struct EstablishmentViewModel {
    var idE: Int
    var name: String
    var lat: Double
    var lng: Double
    var description: String
    var address: String
    var profile_picture_url: String
    //var distance: Int
    var phone: String?
    var rate: Int?
    var price: String?
    
    init(establishment: Establishment) {
        self.idE = establishment.id
        self.name = establishment.name
        self.lat = establishment.lat
        self.lng = establishment.lng
        self.description = establishment.description
        self.address = establishment.address
        self.profile_picture_url = establishment.profile_picture_url
        //self.distance = establishment.distance
        self.phone = establishment.phone ?? ""
        self.rate = establishment.rate ?? 0
        self.price = establishment.price
    }
}


final class DefaultEstablishmentViewModel {
    private let establishmentService: EstablishmentServices
    
    var establishmenViewModel: Array<EstablishmentViewModel>?
    
    init(establishmentService: EstablishmentServices) {
        self.establishmentService = establishmentService
    }
    
    func gettingEstablishmentData(center_lat: Double, center_lng: Double, border_lat: Double, border_lng: Double, search: String, completion: @escaping () -> Void){
        establishmentService.requestMainEstablishment(center_lat: center_lat, center_lng: center_lng, border_lat: border_lat, border_lng: border_lng, search: search, completionHandler: {[weak self] (establishments) in
        
                guard let strongSelf = self else {
                    return
                }
               
            strongSelf.establishmenViewModel = establishments?.establishmentsItems.map({
                return EstablishmentViewModel(establishment: $0)})
                completion()
            }, { (networkError, message) in
                print(networkError!, message)
        })
    }
}
