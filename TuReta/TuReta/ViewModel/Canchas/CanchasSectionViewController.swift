//
//  CanchasSectionViewController.swift
//  TuReta
//
//  Created by Daniel Coria on 10/20/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class CanchasSectionViewController: UIViewController, CollectionViewProtocol {
    
    var handler: CollectionViewProtocolHandler!
    @IBOutlet weak var canchasCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.canchasCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "canchasCell")
        
        // Do any additional setup after loading the view.
        //canchasCollectionView.register(UINib(nibName: "CanchasCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "canchasCell")
        var reference = self
        reference.useProtocolForCollectionView(collectionView: canchasCollectionView)
         
//        let layout = self.canchasCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//        if #available(iOS 10.0, *) {
//            layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
//        }
//        else {
//            layout.estimatedItemSize = CGSize(width: 110, height: 115)
//        }
//        
//        layout.minimumInteritemSpacing = 5
        //layout.itemSize = CGSize(width: 110, height: self.canchasCollectionView.frame.size.height - 20 )
        
    }
    
}

class CollectionViewProtocolHandler: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "canchasCell", for: indexPath) as! CanchasCollectionViewCell
        cell.configureCell(cell: cell)
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
}

protocol CollectionViewProtocol {
    var handler: CollectionViewProtocolHandler! {get set}
    mutating func useProtocolForCollectionView(collectionView: UICollectionView)
}

extension CollectionViewProtocol {
    mutating func useProtocolForCollectionView(collectionView: UICollectionView) {
        handler = CollectionViewProtocolHandler()
        collectionView.delegate = handler
        collectionView.dataSource = handler
    }
}
