//
//  RecoveryPasswordViewModel.swift
//  TuReta
//
//  Created by DC on 30/01/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

final class DefaultRecoveyPasswordViewModel {
    
     private let ownerService: OwnerServices
    
    init(ownerService: OwnerServices) {
        self.ownerService = ownerService
    }
    
    func requestRecoveryPassword(email: String, completion: @escaping(_ response: Bool) -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        
        ownerService.requestRecoveryPassword(email: email, completionHandler: { (data) in
            
            self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
                print(response)
            })
            completion(true)
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func parseBasicOperation(data: Data, completionHandler: @escaping(_ response: JSONArrayType) -> Void){
        do {
            
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            
            
            completionHandler(json)

        }catch {
            print("JSONSerialization error:", error)
        }
    }

}
