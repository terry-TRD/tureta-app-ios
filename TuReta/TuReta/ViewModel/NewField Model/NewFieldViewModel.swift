//
//  NewFieldViewModel.swift
//  TuReta
//
//  Created by DC on 29/02/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

class NewFieldViewModel {
    private let ownerService: OwnerServices
    
    init(ownerService: OwnerServices) {
        self.ownerService = ownerService
    }
    
    
    func createNewField(params: NewField, completion: @escaping(_ response: Bool) -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        ownerService.requestCreateNewField(newFieldParams: params, completionHandler: { (data) in
            
            //self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
            //    guard let strongSelf = self else { return }
            //    print(response)
            //})
            completion(true)
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func removeField(fieldID: Int, completion: @escaping(_ response: Bool) -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        ownerService.requestRemoveField(fieldID: fieldID, completionHandler: { (data) in
            completion(true)
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func updateField(params: NewField, completion: @escaping(_ response: Bool) -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        ownerService.requestUpdateField(newFieldParams: params, completionHandler: { (data) in
            
            //self.parseBasicOperation(data: data, completionHandler: {[weak self] (response) in
            //    guard let strongSelf = self else { return }
            //    print(response)
            //})
            completion(true)
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    
}
