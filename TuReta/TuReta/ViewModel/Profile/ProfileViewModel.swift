//
//  ProfileViewModel.swift
//  TuReta
//
//  Created by DC on 10/5/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit


final class ProfileViewModel {
    var name: String
    var email: String
    var color: String?
    
    private let defaultUserVieModel: DefaultUserViewModel
    
    
    init(userVM: DefaultUserViewModel){
        
        self.name = userVM.userViewModel!.name
        self.email = userVM.userViewModel!.email
        self.defaultUserVieModel = userVM
        self.color = userVM.userViewModel?.color
        //add Observer
        
    }
    
}
