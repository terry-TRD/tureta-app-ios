//
//  FavoriteViewModel.swift
//  TuReta
//
//  Created by Daniel Coria on 1/18/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

final class FavoriteViewModel {
    
    private let establishmentService: EstablishmentServices
    
    var fieldViewModel: [Field]?
    
    var establishmentDetailViewModel: [EstablishmentDetail]?
    
    init(establishmentService: EstablishmentServices) {
        self.establishmentService = establishmentService
    }
    
    
    func requestFav(establishmentID: Int, type: String, completion: @escaping() -> Void,_ errorMessage:  @escaping(_ errorMessage : String) -> Void){
        establishmentService.requestEstablishmentFavorite(establishmentID: establishmentID, type: type, completionHandler: { (data) in
            
            self.parseOperation(data: data, completionHandler: {[weak self] (response) in
                guard let strongSelf = self else { return }
                strongSelf.establishmentDetailViewModel = response
                //print(response)
            })
            completion()
        }, {(networkError, message) in
            print(networkError!, message)
            errorMessage(message)
        })
    }
    
    func parseWithObject(data: Data, completionHandler: @escaping( _ response: EstablishmentDetail) -> Void){
    
        do {
            var info: EstablishmentDetail
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONObjectType else{
                return
            }
            
            info = EstablishmentDetail(JSONObject: json)!
            
            completionHandler(info)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
    
    func parseOperation(data: Data , completionHandler: @escaping( _ response: [EstablishmentDetail]) -> Void){
        
        do {
            var itemsArray : [EstablishmentDetail] = []
            guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? JSONObjectType else{
                return
            }
            
            guard let json = jsonObject["data"] as? JSONArrayType else{
                return
            }
            
            
            let _ = json.compactMap{
                if let field = EstablishmentDetail(JSONObject: $0){
                    itemsArray.append(field)
                }
            }
            completionHandler(itemsArray)

        }catch {
            print("JSONSerialization error:", error)
        }
    }
}
