//
//  FavoriteTableViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 1/17/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit
import Cosmos

class FavoriteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var addressField: UILabel!
    @IBOutlet weak var establishmentRate: CosmosView!
    
    
    var viewModel: EstablishmentDetail? {
        didSet{
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(){
        nameField.text = viewModel?.name
        addressField.text = viewModel?.address
        self.establishmentRate.rating = 0 //Double(viewModel. ?.rate ?? Int(0.0))
    }
    
}
