//
//  HistoryReservationTableViewCell.swift
//  TuReta
//
//  Created by Daniel Coria on 1/18/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit

class HistoryReservationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var canchaName: UILabel!
    @IBOutlet weak var dateRenta: UILabel!
    @IBOutlet weak var statusRenta: UILabel!
    @IBOutlet weak var horarioRenta: UILabel!
    @IBOutlet weak var namePlayer: UILabel!
    
    @IBOutlet weak var codigoReservation: UILabel!
    
    var viewModel: Reservation? {
        didSet{
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateUI(){
        canchaName.text = "Cancha: \(viewModel?.field.name ?? "")"
        dateRenta.text = "\(viewModel?.from.getNiceTimeFormat2() ?? "")"
        let statusLabel = HelperExtension.StatusType(status: viewModel!.status)
        statusRenta.text = "\(viewModel?.from.getOnlyHour() ?? "") HRS - \(viewModel?.to.getOnlyHour() ?? "") HRS" //"Estatus: \(statusLabel)"
        //horarioRenta.text = " \(viewModel?.from.getOnlyHour() ?? "") HRS - \(viewModel?.to.getOnlyHour() ?? "") HRS"
        namePlayer.text = viewModel?.establishment?.name
        horarioRenta.text = "Código: \(viewModel?.code ?? "")" //codigoReservation.text = "Código: \(viewModel?.code ?? "")"
        codigoReservation.text = "\(statusLabel)"
        switch viewModel?.status {
        case 1:
            codigoReservation.textColor = Constants.blueOnePrimaryColor
        case 2:
            codigoReservation.textColor = Constants.greenOnePrimaryColor
        case 3:
            codigoReservation.textColor = Constants.redOnePrimaryColor
        case 4:
            codigoReservation.textColor = Constants.yellowOnePrimaryColor
        case 5:
            codigoReservation.textColor = Constants.redThreePrimaryColor
        default:
            codigoReservation.textColor = Constants.blackOnePrimaryColor
        }
        
        // reference
        //        class func StatusType(status: Int) -> String {
        //            switch status {
        //            case 1:
        //                return StatusField.newReservation.rawValue
        //            case 2:
        //                return StatusField.approved.rawValue
        //            case 3:
        //                return StatusField.rejected.rawValue
        //            case 4:
        //                return StatusField.happening.rawValue
        //            case 5:
        //                return StatusField.finished.rawValue
        //case 6:
        //case 7:
        //            default:
        //                return ""
        //            }
        //        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
