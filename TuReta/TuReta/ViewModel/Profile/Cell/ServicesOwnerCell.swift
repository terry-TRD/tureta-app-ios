//
//  ServicesOwnerCell.swift
//  TuReta
//
//  Created by Daniel Coria on 12/3/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit
import SDWebImage

class ServicesOwnerCell: UICollectionViewCell {
    @IBOutlet weak var imageService: UIImageView!
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var statusService: UISwitch!
    var currentServicesActivated: [Service]?
    
    var viewModel: Service?{
        didSet{
            updateUI()
        }
    }
    
    func updateUI(){
        guard let model = self.viewModel else { return }
        nameService.text = model.name
        
        guard let urlImage = URL(string: model.icon_url) else { return }
        self.imageService.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default-image1"))
        statusService.tag = model.id
        guard let currentServices = currentServicesActivated else { return }
        
        for i in 0..<currentServices.count {
            if(model.id == currentServicesActivated?[i].id ){
                statusService.isOn = true
                break
            }else{
                statusService.isOn = false
            }
        }
        
    }

}

class ServicesTableCell: UITableViewCell {
    @IBOutlet weak var imageService: UIImageView!
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var statusService: UISwitch!
    var currentServicesActivated: [Service]?
    
    var viewModel: Service?{
        didSet{
            updateUI()
        }
    }
    
    func updateUI(){
        guard let model = self.viewModel else { return }
        nameService.text = model.name
        imageService.tintColor = Constants.greenOnePrimaryColor
        //imageService.load(url: URL(string:model.icon_url)!)
        guard let urlImage = URL(string: model.icon_url) else { return }
        imageService.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default-image1"))
        imageService.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default-image1"), options: .refreshCached) { image, error, cacheType, imageURL in
            self.imageService.image = image?.withRenderingMode(.alwaysTemplate)
        }
        statusService.tag = model.id
        guard let currentServices = currentServicesActivated else { return }
        
        for i in 0..<currentServices.count {
            if(model.id == currentServicesActivated?[i].id ){
                statusService.isOn = true
                break
            }else{
                statusService.isOn = false
            }
        }
        
    }

}
