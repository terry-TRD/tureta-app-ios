//
//  User.swift
//  TuReta
//
//  Created by DC on 9/10/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

struct User: Codable {
    
    var id: Int
    var name: String
    var email: String
    var is_verified: Bool?
    var establishments: [Establishments]?
    var rate: Int?
    var color: String
}

struct Establishments: Codable {
    var address: String
    var description: String
    var id: Int
    var lat: Double
    var lng: Double
    var phone: String?
    var name: String
    var profile_picture_url: String
    var reservation_message: String?
}

struct Location: Codable {
    var coordinates: [Double]
}

struct UserResponse: Codable {
    var access_token: String
    var expires_at: String
    var user: User
    var username: String?
    var rate: Int?
}

extension UserResponse: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType){
        guard let access_token = JSONObject["access_token"] as? String,
        let expires_at = JSONObject["expires_at"] as? String,
            let userJSONObject = User(JSONObject: JSONObject["user"] as! JSONObjectType) else{
                return nil
        }
        
        let rate = JSONObject["rate"] as? Int
        
        self.init(access_token: access_token,
                  expires_at: expires_at,
                  user: userJSONObject,
                  rate: rate)
    }
    
    
}

extension User: JSONObjectDecodable {
    
    init?(JSONObject: JSONObjectType){
        guard let id = JSONObject["id"] as? Int,
        let color = JSONObject["color"] as? String,
        let email = JSONObject["email"] as? String else{
                return nil
        }
        
        let establishmentJsonArray = JSONObject["establishments"] as? JSONArrayType 
        let rate = JSONObject["rate"] as? Int
        let name = JSONObject["name"] as? String ?? ""
        var establishmentList: [Establishments] = []
        let _ = establishmentJsonArray?.compactMap {
            if let establishment = Establishments(JSONObject: $0){
                establishmentList.append(establishment)
            }
        }
    
        self.init(id: id,
                  name: name,
                  email: email,
                  establishments: establishmentList,
                  rate: rate,
                  color: color)
    }
    
}

extension Establishments: JSONObjectDecodable {
    
    init?(JSONObject: JSONObjectType){
        guard let address = JSONObject["address"] as? String,
        let description = JSONObject["description"] as? String,
        let id = JSONObject["id"] as? Int,
        let lat = JSONObject["lat"] as? Double,
        let lng = JSONObject["lng"] as? Double,
        let name = JSONObject["name"] as? String,
        let profile_picture_url = JSONObject["profile_picture_url"] as? String else{
                return nil
        }
        let phone = JSONObject["phone"] as? String
        let reservation_message = JSONObject["reservation_message"] as? String
        
        self.init(address: address,
                  description: description,
                  id: id,
                  lat: lat,
                  lng: lng,
                  phone: phone,
                  name: name,
                  profile_picture_url: profile_picture_url,
                  reservation_message: reservation_message)
    }
}
