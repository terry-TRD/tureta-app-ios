//
//  Profile.swift
//  TuReta
//
//  Created by Daniel Coria on 10/5/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

class Profile {
    let user: User
    
    init(with user: User){
        self.user = user
    }
}
