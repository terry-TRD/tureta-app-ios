//
//  Establishment.swift
//  TuReta
//
//  Created by Daniel Coria on 10/26/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

struct Establishment: Codable {
    var id: Int
    var name: String
    var lat: Double
    var lng: Double
    var description: String
    var address: String
    var profile_picture_url: String
    //var distance: Int
    var phone: String?
    var rate: Int?
    var price: String?
}

struct EstablishmentDetail {
    var id: Int
    var name: String
    var lat: Double
    var lng: Double
    var description: String
    var address: String
    var profile_picture_url: String
    var phone: String?
    var reservation_message: String?
    var schedules: [Schedule]?
    var services: [Service]?
    var fields: [Field]
    var is_favorite: Bool
}

extension Establishment: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType) {
        guard let id = JSONObject["id"] as? Int,
        let name = JSONObject["name"] as? String,
        let lat = JSONObject["lat"] as? Double,
        let lng = JSONObject["lng"] as? Double,
        let description = JSONObject["description"] as? String,
        let address = JSONObject["address"] as? String,
        let profile_picture_url = JSONObject["profile_picture_url"] as? String else {
            return nil
        }
        
        let phone = JSONObject["phone"] as? String ?? ""
        //let distance = JSONObject["distance"] as? Int ?? 0
        let rate = JSONObject["rate"] as? Int ?? 0
        let price = JSONObject["price"] as? String ?? ""
        
        self.init(id:id,
                  name: name,
                  lat: lat,
                  lng: lng,
                  description: description,
                  address: address,
                  profile_picture_url: profile_picture_url,
                  //distance: distance,
                  phone: phone,
                  rate: rate,
                  price: price)
    }
    
}

extension EstablishmentDetail: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType){
        guard let id = JSONObject["id"] as? Int,
            let name = JSONObject["name"] as? String,
        let lat = JSONObject["lat"] as? Double,
        let lng = JSONObject["lng"] as? Double,
        let description = JSONObject["description"] as? String,
        let address = JSONObject["address"] as? String,
        let profile_picture_url = JSONObject["profile_picture_url"] as? String else{
            return nil
        }
        
        let phone = JSONObject["phone"] as? String ?? ""
        let reservation_message = JSONObject["reservation_message"] as? String ?? ""
        
        var scheduleList: [Schedule] = []
        let schedulesArray = JSONObject["schedules"] as? JSONArrayType
        let _ = schedulesArray?.compactMap {
            if let schedule = Schedule(JSONObject: $0){
                scheduleList.append(schedule)
            }
        }
        
        var serviceList: [Service] = []
        let serviceArray = JSONObject["services"] as? JSONArrayType
        let _ = serviceArray?.compactMap {
            if let service = Service(JSONObject: $0){
                serviceList.append(service)
            }
        }
        
        let isFavorite = JSONObject["is_favorite"] as? Bool ?? false
        var fieldList: [Field] = []
        let fieldArray = JSONObject["fields"] as? JSONArrayType
        let _ = fieldArray?.compactMap {
            if let field = Field(JSONObject: $0){
                fieldList.append(field)
            }
        }
        
        self.init(id: id,
                  name: name,
                  lat: lat,
                  lng: lng,
                  description: description,
                  address: address,
                  profile_picture_url:profile_picture_url,
                  phone: phone,
                  reservation_message: reservation_message,
                  schedules: scheduleList,
                  services: serviceList,
                  fields: fieldList,
                  is_favorite: isFavorite)
        
    }
}

struct Field {
    var id: Int
    var name: String
    var game_types: String
    var grass_type: Int
    var availability: [FieldAvailability]?
    var prices: [Price]?
    var is_active: Bool
}

struct FieldAvailability {
    let from: String
    let to: String
}

struct Price {
    let price_per_hour: String
    let price_per_half: String
    let game_type: Int
}

struct EstablishmentArray {
    var establishmentsItems: Array<Establishment> = []
    
    init(items: Array<Establishment> ) {
        establishmentsItems = items
    }
}

struct Schedule {
    var day_number: Int
    var from: String
    var to: String
}

struct Service {
    let id: Int
    let name: String
    let icon_url: String
}

//struct JsonObject: Codable {
//    let teams : [Int]
//}

extension Schedule: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType){
        guard let day_number = JSONObject["day_number"] as? Int,
        let from = JSONObject["from"] as? String,
            let to = JSONObject["to"] as? String else{
                return nil
        }
        
        self.init(day_number: day_number,
                  from: from,
                  to: to)
    }
}

extension Service: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType){
        guard let id = JSONObject["id"] as? Int,
        let name = JSONObject["name"] as? String,
            let icon_url = JSONObject["icon_url"] as? String else{
                return nil
        }
        
        self.init(id: id,
                  name: name,
                  icon_url: icon_url)
    }
}


extension Field: JSONObjectDecodable{
    
    init?(JSONObject: JSONObjectType){
        guard let id = JSONObject["id"] as? Int,
            let name = JSONObject["name"] as? String,
            let game_types = JSONObject["game_types"] as? String,
            let grass_type = JSONObject["grass_type"] as? Int else{
                return nil
        }
        
        let availabilityJSONArray = JSONObject["availability"] as? JSONArrayType
        var availabilityList: [FieldAvailability] = []
        let _ = availabilityJSONArray?.compactMap {
            if let time = FieldAvailability(JSONObject: $0){
                availabilityList.append(time)
            }
        }
        
        let pricesJSONArray =  JSONObject["prices"] as? JSONArrayType
        var pricesList: [Price] = []
        let _ = pricesJSONArray?.compactMap{
            if let price = Price(JSONObject: $0){
                pricesList.append(price)
            }
        }
        
        let isActive = JSONObject["is_active"] as? Bool ?? false
        
        self.init(id: id,
                  name: name,
                  game_types: game_types,
                  grass_type: grass_type,
                  availability: availabilityList,
                  prices: pricesList,
                  is_active:isActive)
    }
}

extension Price: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType) {
        guard let price_per_hour = JSONObject["price_per_hour"] as? String,
        let price_per_half = JSONObject["price_per_half"] as? String,
        let game_type = JSONObject["game_type"] as? Int else {
                return nil
        }
        
        self.init(price_per_hour: price_per_hour,
                  price_per_half: price_per_half,
                  game_type: game_type)
    }
}

extension FieldAvailability: JSONObjectDecodable {
    
    init?(JSONObject: JSONObjectType) {
        guard let from = JSONObject["from"] as? String,
            let to = JSONObject["to"] as? String else{
                return nil
        }
        
        self.init(from: from, to: to)
    }
    
}
