//
//  SharedInstance.swift
//  TuReta
//
//  Created by DC on 9/10/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation
import CoreLocation

class UserData {
    
    static var shared: UserResponse?
    static var sharedFirebase: UserToken?
    static var sharedUserLocation: UserLocation?
    static var sharedEstablishment: EstablishmentDetail?
}

struct UserToken {
    var firebaseToken: String
}

struct UserLocation {
    var location: CLLocationCoordinate2D?
}
