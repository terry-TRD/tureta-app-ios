//
//  Reservation.swift
//  TuReta
//
//  Created by Daniel Coria on 12/1/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

struct Reservation {
    var id: Int
    var code: String
    var from: String
    var to: String
    var user: User?
    var establishment: Establishment?
    var field: Field
    var comments: String
    var subtotal: String
    var fee: String
    var total: String
    var status: Int
    var reviews: [Reviews]
    var type: Int
}

struct Reviews {
    var comments: String?
    var createdBy: Int
    var establishmentId: Int?
    var id: Int
    var rate: Int
    var userId: Int?
}

extension Reservation: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType){
        guard let id = JSONObject["id"] as? Int,
            let code = JSONObject["code"] as? String,
            let from = JSONObject["from"] as? String,
            let to = JSONObject["to"] as? String,
            let fieldJSONObject = Field(JSONObject: JSONObject["field"] as! JSONObjectType),
            let subtotal = JSONObject["subtotal"] as? String,
            let fee = JSONObject["fee"] as? String,
            let total = JSONObject["total"] as? String,
            let type = JSONObject["type"] as? Int,
            let status = JSONObject["status"] as? Int else{
                return nil
        }
        var userJSONObject: User?
        if type == ReservationType.app {
            if JSONObject["user"] != nil {
                userJSONObject = User(JSONObject: JSONObject["user"] as! JSONObjectType)
            }
        }
        
        var establishmentJSONObject: Establishment?
        if JSONObject["establishment"] != nil {
            establishmentJSONObject = Establishment(JSONObject: JSONObject["establishment"] as! JSONObjectType)
        }
        
        
        let comments = JSONObject["comments"] as? String ?? ""
        
        let reviewsJsonArray = JSONObject["reviews"] as? JSONArrayType
        var reviewsList: [Reviews] = []
        let _ = reviewsJsonArray?.compactMap {
            if let reviews = Reviews(JSONObject: $0){
                reviewsList.append(reviews)
            }
        }
        
        self.init(id: id,
                  code:code,
                  from: from,
                  to: to,
                  user: userJSONObject,
                  establishment: establishmentJSONObject,
                  field: fieldJSONObject,
                  comments: comments,
                  subtotal: subtotal,
                  fee: fee,
                  total: total,
                  status: status,
                  reviews: reviewsList,
                  type: type)
    }
}

extension Reviews: JSONObjectDecodable {
    init?(JSONObject: JSONObjectType){
        guard let createdBy = JSONObject["created_by"] as? Int,
            let id = JSONObject["id"] as? Int,
            let rate = JSONObject["rate"] as? Int else {
            return nil
        }
            
        let establishmentId = JSONObject["establishment_id"] as? Int ?? 0
        let comments = JSONObject["comments"] as? String ?? ""
        let userID = JSONObject["user_id"] as? Int ?? 0
        
        self.init(comments: comments,
                  createdBy: createdBy,
                  establishmentId: establishmentId,
                  id: id,
                  rate: rate,
                  userId: userID)
    }
}
