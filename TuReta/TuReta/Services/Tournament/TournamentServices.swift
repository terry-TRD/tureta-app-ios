//
//  TournamentServices.swift
//  TuReta
//
//  Created by DC on 03/04/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

protocol TournamentServices {
    
    func requestCreateNewTournament(itemModel: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestTournaments(page: Int, establishmentID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestCreateTeam(idTournament: Int, image: Data?, nameTeam: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestTeams(idTournament: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestTeamsByTournament(tournamentID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestRemoveTournament(itemModel: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestUpdateTournament(itemModel: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestFixturesByRound(itemModel: TournamentModel, roundSelected: Int, establishmentID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestAddFixtureDetails(itemModel: FixtureModel, itemModelTournament: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestUpdateFixtureDetails(itemModel: FixtureModel, itemModelTournament: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestTableTeams(establishmentID: Int, itemModelTournament: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
}


final class DefaultTournamentServices: TournamentServices{
    let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "" ) + "." + (Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")
    
    private let networkClient: TournamentNetworkClient
    //private let networkClient: NetworkClient
    
    init(networkClient: TournamentNetworkClient = NetworkClient()) {
        self.networkClient = networkClient
    }
    
    func requestTeams(idTournament: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "tournament_id": idTournament
        ]
        
        guard let establishment = UserData.shared?.user.establishments else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishment[0].id)/\(Constants.UrlWS.teams)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestGet(url: url, params: parameters, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestTeamsByTournament(tournamentID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        guard let establishment = UserData.shared?.user.establishments else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishment[0].id)/\(Constants.UrlWS.tournaments.rawValue)/\(tournamentID)/\(Constants.UrlWS.teams)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestGet(url: url, params: parameters, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestCreateTeam(idTournament: Int, image: Data?, nameTeam: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "name": nameTeam,
            "tournament_id": idTournament
        ]
        
        guard let establishment = UserData.shared?.user.establishments else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishment[0].id)/\(Constants.UrlWS.teams)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestPostWithImage(url: url, params: parameters, image: image, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestTournaments(page: Int, establishmentID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "page": page
        ]
        
        //guard let establishment = UserData.shared?.user.establishments else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentID)/\(Constants.UrlWS.tournaments)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestGet(url: url, params: parameters, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestCreateNewTournament(itemModel: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        var teams: [Int] = []
        for item in itemModel.teams ?? [] {
            teams.append(item.id ?? 0)
            
        }
        
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "name": itemModel.name ?? "",
            "game_type": itemModel.game_type ?? 0,
            "match_won_points": itemModel.match_won_points ?? 0,
            "tied_match_points": itemModel.tied_match_points ?? 0,
            "lost_match_points": itemModel.lost_match_points ?? 0,
            "number_of_rounds": itemModel.number_of_rounds ?? 1
        ]
        
        guard let establishment = UserData.shared?.user.establishments else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishment[0].id)/\(Constants.UrlWS.tournaments)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestPost(url: url, params: parameters, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestUpdateTournament(itemModel: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        var teams: [Int] = []
        for item in itemModel.teams! {
            teams.append(item.id ?? 0)
            
        }
        
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "name": itemModel.name ?? "",
            "game_type": itemModel.game_type ?? 0,
            "match_won_points": itemModel.match_won_points ?? 0,
            "tied_match_points": itemModel.tied_match_points ?? 0,
            "lost_match_points": itemModel.lost_match_points ?? 0,
            "number_of_rounds": itemModel.number_of_rounds ?? 0
        ]
        
        guard let establishment = UserData.shared?.user.establishments else { return }
        guard let idTournament = itemModel.id else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishment[0].id)/\(Constants.UrlWS.tournaments)/\(idTournament)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestUpdate(url: url, params: parameters, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
        
    }
    
    func requestRemoveTournament(itemModel: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void){
    
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        guard let establishment = UserData.shared?.user.establishments else { return }
        guard let idTournament = itemModel.id else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishment[0].id)/\(Constants.UrlWS.tournaments)/\(idTournament)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestDelete(url: url, params: parameters, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
        
    }
    
    func requestFixturesByRound(itemModel: TournamentModel, roundSelected: Int, establishmentID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void){
    
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "round_number": roundSelected
        ]
        
        //guard let establishment = UserData.shared?.user.establishments else { return }
        guard let idTournament = itemModel.id else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentID)/\(Constants.UrlWS.tournaments)/\(idTournament)/\(Constants.UrlWS.matches)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestGet(url: url, params: parameters, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
        
    }
    
    func requestAddFixtureDetails(itemModel: FixtureModel, itemModelTournament: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "team_1_id": itemModel.team_1_id ?? 0,
            "team_2_id": itemModel.team_2_id ?? 0,
            "field_id": itemModel.field_id ?? 0,
            "date": itemModel.date ?? "",
            "round_number": itemModel.round_number ?? 0
        ]
        
        guard let establishment = UserData.shared?.user.establishments else { return }
        guard let idTournament = itemModelTournament.id else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishment[0].id)/\(Constants.UrlWS.tournaments)/\(idTournament)/\(Constants.UrlWS.matches)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestPost(url: url, params: parameters, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestUpdateFixtureDetails(itemModel: FixtureModel, itemModelTournament: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version,
            "team_1_id": itemModel.team_1_id ?? 0,
            "team_2_id": itemModel.team_2_id ?? 0,
            "field_id": itemModel.field_id ?? 0,
            "date": itemModel.date ?? "",
            "team_1_annotations": itemModel.team_1_annotations ?? 0,
            "team_2_annotations": itemModel.team_2_annotations ?? 0,
            "round_number": itemModel.round_number ?? 0
        ]
        
        guard let establishment = UserData.shared?.user.establishments else { return }
        guard let idTournament = itemModelTournament.id else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishment[0].id)/\(Constants.UrlWS.tournaments)/\(idTournament)/\(Constants.UrlWS.matches)/\(itemModel.id ?? 0)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestUpdate(url: url, params: parameters, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestTableTeams(establishmentID: Int, itemModelTournament: TournamentModel, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let parameters: [String: Any] = [
            "identifier": Bundle.main.bundleIdentifier as AnyObject,
            "platform": "ios",
            "version": version
        ]
        
        guard let itemID = itemModelTournament.id else { return }
        
        guard let url = URL(string: "\(NetworkConstants.baseURL)\(Constants.UrlWS.establishment.rawValue)/\(establishmentID)/\(Constants.UrlWS.tournaments)/\(itemID)/\(Constants.UrlWS.stats.rawValue)") else {
            errorHandler(.urlInvalid, "")
            return
        }
        
        networkClient.requestGet(url: url, params: parameters, completionHandler:
            {[weak self] (jsonResponse) in
                
                completionHandler(jsonResponse)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }

}
