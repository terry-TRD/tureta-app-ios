//
//  ProfileServices.swift
//  TuReta
//
//  Created by Daniel Coria on 10/5/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

protocol ProfileServices {
    func requestProfile(completionHandler: @escaping ( _ user: User?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?,  _ message: String) -> Void )
    
    func requestCloseSession( completionHandler: @escaping (Bool) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
}

final class DefaultProfileServices: ProfileServices {
    
    private let networkClient: ProfileNetworkClient
    //private let networkClient: NetworkClient
    
    init(networkClient: ProfileNetworkClient = NetworkClient()) {
        self.networkClient = networkClient
    }
    
    func requestProfile( completionHandler: @escaping (User?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
       
        let operation = FetchProfileOperation(networkClient: networkClient)
        
        operation.startRequest(completionHandler: {[weak self] (user) in
            guard let strongSelf = self else { return }
            completionHandler(user)
        },{ (networkError, message) in
            errorHandler(.parse, message)
        })
    }
    
    func requestCloseSession( completionHandler: @escaping (Bool) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchProfileOperation(networkClient: networkClient)
        
        operation.startRequestPostCloseSession( completionHandler: { [weak self](data) in
            guard let strongSelf = self else { return }
            
                completionHandler(data)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    
    
}
