//
//  OwnerServices.swift
//  TuReta
//
//  Created by Daniel Coria on 11/28/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

protocol OwnerServices {

    func requestUpdateStatusField(establishmentId: Int, fieldId: Int, isActive: Bool, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestUpdateServiceSatatus(establishmentId: Int, service: Int, activatedStatus: Bool, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestReservationsOwner(establishmentId: Int, type: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestAllServices( completionHandler: @escaping( _ response: Data) -> Void, _ errorHandler: @escaping(NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestUpdateStatusResevation(establishmentID: Int, reservationID: Int, type: String, completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestUpdateRate(reservationID: Int, rate: Int, completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestRecoveryPassword(email: String, completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
     func requestCancelReservation(reservationID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestCreateNewField(newFieldParams: NewField, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestUpdateField(newFieldParams: NewField, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestRemoveField(fieldID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
}

final class DefaultOwnerServices: OwnerServices{
    
    private let networkClient: OwnerNetworkClient
       //private let networkClient: NetworkClient
       
       init(networkClient: OwnerNetworkClient = NetworkClient()) {
           self.networkClient = networkClient
       }
    
    func requestRemoveField(fieldID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchOwnerOperation(networkClient: networkClient)
        
        operation.startRequestRemoveField(fieldID: fieldID, completionHandler: {[weak self] (response) in
            guard let strongSelf = self else { return }
                completionHandler(response)
                },{ (networkError, message) in
                    errorHandler(.parse, message)
            })
    }
    
    func requestCreateNewField(newFieldParams: NewField, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchOwnerOperation(networkClient: networkClient)
        operation.startRequestCreateNewField(newFieldParams: newFieldParams, completionHandler: {[weak self] (response) in
            guard let strongSelf = self else { return }
            completionHandler(response)
            },{ (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestUpdateField(newFieldParams: NewField, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchOwnerOperation(networkClient: networkClient)
        operation.startRequestUpdateField(newFieldParams: newFieldParams, completionHandler: {[weak self] (response) in
            guard let strongSelf = self else { return }
            completionHandler(response)
            },{ (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestUpdateRate(reservationID: Int, rate: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let operation = FetchOwnerOperation(networkClient: networkClient)
        operation.startRequestUpdateRate(reservationID: reservationID, rate: rate, completionHandler: {[weak self] (response) in
            guard let strongSelf = self else { return }
            completionHandler(response)
            },{ (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestUpdateStatusField(establishmentId: Int, fieldId: Int, isActive: Bool, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchOwnerOperation(networkClient: networkClient)
        
        operation.startRequestUpdateStatusField(establishmentId: establishmentId, fieldId: fieldId, isActive: isActive, completionHandler: {[weak self] (response) in
         guard let strongSelf = self else { return }
            completionHandler(response)
        },{ (networkError, message) in
            errorHandler(.parse, message)
        })
    }
    
    func requestReservationsOwner(establishmentId: Int, type: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchOwnerOperation(networkClient: networkClient)
        
        operation.startRequestReservationsOwner(establishmentId: establishmentId, type: type, completionHandler: {[weak self] (response) in
         guard let strongSelf = self else { return }
            completionHandler(response)
        },{ (networkError, message) in
            errorHandler(.parse, message)
        })
    }
    func requestAllServices(completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchOwnerOperation(networkClient: networkClient)
        
        operation.startRequestAllServices( completionHandler: {[weak self] (response) in
         guard let strongSelf = self else { return }
            completionHandler(response)
        },{ (networkError, message) in
            errorHandler(.parse, message)
        })
    }
    func requestUpdateServiceSatatus(establishmentId: Int, service: Int, activatedStatus: Bool, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchOwnerOperation(networkClient: networkClient)
        
        operation.startRequestUpdateServiceStatus(establishmentId: establishmentId, service: service, activatedStatus: activatedStatus, completionHandler: {[weak self] (response) in
         guard let strongSelf = self else { return }
            completionHandler(response)
        },{ (networkError, message) in
            errorHandler(.parse, message)
        })
    }
    
    func requestUpdateStatusResevation(establishmentID: Int, reservationID: Int, type: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchOwnerOperation(networkClient: networkClient)
        
        operation.startRequestUpdateStatusReservation(establishmentID: establishmentID, reservationID: reservationID, type: type, completionHandler: { [weak self](data) in
            guard let strongSelf = self else { return }
            
                completionHandler(data)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestRecoveryPassword(email: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchOwnerOperation(networkClient: networkClient)
        
        operation.startRequestRecoveryPassword(email: email, completionHandler: { [weak self](data) in
            guard let strongSelf = self else { return }
            
                completionHandler(data)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestCancelReservation(reservationID: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        
        let operation = FetchOwnerOperation(networkClient: networkClient)
        
        operation.startRequestCancelReservation(reservationID: reservationID, completionHandler: {[weak self] (response) in
            guard let strongSelf = self else { return }
            completionHandler(response)
            },{ (networkError, message) in
                errorHandler(.parse, message)
        })
        
    }

}
