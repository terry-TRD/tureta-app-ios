//
//  EstablishmentServices.swift
//  TuReta
//
//  Created by Daniel Coria on 10/26/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

protocol EstablishmentServices {
    func requestMainEstablishment(center_lat: Double, center_lng: Double, border_lat: Double, border_lng: Double, search: String,  completionHandler: @escaping ( _ establishment: EstablishmentArray?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestEstablishmentAvailable(establishmentID: Int, date: String, completionHandler: @escaping (_ data: Data ) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, _ message: String) -> Void)
    
    func requestRentField(field: Field, from: String, to: String, type: Int, manualPrice: Double?, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestEstablishmentDetail(idPlace: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
    
    func requestEstablishmentFavorite(establishmentID: Int, type: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void)
}

final class DefaultEstablishmentServices:EstablishmentServices{
    
    private let networkClient: EstablishmentNetworkClient
    //private let networkClient: NetworkClient
    
    init(networkClient: EstablishmentNetworkClient = NetworkClient()) {
        self.networkClient = networkClient
    }
    
    func requestMainEstablishment(center_lat: Double, center_lng: Double, border_lat: Double, border_lng: Double, search: String, completionHandler: @escaping (EstablishmentArray?) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchEstablishmentOperation(networkClient: networkClient)
               
        operation.startRequest(center_lat: center_lat, center_lng: center_lng, border_lat: border_lat, border_lng: border_lng, search: search, completionHandler: {[weak self] (establishment) in
                guard let strongSelf = self else { return }
                   completionHandler(establishment)
               },{ (networkError, message) in
                   errorHandler(.parse, message)
               })
    }
    
    func requestEstablishmentAvailable(establishmentID: Int, date: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchEstablishmentOperation(networkClient: networkClient)
        
        operation.startRequestAvailability(establishmentID: establishmentID, date: date, completionHandler: { [weak self](data) in
            guard let strongSelf = self else { return }
            
                completionHandler(data)
            }, {(networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestRentField(field: Field, from: String, to: String, type: Int, manualPrice: Double?, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchEstablishmentOperation(networkClient: networkClient)
        
        operation.starRequestRentField(field: field, from: from, to: to, type: type, manualPrice: manualPrice, completionHandler: {[weak self] (response) in
         guard let strongSelf = self else { return }
            completionHandler(response)
        },{ (networkError, message) in
            errorHandler(.parse, message)
        })
    }
    
    func requestEstablishmentDetail(idPlace: Int, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchEstablishmentOperation(networkClient: networkClient)
        
        operation.startRequestEstablishmentDetail(idPlace: idPlace, completionHandler: {[weak self] (response) in
            guard let strongSelf = self else { return }
            completionHandler(response)
            },{ (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
    func requestEstablishmentFavorite(establishmentID: Int, type: String, completionHandler: @escaping (Data) -> Void, _ errorHandler: @escaping (NetworkErrorLogin?, String) -> Void) {
        let operation = FetchEstablishmentOperation(networkClient: networkClient)
        
        operation.startRequestEstablishmentFavorite(establishmentID: establishmentID, type: type, completionHandler: {[weak self] (response) in
            guard let strongSelf = self else { return }
            completionHandler(response)
            },{ (networkError, message) in
                errorHandler(.parse, message)
        })
    }
    
}
