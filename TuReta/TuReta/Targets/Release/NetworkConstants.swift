//
//  NetworkConstants.swift
//  TuReta
//
//  Created by DC on 04/08/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import UIKit



struct NetworkConstants: NetworkConstantsProtocol {
    
    static var baseURL = "http://tureta.com.mx/api/v1/"
    
}
