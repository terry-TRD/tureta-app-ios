//
//  NetworkConstantsProtocol.swift
//  TuReta
//
//  Created by DC on 04/08/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

protocol NetworkConstantsProtocol {
    static var baseURL: String { get }
}
