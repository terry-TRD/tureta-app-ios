//
//  ReusableIdentifier.swift
//  TuReta
//
//  Created by DC on 26/03/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

enum CellResusableIdentifier: String {
    
    case inputNameCell = "InputNameCell"
    case optionCell = "OptionCell"
    case basicOptionCell = "BasicOptionTableViewCell"
    case teamCell = "TeamTableViewCell"
    case basicTeamCell = "BasicTeamTableViewCell"
    case ruleCell = "RuleTableViewCell"
    case emptyCell = "EmptyTableViewCell"
    case dateFixtureCell = "DateFixtureTableViewCell"
    case MatchTableViewCell = "MatchTableViewCell"
    case editTeamsScore = "EditTeamsAndScoreTableViewCell"
    case teamRow = "TeamRowTableViewCell"
    case fieldCell = "FieldTableViewCell"
    case headerImage = "HeaderImageTableViewCell"
    case descriptionCell = "DescriptionTableViewCell"
    case chooseDateButtonCell = "ChooseDateButtonTableViewCell"
    case availableFielsdCell = "AvailableCourtsTableViewCell"
    case availableFieldCell = "AvailableCourtTableViewCell"
    case basicDetailCell = "BasicCellDetailFieldTableViewCell"
    case mapCell = "MapTableViewCell"
    case servicesCell = "ServicesPlaceTableViewCell"
    case serviceCell = "ServiceCollectionViewCell"
    case basicCell = "BasicCellTableViewCell"
    case scheduleTimes = "ScheduleTimesTableViewCell"
    case timeCell = "TimeCollectionViewCell"
    case summaryCell = "SummaryTableViewCell"
    case optionalPrice = "OptionalPriceTableViewCell"
    case fixtureCell
    case headerTableTeams = "HeaderTableTeams"
    
}
