//
//  Result.swift
//  TuReta
//
//  Created by Daniel Coria on 10/5/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation
import UIKit

enum Result<T, ErrorType: Error, Message: NSString> {
    case success(T)
    case failure(ErrorType)
    case message(Message)
    
    init(value: T) {
        self = .success(value)
    }
    
    init(error: ErrorType) {
        self = .failure(error)
    }
    
    init(message: Message) {
        self = .message(message)
    }
}

