//
//  UIColor-extension.swift
//  TuReta
//
//  Created by DC on 13/07/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
//    public convenience init<T>(rgb: T, alpha: CGFloat = 1) where T: BinaryInteger {
//        guard rgb > 0 else {
//            self.init(red: 0, green: 0, blue: 0, alpha: alpha)
//            return
//        }
//
//        guard rgb < 0xFFFFFF else {
//            self.init(red: 1, green: 1, blue: 1, alpha: alpha)
//            return
//        }
//
//        let r: CGFloat = CGFloat(((rgb & 0xFF0000) >> 16) / 0xFF)
//        let g: CGFloat = CGFloat((rgb & 0x00FF00) >> 8 / 0xFF)
//        let b: CGFloat = CGFloat((rgb & 0x0000FF) / 0xFF)
//
//        self.init(red: r, green: g, blue: b, alpha: alpha)
//    }
    
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }
    
}
