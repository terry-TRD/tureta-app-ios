//
//  JSONTypes.swift
//  TuReta
//
//  Created by Daniel Coria on 11/8/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation

typealias JSONArrayType = [[String:Any]]
typealias JSONObjectType = [String:Any]
typealias KeyValueStringAnyType = [String:Any]
typealias KeyValueStringType = [String:String]

enum JSONResult {
    case object(result: JSONObjectType)
    case array(result: JSONArrayType)
}

// Allows conforming types to support initialization using data in the form of a jsonObject
protocol JSONObjectDecodable {
    
    init?(JSONObject: JSONObjectType)
}

// Allows conforming types to support initialization using data in the form of a jsonArray
protocol JSONArrayDecodable {
    
    init?(JSONArray: JSONArrayType)
}
