//
//  Extensions.swift
//  TuReta
//
//  Created by DC on 9/6/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import WebKit

enum BackendError: Error {
    case urlError(reason: String)
    case objectSerialization(reason: String)
}

@IBDesignable class CustomTextField: UITextField {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var insetX: CGFloat = 0
    @IBInspectable var insetY: CGFloat = 0
    
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
}

extension UITextField {
    func setLeftIcon(_ icon: UIImage) {
        
        let padding = 8
        let size = 20

        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size) )
        let leftImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: icon.size.width, height: icon.size.height))
        leftImageView.image = icon
        outerView.addSubview(leftImageView)

        leftView = outerView
        leftViewMode = .always
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: frame.height - 1, width: outerView.frame.width + frame.size.width + CGFloat(size) + CGFloat(padding), height: 1.0)
        bottomLine.backgroundColor = UIColor.white.cgColor
        layer.addSublayer(bottomLine)
    }
    
    func changePlaceholderColor(text: String, andColor color: UIColor) {
        attributedPlaceholder =
        NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor : color])
    }
    
    func setBottomLine(color: UIColor) {
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: frame.height - 1, width: self.frame.width, height: 1.0)
        bottomLine.backgroundColor = color.cgColor
        layer.addSublayer(bottomLine)
    }
    
}

@IBDesignable class CustomTextView: UITextView {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
}

class CustomOnlyCornerUIView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
}

class CustomBorderUIImageView: UIImageView {
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
}

class CustomCornerUIView: UIView {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
}

class CustomCornerAndBorderUIView: UIView {
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
}

class CustomSwitch: UISwitch {
    
    @IBInspectable var scale : CGFloat = 1{
        didSet{
            setup()
        }
    }
    
    //from storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    //from code
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private func setup(){
        self.transform = CGAffineTransform(scaleX: scale, y: scale)
    }
    
    override func prepareForInterfaceBuilder() {
        setup()
        super.prepareForInterfaceBuilder()
    }
    
}

extension UIApplication {
    static var getVersionApp: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? ""
    }
    
    static var getBuildApp: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String ?? ""
    }
    
}

extension String {
    var wordList: [String]
    {
        return components(separatedBy: NSCharacterSet.alphanumerics.inverted).filter({$0 != ""})
    }
    
//    var getVersionApp: String {
//        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
//    }
    
    func getTimeFormat() -> String {
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        formatter.locale = Locale(identifier: "es_MX")
        let date = formatter.date(from: self)
        
        
        // Transform date
        
        formatter.dateFormat = "HH:mm"
       // formatter.amSymbol = "AM"
       // formatter.pmSymbol = "PM"
        return (date == nil) ? "" : formatter.string(from: date!)
        
    }
    
    func getNiceTimeFormat2() -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        formatter.locale = Locale(identifier: "es_MX")
        let date = formatter.date(from: self)
        
        formatter.dateFormat = "MMMM d, yyyy"
        return (date == nil) ? "" : formatter.string(from: date!)
    }
    
    func getNiceTimeFormat() -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "es_MX")
        let date = formatter.date(from: self)
        
        formatter.dateFormat = "MMM d, yyyy"
        return (date == nil) ? "" : formatter.string(from: date!)
    }
    
    func getNiceTimeFormat3() -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "es_MX")
        let date = formatter.date(from: self)
        
        formatter.dateFormat = "EEEE d MMMM"
        return (date == nil) ? "" : formatter.string(from: date!)
    }
    
    func getNiceTimeFormat4() -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "HH:mm:ss"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let date = formatter.date(from: self)
        
        formatter.dateFormat = "HH:mm"
        return (date == nil) ? "" : formatter.string(from: date!)
    }
    
    func getOnlyHour() -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        formatter.locale = Locale(identifier: "es_MX")
        let date = formatter.date(from: self)
        
        formatter.dateFormat = "HH:mm"
        return (date == nil) ? "" : formatter.string(from: date!)
    }
    
    func getTimeOnlyDay() -> String{
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "es_MX")
        let date = formatter.date(from: self)
        
        // Transform date
        
        formatter.dateFormat = "dd"
        return (date == nil) ? "" : formatter.string(from: date!)
    }
    
    func getTimeOnlyMonth() -> String{
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "es_MX")
        let date = formatter.date(from: self)
        
        // Transform date
        
        formatter.dateFormat = "MMMM"
        return (date == nil) ? "" : formatter.string(from: date!)
    }
    
    func getTimeOnlyYear() -> String{
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "es_MX")
        let date = formatter.date(from: self)
        
        // Transform date
        
        formatter.dateFormat = "yyyy"
        return (date == nil) ? "" : formatter.string(from: date!)
    }
    
    func getTimeBewteenTwoDates(dateTwo: String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_MX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "HH:mm"
        let dateOne = dateFormatter.date(from: self)
        let dateTwo = dateFormatter.date(from: dateTwo)


        var components1 = NSCalendar.current.dateComponents([.hour, .minute], from: dateOne!)

        let components2 = NSCalendar.current.dateComponents([.hour, .minute, .day, .month, .year], from: dateTwo!)

        components1.year = components2.year;
        components1.month = components2.month;
        components1.day = components2.day;

        let date3 = NSCalendar.current.date(from: components1)

        return String(((date3!.timeIntervalSince(dateTwo!) / 60) * -1) / 60)
    }
    
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool { return isValid(regex: regex.rawValue) }
    func isValid(regex: String) -> Bool { return range(of: regex, options: .regularExpression) != nil }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter { CharacterSet.decimalDigits.contains($0) }
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeACall() {
        guard   isValid(regex: .phone),
            let url = URL(string: "tel://\(self.onlyDigits())"),
            UIApplication.shared.canOpenURL(url) else { return }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
}


extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}

extension UIImageView {
    func load(url: URL){
        DispatchQueue.global().async {[weak self] in
            if let data = try? Data(contentsOf: url){
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async { [unowned self] in
                        self?.image = image
                    }
                }
            }
        }
    }
    
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
    
}

extension UIImage {
    
    class var addNaturalField: UIImage? {
        return UIImage(named: "soccer_field_white")
    }
}
extension String {
    func removeFormatAmount() -> Double {
        let formatter = NumberFormatter()

        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .currency
        formatter.currencySymbol = "$"
        formatter.decimalSeparator = ","

        return formatter.number(from: self) as! Double? ?? 0
     }
}

extension UITableView {
    
    func sizeHeaderToFit() {
        if let headerView = self.tableHeaderView {
            
            headerView.setNeedsLayout()
            headerView.layoutIfNeeded()
            
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var frame = headerView.frame
            frame.size.height = height
            headerView.frame = frame
            
            self.tableHeaderView = headerView
        }
    }
    
}


extension UIViewController {
    open override func awakeFromNib() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension UIView {
    /** Loads instance from nib with the same name. */
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}

//
//extension JSONDecoder {
//    func decodeResponse<T: Decodable>(_ type: T.Type, from response: DataResponse<Data>) -> Result<T> {
//    //func decodeResponse<T: Decodable>(from response: DataResponse<Data>) -> Result<T> {
//        guard response.error == nil else {
//            print(response.error!)
//            return .failure(response.error!)
//        }
//        
//        guard let responseData = response.data else {
//            print("didn't get any data from API")
//            return .failure(BackendError.objectSerialization(reason:
//                "Did not get data in response"))
//        }
//        
//        do {
//            let item = try decode(T.self, from: responseData)
//            return .success(item)
//        } catch {
//            print("error trying to decode response")
//            print(error)
//            return .failure(error)
//        }
//    }
//}
//
