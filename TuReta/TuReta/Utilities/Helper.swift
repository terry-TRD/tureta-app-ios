//
//  Helper.swift
//  TuReta
//
//  Created by DC on 9/13/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit


class HelperExtension: NSObject {
    
    class func DayString(day: Int) -> String{
        switch day {
        case 0:
            return DayWeek.monday.rawValue
        case 1:
            return DayWeek.tuesday.rawValue
        case 2:
            return DayWeek.wednesday.rawValue
        case 3:
            return DayWeek.thursaday.rawValue
        case 4:
            return DayWeek.friday.rawValue
        case 5:
            return DayWeek.saturday.rawValue
        case 6:
            return DayWeek.sunday.rawValue
        default:
            return ""
        }
    }

    class func StatusType(status: Int) -> String {
        switch status {
        case 1:
            return StatusField.newReservation.rawValue
        case 2:
            return StatusField.approved.rawValue
        case 3:
            return StatusField.rejected.rawValue
        case 4:
            return StatusField.happening.rawValue
        case 5:
            return StatusField.finished.rawValue
        case 6:
            return StatusField.canceled.rawValue
        case 7:
            return StatusField.noResponse.rawValue
        default:
            return ""
        }
    }
    
    class func GrassType(type: Int) -> String {
        switch type {
        case 0:
            return GrassTypes.natural.rawValue
        case 1:
            return GrassTypes.syntetic.rawValue
            
        default:
            return ""
        }
    }
    
    class func GameTypeSelected(type: Int) -> Int {
        switch type {
        case 0:
            return 7
        case 1:
            return 11
        case 7:
            return 0
        case 11:
            return 1
            
        default:
            return 0
        }
    }
    
    class func GameType(type: Int) -> String {
        switch type {
        case 0:
            return GameTypes.seven.rawValue
        case 1:
            return GameTypes.eleven.rawValue
        default:
            return ""
        }
    }
    
    class func isValidEmail(emailID: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
    
    class func buildBasicAlertController(title: String, message: String, handler: (() -> Void)? ) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let messageFont = [NSAttributedString.Key.font: UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)]
        let titleFont : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont),
            NSAttributedString.Key.foregroundColor: Constants.blackOnePrimaryColor
        ]
        
        
        let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        
        
        
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (alert) in
            
            if handler != nil {
                
                handler!()
                
            }
            
        }))
        
        return alert
        
    }
    
    
    class func buildBasicAlertWithPickerController(title: String, message: String, handler: (() -> Void)? ) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let messageFont = [NSAttributedString.Key.font: UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)]
        let titleFont : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont),
            NSAttributedString.Key.foregroundColor: Constants.blackOnePrimaryColor
        ]
        
        
        let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        
        
        
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (alert) in
            
            if handler != nil {
                
                handler!()
                
            }
            
        }))
        
        return alert
        
    }
    
    class func buildConfirmationAlertController(title: String, message: String, handler: (() -> Void)? ) -> UIAlertController {
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let messageFont = [NSAttributedString.Key.font: UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)]
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        alert.addAction(UIAlertAction(title: "Si", style: .default, handler: { (alert) in
            
            if handler != nil {
                
                handler!()
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        return alert
        
    }
    
    class func TwoButtonsCustomTitlesController(title: String, buttonTitle1:String, buttonTitle2: String, message: String, handler: ((Bool) -> Void)? ) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let messageFont = [NSAttributedString.Key.font: UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)]
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        
        alert.addAction(UIAlertAction(title: buttonTitle1, style: .default, handler: { (alert) in
            
            if handler != nil {
                var flagForget = Bool()
                flagForget = true
                handler!(flagForget)
                
            }
        }))
        
        
        let cancelAction = UIAlertAction(title: buttonTitle2, style: .default, handler: nil)
        //alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        return alert
        
    }
    
    class func TwoButtonsCustomTitlesControllerWithCancelHandler(title: String, buttonTitle1:String, buttonTitle2: String, message: String, handler: ((Bool) -> Void)? ) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let messageFont = [NSAttributedString.Key.font: UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)]
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        
        alert.addAction(UIAlertAction(title: buttonTitle1, style: .default, handler: { (alert) in
            
            if handler != nil {
                var flagForget = Bool()
                flagForget = true
                handler!(flagForget)
                
            }
        }))
        
        
        let secondOption = UIAlertAction(title: buttonTitle2, style: .default, handler: { (alert) in
            
            handler!(false)
        })
        alert.addAction(secondOption)
        
        let cancelOption = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alert.addAction(cancelOption)
        
        return alert
    
    }
    
    
    
    
    class func TwoButtonsBasicAlertController(title: String, message: String, handler: ((Bool) -> Void)? ) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let messageFont = [NSAttributedString.Key.font: UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)]
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        
        alert.addAction(UIAlertAction(title: "Olvide mi contraseña", style: .default, handler: { (alert) in
            
            if handler != nil {
                var flagForget = Bool()
                flagForget = true
                handler!(flagForget)
                
            }
        }))
        
        
        let cancelAction = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
        //alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        return alert
        
    }
    
    class func TextFieldAlertController(title: String, message: String, placeHolder: String, handler: ((_ messageAlert: String, _ inputText: String) -> Void)? ) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        guard let customFont = UIFont(name: "Poppins-Regular", size: UIFont.labelFontSize) else {
            fatalError("")
        }
        
        let messageFont = [NSAttributedString.Key.font: UIFontMetrics(forTextStyle: .headline).scaledFont(for: customFont)]
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        alert.addTextField { (textField : UITextField) -> Void in
            textField.placeholder =  placeHolder // "Correo Electrónico"
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .default) { (result : UIAlertAction) -> Void in
        }
        
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            let string = (alert.textFields?[0].text!)
            
            handler!("",string!)
            
        }
        
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        return alert
        
    }
}
