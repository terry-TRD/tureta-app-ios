//
//  ButtonsExtension.swift
//  TuReta
//
//  Created by DC on 9/6/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit


class CustomCornerButton: UIButton {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
}

class ButtonWithRightImage: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            let spacing: CGFloat = 10.0 / 2
            imageEdgeInsets = UIEdgeInsets(top: 0, left: self.frame.width - imageView!.frame.width, bottom: 0, right: -(self.frame.width - imageView!.frame.width))
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageView!.frame.width/2, bottom: 0, right: imageView!.frame.width/2)
            self.contentEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: spacing)
        }
    }
}

