//
//  Date+Utils.swift
//  TuReta
//
//  Created by DC on 09/08/20.
//  Copyright © 2020 Terry. All rights reserved.
//

import Foundation

extension String{
    func getDate() -> Date {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "HH:mm"
        formatter.amSymbol = ""
        formatter.pmSymbol = ""
        formatter.locale = Locale(identifier: "es_MX")
        return formatter.date(from: self) ?? Date()
    }
}

extension String {
  func toDate(withFormat format: String = "yyyy-MM-dd HH:mm") -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    dateFormatter.timeZone = TimeZone.current
    guard let date = dateFormatter.date(from: self) else {
      preconditionFailure("Take a look to your format")
    }
    return date
  }
}


extension Date {
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func getString() -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "HH:mm"
        formatter.amSymbol = ""
        formatter.pmSymbol = ""
        formatter.locale = Locale(identifier: "es_MX")
        return formatter.string(from: self)
    }
}
