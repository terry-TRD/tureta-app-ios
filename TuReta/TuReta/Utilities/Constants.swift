//
//  Constants.swift
//  TuReta
//
//  Created by DC on 9/6/19.
//  Copyright © 2019 Terry. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    static let greenOnePrimaryColor = UIColor(red: 24.0/255, green: 178.0/255, blue: 100.0/255, alpha: 1)
    static let greenTwoPrimaryColor = UIColor(red: 118.0/255, green: 204.0/255, blue: 155.0/255, alpha: 1)
    static let greenThreePrimaryColor = UIColor(red: 198.0/255, green: 240.0/255, blue: 215.0/255, alpha: 1)
    static let greenfourPrimaryColor = UIColor(red: 41.0/255, green: 173.0/255, blue: 107.0/255, alpha: 1)
    
    static let blackOnePrimaryColor = UIColor(red: 46.0/255, green: 46.0/255, blue: 46.0/255, alpha: 1)
    static let blackTwoPrimaryColor = UIColor(red: 86.0/255, green: 86.0/255, blue: 86.0/255, alpha: 1)
    static let blackThreePrimaryColor = UIColor(red: 181.0/255, green: 181.0/255, blue: 181.0/255, alpha: 1)
    
    static let blueOnePrimaryColor = UIColor(red: 44.0/255, green: 103.0/255, blue: 255.0/255, alpha: 1)
    
    static let yellowOnePrimaryColor = UIColor(red: 242.0/255, green: 184.0/255, blue: 35.0/255, alpha: 1)
    
    static let redOnePrimaryColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 105.0/255, alpha: 1)
    static let redThreePrimaryColor =  UIColor(red: 99.0/255, green: 00.0/255, blue: 66.0/255, alpha: 1)
    
    
    static let googleMapSDKKey = "AIzaSyCRoRAOyzIzTT_ruCfU87-hvPwJeeIaDSk"
    static let appCenterApiKey = "e6b22f8d-c8be-4ee3-bc32-27ccc5e4fe9c"


    static let versionApp = "1.0"
    static let success = 200
    static let successCreated = 201
    static let successWithMessage = 401
    static let successWithMessage2 = 409
    static let successWithError = 422
    static let codeErrorRequestDriver = 210
    static let codeError = 403
    
    public enum UrlWS: String {
        
        case register = "register"
        case login = "login"
        case logout = "logout"
        case media = "media"
        case profile = "profile"
        case establishment = "establishments"
        case availability = "availability"
        case reservation = "reservations"
        case fields = "fields"
        case services = "services"
        case service = "service"
        case favorites = "favorites"
        case users = "users"
        case reviews = "reviews"
        case cancel = "cancel"
        case resetPasswordUser = "password"
        case tournaments = "tournaments"
        case teams = "teams"
        case matches = "matches"
        case stats = "stats"
        
    }
    
}

enum DayWeek: String {
    // TODO: convert this in Localizer strings
    case monday = "Lunes"
    case tuesday = "Martes"
    case wednesday = "Miercoles"
    case thursaday = "Jueves"
    case friday = "Viernes"
    case saturday = "Sabado"
    case sunday = "Domingo"
    
}

enum StatusField: String {
    case newReservation = "Nueva Reservación"
    case approved = "Aprobada"
    case rejected = "Rechazada"
    case happening = "En proceso"
    case finished = "Finalizada"
    case canceled = "Cancelada"
    case noResponse = "Sin confirmación"
}

enum GrassTypes: String {
    case natural = "Natural"
    case syntetic = "Sintetico"
}

enum GameTypes: String {
    case seven = "7 jugadores"
    case eleven = "11 jugadores"
}


enum GrassType {
    static let FIELD_GRASS_TYPE_NATURAL = 1
    static let FIELD_GRASS_TYPE_SYNTHETIC = 2
}

enum GameType{
    static let seven = 7
    static let eleven = 11
}

enum ReservationType {
    static let app = 1
    static let manual = 2
}



class AvatarColors {
    
    static let greenOneColor = UIColor(rgb: 0x18B264) //UIColor(red: 24.0/255, green: 178.0/255, blue: 100.0/255, alpha: 1)
    static let greenTwoColor = UIColor(rgb: 0x73CD9A) //UIColor(red: 115.0/255, green: 205.0/255, blue: 154.0/255, alpha: 1)
    static let greenThreeColor = UIColor(rgb: 0xC6F0D7) //UIColor(red: 198.0/255, green: 240.0/255, blue: 215.0/255, alpha: 1)
    
    static let yellowOneColor = UIColor(rgb: 0xF4B900) //UIColor(red: 244.0/255, green: 185.0/255, blue: 0.0/255, alpha: 1)
    static let yellowTwoColor = UIColor(rgb: 0xFBD068) //UIColor(red: 251.0/255, green: 208.0/255, blue: 104.0/255, alpha: 1)
    static let yellowThreeColor = UIColor(rgb: 0xFAE5B9) //UIColor(red: 250.0/255, green: 229.0/255, blue: 185.0/255, alpha: 1)
    
    static let redOneColor = UIColor(rgb: 0xFF4466) //UIColor(red: 255.0/255, green: 68.0/255, blue: 102.0/255, alpha: 1)
    static let redTwoColor = UIColor(rgb: 0xF56A89) //UIColor(red: 245.0/255, green: 106.0/255, blue: 137.0/255, alpha: 1)
    static let redThreeColor = UIColor(rgb: 0xF4C0CF) //UIColor(red: 244.0/255, green: 192.0/255, blue: 207.0/255, alpha: 1)
    
    static let blueOneColor = UIColor(rgb: 0x2661FF) //UIColor(red: 38.0/255, green: 97.0/255, blue: 255.0/255, alpha: 1)
    static let blueTwoColor =  UIColor(rgb: 0x709AFF) //UIColor(red: 38.0/255, green: 97.0/255, blue: 255.0/255, alpha: 1)
    static let blueThreeColor = UIColor(rgb: 0xAFC7F9) //UIColor(red: 112.0/255, green: 154.0/255, blue: 255.0/255, alpha: 1)
}
enum AvatarColorsHex {
    
    static let greenOneColor = 0x18B264 //UIColor(red: 24.0/255, green: 178.0/255, blue: 100.0/255, alpha: 1)
    static let greenTwoColor = 0x73CD9A //UIColor(red: 115.0/255, green: 205.0/255, blue: 154.0/255, alpha: 1)
    static let greenThreeColor = 0xC6F0D7 //UIColor(red: 198.0/255, green: 240.0/255, blue: 215.0/255, alpha: 1)
    
    static let yellowOneColor = 0xF4B900 //UIColor(red: 244.0/255, green: 185.0/255, blue: 0.0/255, alpha: 1)
    static let yellowTwoColor = 0xFBD068 //UIColor(red: 251.0/255, green: 208.0/255, blue: 104.0/255, alpha: 1)
    static let yellowThreeColor = 0xFAE5B9 //UIColor(red: 250.0/255, green: 229.0/255, blue: 185.0/255, alpha: 1)
    
    static let redOneColor = 0xFF4466 //UIColor(red: 255.0/255, green: 68.0/255, blue: 102.0/255, alpha: 1)
    static let redTwoColor = 0xF56A89 //UIColor(red: 245.0/255, green: 106.0/255, blue: 137.0/255, alpha: 1)
    static let redThreeColor = 0xF4C0CF //UIColor(red: 244.0/255, green: 192.0/255, blue: 207.0/255, alpha: 1)
    
    static let blueOneColor = 0x2661FF //UIColor(red: 38.0/255, green: 97.0/255, blue: 255.0/255, alpha: 1)
    static let blueTwoColor =  0x709AFF //UIColor(red: 38.0/255, green: 97.0/255, blue: 255.0/255, alpha: 1)
    static let blueThreeColor = 0xAFC7F9 //UIColor(red: 112.0/255, green: 154.0/255, blue: 255.0/255, alpha: 1)
    
}


class ImageName: NSObject{
    
    enum type: String {
        case iconEmail = "icn_email"
        case iconUser = "icn-user3"
        case iconPassword = "icn_password"
        case iconField = "icn_field"
        case iconCalendar = "icn-calendar"
        case iconClock = "icn-clock"
        case iconGroup = "icn_group"
        case iconLiked = "icn-liked"
        case iconUnLiked = "icn-unliked"
        case iconPin = "icn-pin"
    }
}
